var mapa = {};
var markers = [];

mapa.options =  {
    zoom: 11
    ,center: new google.maps.LatLng(-34.608056,-58.370278)
    ,mapTypeId: google.maps.MapTypeId.ROADMAP
};



mapa.Dibujar = function(places,popUp){

    var places = places || null;
    var n=1;
    if (places !=0 ){
        var place = new Array();
        for(var i in places){
            place[i] = new google.maps.LatLng(parseFloat(places[i]['lat']),parseFloat(places[i]['lng']));
        }
        for(var i in place){
            var marker = mapa.marker(place[i],places[i]["notas"],'',places[i]["marker"]);
            if(popUp){
                mapa.addPopUP(marker,places[i]["contenidoPopUp"],places[i]["marker"])
            }
        }
    }
    mapa.map =  new google.maps.Map(document.getElementById('map'), mapa.options);
}

mapa.addMarker  = function(coordenadas,titulo,nota,icono,id){
    var place = new google.maps.LatLng((coordenadas.lat),parseFloat(coordenadas.lng));
    mapa.marker(place,titulo,nota,icono,id)
    mapa.map.setZoom(20);
    mapa.map.setCenter(place);


}

mapa.marker = function(pos,titulo,nota,icono,id){

    var marker =   new google.maps.Marker({
        position: pos
        , map: mapa.map
        , title: titulo || ''
        , note:nota || null
        , icon:icono || null
        ,id:id||null
    });
    markers.push(marker);
    return marker;

}

mapa.setAllMap = function (map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }

}

// Removes the markers from the map, but keeps them in the array.
mapa.clearMarkers = function () {
    mapa.setAllMap(null);
}

mapa.addPopUP = function(marker,contentString) {
    var infowindow = new google.maps.InfoWindow();
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString);
        infowindow.open(mapa.map, marker);
    });
}


