var drawingManager;
var selectedShape;
var selectedColor;
var geocoder;
var utils;
var shapes  = [];
var byId   = function(s){return document.getElementById(s)};


function clearSelection() {
    if (selectedShape) {
        selectedShape.setEditable(false);
        selectedShape = null;
    }
}

function setSelection(shape) {
    clearSelection();
    selectedShape = shape;

    google.maps.event.addListener(shape, 'mousemove', function (event) {

    });
    shape.setEditable(true);
    //selectColor($("#color").val());
}

function deleteSelectedShape(e) {
    e.preventDefault();
    if (selectedShape) {
        selectedShape.setMap(null);
    }
}

function selectColor(color) {
    selectedColor = color;
    var polylineOptions = drawingManager.get('polylineOptions');
    polylineOptions.strokeColor = color;
    drawingManager.set('polylineOptions', polylineOptions);
}

function setSelectedShapeColor(color) {
    if (selectedShape) {
        if (selectedShape.type == google.maps.drawing.OverlayType.POLYLINE)  return selectedShape.set('strokeColor', color);
        selectedShape.set('fillColor', color);
        selectedShape.set('strokeColor', color);
    }
}

function setLatLng(marker) {
    var lng = marker.position.B;
    var lat = marker.position.k;
    $("#lat").val(lat);
    $("#lng").val(lng);
}

function setStreet(marker) {
    geocoder.geocode({'latLng': marker.position}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            $("#geocomplete").val(results[0].formatted_address);
        } else {
            console.log(google.maps.GeocoderStatus);
        }
    });
}

function addClickListnerMaker(marker) {
    google.maps.event.addListener(marker, 'click', function (event) {
        alert("dsad");
    });
}

function setListenerDragMaker(marker) {
    google.maps.event.addListener(marker, 'drag', function (event) {
        setStreet(marker);
        setLatLng(marker);
    });
}

function initializeColor() {
    var color = "red";
    selectColor(color);
    setSelectedShapeColor(color);
}

function initializeDraw(mapa) {
    var map = mapa;
    geocoder = new google.maps.Geocoder();
    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControlOptions: {
            drawingModes: [
                google.maps.drawing.OverlayType.POLYLINE
            ]
        },
        markerOptions: {
            draggable: true
        },
        polylineOptions: {
            editable: true
        },
        map: map
    });

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {

        if (e.type != google.maps.drawing.OverlayType.MARKER) {
            // Switch back to non-drawing mode after drawing a shape.
            drawingManager.setDrawingMode(null);

            // Add an event listener that selects the newly-drawn shape when the user
            // mouses down on it.
            var newShape = e.overlay;
            newShape.type = e.type;
            //newShape.set("id",1);

            google.maps.event.addListener(newShape, 'click', function() {
                setSelection(newShape);
            });

            setSelection(newShape);
            shapes.push(newShape);
        }else{
            var marker = e.overlay;
            setLatLng(marker);
            setStreet(marker);
            addClickListnerMaker(marker);
            setListenerDragMaker(marker);
        }
    });

    // Clear the current selection when the drawing mode is changed, or when the
    // map is clicked.
    google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
    google.maps.event.addListener(map, 'click', clearSelection);
    //google.maps.event.addDomListener(document.getElementById('delete-button'), 'click', deleteSelectedShape);
    google.maps.event.addDomListener(byId('btn-guardar'), 'click', function(){
        var data=IO.IN(shapes,false);
        byId('txt-info-polyline').value=JSON.stringify(data);
    });

    initializeColor();
}

$(function(){
    $(document).keyup(function(e) {if (e.keyCode == 27) {clearSelection();}});
});