<?php

Route::get('/', 'AuthController@showLogin');
Route::get('login', 'AuthController@showLogin');
Route::post('login', 'AuthController@postLogin');

Route::get('accessForm', ['uses'=>'AuthController@accessForm','as'=>'accessForm.login']);
Route::post('accessForm.validateFormAccess', ['uses' => 'AuthController@validateFormAccess','as' => 'accessForm.validateFormAccess']);


Route::get('files/descargar',array('as'=>'archivos.descargar','uses'=>function(){
    $path = public_path('assets/SDJ2018.pdf');
    return Response::download($path);
}));

Route::group(array('prefix' => 'formulario'), function () {

    Route::get("/", array('as' => 'formulario.declaraciones_juradas.create','uses' => 'DeclaracionJuradaController@create'));
    Route::post("declaraciones_juradas.eliminar", array('as' => 'formulario.declaraciones_juradas.eliminar','uses' => 'DeclaracionJuradaController@eliminar'));
    Route::post("/", array('as' => 'formulario.declaraciones_juradas.store','uses' => 'DeclaracionJuradaController@store'));
    Route::put("/{declaraciones_juradas}", array('as' => 'formulario.declaraciones_juradas.update','uses' => 'DeclaracionJuradaController@update'));
    Route::patch("/{declaraciones_juradas}", array('as' => 'formulario.declaraciones_juradas.update','uses' => 'DeclaracionJuradaController@update'));
    Route::get("/{declaraciones_juradas}/export_pdf", array('as' => 'formulario.declaraciones_juradas.export_pdf', 'uses' => 'DeclaracionJuradaController@exportPdf'));
    Route::get("/{declaraciones_juradas}/edit", array('as' => 'formulario.declaraciones_juradas.edit','uses' => 'DeclaracionJuradaController@edit'));
    Route::get("localidades/getByProvincia", array('as' => 'localidades.getByProvincia', 'uses' => 'LocalidadesController@getLocalidadesByProvincia'));
    Route::get("personal/getByFuerza", array('as' => 'personal.getByFuerza', 'uses' => 'PersonalController@getPersonalByFuerza'));
    Route::get("grado/getByFuerzaAndTipoPersonal", array('as' => 'grado.getByFuerzaAndTipoPersonal', 'uses' => 'GradoController@getByFuerzaAndTipoPersonal'));
    Route::get("agrupamiento_modalidad/getByFuerzaAndTipoPersonal", array('as' => 'agrupamiento_modalidad.getByFuerzaAndTipoPersonal', 'uses' => 'AgrupamientoModalidadController@getByFuerzaAndTipoPersonal'));
    Route::get("registro/getRegistry", array('as' => 'formulario.registro.getRegistry', 'uses' => 'DeclaracionJuradaController@getRegistry'));


    Route::get("formulario/{user}", array('as' => 'formulario.user', 'uses' => 'DeclaracionJuradaController@user'));

});

Route::group(array('before' => 'auth'), function() {

    Route::get("declaraciones_juradas/{declaraciones_juradas}/obtener", array('as' => 'declaraciones_juradas.obtener', 'uses' => 'DeclaracionJuradaController@obtener'));

    Route::get("declaraciones_juradas/{declaraciones_juradas}/export_pdf", array('as' => 'declaraciones_juradas.export_pdf', 'uses' => 'DeclaracionJuradaController@exportPdf'));
    Route::get('declaraciones_juradas/{declaracion_jurada}/updateRecibida',['as'=>'declaraciones_juradas.updateRecibida','uses'=>'DeclaracionJuradaController@updateRecibida']);
    Route::get('declaraciones_juradas/{declaracion_jurada}/updateAceptada',['as'=>'declaraciones_juradas.updateAceptada','uses'=>'DeclaracionJuradaController@updateAceptada']);
    Route::get('declaraciones_juradas/{declaracion_jurada}/updateEliminada',['as'=>'declaraciones_juradas.updateEliminada','uses'=>'DeclaracionJuradaController@updateEliminada']);
    Route::get('declaraciones_juradas/{declaracion_jurada}/updateEntregadoARecursosHumanos',['as'=>'declaraciones_juradas.updateEntregadoARecursosHumanos','uses'=>'DeclaracionJuradaController@updateEntregadoARecursosHumanos']);
    Route::resource("declaraciones_juradas", "DeclaracionJuradaController");
    Route::get('logout', 'AuthController@logOut');
    Route::get('/', 'UsuariosController@getCambiarClave');
    
    Route::post("usuarios/dni", "UsuariosController@validarDni");
    Route::get("usuarios/clave", array('as' => 'usuarios.clave', 'uses' => 'UsuariosController@getCambiarClave'));
    Route::post("usuarios/clave", array('as' => 'usuarios.clave', 'uses' => 'UsuariosController@postCambiarClave'));
    Route::get("usuarios/destroy/{id}", array('as' => 'usuarios.destroy', 'uses' => 'UsuariosController@destroy'));
    Route::get("usuarios/enable/{id}", array('as' => 'usuarios.enable', 'uses' => 'UsuariosController@enable'));
    Route::post("usuarios/buscar",array('as' => 'usuarios.buscar','uses'  => 'UsuariosListadoController@buscar'));
    Route::get("usuarios/listado",array('as' => 'usuarios.listado','uses'  => 'UsuariosListadoController@index'));
    Route::resource("usuarios", "UsuariosController");
    Route::get("usuariosFormulario/listado",array('as' => 'usuariosFormulario.listado','uses'  => 'UsuariosFormularioListadoController@index'));
    Route::post("usuariosFormulario/buscar",array('as' => 'usuariosFormulario.buscar','uses'  => 'UsuariosFormularioListadoController@buscar'));
    Route::resource("usuariosFormulario", "UsuariosFormularioListadoController");

    Route::get("mail/sendMail", array('as' => 'mail.sendMail', 'uses' => 'MailController@sendMail'));

    Route::post("listado/buscar",array('as' => 'listado.buscar','uses'  => 'ListadoController@buscar'));
    Route::resource("listado", "ListadoController");

    Route::get("estadisticas",array('as'=>'estadisticas.index','uses'=>'EstadisticasController@index'));

    #Route::post("listadoHistorico/buscar",array('as' => 'listado.buscar_historico','uses'  => 'HistoricoListadoController@buscar'));
    #Route::resource("listadoHistorico", "HistoricoListadoController");

    

});

Event::listen('404', function () {return Response::error('404');});
