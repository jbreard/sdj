<?php
class Time {

    static $sFecha;
    static $sFormato;
    static $oFecha;

    function __construct($sFecha)
    {
        $this->oFecha =  new DateTime($sFecha);

    }

    public static function fecha() {
        static::$oFecha =  new DateTime(static::$sFecha);
        return static::$oFecha->format(static::$sFormato);

    }

    public static function FormatearToMysql($sFecha) {
        if(!empty($sFecha)){
            if(strpos($sFecha,"/") > 0)
                self::$sFecha = str_replace('/', '-', $sFecha);
            else
                self::$oFecha =  new DateTime($sFecha);

            self::$sFormato = "Y-m-d";
            return self::fecha();
        }
        return "0000-00-00";
    }

    public static function FormatearToNormal($sFecha){
        if(is_null($sFecha)) return "";
        if($sFecha == '0000-00-00') return "";
        self::$sFecha = $sFecha;
        self::$sFormato = "d/m/Y";
        return self::fecha();
    }

    public static  function  FormatearHoraFecha($sFechaHora){
        $fecha = \Carbon::createFromFormat('Y-m-d H:i:s',$sFechaHora);
        return $fecha->format('d/m/Y H:i:s');
    }

    public static  function  FormatearHoraFechaMysql($sFechaHora){
        if($sFechaHora == '0000-00-00') return "";
        if(empty($sFechaHora)) return null;
        $fecha = \Carbon::createFromFormat('d/m/Y H:i:s',$sFechaHora);
        return $fecha->format('Y-m-d H:i:s');

    }

}
 