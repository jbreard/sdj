<?php namespace DeclaracionJurada\Transformers;

use DeclaracionJurada\Clases\UrlHash;
use DeclaracionJurada\Repository\SelectOptionsRepo;
use Illuminate\Support\Facades\Config;

class DeclaracionJuradaTransformer
{
    private $urlHash;
    /**
     * @var SelectOptionsRepo
     */
    private $selectOptionsRepo;

    protected $validaciones;

    public function __construct()
    {
        $this->urlHash = new UrlHash();
        $this->selectOptionsRepo = new SelectOptionsRepo;

        $this->setValidaciones(array(
            "apellido"=> Config::get('app.textos.validaciones.apellido'),
            "nombres"=> Config::get('app.textos.validaciones.nombres'),
            "nacimiento"=> Config::get('app.textos.validaciones.nacimiento'),
            "estado_civil"=> Config::get('app.textos.validaciones.estado_civil'),
            "select_default"=> Config::get('app.textos.validaciones.select_default'),
            "provincia"=> Config::get('app.textos.validaciones.provincia'),
            "localidad"=> Config::get('app.textos.validaciones.localidad'),
            "destino_revista"=> Config::get('app.textos.validaciones.destino_revista'),
            "cuerpo_agrupamiento"=> Config::get('app.textos.validaciones.cuerpo_agrupamiento'),
            "escalafon"=> Config::get('app.textos.validaciones.escalafon'),
            "cargo_funcion"=> Config::get('app.textos.validaciones.cargo_funcion'),
            "domicilio_real"=> Config::get('app.textos.validaciones.domicilio_real'),
            "documento"=> Config::get('app.textos.validaciones.documento'),
            "legajo_personal"=> Config::get('app.textos.validaciones.legajo_personal'),
            "telefono"=> Config::get('app.textos.validaciones.telefono'),
            "llamados_servicio"=> Config::get('app.textos.validaciones.llamados_servicio'),
            "email"=> Config::get('app.textos.validaciones.email'),
            "monto_anual_aproximado"=> Config::get('app.textos.validaciones.monto_anual_aproximado'),
            "monto_anual_neto"=> Config::get('app.textos.validaciones.monto_anual_neto'),
            "monto_anual_bruto"=> Config::get('app.textos.validaciones.monto_anual_bruto'),
            "cuil_cuit"=> Config::get('app.textos.validaciones.cuil_cuit'),
            "fuerza_perteneciente"=> Config::get('app.textos.validaciones.fuerza_perteneciente'),
            "campo_requerido"=> Config::get('app.textos.validaciones.campo_requerido'),
            "id_situacion_revista"=> Config::get('app.textos.validaciones.id_situacion_revista'),
            "id_tipo_sexo"=> Config::get('app.textos.validaciones.id_tipo_sexo'),
            "id_tipo_personal"=> Config::get('app.textos.validaciones.id_tipo_personal'),
            "id_grado"=> Config::get('app.textos.validaciones.id_grado'),
        ));
    }

    public function getValidaciones()
    {
        return $this->validaciones;
    }

    public function setValidaciones($validaciones)
    {
        $this->validaciones = $validaciones;
    }

    private function getSelectOptions()
    {
        return $this->selectOptionsRepo->getSelectOptions();
    }

    public function prepareDataForShow(&$declaracionJurada)
    {

        $data = $declaracionJurada->toArray();
        $data['persona']['familiar'] = $this->prepareDataFamiliares($declaracionJurada);
        $data['inmueble'] = $this->prepareDataDefault($declaracionJurada->inmueble);
        $data['vehiculo'] = $this->prepareDataDefault($declaracionJurada->vehiculo);
        $data['bien'] = $this->prepareDataDefault($declaracionJurada->bien);
        $data['deuda'] = $this->prepareDataDefault($declaracionJurada->deuda);
        $data['derecho_expectativa'] = $this->prepareDataDefault($declaracionJurada->derechoExpectativa);
        $data['ingreso'] = $this->prepareDataIngresos($declaracionJurada);

        return $data;
    }

    public function prepareFormDataForEdit($declaracionJurada)
    {
        $declaracionJurada = $this->prepareDataForShow($declaracionJurada);

        $datos = $this->getSelectOptions();
        $datos["declaracion_jurada"] = $declaracionJurada;

        $datos["hashId"] = $this->encrypt($declaracionJurada['id']);
        $datos["redirect"] = Config::get('app.textos.validaciones.redirect');

        $datos["validaciones"] = $this->getValidaciones();

        $datos["cantidad_conyuge"] = count($declaracionJurada['persona']['familiar']['conyuge']);
        $datos["cantidad_hijo"] = count($declaracionJurada['persona']['familiar']['hijo']);
        $datos["cantidad_otra_persona"] = count($declaracionJurada['persona']['familiar']['otra_persona']);
        $datos["cantidad_padre_madre"] = count($declaracionJurada['persona']['familiar']['padre_madre']);
        $datos["cantidad_conviviente_fuerza"] = count($declaracionJurada['persona']['familiar']['conviviente_fuerza']);
        $datos["cantidad_cuenta_bancaria"] = count($declaracionJurada['cuenta_bancaria']);
        $datos["cantidad_cuenta_bancaria_exterior"] = count($declaracionJurada['cuenta_bancaria_exterior']);
        $datos["cantidad_bonos_titulos_acciones"] = count($declaracionJurada['bonos_titulos_acciones']);
        $datos["cantidad_tarjeta_credito"] = count($declaracionJurada['tarjeta_credito']);
        $datos["cantidad_dinero_efectivo"] = count($declaracionJurada['dinero_efectivo']);
        $datos["cantidad_acreencias"] = count($declaracionJurada['acreencias']);
        $datos["cantidad_deuda"] = count($declaracionJurada['deuda']);
        $datos["cantidad_ingreso_extraordinario"] = count($declaracionJurada['ingreso_extraordinario']);
        $datos["cantidad_ingreso_externo"] = count($declaracionJurada['ingreso_externo']);
        $datos["cantidad_ingreso"] = 3;

        $datos["cantidad_derecho_expectativa"]['propio'] = count($declaracionJurada['derecho_expectativa']['propio']);
        $datos["cantidad_derecho_expectativa"]['conyuge'] = count($declaracionJurada['derecho_expectativa']['conyuge']);
        $datos["cantidad_derecho_expectativa"]['hijo'] = count($declaracionJurada['derecho_expectativa']['hijo']);

	    $datos["cantidad_deuda"] = array();
	    $datos["cantidad_deuda"]['propio'] = count($declaracionJurada['deuda']['propio']);
        $datos["cantidad_deuda"]['conyuge'] = count($declaracionJurada['deuda']['conyuge']);
        $datos["cantidad_deuda"]['hijo'] = count($declaracionJurada['deuda']['hijo']);

        $datos["cantidad_inmueble"]['propio'] = count($declaracionJurada['inmueble']['propio']);
        $datos["cantidad_inmueble"]['conyuge'] = count($declaracionJurada['inmueble']['conyuge']);
        $datos["cantidad_inmueble"]['hijo'] = count($declaracionJurada['inmueble']['hijo']);

        $datos["cantidad_vehiculo"]['propio'] = count($declaracionJurada['vehiculo']['propio']);
        $datos["cantidad_vehiculo"]['conyuge'] = count($declaracionJurada['vehiculo']['conyuge']);
        $datos["cantidad_vehiculo"]['hijo'] = count($declaracionJurada['vehiculo']['hijo']);

        $datos["cantidad_bien"]['propio'] = count($declaracionJurada['bien']['propio']);
        $datos["cantidad_bien"]['conyuge'] = count($declaracionJurada['bien']['conyuge']);
        $datos["cantidad_bien"]['hijo'] = count($declaracionJurada['bien']['hijo']);

        $datos['form_data'] = array();
        $datos['action'] = 'Editar';

        return $datos;
    }

    private function prepareDataDefault(&$object)
    {
        $rows = $object->toArray();

        /**
         * Info:
         * $data Contiene en sus keys los ENUM creados en las tablas
         * que estan compartidas entre las personas (propio, conyuge, hijo).
         * Si se crea un nuevo tipo de persona en los ENUM de las tablas, se debe
         * agregar esa opcion en este array.
         */
        $data = [
            'propio' => [],
            'conyuge' => [],
            'hijo' => [],
        ];

        foreach ($rows as $row) {
            $key = $row['tipo_persona'];
            array_push($data[$key], $row);
        }

        return $data;
    }

    private function prepareDataFamiliares($declaracionJurada)
    {
        $familiar = $declaracionJurada->persona->familiar;
        $familiares = $familiar->toArray();

        $dataFamiliares = [
            'conyuge' => [],
            'hijo' => [],
            'otra_persona' => [],
            'padre_madre' => [],
            'conviviente_fuerza' => [],
        ];

        foreach ($familiares as $dataFamiliar) {
            #TODO Mejorar esta negrada
            # En la tabla Familiares, en el campo Tipo los ' ' son reenplazados por '_'
            # para lograr armar la key

            $key = str_replace(' ', '_', $dataFamiliar['tipo']);
            array_push($dataFamiliares[$key], $dataFamiliar);
        }
        return $dataFamiliares;
    }

    public function prepareFormDataForCreate()
    {
        $datos = $this->getSelectOptions();
        $datos["declaracion_jurada"] = null;
        $datos["version_formulario"] = 0;
        $datos["redirect"] = Config::get('app.textos.redirect');
        $datos["validaciones"] = $this->getValidaciones();
        $datos["cantidad_ingreso"] = 3;

        return $datos;
    }

    public function descrypt($hash)
    {
        return $this->urlHash->desencriptar($hash);
    }

    public function encrypt($data)
    {
        return $this->urlHash->encriptar($data);
    }

    private function prepareDataIngresos($declaracionJurada)
    {
        $instancia = $declaracionJurada->ingreso;
        $ingresos = $instancia->toArray();

        $data = [];

        foreach($ingresos as $ingreso)
        {
            if($ingreso['tipo_ingreso']=='Propio de la Fuerza')
                $data['fuerza'] = $ingreso;
            elseif($ingreso['tipo_ingreso']=='Adicional')
                $data['adicional'] = $ingreso;
            elseif($ingreso['tipo_ingreso']=='Extraordinario')
                $data['extraordinario'] = $ingreso;
            elseif($ingreso['tipo_ingreso']=='Externo')
                $data['externo'] = $ingreso;
            else
                $data['otros'] = $ingreso;
        }

        return $data;
    }

}
