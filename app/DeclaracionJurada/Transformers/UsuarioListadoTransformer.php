<?php namespace DeclaracionJurada\Transformers;

use Illuminate\Support\Facades\Input;

class UsuarioListadoTransformer
{

    public function __construct()
    {
    }

    public function prepareResponse($response)
    {
        return $response;
    }        

    /**
     * @return mixed
     */
    public function prepareData()
    {
        $data = Input::all();

        return $data;
    }

}