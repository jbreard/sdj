<?php namespace DeclaracionJurada\Transformers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ListadoTransformer
{

    protected $excelManager;
    private $declaracionJuradaTransformer;

    public function __construct()
    {
        $this->declaracionJuradaTransformer = new DeclaracionJuradaTransformer();        
    }

    public function prepareResponse($response)
    {
        $data = $response;

        if (is_array($data) && !empty($data)) {
            for ($i = 0; $i < count($data); $i++)
                $data[$i]['hash'] = $this->declaracionJuradaTransformer->encrypt($data[$i]['id']);
        }

        $response = $data;

        return $response;
    }        

    /**
     * @return mixed
     */
    public function prepareData()
    {
        $data = Input::all();

        // Si es RRHH, ve solo los que pertenece su fuerza
        if(Auth::user()->id_perfil == 4 || Auth::user()->id_perfil == 3){
            $data['id_fuerza'] = Auth::user()->id_fuerza;
        }

        return $data;
    }

}