<?php namespace DeclaracionJurada\Transformers;

use DeclaracionJurada\Repository\ValidacionUsersRepo;

class CaracterDeclaracionTransformer
{
    protected $validacionUserRepo;

    public function __construct()
    {
        $this->validacionUserRepo = new ValidacionUsersRepo();
    }

    public function prepareDataByAccessForm($accessFormData)
    {
        return $this->validacionUserRepo->getDataByAccessForm($accessFormData);
    }


}