<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 19/07/17
 * Time: 14:35
 */

namespace DeclaracionJurada\Repository;


use User;

class UsuarioRepo extends BaseRepo
{

    public function getModel()
    {
        return New User();
    }

    public function getData($data)
    {
        $model = $this->getModel()->with('perfil','fuerza');

        foreach ($data as $field=>$value) {
            if ($field == 'page') continue;

            if($field != 'activo') {
                $model = $model->where($field,"like","%{$value}%");
            }else {
                $aux = explode(',',$value);
                $model = $model->whereIn($field,$aux);
            }

        }

        return $model->paginate(15);
    }
}