<?php namespace DeclaracionJurada\Repository;

use Familiar;

class FamiliarRepo extends BaseRepo
{

    public function getModel()
    {
        return new Familiar();
    }

    public function newFamiliar()
    {
        return new Familiar();
    }

}