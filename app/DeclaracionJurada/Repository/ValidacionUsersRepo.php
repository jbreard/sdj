<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 07/06/17
 * Time: 16:26
 */
namespace DeclaracionJurada\Repository;


use DeclaracionJurada\Repository\BaseRepo;
use TipoCaracterDeclaracion;
use ValidacionUsers;
use Persona;
use DeclaracionJurada;

class ValidacionUsersRepo extends BaseRepo
{

    public function getModel()
    {
        return new ValidacionUsers();
    }

    public function getIfExists($dni,$legajo,$id_fuerza)
    {

        $resultado = $this->getModel();

        $resultado = $resultado->where('dni',$dni);
        $resultado = $resultado->where('id_fuerza',$id_fuerza);

        if($dni != $legajo)
            $resultado = $resultado->where('legajo',$legajo);

        $resultado = $resultado->first();

        return $resultado;
//        dd($resultado);
    }

    /**
     * @param $accessFormData
     * @return array
     */
    public function getDataByAccessForm($accessFormData)
    {
        $caraterDeclaracion = [
            'ALTA' => 1,
            'ANUAL' => 3,
            'BAJA' => 5,
        ];

        $idUsuario = array_get($accessFormData, 'user');
        $email = array_get($accessFormData, 'user_email');

        $usuario = ValidacionUsers::find($idUsuario);
        //dd($usuario->tipo);
        $usuariosm = ValidacionUsers::where('dni', '=', $usuario->dni)->get();

        /*echo "<pre>";
        var_dump($usuariosm);
        echo "</pre>";*/
        $cartel='';
        $idTipo2 = '';
        if(count($usuariosm)==2){
            $cartelx = "Ud debe hacer: ";
            $i=0;
            $idTipo2 = '4';
            foreach ($usuariosm as $usuariox) {
                //$cartelx.=$usuariox->tipo." - ".$usuariox->periodo_fiscal.". ";
                $cartelx = "Usted debe completar su DDJJ ANUAL PF 2019 y BAJA PF 2020 Una vez que cargue la ANUAL, deberá cargar su DDJJ BAJA. Una vez confeccionadas ambas debe presentarlas.";


                $usuariom[$i] = $usuariox;
                $i++;
            }
             $cartel = $cartelx;


        }

            $idTipo = $caraterDeclaracion[trim($usuario->tipo)];
        if ($idTipo2==4) {
            $idTipo = 4;
        }

            if ($idTipo == 1) {
                //ALTA
                $cartel = "Usted debe hacer su DDJJ de ALTA: deberá informar el patrimonio que poseía al momento de ingresar a la Fuerza.";
                $whereIn = [1, 3];
                $comboPeriodoFiscal = array(date("Y") => date("Y"),date("Y") - 1 => date("Y") - 1);

            } else if ($idTipo == 3) {
                //ANUAL
                $whereIn = [3, 5];
                $comboPeriodoFiscal = array(date("Y") - 1 => date("Y") - 1,date("Y") => date("Y"));

            }else if ($idTipo == 4) {
                //BAJA Y ANUAL
                $whereIn = [3, 5];
                $comboPeriodoFiscal = array(date("Y") - 1 => date("Y") - 1,date("Y") => date("Y"));

             }else {
                $whereIn = [5, 3];
                $comboPeriodoFiscal = array(date("Y") => date("Y"), date("Y") - 1 => date("Y") - 1);
                //$comboCaracterDeclaracion = ['3 '=>"ANUAL",'5 '=>"BAJA"];

             }

            $comboCaracterDeclaracion = TipoCaracterDeclaracion::whereIn('id', $whereIn)->lists('descripcion', 'id');
//dd($comboCaracterDeclaracion);


        return [
            'cartel' => $cartel,
            'combo_caracter_declaracion' => $comboCaracterDeclaracion,
            'comboPeriodoFiscal' => $comboPeriodoFiscal,
            'access_form_data' => [
                'nombres' => $usuario->nombre,
                'apellido' => $usuario->apellido,
                'dni' => $usuario->dni,
                'legajo' => $usuario->legajo,
                'id_fuerza' => $usuario->id_fuerza,
                'email' => $email,
            ]
        ];
    }

    public function getData($data)
    {

        $model = $this->getModel()->with('fuerza');

        foreach ($data as $field=>$value) {
            if ($field == 'page') continue;

            if($field != 'tipo') {
                $model = $model->where($field,"like","%{$value}%");
            }else {
                $aux = explode(',',$value);
                $model = $model->whereIn($field,$aux);
            }

        }

        return $model->paginate(15);
    }

}