<?php namespace DeclaracionJurada\Repository;


abstract class BaseRepo {

    protected $model;


    abstract public function getModel();

    function __construct()
    {
        $this->model = $this->getModel();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function delete($id)
    {
        $model = $this->find($id);
        return $model->delete();
    }

    public function all()
    {
        return $this->model->all();
    }

    public function where($campo,$valor,$param='='){
        return $this->model->where($campo,$param,$valor)->get();

    }

    public function paginate($limit = 15){
        return $this->model->paginate($limit);

    }

    public function getSolicitudParadero($idPersona){

        return $this->model->where('id_persona','=',$idPersona)->firstOrFail();
    }

} 