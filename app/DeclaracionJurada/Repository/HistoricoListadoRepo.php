<?php namespace DeclaracionJurada\Repository;

use DeclaracionJurada;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Time;


class HistoricoListadoRepo extends BaseRepo
{

    public function getModel()
    {
        $model = new DeclaracionJurada();
        $model->setConnection('mysql_historico');

        return $model;
    }

    public function newPersona()
    {
        $declaracionJurada = new DeclaracionJurada();
        return $declaracionJurada;
    }

    public function getDeclaracionJuradaById($id)
    {
        $declaracionJurada = $this->getModel();
        return $declaracionJurada->with([
            'caracter',
            'persona.familiar.persona',
            'persona.familiar.persona.tipoVinculo',
            'persona.familiar.persona.categoriaTributaria',
            'persona.fuerza',
            'persona.sexo',
            'persona.provincia',
            'persona.localidad',
            'persona.estadoCivil',
            'persona.tipoPersonal',
            'persona.grado',
            'persona.situacionRevista',
            'persona.tipoPersonalFuerza',
            'cuentaBancaria',
            'cuentaBancaria.tipoCuentaBancaria',
            'cuentaBancaria.tipoMoneda',
            'cuentaBancariaExterior',
            'cuentaBancariaExterior.tipoCuentaBancariaExterior',
            'cuentaBancariaExterior.tipoMoneda',
            'bonosTitulosAcciones',
            'bonosTitulosAcciones.tipoBono',
            'dineroEfectivo',
            'dineroEfectivo.tipoMoneda',
            'derechoExpectativa.persona',
            'derechoExpectativa.tipoDerechoExpectativa',
            'derechoExpectativa.tipoFuente',
            'derechoExpectativa.tipoCuotaFuente',
            'acreencias',
            'acreencias.tipoMoneda',
            'ingreso',
            'ingreso.moneda',
            'ingreso.salario',
            'ingreso.tipoIngresoExtraordinario',
            'ingresoExterno',
            'ingresoExterno.moneda',
            'ingresoExterno.tipoIngreso',
            'ingresoExterno.fuerza',
            'ingresoExterno.ingresoExtraordinario',
            'ingresoExterno.ingresoOtraActividad',
            'ingresoExterno.categoriaTributaria',
	        'ingresoExtraordinario',
            'ingresoExtraordinario.moneda',
            'ingresoExtraordinario.tipoIngreso',
            'inmueble.persona',
            'inmueble.provincia',
            'inmueble.localidad',
            'inmueble.tipoInmueble',
            'inmueble.origen',
            'inmueble.unidadSuperficie',
            'inmueble.tipoCaracter',
            'inmueble.tipoModoAdquisicion',
            'inmueble.tipoFormaPago',
            'inmueble.pais',
            'inmueble.monedaValorAdquisicion',
            'inmueble.monedaValorEstimado',
            'vehiculo.persona',
            'vehiculo.origen',
            'vehiculo.tipoVehiculo',
            'vehiculo.tipoVehiculoNuevo',
            'vehiculo.tipoCaracter',
            'vehiculo.tipoModoAdquisicion',
            'vehiculo.formaPago',
            'vehiculo.pais',
            'vehiculo.provincia',
            'vehiculo.localidad',
            'vehiculo.monedaValorEstimado',
            'vehiculo.monedaValorSeguro',
            'bien.persona',
            'bien.tipoBien',
            'bien.origen',
            'bien.tipoCaracter',
            'bien.tipoAdquisicion',
            'deuda.persona',
            'deuda.tipoDeuda',
            'deuda.tipoMoneda',
            'tarjetaCredito',
            'tarjetaCredito.entidadEmisora',
            'tarjetaCredito.tipoTarjetaCredito',
            'tarjetaCredito.tipoTitular',
        ])
            ->where('id',$id)
            ->first();
    }

    public function getIdPersonaByIdDeclaracionJurada($id)
    {
        $declaracionJurada = DeclaracionJurada::where('id',$id)->first();
        return $declaracionJurada->id_persona;
    }

    public function getData($data = array(), $type = 'search')
    {
        return call_user_func([$this,$type],$data);
    }

    public function getVersionByIdDeclaracionJurada($id)
    {
        $declaracionJurada = DeclaracionJurada::where('id',$id)->first();
        return $declaracionJurada->version;
    }

    public function getMessageRedirect($hash, $tipoEstado)
    {
        $message = sprintf(Config::get('app.textos.validaciones.no_edit_by_tipoEstado'), $hash, $tipoEstado);
        $redirect = sprintf("<p class='bg-danger error'>%s</p>",$message);
        return $redirect;
    }

    private function excel($data)
    {
        return $this->filterInDeclaracionJuradaByQuery($data);
    }

    private function search($data)
    {        
        $remember = Config::get('app.declaracion_jurada.cache.listado');

        ### Filtros en Declaracion Jurada
        $declaracionJurada = $this->filterInDeclaracionJurada($data);

        if(isset($data['nombres'])){
            $dato = $data['nombres'];
            $declaracionJurada = $declaracionJurada->whereHas('persona',function($q) use($dato){
                $q->where('nombres','like','%'.$dato.'%');
            });
        }

        if(isset($data['apellido'])){
            $dato=$data['apellido'];
            $declaracionJurada = $declaracionJurada->whereHas('persona',function($q) use($dato){
                $q->where('apellido','like','%'.$dato.'%');
            });
        }

        if(isset($data['documento'])){
            $dato=$data['documento'];
            $declaracionJurada = $declaracionJurada->whereHas('persona',function($q) use($dato){
                $q->where('documento','like','%'.$dato.'%');
            });
        }

        if(isset($data['id_fuerza'])){
            $idFuerza = $data['id_fuerza'];
            $declaracionJurada = $declaracionJurada->whereHas('persona',function($q) use($idFuerza){
                $q->whereHas('fuerza',function($q) use($idFuerza){
                    $q->where('id','=',$idFuerza);
                });
            });
        }

        return $declaracionJurada->paginate(15);
    }

    private function filterInDeclaracionJurada($data)
    {
        $declaracionJurada = $this->getModel()->on('mysql_historico')->with([
            'persona',
            'persona.fuerza',
        ]);

        if(isset($data['fecha_creada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_creada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_creada_hasta'])) ? Time::FormatearToMysql($data['fecha_creada_hasta']) . ' 23:59:59' : 'NOW()';
            $declaracionJurada = $declaracionJurada
                ->where('declaraciones_juradas.created_at','>=', $desde)
                ->where('declaraciones_juradas.created_at','<=', $hasta);
        }

        if(isset($data['fecha_aceptada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_aceptada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_aceptada_hasta'])) ? Time::FormatearToMysql($data['fecha_aceptada_hasta']) . ' 23:59:59' : 'NOW()';
            $declaracionJurada = $declaracionJurada
                ->where('declaraciones_juradas.accepted_at','>=', $desde)
                ->where('declaraciones_juradas.accepted_at','<=', $hasta);
        }

        if(isset($data['fecha_recibida_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_recibida_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_aceptada_hasta'])) ? Time::FormatearToMysql($data['fecha_recibida_hasta']) . ' 23:59:59' : 'NOW()';
            $declaracionJurada = $declaracionJurada
                ->where('declaraciones_juradas.received_at','>=', $desde)
                ->where('declaraciones_juradas.received_at','<=', $hasta);
        }

        if(isset($data['fecha_eliminada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_eliminada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_aceptada_hasta'])) ? Time::FormatearToMysql($data['fecha_eliminada_hasta']) . ' 23:59:59' : 'NOW()';
            $declaracionJurada = $declaracionJurada
                ->where('declaraciones_juradas.deleted_at','>=', $desde)
                ->where('declaraciones_juradas.deleted_at','<=', $hasta);
        }

        if(isset($data['tipo_estado']))
        {
            $estados = explode(',',$data['tipo_estado']);
            $declaracionJurada = $declaracionJurada->whereIn('tipo_estado',$estados);
        }

        if(isset($data['nro_formulario'])) $declaracionJurada = $declaracionJurada->where('id','like','%'.$data['nro_formulario']."%");

        if(isset($data['nro_version'])) $declaracionJurada = $declaracionJurada->where('version','like','%'.$data['nro_version']."%");
        
        if(isset($data['personas']) && !empty($data['personas'])){
            foreach($data['personas'] as $idPersona)
            {
                $declaracionJurada = $declaracionJurada->orWhere('id_persona',$idPersona);
            }
        }

        return $declaracionJurada;
    }
    private function filterInDeclaracionJuradaByQuery($data)
    {
        $createdAt = '';
        $deletedAt = '';
        $receivedAt = '';
        $aceptedAt = '';
        $nroFormulario = '';
        $version = '';
        $tipoEstado = '';

        if(isset($data['fecha_creada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_creada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_creada_hasta'])) ? Time::FormatearToMysql($data['fecha_creada_hasta']) . ' 23:59:59' : 'NOW()';
            $createdAt = " declaraciones_juradas.created_at >= '{$desde}' AND declaraciones_juradas.created_at <= '{$hasta}'";

        }

        if(isset($data['fecha_aceptada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_aceptada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_aceptada_hasta'])) ? Time::FormatearToMysql($data['fecha_aceptada_hasta']) . ' 23:59:59' : 'NOW()';
            $aceptedAt = " declaraciones_juradas.accepted_at >= '{$desde}' AND declaraciones_juradas.accepted_at <= '{$hasta}'";

        }

        if(isset($data['fecha_recibida_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_recibida_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_recibida_hasta'])) ? Time::FormatearToMysql($data['fecha_recibida_hasta']) . ' 23:59:59' : 'NOW()';
            $receivedAt = " declaraciones_juradas.received_at >= '{$desde}' AND declaraciones_juradas.received_at <= '{$hasta}'";

        }

        if(isset($data['fecha_eliminada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_eliminada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_eliminada_hasta'])) ? Time::FormatearToMysql($data['fecha_eliminada_hasta']) . ' 23:59:59' : 'NOW()';
            $deletedAt = " declaraciones_juradas.deleted_at >= '{$desde}' AND declaraciones_juradas.deleted_at <= '{$hasta}'";
        }

        if(isset($data['tipo_estado'])){
            $string = '';
            $estados = explode(',',$data['tipo_estado']);

            foreach ($estados as $index=>$estado){
                $string.= ($index>0) ? "," : "";
                $string.= "'{$estado}'";
            }

            $tipoEstado = " declaraciones_juradas.tipo_estado IN ({$string})";
        }

        if(isset($data['nro_formulario'])){
            $nroFormulario = " declaraciones_juradas.id LIKE '%{$data['nro_formulario']}%'";
        }

        if(isset($data['nro_version'])){
            $version = " declaraciones_juradas.version LIKE '{$data['nro_version']}%'";
        }

        if(isset($data['nombres'])){
            $personaNombre = " personas.nombres LIKE '%{$data['nombres']}%'";
        }

        if(isset($data['apellido'])){
            $personaApellido = " personas.apellido LIKE '%{$data['apellido']}%'";
        }

        if(isset($data['documento'])){
            $personaDocumento = " personas.documento LIKE '%{$data['documento']}%'";
        }

        // Si es RRHH, ve solo los que pertenece su fuerza

        if(Auth::user()->id_perfil == 4 || Auth::user()->id_perfil == 3){
            $idFuerza = Auth::user()->id_fuerza;
            $personaFuerza = " personas.id_fuerza = {$idFuerza}";
        }else if(isset($data['id_fuerza'])){
            $idFuerza = $data['id_fuerza'];
            $personaFuerza = " personas.id_fuerza = {$idFuerza}";
        }

        $query = <<<QUERY
SELECT 
declaraciones_juradas.id,
declaraciones_juradas.version,
declaraciones_juradas.tipo_estado,
declaraciones_juradas.created_at,
declaraciones_juradas.accepted_at,
declaraciones_juradas.received_at,
declaraciones_juradas.deleted_at,
personas.nombres,
personas.apellido,
personas.documento,
fuerzas.descripcion as 'fuerza'
FROM 
declaraciones_juradas 
INNER JOIN personas on personas.id = declaraciones_juradas.id_persona
INNER JOIN fuerzas on fuerzas.id = personas.id_fuerza
WHERE 1=1
QUERY;

        if(!empty($personaNombre)) $query.= " AND {$personaNombre}";
        if(!empty($personaApellido)) $query.= " AND {$personaApellido}";
        if(!empty($personaDocumento)) $query.= " AND {$personaDocumento}";
        if(!empty($personaFuerza)) $query.= " AND {$personaFuerza}";

        if(!empty($createdAt)) $query.= " AND {$createdAt}";
        if(!empty($aceptedAt)) $query.= " AND {$aceptedAt}";
        if(!empty($deletedAt)) $query.= " AND {$deletedAt}";
        if(!empty($receivedAt)) $query.= " AND {$receivedAt}";
        if(!empty($nroFormulario)) $query.= " AND {$nroFormulario}";
        if(!empty($version)) $query.= " AND {$version}";
        if(!empty($tipoEstado)) $query.= " AND {$tipoEstado}";

        $idUsuario = Auth::user()->id;
        $cacheString = md5($query) . $idUsuario;

        return Cache::remember($cacheString, 10 , function() use($query){
            return json_decode(json_encode(DB::select(DB::raw($query))), true);
        });

    }

}
