<?php namespace DeclaracionJurada\Repository;

use Acreencia;
use Bien;
use BonosTitulosAcciones;
use CuentaBancaria;
use CuentaBancariaExterior;
use DeclaracionJurada;
use DeclaracionJurada\Transformers\DeclaracionJuradaTransformer;
use DerechoExpectativa;
use Deuda;
use DineroEfectivo;
use Familiar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Ingreso;
use IngresoExterno;
use IngresoExtraordinario;
use Inmueble;
use Persona;
use TarjetaCredito;
use Time;
use Vehiculo;

class DeclaracionJuradaRepo extends BaseRepo
{

    public function getModel()
    {
        return new DeclaracionJurada();
    }

    public function newPersona()
    {
        $declaracionJurada = new DeclaracionJurada();
        return $declaracionJurada;
    }

    public function getDeclaracionJuradaById($id)
    {
        $declaracionJurada = $this->getModel();
        return $declaracionJurada->with([
            'caracter',
            'persona.familiar.persona',
            'persona.familiar.persona.tipoVinculo',
            'persona.familiar.persona.categoriaTributaria',
            'persona.familiar.persona.tipoDocumento',
            'persona.familiar.persona.fuerza',
            'persona.familiar.persona.tipoActividadConvivienteFuerza',
            'persona.familiar.persona.tipoHijoConvive',
            'persona.fuerza',
            'persona.sexo',
            'persona.provincia',
            'persona.localidad',
            'persona.estadoCivil',
            'persona.tipoPersonal',
            'persona.tipoAgrupamientoModalidad',
            'persona.grado',
            'persona.situacionRevista',
            'persona.tipoPersonalFuerza',
            'cuentaBancaria',
            'cuentaBancaria.tipoCuentaBancaria',
            'cuentaBancaria.tipoMoneda',
            'cuentaBancariaExterior',
            'cuentaBancariaExterior.tipoCuentaBancariaExterior',
            'cuentaBancariaExterior.tipoMoneda',
            'bonosTitulosAcciones',
            'bonosTitulosAcciones.tipoBono',
            'dineroEfectivo',
            'dineroEfectivo.tipoMoneda',
            'derechoExpectativa.persona',
            'derechoExpectativa.tipoDerechoExpectativa',
            'derechoExpectativa.tipoFuente',
            'derechoExpectativa.tipoCuotaFuente',
            'acreencias',
            'acreencias.tipoMoneda',
            'ingreso',
            'ingreso.moneda',
            'ingreso.salario',
            'ingreso.tipoIngresoExtraordinario',
            'ingresoExterno',
            'ingresoExterno.moneda',
            'ingresoExterno.tipoIngreso',
            'ingresoExterno.fuerza',
            'ingresoExterno.ingresoExtraordinario',
            'ingresoExterno.ingresoOtraActividad',
            'ingresoExterno.categoriaTributaria',
	        'ingresoExtraordinario',
            'ingresoExtraordinario.moneda',
            'ingresoExtraordinario.tipoIngreso',
            'inmueble.persona',
            'inmueble.provincia',
            'inmueble.localidad',
            'inmueble.tipoInmueble',
            'inmueble.origen',
            'inmueble.unidadSuperficie',
            'inmueble.tipoCaracter',
            'inmueble.tipoModoAdquisicion',
            'inmueble.tipoFormaPago',
            'inmueble.pais',
            'inmueble.monedaValorAdquisicion',
            'inmueble.monedaValorEstimado',
            'vehiculo.persona',
            'vehiculo.origen',
            'vehiculo.tipoVehiculo',
            'vehiculo.tipoVehiculoNuevo',
            'vehiculo.tipoCaracter',
            'vehiculo.tipoModoAdquisicion',
            'vehiculo.formaPago',
            'vehiculo.pais',
            'vehiculo.provincia',
            'vehiculo.localidad',
            'vehiculo.monedaValorEstimado',
            'vehiculo.monedaValorSeguro',
            'bien.persona',
            'bien.tipoBien',
            'bien.origen',
            'bien.tipoCaracter',
            'bien.tipoAdquisicion',
            'deuda.persona',
            'deuda.tipoDeuda',
            'deuda.tipoMoneda',
            'tarjetaCredito',
            'tarjetaCredito.entidadEmisora',
            'tarjetaCredito.tipoTarjetaCredito',
            'tarjetaCredito.tipoTitular',
        ])
            ->where('id',$id)
            ->first();
    }

    public function prepareDataForShow(&$declaracionJurada)
    {
        $data = $declaracionJurada->toArray();
        $data['persona']['familiar'] = $this->prepareDataFamiliares($declaracionJurada);
        $data['inmueble'] = $this->prepareDataDefault($declaracionJurada->inmueble);
        $data['vehiculo'] = $this->prepareDataDefault($declaracionJurada->vehiculo);
        $data['bien'] = $this->prepareDataDefault($declaracionJurada->bien);

        return $data;
    }

    public function getMailByIdDeclaracionJurada($id)
    {
        $declaracionJurada = DeclaracionJurada::with('persona')->where('id',$id)->first();
        return $declaracionJurada->persona->email;
    }

    public function getIdPersonaByIdDeclaracionJurada($id)
    {
        $declaracionJurada = DeclaracionJurada::where('id',$id)->first();
        return $declaracionJurada->id_persona;
    }

    public function getData($data = array(), $type = 'search')
    {
        return call_user_func([$this,$type],$data);
    }

    public function getVersionByIdDeclaracionJurada($id)
    {
        $declaracionJurada = DeclaracionJurada::where('id',$id)->first();
        return $declaracionJurada->version;
    }

    public function getMessageRedirect($hash, $tipoEstado)
    {
        $message = sprintf(Config::get('app.textos.validaciones.no_edit_by_tipoEstado'), $hash, $tipoEstado);
        $redirect = sprintf("<p class='bg-danger error'>%s</p>",$message);
        return $redirect;
    }

    public function getVersionFormularioById($hash)
    {
        $declaracionJuradaTransformer = new DeclaracionJuradaTransformer();
        $idDeclaracionJurada = $declaracionJuradaTransformer->descrypt($hash);
        $declaracionJurada = DeclaracionJurada::where('id',$idDeclaracionJurada)->first();
        return $declaracionJurada->version_formulario;
    }

    private function excel($data)
    {
        return $this->filterInDeclaracionJuradaByQuery($data);
    }

    private function search($data)
    {
        $remember = Config::get('app.declaracion_jurada.cache.listado');

        ### Filtros en Declaracion Jurada
        $declaracionJurada = $this->filterInDeclaracionJurada($data);

        if(isset($data['nombres'])){
            $dato = $data['nombres'];
            $declaracionJurada = $declaracionJurada->whereHas('persona',function($q) use($dato){
                $q->where('nombres','like','%'.$dato.'%');
            });
        }

        if(isset($data['apellido'])){
            $dato=$data['apellido'];
            $declaracionJurada = $declaracionJurada->whereHas('persona',function($q) use($dato){
                $q->where('apellido','like','%'.$dato.'%');
            });
        }

        if(isset($data['documento'])){
            $dato=$data['documento'];
            $declaracionJurada = $declaracionJurada->whereHas('persona',function($q) use($dato){
                $q->where('documento','like','%'.$dato.'%');
            });
        }

        if(isset($data['id_grado'])){
            $dato=$data['id_grado'];
            $declaracionJurada = $declaracionJurada->whereHas('persona',function($q) use($dato){
                $q->where('id_grado','=',$dato);
            });
        }

        if(isset($data['id_tipo_personal'])){
            $dato=$data['id_tipo_personal'];
            $declaracionJurada = $declaracionJurada->whereHas('persona',function($q) use($dato){
                $q->where('id_tipo_personal','=',$dato);
            });
        }

        if(isset($data['id_fuerza'])){
            $idFuerza = $data['id_fuerza'];
            $declaracionJurada = $declaracionJurada->whereHas('persona',function($q) use($idFuerza){
                $q->whereHas('fuerza',function($q) use($idFuerza){
                    $q->where('id','=',$idFuerza);
                });
            });
        }

        return $declaracionJurada->paginate(15);
    }

    private function filterInDeclaracionJurada($data)
    {
        $declaracionJurada = $this->getModel()->with([
            'persona',
            'persona.fuerza',
            'persona.grado',
            'persona.tipoPersonal',
            'caracter'
        ]);

//        dd($declaracionJurada->first()->toArray());

        if(isset($data['fecha_creada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_creada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_creada_hasta'])) ? Time::FormatearToMysql($data['fecha_creada_hasta']) . ' 23:59:59' : 'NOW()';
            $declaracionJurada = $declaracionJurada
                ->where('declaraciones_juradas.created_at','>=', $desde)
                ->where('declaraciones_juradas.created_at','<=', $hasta);
        }

        if(isset($data['fecha_aceptada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_aceptada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_aceptada_hasta'])) ? Time::FormatearToMysql($data['fecha_aceptada_hasta']) . ' 23:59:59' : 'NOW()';
            $declaracionJurada = $declaracionJurada
                ->where('declaraciones_juradas.accepted_at','>=', $desde)
                ->where('declaraciones_juradas.accepted_at','<=', $hasta);
        }

        if(isset($data['fecha_recibida_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_recibida_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_recibida_hasta'])) ? Time::FormatearToMysql($data['fecha_recibida_hasta']) . ' 23:59:59' : 'NOW()';
            $declaracionJurada = $declaracionJurada
                ->where('declaraciones_juradas.received_at','>=', $desde)
                ->where('declaraciones_juradas.received_at','<=', $hasta);
        }

        if(isset($data['fecha_eliminada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_eliminada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_eliminada_hasta'])) ? Time::FormatearToMysql($data['fecha_eliminada_hasta']) . ' 23:59:59' : 'NOW()';
            $declaracionJurada = $declaracionJurada
                ->where('declaraciones_juradas.deleted_at','>=', $desde)
                ->where('declaraciones_juradas.deleted_at','<=', $hasta);
        }

        if(isset($data['tipo_estado']))
        {
            $estados = explode(',',$data['tipo_estado']);
            $declaracionJurada = $declaracionJurada->whereIn('tipo_estado',$estados);
        }

        if(isset($data['nro_formulario'])) $declaracionJurada = $declaracionJurada->where('id','like','%'.$data['nro_formulario']."%");

        if(isset($data['nro_version'])) $declaracionJurada = $declaracionJurada->where('version','like','%'.$data['nro_version']."%");
        
        if(isset($data['personas']) && !empty($data['personas'])){
            foreach($data['personas'] as $idPersona)
            {
                $declaracionJurada = $declaracionJurada->orWhere('id_persona',$idPersona);
            }
        }

        return $declaracionJurada;
    }
    private function filterInDeclaracionJuradaByQuery($data)
    {
        $createdAt = '';
        $deletedAt = '';
        $receivedAt = '';
        $aceptedAt = '';
        $nroFormulario = '';
        $version = '';
        $tipoEstado = '';

        if(isset($data['fecha_creada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_creada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_creada_hasta'])) ? Time::FormatearToMysql($data['fecha_creada_hasta']) . ' 23:59:59' : 'NOW()';
            $createdAt = " declaraciones_juradas.created_at >= '{$desde}' AND declaraciones_juradas.created_at <= '{$hasta}'";

        }

        if(isset($data['fecha_aceptada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_aceptada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_aceptada_hasta'])) ? Time::FormatearToMysql($data['fecha_aceptada_hasta']) . ' 23:59:59' : 'NOW()';
            $aceptedAt = " declaraciones_juradas.accepted_at >= '{$desde}' AND declaraciones_juradas.accepted_at <= '{$hasta}'";

        }

        if(isset($data['fecha_recibida_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_recibida_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_recibida_hasta'])) ? Time::FormatearToMysql($data['fecha_recibida_hasta']) . ' 23:59:59' : 'NOW()';
            $receivedAt = " declaraciones_juradas.received_at >= '{$desde}' AND declaraciones_juradas.received_at <= '{$hasta}'";

        }

        if(isset($data['fecha_eliminada_desde']))
        {
            $desde = Time::FormatearToMysql($data['fecha_eliminada_desde']) . ' 00:00:00';
            $hasta = (isset($data['fecha_eliminada_hasta'])) ? Time::FormatearToMysql($data['fecha_eliminada_hasta']) . ' 23:59:59' : 'NOW()';
            $deletedAt = " declaraciones_juradas.deleted_at >= '{$desde}' AND declaraciones_juradas.deleted_at <= '{$hasta}'";
        }

        if(isset($data['tipo_estado'])){
            $string = '';
            $estados = explode(',',$data['tipo_estado']);

            foreach ($estados as $index=>$estado){
                $string.= ($index>0) ? "," : "";
                $string.= "'{$estado}'";
            }

            $tipoEstado = " declaraciones_juradas.tipo_estado IN ({$string})";
        }

        if(isset($data['nro_formulario'])){
            $nroFormulario = " declaraciones_juradas.id LIKE '%{$data['nro_formulario']}%'";
        }

        if(isset($data['nro_version'])){
            $version = " declaraciones_juradas.version LIKE '{$data['nro_version']}%'";
        }

        if(isset($data['nombres'])){
            $personaNombre = " personas.nombres LIKE '%{$data['nombres']}%'";
        }

        if(isset($data['apellido'])){
            $personaApellido = " personas.apellido LIKE '%{$data['apellido']}%'";
        }

        if(isset($data['documento'])){
            $personaDocumento = " personas.documento LIKE '%{$data['documento']}%'";
        }

        // Si es RRHH, ve solo los que pertenece su fuerza

        if(Auth::user()->id_perfil == 4 || Auth::user()->id_perfil == 3){
            $idFuerza = Auth::user()->id_fuerza;
            $personaFuerza = " personas.id_fuerza = {$idFuerza}";
        }else if(isset($data['id_fuerza'])){
            $idFuerza = $data['id_fuerza'];
            $personaFuerza = " personas.id_fuerza = {$idFuerza}";
        }

        $query = <<<QUERY
SELECT 
declaraciones_juradas.id,
declaraciones_juradas.version,
declaraciones_juradas.tipo_estado,
declaraciones_juradas.created_at,
declaraciones_juradas.accepted_at,
declaraciones_juradas.received_at,
declaraciones_juradas.deleted_at,
personas.nombres,
personas.apellido,
personas.documento,
fuerzas.descripcion as 'fuerza'
FROM 
declaraciones_juradas 
INNER JOIN personas on personas.id = declaraciones_juradas.id_persona
INNER JOIN fuerzas on fuerzas.id = personas.id_fuerza
WHERE 1=1
QUERY;

        if(!empty($personaNombre)) $query.= " AND {$personaNombre}";
        if(!empty($personaApellido)) $query.= " AND {$personaApellido}";
        if(!empty($personaDocumento)) $query.= " AND {$personaDocumento}";
        if(!empty($personaFuerza)) $query.= " AND {$personaFuerza}";

        if(!empty($createdAt)) $query.= " AND {$createdAt}";
        if(!empty($aceptedAt)) $query.= " AND {$aceptedAt}";
        if(!empty($deletedAt)) $query.= " AND {$deletedAt}";
        if(!empty($receivedAt)) $query.= " AND {$receivedAt}";
        if(!empty($nroFormulario)) $query.= " AND {$nroFormulario}";
        if(!empty($version)) $query.= " AND {$version}";
        if(!empty($tipoEstado)) $query.= " AND {$tipoEstado}";

        $idUsuario = Auth::user()->id;
        $cacheString = md5($query) . $idUsuario;

        #return Cache::remember($cacheString, 10 , function() use($query){
            return json_decode(json_encode(DB::select(DB::raw($query))), true);
        #});

    }

    private function filterInPersona($data)
    {
        $remember = Config::get('app.declaracion_jurada.cache.combos');
        $personasRepo = new PersonaRepo();
        $persona = $personasRepo->getModel();
        $persona = (isset($data['nombres'])) ? $persona->where('nombres','like','%'.$data['nombres'].'%') : $persona;
        $persona = (isset($data['apellido'])) ? $persona->where('apellido','like','%'.$data['apellido'].'%') : $persona;
        $persona = (isset($data['documento'])) ? $persona->where('documento','like','%'.$data['documento'].'%') : $persona;

        #Si es RRHH o Dependencia, ve solo los que pertenece su fuerza
        if( Auth::user()->id_perfil == 4 || Auth::user()->id_perfil == 3 )
        {
            $idFuerza = Auth::user()->id_fuerza;
            $persona = $persona->where('id_fuerza',$idFuerza);
        } else if(isset($data['id_fuerza'])) {
            $idFuerza = $data['id_fuerza'];
            $persona = $persona->where('id_fuerza',$idFuerza);
        }

        $persona->where('es_familiar',1);

        $personas = [];
        $persona = $persona->remember($remember);

        $persona->chunk(10000, function ($items) use (&$personas) {
            foreach($items as $item) {
                array_push($personas, $item->id);
            }
        });

        return $personas;
    }

    public function setDataInRedis($data)
    {

        $redis = Redis::connection();

        $idDeclaracionJurada = array_get($data, 'id_declaracion_jurada');

        $key = "ddjj_{$idDeclaracionJurada}";
        $value = json_encode($data);

        $redis->set($key, $value);

        return $key;

    }

    public function getDataInRedis($key, $subKey=null)
    {
        $redis = Redis::connection();

        $data = json_decode($redis->get($key));
        
        if(!is_null($subKey)) $data = array_get($data,$subKey);        

        return $data;
    }

    public function deleteRecord($id, $modelo)
    {
        if($modelo == "conyuge"
            || $modelo == "hijo"
            || $modelo == "otras_convivientes"
            || $modelo == "padre_madre"
            || $modelo == "conviviente_fuerza"
        )
        {
            $this->getModelDelete($modelo,$id);

        }
        else
        {
            $model = $this->getModelDelete($modelo,$id);
//            dd($model);
            $model->where("id",$id)->delete();

        }
    }



    private function getModelDelete($modelo,$id="")
    {
        $model = null;
        switch($modelo)
        {
            case ($modelo == "conyuge"
                || $modelo == "hijo"
                || $modelo == "otras_convivientes"
                || $modelo == "padre_madre"
                || $modelo == "conviviente_fuerza"
            );
                $model = new Persona();
                $model->where("id",$id)->delete();

                $model_familiar = new Familiar();
                $model_familiar->where('id_familiar',$id)->delete();
                break;
            case "inmuebles_propios";
                $model = new Inmueble();
                break;
            case ($modelo == "vehiculos_propios" || $modelo == "vehiculos_conyuge" || $modelo == "vehiculos_hijos");
                $model = new Vehiculo();
                break;
            case "cuenta_bancaria_propia";
                $model = new CuentaBancaria();
                break;
            case "cuenta_bancaria_exterior";
                $model = new CuentaBancariaExterior();
                break;
            case "cuenta_bancaria_bonos_titulos_acciones";
                $model = new BonosTitulosAcciones();
                break;
            case ($modelo == "derechos_expectativas_propios" || $modelo == "derecho_expectativa_conyuge" || $modelo == "derecho_expectativa_hijo");
                $model = new DerechoExpectativa();
                break;
            case ($modelo == "otros_bienes" || $modelo == "bienes_conyuge");
                $model = new Bien();
                break;
            case "dinero_efectivo";
                $model = new DineroEfectivo();
                break;
            case "acreencias";
                $model = new Acreencia();
                break;
            case "ingreso_extraordinario";
                $model = new IngresoExtraordinario();
                break;
            case "ingresos_externos";
                $model = new IngresoExterno();
                break;
            case "tarjeta_credito";
                $model = new TarjetaCredito();
                break;
            case ($modelo == "deudas_propias" || $modelo == "deudas_conyuge" || $modelo == "deudas_hijo");
                $model = new Deuda();
                break;
            case ($modelo == "inmuebles_conyuge" || $modelo == "inmuebles_hijos");
                $model = new Inmueble();
                break;

        }
        return $model;

//        if($modelo == "conyuge" || $modelo == "hijo" || $modelo == "otras_convivientes")
//        {
//            $model = new Persona();
//            $model->where("id",$id)->delete();
//
//            $model_familiar = new Familiar();
//            $model_familiar->where('id_familiar',$id)->delete();
//        }
//        if($modelo == "inmuebles_propios")
//        {
//            return new Inmueble();
//        }
//        if($modelo == "vehiculos_propios")
//        {
//            return new Vehiculo();
//        }
//        if($modelo == "cuenta_bancaria_propia")
//        {
//            return new CuentaBancaria();
//        }
//        if($modelo == "cuenta_bancaria_exterior")
//        {
//            return new CuentaBancariaExterior();
//        }
//        if($modelo == "cuenta_bancaria_bonos_titulos_acciones")
//        {
//            return new BonosTitulosAcciones();
//        }
//        if($modelo == "derechos_expectativas_propios")
//        {
//            return new DerechoExpectativa();
//        }
//        if($modelo == "otros_bienes")
//        {
//            return new Bien();
//        }
//        if($modelo == "dinero_efectivo")
//        {
//            return new DineroEfectivo();
//        }
//        if($modelo == "acreencias")
//        {
//            return new Acreencia();
//        }
//        if($modelo == "ingreso_extraordinario")
//        {
//            return new IngresoExtraordinario();
//        }
//        if($modelo == "ingresos_externos")
//        {
//            return new IngresoExterno();
//        }
//        if($modelo == "tarjeta_credito")
//        {
//            return new TarjetaCredito();
//        }
//        if($modelo == "deudas_propias")
//        {
//            return new Deuda();
//        }
//
//        return new Persona();
    }

}
