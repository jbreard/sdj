<?php namespace DeclaracionJurada\Repository;

use EntidadEmisoraTC;
use EstadoCivil;
use Fuerza;
use FuerzaConyuge;
use Illuminate\Support\Facades\Config;
use OrigenBien;
use OrigenInmueble;
use OrigenVehiculo;
use Pais;
use Provincia;
use TipoActoAdministrativo;
use TipoBien;
use TipoBono;
use TipoCaracter;
use TipoCaracterDeclaracion;
use TipoCategoriaTributaria;
use TipoConvivienteFuerzaActividad;
use TipoCuentaBancaria;
use TipoCuota;
use TipoCuotaPaga;
use TipoCuotaRestante;
use TipoDerechoExpectativa;
use TipoDeuda;
use TipoDocumento;
use TipoFormaPago;
use TipoFormaPagoVehiculo;
use TipoFuente;
use TipoHijoConvive;
use TipoIngresoActividad;
use TipoIngresoExterno;
use TipoIngresoExtraordinario;
use TipoIngresoExtraordinarioExterno;
use TipoInmueble;
use TipoLlamadoPrestaServicio;
use TipoModoAdquisicion;
use TipoModoAdquisicionInmueble;
use TipoMoneda;
use TipoPersonalFuerza;
use TipoSexo;
use TipoSituacionRevista;
use TipoTarjetaCredito;
use TipoTitular;
use TipoTrabaja;
use TipoVehiculo;
use TipoVinculo;
use UnidadSuperficie;

class SelectOptionsRepo
{
    /**
     * @return array
     */
    public function getSelectOptions()
    {
        $empty = [0 => ''];
        $remember = Config::get('app.declaracion_jurada.cache.combos');

        $fuerzasConyuges = FuerzaConyuge::orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $fuerzas = Fuerza::remember($remember)->whereIn('id',[1,2,3,4])->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $estadosCiviles = EstadoCivil::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposCategoriaTributaria = TipoCategoriaTributaria::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $paises = Pais::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposFuente = TipoFuente::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
	    $tiposCaracterDeclaracion = TipoCaracterDeclaracion::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposVinculos = TipoVehiculo::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposSexo = TipoSexo::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposSituacionRevista = TipoSituacionRevista::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposInmueble = TipoInmueble::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposCaracter = TipoCaracter::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposModoAdquisicion = TipoModoAdquisicion::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposModoAdquisicionInmueble = TipoModoAdquisicionInmueble::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposFormaPago = TipoFormaPago::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposFormaPagoVehiculo = TipoFormaPagoVehiculo::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tipoTrabaja = TipoTrabaja::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tipoCuota = TipoCuota::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tipoDerechoExpectativa = TipoDerechoExpectativa::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tipoTitular = TipoTitular::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tipoIngresoExtraordinario = TipoIngresoExtraordinario::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tipoIngresoExtraordinarioExterno = TipoIngresoExtraordinarioExterno::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tipoIngresoExterno = TipoIngresoExterno::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tipoIngresoActividad = TipoIngresoActividad::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $provincias = Provincia::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tipoLlamadosPrestarServicio = TipoLlamadoPrestaServicio::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tipoPersonalFuerza = TipoPersonalFuerza::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $actosAdministrativos = TipoActoAdministrativo::remember($remember)->orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $tiposUnidadesSuperficie = UnidadSuperficie::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposDeuda = TipoDeuda::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposTarjetaCredito = TipoTarjetaCredito::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $entidadesEmisoraTc = EntidadEmisoraTC::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposMoneda = TipoMoneda::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $origenesInmueble = OrigenInmueble::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $origenesBien = OrigenBien::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $origenesVehiculos = OrigenVehiculo::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposBien = TipoBien::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposVinculo = TipoVinculo::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposVinculoconviviente = TipoVinculo::whereIn('id',[1,2])->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposVinculoPadreMadre = TipoVinculo::whereIn('id',[3,4])->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposVinculoConvivienteFuerza = TipoVinculo::orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposCuentaBancaria = TipoCuentaBancaria::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposBonos = TipoBono::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposConvivienteFuerzaActividades = TipoConvivienteFuerzaActividad::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposHijoConvive = TipoHijoConvive::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');
        $tiposDocumento = TipoDocumento::remember($remember)->orderBy('descripcion', 'ASC')->lists('descripcion', 'id');;

        $comboFuerzas = $empty + $fuerzas;
        $comboFuerzasConyuges = $empty + $fuerzasConyuges;
        $comboTipoInmueble = $empty + $tiposInmueble;
        $comboTiposCaracter = $empty + $tiposCaracter;
        $comboTiposModoAdquisicion = $empty + $tiposModoAdquisicion;
        $comboTiposModoAdquisicionInmueble = $empty + $tiposModoAdquisicionInmueble;
        $comboTiposFormaPago = $empty + $tiposFormaPago;
        $comboTiposFormaPagoVehiculo = $empty + $tiposFormaPagoVehiculo;
        $comboPaises = $empty + $paises;
        $comboTrabaja = $empty + $tipoTrabaja;
        $comboTipoCategoriaTributaria = $empty + $tiposCategoriaTributaria;
        $comboTiposFuentes = $empty + $tiposFuente;
        $comboTiposCuotas = $empty + $tipoCuota;
        $comboTipoDerechoExpectativa = $empty + $tipoDerechoExpectativa;
        $comboTipoTitular = $empty + $tipoTitular;
        $comboTipoIngresoExtraordinario = $empty + $tipoIngresoExtraordinario;
        $comboTipoIngresoExtraordinarioExterno = $empty + $tipoIngresoExtraordinarioExterno;
        $comboTipoIngresoExterno = $empty + $tipoIngresoExterno ;
        $comboTipoIngresoActividad = $empty + $tipoIngresoActividad;
        $comboProvincias = $empty + $provincias;
        $comboLlamadosPrestarServicio = $empty + $tipoLlamadosPrestarServicio;
        $comboPersonalFuerza = $empty + $tipoPersonalFuerza;
        $comboActosAdministrativos = $empty + $actosAdministrativos;
	    $comboCaracterDeclaracion = $tiposCaracterDeclaracion;
	    $comboTipoVinculo = $empty + $tiposVinculos;
        $comboTiposSexo = $empty + $tiposSexo;
        $comboEstadosCiviles = $empty + $estadosCiviles;
        $comboSituacionRevista = $empty + $tiposSituacionRevista;
        $comboOrigenes = $empty + $origenesInmueble;
        $comboUnidadesSuperficie = $empty + $tiposUnidadesSuperficie;
        $comboTiposDeuda = $empty + $tiposDeuda;
        $comboCuentasBancarias = $empty + $tiposCuentaBancaria;
        $comboTiposTarjetaCredito = $empty + $tiposTarjetaCredito;
        $comboEntidadesEmisoraTc = $empty + $entidadesEmisoraTc;
        $comboTiposMoneda = $empty + $tiposMoneda;
        $comboOrigenesInmueble = $empty + $origenesInmueble;
        $comboOrigenBien = $empty + $origenesBien;
        $comboOrigenesVehiculo = $empty + $origenesVehiculos;
        $comboTiposCuentaBancaria = $empty + $tiposCuentaBancaria;
        $comboTiposBien = $empty + $tiposBien;
        $comboTiposVinculo = $empty + $tiposVinculo;
        $comboTiposVinculoConviviente = $empty + $tiposVinculoconviviente;
        $comboTiposBonos = $empty + $tiposBonos;
        $comboTiposConvivienteFuerzaActividad = $empty + $tiposConvivienteFuerzaActividades;
        $comboTiposHijoConvive = $empty + $tiposHijoConvive;
        $comboTiposConvivienteFuerzaVinculo = $empty + $tiposVinculoConvivienteFuerza;
        $comboTiposFuerzasConviviente = $empty + $fuerzasConyuges;
        $comboTiposVinculoPadreMadre = $empty + $tiposVinculoPadreMadre;
        $comboTiposDocumento = $empty + $tiposDocumento;

        $datos = array(
            'combo_estado_civil' => $comboEstadosCiviles,
            'combo_provincias' => $comboProvincias,
            'combo_fuerzas' => $comboFuerzas,
            'combo_fuerzas_conyuges' => $comboFuerzasConyuges,
            'combo_origenes' => $comboOrigenes,
            'combo_unidades_superficie' => $comboUnidadesSuperficie,
            'combo_tipos_vehiculo' => $comboTipoVinculo,
            'combo_tipos_deuda' => $comboTiposDeuda,
            'combo_cuentas_bancarias' => $comboCuentasBancarias,
            'combo_tipos_tarjeta_credito' => $comboTiposTarjetaCredito,
            'combo_entidades_emisora_tc' => $comboEntidadesEmisoraTc,
            'combo_tipos_moneda' => $comboTiposMoneda,
            'combo_origenes_inmueble' => $comboOrigenesInmueble,
            'combo_origenes_bien' => $comboOrigenBien,
            'combo_origenes_vehiculo' => $comboOrigenesVehiculo,
            'combo_tipos_cuenta_bancaria' => $comboTiposCuentaBancaria,
            'combo_tipos_inmueble' => $comboTipoInmueble,
            'combo_tipos_bien' => $comboTiposBien,
            'combo_tipos_sexo' => $comboTiposSexo,
            'combo_tipos_situaciones_revista' => $comboSituacionRevista,
            'combo_tipos_vinculos' => $comboTiposVinculo,
            'combo_tipos_vinculos_conviviente' => $comboTiposVinculoConviviente,
            'combo_tipos_derecho_expectativa' => $comboTipoDerechoExpectativa,
            'combo_tipos_fuentes' => $comboTiposFuentes,
            'combo_tipos_cuotas' => $comboTiposCuotas,
            'combo_tipos_caracter' => $comboTiposCaracter,
            'combo_tipos_modos_adquisicion' => $comboTiposModoAdquisicion,
            'combo_tipos_modos_adquisicion_inmueble' => $comboTiposModoAdquisicionInmueble,
            'combo_tipos_formas_pago' => $comboTiposFormaPago,
            'combo_tipos_formas_pago_vehiculo' => $comboTiposFormaPagoVehiculo,
            'combo_paises' => $comboPaises,
            'combo_tipo_trabaja' => $comboTrabaja,
            'combo_tipo_titular' => $comboTipoTitular,
            'combo_tipos_categorias_tributarias' => $comboTipoCategoriaTributaria,
            'combo_tipos_ingresos_extraordinario' => $comboTipoIngresoExtraordinario,
            'combo_tipos_ingresos_extraordinario_externo' => $comboTipoIngresoExtraordinarioExterno,
            'combo_tipos_bonos' => $comboTiposBonos,
            'combo_tipos_ingresos_externos' => $comboTipoIngresoExterno,
            'combo_tipos_ingresos_actividades' => $comboTipoIngresoActividad,
            'combo_llamados_prestar_servicio' => $comboLlamadosPrestarServicio,
            'combo_personal_fuerza' => $comboPersonalFuerza,
            'combo_actos_administrativos' => $comboActosAdministrativos,            
	        'combo_caracter_declaracion' => $comboCaracterDeclaracion,
	        'combo_tipo_actividad' => $comboTiposConvivienteFuerzaActividad ,
	        'combo_hijo_convive' => $comboTiposHijoConvive ,
	        'combo_tipos_vinculos_fuerza' => $comboTiposConvivienteFuerzaVinculo ,
	        'combo_tipos_fuerza_conviviente' => $comboTiposFuerzasConviviente ,
	        'combo_tipos_vinculos_padre_madre' => $comboTiposVinculoPadreMadre ,
	        'combo_tipos_documentos' => $comboTiposDocumento ,
        );

        return $datos;
    }
}
