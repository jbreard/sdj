<?php namespace DeclaracionJurada\Repository;

use Ingreso;

class IngresoRepo extends BaseRepo
{

    public function getModel()
    {
        return new Ingreso();
    }

    public function newIngreso()
    {
        $ingreso = new Ingreso();
        return $ingreso;
    }

}