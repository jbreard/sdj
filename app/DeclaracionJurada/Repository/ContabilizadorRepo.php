<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 19/07/17
 * Time: 14:35
 */

namespace DeclaracionJurada\Repository;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ContabilizadorRepo extends BaseRepo
{

    private $cacheGraph;

    /**
     * ContabilizadorRepo constructor.
     * @param $cacheGraph
     */
    public function __construct()
    {
        parent::__construct();
        $this->cacheGraph = Config::get('app.declaracion_jurada.cache.graficos');
    }


    public function getModel()
    {
        return null;
    }

    public function getCountEliminadas() {

        $query = <<<QUERY
SELECT count(*) as 'count'
FROM declaraciones_juradas 
WHERE 
declaraciones_juradas.deleted_at  IS NOT NULL
QUERY;


        return Cache::remember('getCountEliminadas', $this->cacheGraph , function() use($query){
            return DB::select(DB::raw($query));
        });
    }


    public function getCountCreadas () {

        $query = <<<QUERY
SELECT count(*) as 'count'
FROM declaraciones_juradas 
WHERE 
declaraciones_juradas.deleted_at  is null
and declaraciones_juradas.received_at is null
and (
declaraciones_juradas.tipo_estado = 'Cargado'
or declaraciones_juradas.tipo_estado = 'Modificado'
)
QUERY;


        return Cache::remember('getCountCreadas', $this->cacheGraph , function() use($query){
            return DB::select(DB::raw($query));
        });
    }

    public function getCountAceptadas () {

        $query = <<<QUERY
SELECT count(*) as 'count'
FROM declaraciones_juradas 
WHERE 
declaraciones_juradas.deleted_at  is null
and declaraciones_juradas.accepted_at is not null
and declaraciones_juradas.received_at is null
and ( 
declaraciones_juradas.tipo_estado = 'Entregado en Dependencia' 
or declaraciones_juradas.tipo_estado = 'Entregado a RRHH'
)
QUERY;


        return Cache::remember('getCountAceptadas', $this->cacheGraph , function() use($query){
            return DB::select(DB::raw($query));
        });
    }

    public function getCountRecibidas () {

        $query = <<<QUERY
SELECT count(*) as 'count'
FROM declaraciones_juradas 
WHERE 
declaraciones_juradas.deleted_at  is null
and declaraciones_juradas.accepted_at is not null
and declaraciones_juradas.received_at is not null
and declaraciones_juradas.tipo_estado = 'Recibido'
QUERY;


        return Cache::remember('getCountRecibidas', $this->cacheGraph , function() use($query){
            return DB::select(DB::raw($query));
        });
    }

    public function getCountTotal () {

        $query = <<<QUERY
SELECT count(*) as 'count' FROM declaraciones_juradas
QUERY;


        return Cache::remember('getCountTotal', $this->cacheGraph , function() use($query){
            return DB::select(DB::raw($query));
        });
    }

    public function getCountUbicacionByIdFuerza($idFuerza) {
        $result = [
            'keys' => '',
            'counts' => '',
            'total' => 0,
        ];

        $query = <<<QUERY
SELECT
provincias.descripcion as 'key'
,count(*) as 'count'

FROM
personas
INNER JOIN declaraciones_juradas on declaraciones_juradas.id_persona = personas.id
INNER JOIN provincias on provincias.id = personas.id_provincia
INNER JOIN fuerzas on fuerzas.id = personas.id_fuerza

where personas.id_fuerza = {$idFuerza}

GROUP BY personas.id_provincia
ORDER BY provincias.descripcion ASC
QUERY;

        $remember = Cache::remember('getCountUbicacionByIdFuerza' . $idFuerza, $this->cacheGraph, function () use ($query) {
            return json_decode(json_encode(DB::select(DB::raw($query))), true);
        });


        array_map(function($row) use(&$result){
            $result['keys'] = $result['keys'] . ',"' . $row['key'] .'"';
            $result['counts'] = $result['counts'] . "," .$row['count'];
            $result['total'] = $result['total'] + (integer) $row['count'];
        }, $remember);

        return $result;
    }

    public function getCountInmueblesByIdFuerza($idFuerza)
    {
        $result = [
            'keys' => '',
            'total' => 0,
        ];

        $query = <<<QUERY
SELECT
provincias.descripcion as 'key'
,tipos_inmueble.descripcion as 'label'
,count(*) as 'count'

FROM
inmuebles
INNER JOIN personas on personas.id = inmuebles.id_persona
INNER JOIN declaraciones_juradas on declaraciones_juradas.id_persona = personas.id
INNER JOIN fuerzas on fuerzas.id = personas.id_fuerza
INNER JOIN provincias on provincias.id = inmuebles.id_provincia
INNER JOIN tipos_inmueble on tipos_inmueble.id = inmuebles.id_tipo_inmueble

WHERE personas.id_fuerza = {$idFuerza}
AND inmuebles.deleted_at IS NULL

GROUP BY inmuebles.id_provincia, inmuebles.id_tipo_inmueble
QUERY;
        $remember = Cache::remember('getCountInmueblesByIdFuerza' . $idFuerza, $this->cacheGraph, function () use ($query) {
            return json_decode(json_encode(DB::select(DB::raw($query))), true);
        });

        $anterior = '';
        array_map(function($row) use(&$result, &$anterior){
            $actual = $row['key'];

            if($actual<>$anterior) $result['keys'] .= '"' . $row['key'] .'",';

            $result['labels'][$row['key']]['label'][] = $row['label'];
            $result['labels'][$row['key']]['counts'][] = $row['count'];
            $result['total'] += (int) $row['count'];

            $anterior = $actual;

        }, $remember);

        return $result;

    }


}