<?php namespace DeclaracionJurada\Repository;

use Persona;

class PersonaRepo extends BaseRepo
{

    public function getModel()
    {
        return new Persona();
    }

    public function newPersona()
    {
        $persona = new Persona();
        return $persona;
    }

}