<?php namespace DeclaracionJurada\Entities\Historico;
class Bien extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',
        'id_tipo_bien',
        'id_tipo_caracter',
        'id_tipo_adquisicion',
        'id_origen',
        'descripcion',
        'monto',
        'tipo_persona',
        'otro_origen',
        'otro_tipo_bien',
    );
    protected $table = 'bienes';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }
    public function tipoCaracter()
    {
        return $this->hasOne('TipoCaracter', 'id', 'id_tipo_caracter');
    }
    public function tipoAdquisicion()
    {
        return $this->hasOne('TipoModoAdquisicion', 'id', 'id_tipo_adquisicion');
    }

    public function origen()
    {
        return $this->hasOne('OrigenBien', 'id', 'id_origen');
    }

    public function tipoBien()
    {
        return $this->hasOne('TipoBien', 'id', 'id_tipo_bien');
    }
}