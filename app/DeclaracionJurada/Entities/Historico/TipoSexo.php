<?php namespace DeclaracionJurada\Entities\Historico;
class TipoSexo extends \Eloquent {
    protected $table = 'tipos_sexo';
    protected $fillable = array('id','descripcion');
} 