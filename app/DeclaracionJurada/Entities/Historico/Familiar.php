<?php namespace DeclaracionJurada\Entities\Historico;
class Familiar extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_persona',
        'id_familiar',
        'tipo',
    );

    protected $table = 'familiares';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_familiar');
    }

}