<?php namespace DeclaracionJurada\Entities\Historico;

use Eloquent;

class DeclaracionJurada extends Eloquent
{

    protected $fillable = array(
        'id',
        'id_persona',
        'numero_anio',
        'fecha_acto_administrativo',
        'tipo_estado',
        'periodo_fiscal',
        'deleted_at',
        'accepted_at',
        'received_at',
        'version',
    );
    protected $table = 'declaraciones_juradas';

    public function persona()
    {
        return $this->hasOne('DeclaracionJurada\Entities\Historico\Persona', 'id', 'id_persona');
    }

    public function cuentaBancaria()
    {
        return $this->hasMany('DeclaracionJurada\Entities\Historico\CuentaBancaria', 'id_declaracion_jurada', 'id');
    }

    public function ingresoExtraordinario()
    {
        return $this->hasMany('DeclaracionJurada\Entities\Historico\IngresoExtraordinario', 'id_declaracion_jurada', 'id');
    }

    public function cuentaBancariaExterior()
    {
        return $this->hasMany('DeclaracionJurada\Entities\Historico\CuentaBancariaExterior', 'id_declaracion_jurada', 'id');
    }    

    public function deuda()
    {
        return $this->hasMany('DeclaracionJurada\Entities\Historico\Deuda', 'id_declaracion_jurada', 'id');
    }

    public function ingreso()
    {
        return $this->hasMany('DeclaracionJurada\Entities\Historico\Ingreso', 'id_declaracion_jurada', 'id');
    }

    public function ingresoExterno()
    {
        return $this->hasMany('DeclaracionJurada\Entities\Historico\IngresoExterno', 'id_declaracion_jurada', 'id');
    }

    public function tarjetaCredito()
    {
        return $this->hasMany('DeclaracionJurada\Entities\Historico\TarjetaCredito', 'id_declaracion_jurada', 'id');
    }

    public function inmueble()
    {
        return $this->hasMany('DeclaracionJurada\Entities\Historico\Inmueble', 'id_declaracion_jurada', 'id');
    }

    public function vehiculo()
    {
        return $this->hasMany('DeclaracionJurada\Entities\Historico\Vehiculo', 'id_declaracion_jurada', 'id');
    }

    public function bien()
    {
        return $this->hasMany('DeclaracionJurada\Entities\Historico\Bien', 'id_declaracion_jurada', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

    public function getDeletedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

    public function getAcceptedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

    public function getReceivedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

}