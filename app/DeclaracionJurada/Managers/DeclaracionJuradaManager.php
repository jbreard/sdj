<?php namespace DeclaracionJurada\Managers;

use Illuminate\Support\Facades\Config;

class DeclaracionJuradaManager extends BaseManager
{
    protected $estadosNotEdit = array();

    public function __construct($entity, $data)
    {
        parent::__construct($entity, $data);

        $this->estadosNotEdit = array_except(Config::get('app.declaracion_jurada.estados'),[0,1]);
    }

    public function getRules()
    {
        return array(
            'recaptcha_response_field' => 'required|recaptcha',
        );
    }

    public function getErrorMessages()
    {
        return array(
            'recaptcha_response_field.required' => '<label class="error">' . Config::get('app.textos.validaciones.recaptcha_response_field_required') . '</label>',
            'recaptcha_response_field.recaptcha' => '<label class="error">' . Config::get('app.textos.validaciones.recaptcha_response_field_recaptcha') . '</label>',
        );
    }

    public function canEditByTipoEstado()
    {
        return in_array( $this->data['tipo_estado'], $this->estadosNotEdit );
    }

}