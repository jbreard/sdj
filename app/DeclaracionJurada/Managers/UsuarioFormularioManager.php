<?php namespace DeclaracionJurada\Managers;


use Illuminate\Support\Facades\Config;


class UsuarioFormularioManager extends BaseManager
{
    public function getRules()
    {
        return array(
            'nombre' => 'required',
            'apellido' => 'required',
            'tipo' => 'required',
            'legajo' => 'required',
            'grado' => 'required',
            'dni' => 'required',
            'id_fuerza' => 'required|not_in:0',
        );
    }

    public function getErrorMessages()
    {
        return array(
            'nombre.required' => '<label class="error">' . Config::get('app.textos.validaciones.nombres') . '</label>' ,
            'apellido.required' => '<label class="error">' . Config::get('app.textos.validaciones.apellido') . '</label>'  ,
            'nacimiento.required' => '<label class="error">' . Config::get('app.textos.validaciones.nacimiento') . '</label>' ,
            'tipo.required' => '<label class="error">' . Config::get('app.textos.validaciones.campo_requerido') . '</label>' ,
            'dni.required' => '<label class="error">' . Config::get('app.textos.validaciones.documento') . '</label>'  ,
            'id_fuerza.required' => '<label class="error">' . Config::get('app.textos.validaciones.fuerza_perteneciente') . '</label>'  ,
            'id_fuerza.not_in' => '<label class="error">' . Config::get('app.textos.validaciones.fuerza_perteneciente') . '</label>'  ,
        );
    }

    public function backup()
    {
        return $this->entity->toJson();
    }

}