<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 19/04/17
 * Time: 16:19
 */

namespace DeclaracionJurada\Managers;


class EntityManager
{
    protected $data;

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * EntityManager constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function createEachEntity($rows, &$model, $noEmptyField, $tipoPersona = null, $personas = null)
    {
        if (is_array($rows)) 
        {
            
            if (!empty($tipoPersona)) {
                
                $this->createEntityForPerson($rows, $model, $noEmptyField, $tipoPersona, $personas);
                
            } else {
                
                foreach ($rows as $row) {
                    
                    if (!empty($row[$noEmptyField])) 
                    {
                        
                        $row = array_merge($row, $this->getData());
                        
                        $model->create($row);
                        
                    }
                    
                }
            }
        }
        
    }

    private function createEntityForPerson($rows, &$model, $noEmptyField, $tipoPersona, $personas)
    {
        $i = 0;
        $idPersonaAnterior = 0;
        
        foreach ($rows as $row) {
            
            if (!empty($row[$noEmptyField])) {
                
                if (is_array($personas)) 
                {

                    if (array_key_exists($i, $personas)) {
                        
                        $idPersona = $personas[$i];
                        
                    } else {
                        
                        $idPersona = $idPersonaAnterior;
                        
                    }

                    $idDeclaracionJurada = array_get($this->getData(), 'id_declaracion_jurada');
                    
                    $data = [
                        'id_persona' => $idPersona,
                        'id_declaracion_jurada' => $idDeclaracionJurada,
                        'tipo_persona' => $tipoPersona
                    ];
                    
                    $idPersonaAnterior = $idPersona;
                    
                } else {
                    
                    $data = array_merge($this->getData(), ['tipo_persona' => $tipoPersona]);
                    
                }

                $row = array_merge($row, $data);
                
                $model->create($row);
                
                $i++;
            }
            
        }
        
    }

}