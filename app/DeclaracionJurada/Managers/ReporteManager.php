<?php namespace DeclaracionJurada\Managers;

use Queue;

class ReporteManager extends BaseManager
{
    protected $excelManager;

    public function __construct($entity, $data)
    {
        parent::__construct($entity, $data);       
        
    }

    public function getRules()
    {
        return array();
    }

    public function getErrorMessages()
    {
        return array();
    }

    public function getReport($data, $pathSave)
    {
        Queue::push('DeclaracionJurada\Queues\ExcelQueue@create',['data'=>$data,'pathSave'=>$pathSave]);
        
    }
}