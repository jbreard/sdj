<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 23/11/16
 * Time: 11:35
 */

namespace DeclaracionJurada\Managers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use PHPExcel;
use PHPExcel_Writer_Excel2007;
use PHPExcel_Writer_Exception;

class ExcelManager
{
    /**
     * ExcelRepo constructor.
     */
    public function __construct()
    {
    }

    public function create($data, $pathSave)
    {
        Log::info('asdasdasd');
        $columnExcept = ['id'];
        $objPHPExcel = new PHPExcel();
        $columnTransformer = Config::get('app.declaracion_jurada.order.reportes.excel');

        $columns = array_keys($data[0]);

        try {

            // Set properties
            $objPHPExcel->getProperties()->setCreator("Ministerio de Seguridad");
            $objPHPExcel->getProperties()->setLastModifiedBy("Ministerio de Seguridad");
            $objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
            $objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
            $objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");

            // Add some data
            $objPHPExcel->setActiveSheetIndex(0);

            $rowIndex = 1;
            foreach ($columns as $index=>$value)
            {
                if(!in_array($value, $columnExcept))
                {
                    $column = $columnTransformer[$index]['order'] - 1;
                    $description = $columnTransformer[$index]['description'];
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $rowIndex , $description);
                }
            }

            foreach ($data as $row => $values)
            {
                $rowIndex++;
                $values = array_except($values,$columnExcept);

                foreach ($values as $columnName => $value)
                {
                    $index = array_search($columnName,$columns);
                    $column = $columnTransformer[$index]['order'] - 1;
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $rowIndex , $value);
                }
            }


            // Rename sheet
            $objPHPExcel->getActiveSheet()->setTitle('Simple');

            // Save Excel 2007 file
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

            $objWriter->save($pathSave);

            $this->notificateExcelCreation($pathSave);

        } catch (PHPExcel_Writer_Exception $exception) {

            Log::info("Error Export To Excel:");
            Log::info($exception->getMessage());

        }

    }

    /**
     * @param $pathSave
     */
    public function notificateExcelCreation($pathSave)
    {

        $email = "david.colman@minseg.gob.ar";        
        $linkToView = link_to_route('formulario.declaraciones_juradas.export_pdf', 'ver aqui');
        $contenido = View::make('mails.excel_creation', compact('linkToView'))->render();
        $destinatarios = [
            [
                'mail' => $email
            ]
        ];
        
        
        
        Queue::push('DeclaracionJurada\Queues\MailQueue@send', [
            'data' => [
                'content' => $contenido,
                'subject' => 'Declaración Jurada',
                'sender' => 'Ministerio de Seguridad',
                'mails' => $destinatarios,
            ],
            'pathSave' => $pathSave
        ]);
    }

}