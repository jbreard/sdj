<?php
/**
 * Created by PhpStorm.
 * User: dc
 * Date: 03/07/17
 * Time: 21:14
 */

namespace DeclaracionJurada\Managers;

use DeclaracionJurada\Transformers\DeclaracionJuradaTransformer;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;

class MailEnvioMasivo
{
    static function sendMailMasivo()
    {
        $notificationManager = new NotificationManager();
        $mails = Config::get('mailusers.users');
        $i = 0;
        foreach($mails as $user) {
            $i++;

            $mail = $user[0];
            $usuario = $user[1];//usuario


            $validator = Validator::make(array(
                'email'=>$mail
            ), array(
                'email' => 'email',
            ));

            if(!$validator->fails()) {
                Log::info("#{$i} : p=".$mail." u=".$usuario." m=".$user[0]);

                $contenido = View::make('mails.envio_masivo_new_users', array('usuario'=>$usuario,'mail'=>$user[2]))->render();

                $notificationManager->notificate([
                    'content' => $contenido,
                    'subject' => 'Ministerio de Seguridad - Usuario SDJ',
                    'sender' => '',
                    'mails' => [
                        [
                            'mail' => $mail
                        ]
                    ],
                ]);
            } else {
                Log::info($mail);
                Log::info($validator->messages()->toArray());
            }

        }

    }

    static function sendMailMasivoQueue()
    {
        //$mails = Config::get('mailusers.users');

        $mails = array(
        'users' => array(
            ['colmanrodrigo@gmail.com.ar','INICIO',0],
            ['david.colman@minseg.gob.ar','INICIO',0],
        ));

        foreach($mails as $user) {

            Queue::push('DeclaracionJurada\Queues\MailQueue@send', [
                'mail'=>$user[0],//mail
                'u'=> $user[1],//usuario
                'p'=>$user[2],//password
            ]);

        }

    }

    public function sendMailUser($mail,$usuario,$password='')
    {
        $notificationManager = new NotificationManager();


        Log::info($mail." ".$usuario);

        if(empty($password)) $password = $mail;

        $contenido = View::make('mails.envio_masivo_new_users', array('usuario'=>$usuario,'mail'=>$password))->render();

        $notificationManager->notificate([
            'content' => $contenido,
            'subject' => 'Ministerio de Seguridad - Usuario SDJ',
            'sender' => '',
            'mails' => [
                [
                    'mail' => $mail
                ]
            ],
        ]);
    }

    public static function sendMailDeclaracionJurada($email, $idDeclaracionJurada, $encrypt = true)
    {

        $validator = Validator::make(array(
            'email'=>$email
        ), array(
            'email' => 'email',
        ));

        if(!$validator->fails()) {

            if(Config::get('app.debug')) $email = 'ddjjfuerzas@minseg.gob.ar';

            $url = Config::get('app.url');

            if($encrypt){
                $declaracionJuradaTransformer = new DeclaracionJuradaTransformer();
                $hash = $declaracionJuradaTransformer->encrypt($idDeclaracionJurada);
            } else {
                $hash = $idDeclaracionJurada;
            }

            $linkToEdit = <<<HTML
<a href="{$url}/formulario/{$hash}/edit" target="_blank">editar aqui</a>
HTML;

            $linkToView = <<<HTML
<a href="{$url}/formulario/{$hash}/export_pdf" target="_blank">ver aqui</a>
HTML;

            $contenido = View::make('mails.contenido', compact('linkToEdit', 'linkToView'))->render();

            $destinatarios = [
                [
                    'mail' => $email
                ]
            ];

            $notifactionManager = new NotificationManager();

            $notifactionManager->notificate([
                'content' => $contenido,
                'subject' => 'Declaración Jurada',
                'sender' => 'Ministerio de Seguridad',
                'mails' => $destinatarios,
            ]);

        } else {
            Log::info($email);
            Log::info($validator->messages()->toArray());
        }

    }   


    public static function sendMailMasivoDeclaraciones()
    {
        $mails = Config::get('mailusers.users');

        foreach($mails as $user) {
            Log::info("iddeclaracion: " . $user[1]);
            self::sendMailDeclaracionJurada($user[0], $user[1]);
        }

    }

}
