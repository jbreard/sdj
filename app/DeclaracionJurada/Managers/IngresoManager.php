<?php namespace DeclaracionJurada\Managers;

class IngresoManager extends BaseManager
{
    public function getRules()
    {
        return array(
            'monto_anual_aproximado' => 'required',
        );
    }

    public function getErrorMessages()
    {
        return array(
            'monto_anual_aproximado.required' => '<label class="error">Completar monto anual aproximado</label>' ,
        );
    }
}