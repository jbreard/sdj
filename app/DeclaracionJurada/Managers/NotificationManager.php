<?php namespace DeclaracionJurada\Managers;

use DeclaracionJurada\Clases\Mensajes;
use Illuminate\Support\Facades\Log;
use Swift_TransportException;

class NotificationManager
{
    private $mensaje;

    public function __construct()
    {
        $this->mensaje = new Mensajes();
    }

    public function notificate($data = [], $method = 'sendMail')
    {
        call_user_func([$this,$method],$data);
    }

    private function sendMail($data)
    {
        try {
            $this->mensaje->sendEmail([
                'data' => [
                    'contenido' => $data['content'],
                    'asunto' => $data['subject'],
                ],
                'remitente' => $data['sender'],
                'mails' => $data['mails'],
            ]);

            #Log::info("SEND MAIL OK: ");
            Log::info($data['mails']);

        } catch (Swift_TransportException $exception) {
            Log::info("Error: Swift_TransportException");
            Log::info($exception->getMessage());
            Log::info("mails: " . $data['mails']);
        }

    }
}
