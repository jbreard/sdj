<?php namespace DeclaracionJurada\Managers;

use DeclaracionJurada\Repository\DeclaracionJuradaRepo;
use DeclaracionJurada\Repository\FamiliarRepo;
use DeclaracionJurada\Repository\PersonaRepo;
use Familiar;
use Illuminate\Support\Facades\Config;
use Persona;

class PersonaManager extends BaseManager
{
    public function getRules()
    {
        return array(
            'nombres' => 'required',
            'apellido' => 'required',
            'id_estado_civil' => 'required',
            'id_provincia' => 'required',
            'id_localidad' => 'required',
            'id_tipo_personal' => 'required',
            'nacimiento' => 'required',
            'documento' => 'required',
            'telefono' => 'required',
            'id_fuerza' => 'required|not_in:0',
            'email' => 'required|email'
        );
    }

    public function getErrorMessages()
    {
        return array(
            'nombres.required' => '<label class="error">' . Config::get('app.textos.validaciones.nombre') . '</label>' ,
            'apellido.required' => '<label class="error">' . Config::get('app.textos.validaciones.apellido') . '</label>'  ,
            'nacimiento.required' => '<label class="error">' . Config::get('app.textos.validaciones.nacimiento') . '</label>' ,
            'documento.required' => '<label class="error">' . Config::get('app.textos.validaciones.documento') . '</label>'  ,
            'id_estado_civil.required' => '<label class="error">' . Config::get('app.textos.validaciones.id_estado_civil') . '</label>'  ,
            'id_provincia.required' => '<label class="error">' . Config::get('app.textos.validaciones.id_provincia') . '</label>'  ,
            'id_localidad.required' => '<label class="error">' . Config::get('app.textos.validaciones.id_localidad') . '</label>'  ,
            'id_tipo_personal.required' => '<label class="error">' . Config::get('app.textos.validaciones.id_tipo_personal') . '</label>'  ,
            'id_fuerza.required' => '<label class="error">' . Config::get('app.textos.validaciones.fuerza_perteneciente') . '</label>'  ,
            'id_fuerza.not_in' => '<label class="error">' . Config::get('app.textos.validaciones.fuerza_perteneciente') . '</label>'  ,
            'telefono.required' =>'<label class="error">' . Config::get('app.textos.validaciones.telefono') . '</label>',
            'email.required' => '<label class="error">' . Config::get('app.textos.validaciones.email') . '</label>',
            'email.email' => '<label class="error">' . Config::get('app.textos.validaciones.email_no_valido') . '</label>'
        );
    }


    public function createFamiliares($idPersona, $key)
    {
        $declaracionJuradaRepo = new DeclaracionJuradaRepo();

        $persona = $this->entity;

        $familiarRepo = new FamiliarRepo();
        $familiar = $familiarRepo->newFamiliar();       

        $datosConyuge = $declaracionJuradaRepo->getDataInRedis($key, 'datos_conyuge');
        $datosHijos = $declaracionJuradaRepo->getDataInRedis($key, 'datos_hijos');
        $datosOtrasPersonas =$declaracionJuradaRepo->getDataInRedis($key, 'datos_otras_personas');

        return [
          'conyuges' => $this->createConyuges($persona, $familiar, $idPersona, $datosConyuge),
          'hijos' => $this->createHijos($persona, $familiar, $idPersona, $datosHijos),
          'otras_personas' => $this->createOtrasPersonas($persona, $familiar, $idPersona, $datosOtrasPersonas),
        ];
    }

    private function createEachFamiliar(&$persona, &$familiar, $idPersona, $rows, $noEmptyField, $tipoFamiliar)
    {
        $personas = [];
        if (is_array($rows)) {
            $esFamiliar = ['es_familiar' => 1];

            foreach ($rows as $row) {
                $fieldNotEmpty = !empty($row[$noEmptyField]);
                if ($fieldNotEmpty) {
                    $row = array_merge($row, $esFamiliar);
                    $newPersona = $persona->create($row);
                    $idFamiliar = $newPersona->id;
                    $familiar->create([
                        'id_persona' => $idPersona,
                        'id_familiar' => $idFamiliar,
                        'tipo' => $tipoFamiliar
                    ]);
                    array_push($personas, $idFamiliar);
                }
            }
        }

        return $personas;
    }

    private function createConyuges(&$persona, &$familiar, $idPersona, $data)
    {
        return $this->createEachFamiliar($persona, $familiar, $idPersona, $data, 'apellido', 1);
    }

    private function createHijos(&$persona, &$familiar, $idPersona, $data)
    {
        return $this->createEachFamiliar($persona, $familiar, $idPersona, $data, 'apellido', 2);
    }

    private function createOtrasPersonas(&$persona, &$familiar, $idPersona, $data)
    {
        return $this->createEachFamiliar($persona, $familiar, $idPersona, $data, 'apellido', 3);
    }

    public function isEqualToFakeField()
    {
        $errors = array();

        $nombre = (string) array_get($this->getData(),'nombres');
        $nombreFake = (string) array_get($this->getData(),'nombres_fake');

        $equalNombre = ($nombre === $nombreFake);

        if($equalNombre == false) $errors[0]['nombres'] = Config::get('app.textos.validaciones.nombres_fake');

        
        $apellido = (string) array_get($this->getData(),'apellido');
        $apellidoFake = (string) array_get($this->getData(),'apellido_fake');

        $equalApellido = ($apellido === $apellidoFake);

        if($equalApellido == false) $errors[0]['apellido'] = Config::get('app.textos.validaciones.apellido_fake');

        
        $documento = (string) array_get($this->getData(),'documento');
        $documentoFake = (string) array_get($this->getData(),'documento_fake');
        
        $equalDocumento = ($documento === $documentoFake);

        if($equalDocumento == false) $errors[0]['documento'] = Config::get('app.textos.validaciones.documento_fake');

        if(!empty($errors)) {
            $this->setErrors($errors);
            
            //Log para rastrear estos errores
            \Log::info($this->getData());
            
            return false;
        } else {
            return true;
        }
    }

    private function getData()
    {
        return $this->data;
    }

}