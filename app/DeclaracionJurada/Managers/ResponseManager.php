<?php namespace DeclaracionJurada\Managers;

class ResponseManager
{
    public function responseWithErrors($data = [], $messages = '')
    {
        return [
            'data'=>$data,
            'error'=>true,
            'messages'=>$messages
        ];
    }

    public function responseOk($data = [], $messages = '')
    {
        return [
            'data' => $data,
            'error'=>false,
            'messages'=>$messages
        ];
    }
}