<?php namespace DeclaracionJurada\Managers;

use Illuminate\Support\Facades\Validator;

abstract class BaseManager {

    protected $entity;
    protected $data;
    protected $errorMessages;
    protected $errors = [];

    public function __construct($entity, $data)
    {
        $this->entity = $entity;
        $this->data   = $data;
    }

    abstract public function getRules();
    abstract public function getErrorMessages();

    public function isValid()
    {
        $validator   = Validator::make($this->data, $this->getRules(),$this->getErrorMessages());
        $this->setErrors($validator->messages()->toArray());
        return $validator->passes();
    }

    public function save()
    {
        $this->entity->fill($this->data);
        $this->entity->save();
        return $this->entity->id;
    }

    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function setErrors($errors)
    {
        $this->errors = $errors;
    }


} 