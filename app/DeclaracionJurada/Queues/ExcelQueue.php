<?php namespace DeclaracionJurada\Queues;

use DeclaracionJurada\Managers\ExcelManager;
use Illuminate\Support\Facades\Log;

/**
 * Created by PhpStorm.
 * User: dc
 * Date: 25/11/16
 * Time: 17:04
 */
class ExcelQueue
{
    protected $excelManager;

    public function __construct()
    {
        $this->excelManager = new ExcelManager();
    }

    public function create($job, $data)
    {

        $pathSave = $data['pathSave'];
        $data = $data['data'];

        Log::info($pathSave);
        
        $this->excelManager->create($data, $pathSave);

        $job->delete();

    }

}