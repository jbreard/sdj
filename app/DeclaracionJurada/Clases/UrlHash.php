<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 02/08/16
 * Time: 16:49
 */

namespace DeclaracionJurada\Clases;


class UrlHash {

    public function encriptar($cadena){
        $key='2017';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
        $encrypted =str_replace(array('+', '/'), array('-', '_'), base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key)))));
        return $encrypted; //Devuelve el string encriptado

    }

    public  function desencriptar($cadena){
        $cadena = base64_decode(str_replace(array('-', '_'), array('+', '/'), $cadena));
        $key='2017';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))), "\0");
	//\Log::info($decrypted);

        return $decrypted;  //Devuelve el string desencriptado

    }


} 
