<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 20/04/15
 * Time: 15:27
 */

namespace DeclaracionJurada\Clases;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Swift_TransportException;

class Mensajes {

    public function sendEmail($datos)
    {
        if(Config::get('app.modulos.mensajes'))
        {
            $headerOptions = Config::get('mail.header_options');
            $inputData = $datos['data'];
            $remitente = $datos['remitente'];
            $destinatarios  = $datos['mails'];
            $asunto = $inputData['asunto'];
            $dataMail = [
                "contenido" => $inputData["contenido"],
                "remitente" => $remitente
            ];
            
            try {
                Mail::send('mails.cuerpo',
                    $dataMail,
                    function ($message) use ($destinatarios, $asunto, $headerOptions) {
                        $message->getheaders()->addTextHeader("Disposition-Notification-To", $headerOptions["Disposition-Notification-To"]);
                        $message->getheaders()->addTextHeader("X-Confirm-Reading-To", $headerOptions["X-Confirm-Reading-To"]);

                        if(!empty($destinatarios) && is_array($destinatarios))
                        {
                            foreach ($destinatarios as $destinatario) {
                                $email = $destinatario['mail'];
                                if (!empty($email)) $message->to($email);
                            }
                        }

                        $message->subject($asunto);
                    }
                );
            } catch (Swift_TransportException $exception) {
                Log::info("Error: Swift_TransportException - sendmailprocesss");
                Log::info($exception->getMessage());
                Log::info($destinatarios);
            }           

        }
    }

}


