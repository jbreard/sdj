<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration {

    protected $table = 'usuarios';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table))
        {

            Schema::create($this->table, function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('nombre',25);
                $table->string('apellido',25);
                $table->string('email',50);
                $table->string('password');
                $table->integer('dni');
                $table->integer('id_fuerza')->unsigned()->default(0);
                $table->integer('id_perfil')->unsigned();
                $table->string('username',25);
                $table->string('remember_token',100);
                $table->string('telefono');
                $table->string('contacto_laboral');
                $table->string('area');
                $table->tinyInteger('activo')->unsigned()->default(1);

             //   $table->foreign("id_perfil")->references("id")->on("perfiles")->onDelete('cascade');

                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable($this->table))
        {
            Schema::drop($this->table);
        }
    }

}
