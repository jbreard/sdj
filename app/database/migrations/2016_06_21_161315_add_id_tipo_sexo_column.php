<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdTipoSexoColumn extends Migration {

	protected $table = "declaraciones_juradas";

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable($this->table))
		{

			Schema::create($this->table, function($table)
			{
				$table->integer('id_tipo_sexo')->unsigned();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (!Schema::hasTable($this->table)) {
			if (Schema::hasColumn($this->table, 'id_tipo_sexo')) {
				Schema::table($this->table, function ($table) {
					$table->dropColumn('id_tipo_sexo');
				});
			}
		}
	}

}
