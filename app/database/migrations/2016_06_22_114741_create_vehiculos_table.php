<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculosTable extends Migration {

	protected $table = "vehiculos";

	public function up()
	{
		if (!Schema::hasTable($this->table))
		{

			Schema::create($this->table, function($table)
			{
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('id_tipo_vehiculo')->unsigned();
				$table->integer('id_origen')->unsigned();
				$table->integer('id_persona')->unsigned();
				$table->integer('id_declaracion_jurada')->unsigned();
				$table->string('marca');
				$table->string('modelo');
				$table->string('dominio');
				$table->string('anio');
				$table->string('valor_aproximado');
				$table->string('porcentaje');
				$table->string('otro_origen');
				$table->enum('tipo_persona', array('propio', 'conyuge', 'hijo'));
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable($this->table))
		{
			Schema::drop($this->table);
		}
	}

}
