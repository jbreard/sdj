<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamiliaresTable extends Migration {

	protected $table = "familiares";

	public function up()
	{
		if (!Schema::hasTable($this->table))
		{

			Schema::create($this->table, function($table)
			{
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('id_persona')->unsigned();
				$table->integer('id_familiar')->unsigned();
				$table->enum('tipo', array('conyuge', 'hijo', 'otra persona'));
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable($this->table))
		{
			Schema::drop($this->table);
		}
	}

}
