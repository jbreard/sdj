<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Config;

class CreateIngresosTable extends Migration {

	protected $table = "ingresos";

	public function up()
	{
		if (!Schema::hasTable($this->table))
		{

			Schema::create($this->table, function($table)
			{
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('id_persona')->unsigned();
				$table->integer('id_declaracion_jurada')->unsigned();
				$table->integer('id_tipo_ingreso')->unsigned();
				$table->integer('id_moneda')->unsigned();
				$table->integer('id_salario')->unsigned();
				$table->enum('tipo_ingreso', Config::get('app.declaracion_jurada.tipo_ingreso'))->dafault(1);
				$table->string('monto_anual_aproximado');
				$table->string('servicio_policia_adicional');
				$table->string('otros_ingresos');
				$table->string('otro_tipo_moneda');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable($this->table))
		{
			Schema::drop($this->table);
		}
	}

}
