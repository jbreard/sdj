<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropHijoDeColumn extends Migration {

	protected $table = "declaraciones_juradas";

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable($this->table)) {
			if (Schema::hasColumn($this->table, 'hijo_de')) {
				Schema::table($this->table, function ($table) {
					$table->dropColumn('hijo_de');
				});
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
