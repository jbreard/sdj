<?php

use Illuminate\Support\Facades\Config;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclaracionJuradaTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    
    protected $table = "declaraciones_juradas";
    
    public function up()
    {
        if (!Schema::hasTable($this->table))
        {

            Schema::create($this->table, function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('version')->unsigned()->default(1);
                $table->integer('id_persona')->unsigned();
                $table->enum('tipo_estado', Config::get('app.declaracion_jurada.estados'))->dafault(1);
                $table->timestamp('accepted_at')->default(null)->nullable();
                $table->timestamp('received_at')->default(null)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable($this->table))
        {
            Schema::drop($this->table);
        }
    }

}
