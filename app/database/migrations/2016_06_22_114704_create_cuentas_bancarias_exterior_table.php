<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasBancariasExteriorTable extends Migration {

	protected $table = "cuentas_bancarias_exterior";

	public function up()
	{
		if (!Schema::hasTable($this->table))
		{

			Schema::create($this->table, function($table)
			{
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('id_persona')->unsigned();
				$table->integer('id_declaracion_jurada')->unsigned();
				$table->string('banco');
				$table->string('tipo_banco');
				$table->string('numero_cuenta');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable($this->table))
		{
			Schema::drop($this->table);
		}
	}

}
