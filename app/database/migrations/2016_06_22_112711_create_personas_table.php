<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */

	protected $table = "personas";

	public function up()
	{
		if (!Schema::hasTable($this->table))
		{

			Schema::create($this->table, function($table)
			{
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->string('id_provincia');
				$table->integer('id_localidad')->unsigned();
				$table->integer('id_estado_civil')->unsigned();
				$table->integer('id_fuerza')->unsigned();
				$table->integer('id_tipo_personal')->unsigned();
				$table->integer('id_tipo_sexo')->unsigned();
				$table->string('nombres');
				$table->string('apellido');
				$table->date('nacimiento');
				$table->string('nacimiento_lugar');
				$table->string('documento');
				$table->string('cuil_cuit');
				$table->string('domicilio_real');
				$table->string('codigo_postal');
				$table->string('telefono');
				$table->string('email');
				$table->string('grado');
				$table->string('legajo_personal');
				$table->string('destino_revista');
				$table->string('ocupacion');
				$table->string('domicilio_laboral');
				$table->string('vinculo_parentesco');
				$table->boolean('es_familiar')->default(0);
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable($this->table))
		{
			Schema::drop($this->table);
		}
	}

}
