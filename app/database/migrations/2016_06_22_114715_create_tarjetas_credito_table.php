<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTarjetasCreditoTable extends Migration {

	protected $table = "tarjetas_credito";

	public function up()
	{
		if (!Schema::hasTable($this->table))
		{

			Schema::create($this->table, function($table)
			{
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('id_persona')->unsigned();
				$table->integer('id_entidad_emisora')->unsigned();
				$table->integer('id_declaracion_jurada')->unsigned();
				$table->integer('id_tipo_tarjeta_credito')->unsigned();
				$table->string('banco');
				$table->string('numero');
				$table->string('otra_entidad_emisora');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable($this->table))
		{
			Schema::drop($this->table);
		}
	}

}
