<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBienesTable extends Migration {

	protected $table = "bienes";

	public function up()
	{
		if (!Schema::hasTable($this->table))
		{

			Schema::create($this->table, function($table)
			{
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('id_persona')->unsigned();
				$table->integer('id_declaracion_jurada')->unsigned();
				$table->integer('id_tipo_bien')->unsigned();
				$table->integer('id_origen')->unsigned();
				$table->string('descripcion');
				$table->string('monto');
				$table->string('otro_origen');
				$table->string('otro_tipo_bien');
				$table->enum('tipo_persona', array('propio', 'conyuge', 'hijo'));
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable($this->table))
		{
			Schema::drop($this->table);
		}
	}

}
