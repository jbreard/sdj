<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInmueblesTable extends Migration {

	protected $table = "inmuebles";

	public function up()
	{
		if (!Schema::hasTable($this->table))
		{

			Schema::create($this->table, function($table)
			{
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->string('id_provincia');
				$table->integer('id_localidad')->unsigned();
				$table->integer('id_tipo_inmueble')->unsigned();
				$table->integer('id_unidad_superficie')->unsigned();
				$table->integer('id_origen')->unsigned();
				$table->integer('id_persona')->unsigned();
				$table->integer('id_declaracion_jurada')->unsigned();
				$table->string('domicilio_calle');
				$table->string('domicilio_numero');
				$table->string('superficie');
				$table->string('porcentaje');
				$table->string('otro_origen');
				$table->enum('tipo_persona', array('propio', 'conyuge', 'hijo'));
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable($this->table))
		{
			Schema::drop($this->table);
		}
	}

}
