<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeudasTable extends Migration {

	protected $table = "deudas";

	public function up()
	{
		if (!Schema::hasTable($this->table))
		{

			Schema::create($this->table, function($table)
			{
				$table->engine = 'InnoDB';
				$table->increments('id');
				$table->integer('id_persona')->unsigned();
				$table->integer('id_declaracion_jurada')->unsigned();
				$table->integer('id_tipo_deuda')->unsigned();
				$table->integer('id_tipo_moneda')->unsigned();
				$table->string('acreedor');
				$table->string('monto');
				$table->string('otro_tipo_moneda');
				$table->string('otro_tipo_deuda');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable($this->table))
		{
			Schema::drop($this->table);
		}
	}

}
