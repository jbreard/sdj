<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameNombreColumn extends Migration {

	protected $table = "declaraciones_juradas";

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable($this->table)) {
			if (Schema::hasColumn($this->table, 'nombre')) {
				Schema::table($this->table, function ($table) {
					$table->renameColumn('nombre', 'nombres');
				});
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
