<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropHijoDe2Column extends Migration {

	protected $table = "declaraciones_juradas";

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable($this->table)) {
			if (Schema::hasColumn($this->table, 'hijo_de_2')) {
				Schema::table($this->table, function ($table) {
					$table->dropColumn('hijo_de_2');
				});
			}
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
