<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposInmuebleTable extends Migration {

	protected $table = 'tipos_inmueble';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable($this->table)) {

			Schema::create($this->table, function($table) {
				$table -> engine = 'InnoDB';
				$table -> increments('id');
				$table -> string('descripcion');
				$table -> timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop($this->table);
	}

}
