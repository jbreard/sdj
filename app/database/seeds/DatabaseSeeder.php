<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();


        $this->call('UsuarioTableSeeder');
        $this->call('PerfilTableSeeder');
        $this->call('FuerzasTableSeeder');
        $this->call('EstadosCivilesTableSeeder');
        $this->call('OrigenesInmuebleTableSeeder');
        $this->call('OrigenesBienTableSeeder');
        $this->call('OrigenesVehiculoTableSeeder');
        $this->call('UnidadesSuperficieTableSeeder');
        $this->call('TiposVehiculoTableSeeder');
        $this->call('TiposDeudaTableSeeder');
        $this->call('TiposCuentaBancariaTableSeeder');
        $this->call('TiposTarjetaCreditoTableSeeder');
        $this->call('EntidadEmisoraTableSeeder');
        $this->call('TipoMonedaTableSeeder');
        $this->call('TipoInmuebleTableSeeder');
        $this->call('TipoBienTableSeeder');
        $this->call('TipoSexoTableSeeder');
        $this->call('TipoVinculoTableSeeder');
        $this->call('ProvinciasTableSeeder');
        $this->call('LocalidadesTableSeeder');


    }

}

class UsuarioTableSeeder extends Seeder {

    public function run()
    {
        $usuarios = [
            [
                "nombre" => "admin"
                ,"apellido"=>"admin"
                ,"email"=>"admin@admin.com"
                ,"dni"=>"12345678"
                ,"id_perfil"=>1
                ,"username"=>"admin"
                ,"password"=>Hash::make('123456')
            ],
            [
                "nombre" => "test"
                ,"apellido"=>"test"
                ,"email"=>"test@test.com"
                ,"dni"=>"22222222"
                ,"id_perfil"=>2
                ,"id_fuerza"=>1
                ,"username"=>"test"
                ,"password"=>Hash::make('123456')
            ],
            [
                "nombre" => "dependencia"
                ,"apellido"=>"dependencia"
                ,"email"=>"dependencia@dependencia.com"
                ,"dni"=>"33333333"
                ,"id_perfil"=>3
                ,"id_fuerza"=>1
                ,"username"=>"dependencia.pfa"
                ,"password"=>Hash::make('123456')
            ],
            [
                "nombre" => "rrhh"
                ,"apellido"=>"rrhh"
                ,"email"=>"rrhh@rrhh.com"
                ,"dni"=>"44444444"
                ,"id_perfil"=>4
                ,"id_fuerza"=>1
                ,"username"=>"rrhh.pfa"
                ,"password"=>Hash::make('123456')
            ],
            [
                "nombre" => "dependencia"
                ,"apellido"=>"dependencia"
                ,"email"=>"dependencia@dependencia.com"
                ,"dni"=>"33333333"
                ,"id_perfil"=>3
                ,"id_fuerza"=>2
                ,"username"=>"dependencia.gna"
                ,"password"=>Hash::make('123456')
            ],
            [
                "nombre" => "rrhh"
                ,"apellido"=>"rrhh"
                ,"email"=>"rrhh@rrhh.com"
                ,"dni"=>"44444444"
                ,"id_perfil"=>4
                ,"id_fuerza"=>2
                ,"username"=>"rrhh.gna"
                ,"password"=>Hash::make('123456')
            ],
            [
                "nombre" => "dependencia"
                ,"apellido"=>"dependencia"
                ,"email"=>"dependencia@dependencia.com"
                ,"dni"=>"33333333"
                ,"id_perfil"=>3
                ,"id_fuerza"=>3
                ,"username"=>"dependencia.pna"
                ,"password"=>Hash::make('123456')
            ],
            [
                "nombre" => "rrhh"
                ,"apellido"=>"rrhh"
                ,"email"=>"rrhh@rrhh.com"
                ,"dni"=>"44444444"
                ,"id_perfil"=>4
                ,"id_fuerza"=>3
                ,"username"=>"rrhh.pna"
                ,"password"=>Hash::make('123456')
            ],
            [
                "nombre" => "dependencia"
                ,"apellido"=>"dependencia"
                ,"email"=>"dependencia@dependencia.com"
                ,"dni"=>"33333333"
                ,"id_perfil"=>3
                ,"id_fuerza"=>4
                ,"username"=>"dependencia.psa"
                ,"password"=>Hash::make('123456')
            ],
            [
                "nombre" => "rrhh"
                ,"apellido"=>"rrhh"
                ,"email"=>"rrhh@rrhh.com"
                ,"dni"=>"44444444"
                ,"id_perfil"=>4
                ,"id_fuerza"=>4
                ,"username"=>"rrhh.psa"
                ,"password"=>Hash::make('123456')
            ],
            [
                "nombre" => "ministerio"
                ,"apellido"=>"ministerio"
                ,"email"=>"ministerio@ministerio.com"
                ,"dni"=>"55555555"
                ,"id_perfil"=>5
                ,"username"=>"ministerio"
                ,"password"=>Hash::make('123456')
            ],
        ];

        foreach($usuarios as $usuario){
            User::create($usuario);
        }

    }

}


class FuerzasTableSeeder extends Seeder {

    public function run()
    {
        $Fuerzas = [
            [ "descripcion" => "Policía Federal Argentina",],
            [ "descripcion" => "Gendarmería Nacional Argentina"],
            [ "descripcion" => "Prefectura Naval Argentina"],
            [ "descripcion" => "Policía de Seguridad Aeroportuaria"],
        ];

        foreach ($Fuerzas as $key => $value) {
            Fuerza::create($value);
        }

    }

}

class EstadosCivilesTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Soltero/a",],
            [ "descripcion" => "Casado/a"],
            [ "descripcion" => "Divorciado/a"],
            [ "descripcion" => "Viudo/a"],
            [ "descripcion" => "En concubinato"],
        ];

        foreach ($data as $key => $value) {
            EstadoCivil::create($value);
        }

    }

}


class PerfilTableSeeder extends Seeder {

    public function run()
    {
        Perfil::create(['descripcion' => 'administrador']);
        Perfil::create(['descripcion' => 'operador']);
        Perfil::create(['descripcion' => 'Dependencia']);
        Perfil::create(['descripcion' => 'RRHH']);
        Perfil::create(['descripcion' => 'Ministerio']);
    }

}

class ProvinciasTableSeeder extends Seeder
{

    public function run()
    {        

        $provincias = \Config::get('seeders.provincias');
        foreach ($provincias as $key => $value) {
            Provincia::create($value);
        }

    }
}

class LocalidadesTableSeeder extends Seeder
{

    public function run()
    {

        $localidades = \Config::get('seeders.localidades');
        foreach ($localidades as $key => $value) {
            Localidad::create($value);
        }

    }
}

class OrigenesInmuebleTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Propio"],
            [ "descripcion" => "Ganancial"],
            [ "descripcion" => "Herencia"],
            [ "descripcion" => "Donación"],
            [ "descripcion" => "Otro (especificar)"],
        ];

        foreach ($data as $key => $value) {
            OrigenInmueble::create($value);
        }

    }

}
class UnidadesSuperficieTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Metros"],
            [ "descripcion" => "Hectáreas"],
        ];

        foreach ($data as $key => $value) {
            UnidadSuperficie::create($value);
        }

    }

}
class TiposVehiculoTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Auto"],
            [ "descripcion" => "Camioneta"],
            [ "descripcion" => "Moto"],
            [ "descripcion" => "Cuatriciclo"],
            [ "descripcion" => "Motosky"],
            [ "descripcion" => "Autoportante"],
            [ "descripcion" => "Aeronaves"],
            [ "descripcion" => "Embarcaciones"]
        ];

        foreach ($data as $key => $value) {
            TipoVehiculo::create($value);
        }

    }

}
class TiposDeudaTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Bancarias"],
            [ "descripcion" => "Comerciales"],
            [ "descripcion" => "Tárjetas de crédito"],
            [ "descripcion" => "Otros (especificar)"],
        ];

        foreach ($data as $key => $value) {
            TipoDeuda::create($value);
        }

    }

}
class TiposCuentaBancariaTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Caja de Ahorro"], 
            [ "descripcion" => "Cuenta Corriente"], 
            [ "descripcion" => "Cuenta en dólares"], 
            [ "descripcion" => "Otro (especificar)"] 
        ];

        foreach ($data as $key => $value) {
            TipoCuentaBancaria::create($value);
        }

    }

}
class TiposTarjetaCreditoTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Propia"],
            [ "descripcion" => "Extensión"],
        ];

        foreach ($data as $key => $value) {
            TipoTarjetaCredito::create($value);
        }

    }

}
class EntidadEmisoraTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Mastercard"],
            [ "descripcion" => "Visa"],
            [ "descripcion" => "Cabal"],
            [ "descripcion" => "American Express"],
            [ "descripcion" => "Diners"],
            [ "descripcion" => "Otro (especificar)"]
        ];

        foreach ($data as $key => $value) {
            EntidadEmisoraTC::create($value);
        }

    }

}
class TipoMonedaTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Pesos"],
            [ "descripcion" => "Dolares"],
            [ "descripcion" => "Euros"],
            [ "descripcion" => "Otro (especificar)"]
        ];

        foreach ($data as $key => $value) {
            TipoMoneda::create($value);
        }

    }

}
class TipoInmuebleTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Casa",],
            [ "descripcion" => "Departamento"],
            [ "descripcion" => "PH"],
            [ "descripcion" => "Local"],
            [ "descripcion" => "Cochera"],
            [ "descripcion" => "Terreno"],
            [ "descripcion" => "Chacra"],
            [ "descripcion" => "Campo"],
        ];

        foreach ($data as $key => $value) {
            TipoInmueble::create($value);
        }

    }

}

class TipoBienTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Inmuebles en el exterior"],
            [ "descripcion" => "Inversiones"],
            [ "descripcion" => "Bienes muebles registrables en el exterior"],
            [ "descripcion" => "Otros (especificar)"],
        ];

        foreach ($data as $key => $value) {
            TipoBien::create($value);
        }

    }

}

class OrigenesBienTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Propio"],
            [ "descripcion" => "Ganancial"],
            [ "descripcion" => "Herencia"],
            [ "descripcion" => "Donación"],
            [ "descripcion" => "Otro (especificar)"],
        ];

        foreach ($data as $key => $value) {
            OrigenBien::create($value);
        }

    }

}
class OrigenesVehiculoTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Propio"],
            [ "descripcion" => "Ganancial"],
            [ "descripcion" => "Herencia"],
            [ "descripcion" => "Donación"],
            [ "descripcion" => "Otro (especificar)"],
        ];

        foreach ($data as $key => $value) {
            OrigenVehiculo::create($value);
        }

    }

}
class TipoSexoTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Femenino"],
            [ "descripcion" => "Masculino"],
        ];

        foreach ($data as $key => $value) {
            TipoSexo::create($value);
        }

    }

}
class TipoPersonalTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Superior"],
            [ "descripcion" => "Subalterno"],
            [ "descripcion" => "Auxiliar"],
        ];

        foreach ($data as $key => $value) {
            TipoPersonal::create($value);
        }

    }

}

class TipoVinculoTableSeeder extends Seeder {

    public function run()
    {
        $data = [
            [ "descripcion" => "Cónyuge"],
            [ "descripcion" => "Conviviente"],
        ];

        foreach ($data as $key => $value) {
            TipoVinculo::create($value);
        }

    }

}