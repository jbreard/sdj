@extends ('header')
@section ('title') Listado Usuarios @stop

@section ('content')
@if(Session::has('success'))
	<br>
	<div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
@endif
<style media="all" type="text/css">
	.usuario_0 {
		background-color: #CD5C5C;
	}

	.usuario_1 {
		background-color: #00ff75;
	}
</style>
<h2>Listados de Usuarios</h2>
<table class="table table-fit">
	<thead>
		<tr>
			<th>Estado</th>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>DNI</th>
			<th>Usuario</th>
			<th>Perfil</th>
			<th>Fuerza</th>
			<th>Email</th>
			<th>Accionefdssdf</th>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $key => $value)
		<tr>
			<td style ="" class="usuario_{{$value->activo}}"></td>
			<td style ="word-break:break-all;">{{ $value->nombre }}</td>
			<td style ="word-break:break-all;">{{ $value->apellido }}</td>
			<td style ="word-break:break-all;">{{ $value->dni }}</td>
			<td style ="word-break:break-all;">{{ $value->username }}</td>
			<td style ="word-break:break-all;">{{ $perfiles[$value->id_perfil] or ''}}</td>
			<td style ="word-break:break-all;">{{ $fuerzas[$value->id_fuerza] or '' }}</td>
			<td style ="word-break:break-all;">{{ $value->email }}</td>
			<td style ="word-break:break-all;">
				@if(Auth::user()->id_perfil==5 || Auth::user()->id_perfil==6 || Auth::user()->id_perfil==1 )
					<a class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Editar" href="{{ route('usuarios.show',$value->id) }}"></a>
					@if($value->activo == 1)
						<a class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Eliminar" href="{{ route('usuarios.destroy',$value->id) }}"></a>
					@else
						<a class="glyphicon glyphicon-ok" data-toggle="tooltip" data-placement="top" title="Habilitar" href="{{ route('usuarios.enable',$value->id) }}"></a>
					@endif

				@endif
			</td>
		</tr>
	@endforeach
	</tbody>
</table>

@stop