<!DOCTYPE html>
<html>
<head>
    <title>@yield('title', 'Sistema de Declaraciones Juradas')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ URL::asset('bootstrap/images/favicon.ico')}}">
    <!-- Bootstrap core CSS -->
    {{ HTML::style('assets/css/jquery-ui.css') }}
    {{ HTML::style('bootstrap/dist/css/sticky-footer-navbar.css', array('media' => 'screen')) }}
    {{ HTML::style('bootstrap/dist/css/bootstrap.css', array('media' => 'screen')) }}
    {{ HTML::style('bootstrap/dist/css/login.css', array('media' => 'screen')) }}
    <style>
        @font-face {
            font-family: 'Roboto Condensed';
            font-style: normal;
            font-weight: 700;
            src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url('{{ URL::asset("assets/fonts/b9QBgL0iMZfDSpmcXcE8nCSLrGe-fkSRw2DeVgOoWcQ.woff") }}') format('woff');
        }
        
        a.link-form-ddjj {
            color: #FFFFFF;
            text-decoration:none;
        }
    </style>
    @yield('script')
</head>
<body>
<div class="cont-logoinst"></div>
<h2 class="form-signin-heading titulo-sist">Sistema de Declaraciones Juradas</h2>
 
<div class="container cont-login form-group">
		{{ Form::open(array('url' => '/login','class'=>'form-signin','role'=>'form')) }}

            <div class="row">
                <div class="col-md-12">
                    {{ Form::text('username', null, array('placeholder' => 'Usuario', 'class' => 'form-control input-lg','required'=>'required','autocomplete'=>'off')) }}
                </div>
                <div class="col-md-12">
                    {{ Form::password('password',array('placeholder' => 'Clave', 'class' => 'form-control input-lg')) }}
                    @if(Session::has('mensaje_error'))
                        {{ Session::get('mensaje_error') }}
                    @endif
                </div>
                <div class="col-md-12">
                    {{Form::captcha()}}
                    <span class="error">{{ $errors->first('g-recaptcha-response')  }}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"><br></div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {{ Form::submit('Enviar',array("class"=>"btn btn-lg btn-primary btn-block"   )) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"><br></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a class="link-form-ddjj" href="{{route("accessForm.login")}}">Ir a Formulario Declaración Jurada</a>
                </div>
            </div>


        {{ Form::close() }}
</div>
<div class="cont-logodir"><img src="{{ URL::asset('bootstrap/images/logo-direccion.png')}}" /></div>
{{ HTML::script('assets/js/jquery.min.js') }}
{{ HTML::script('assets/js/jquery-ui.min.js') }}
{{ HTML::script('bootstrap/dist/js/bootstrap.min.js') }}
</body>
</html>



