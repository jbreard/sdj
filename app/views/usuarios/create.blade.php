@extends ('header')
<?php
    if ($user->exists):
        $form_data = array('route' => array('usuarios.update', $user->id), 'method' => 'PATCH');
        $action    = 'Editar';
    else:
        $form_data = array('route' => 'usuarios.store', 'method' => 'POST');
        $action    = 'Crear';        
    endif;

?>
@section ('title') {{$action}} usuario @stop

@section ('content')
<h1>{{$action}} Usuario</h1>
<div class="alert alert-danger fade in" style="display:none" id="errores">
      
     
</div>

{{ Form::model($user,$form_data, array('role' => 'form')) }}
<div class="row">
	<div class="form-group col-md-4">
		{{ Form::label('id', 'Perfil') }}
		{{ Form::select('id_perfil',$perfiles ,null,array('class' => 'form-control','id'=>'id_perfil')) }}
	</div>
	<div class="form-group col-md-4">
		{{ Form::label('area', 'Area') }}
		{{ Form::text('area', null, array('placeholder' => 'Ingrese el area del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('area') }}
	</div>
</div>
<div class="row">
	<div class="form-group col-md-4">
		{{ Form::label('fuerza', 'Fuerza a la que pertenece') }} <small class="" id="span-alert-fuerza"></small>
		{{ Form::select('id_fuerza',$fuerzas ,null,array('class' => 'form-control','id'=>'id_fuerza')) }}
		{{ $errors->first('id_fuerza') }}
	</div>
    <div class="form-group col-md-4">
        {{ Form::label('username', 'Usuario') }}
        {{ Form::text('username', null, array('placeholder' => 'Ingrese el usuario', 'class' => 'form-control')) }}
        {{ $errors->first('username') }}
    </div>
</div>
<div class="row">
	<div class="form-group col-md-4">
		{{ Form::label('nombre', 'Nombre') }}
		{{ Form::text('nombre', null, array('placeholder' => 'Ingrese el nombre del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('nombre') }}
	</div>
	<div class="form-group col-md-4">
		{{ Form::label('apellido', 'Apellido') }}
		{{ Form::text('apellido', null, array('placeholder' => 'Ingrese el apellido del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('apellido') }}
	</div>
</div>
<div class="row">
	<div class="form-group col-md-3">
		{{ Form::label('dni', 'Dni') }}
		{{ Form::text('dni', null, array('placeholder' => 'Ingrese el dni del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('dni') }}
	</div>
	<div class="form-group col-md-3">
		{{ Form::label('email', 'Mail') }}
		{{ Form::text('email', null, array('placeholder' => 'Ingrese el mail del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('email') }}
	</div>
	<div class="form-group col-md-3">
		{{ Form::label('password_new', 'Nueva Contraseña') }}
		{{ Form::text('password_new', null, array('placeholder' => 'Ingrese el password del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('password') }}
	</div>
</div>
<div class="row">
	<div class="form-group col-md-4">
		{{ Form::label('telefono', 'Teléfono') }}
		{{ Form::text('telefono', null, array('placeholder' => 'Ingrese el teléfono del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('telefono') }}
	</div>
	<div class="form-group col-md-4">
		{{ Form::label('contacto_laboral', 'Contacto Laboral') }}
		{{ Form::text('contacto_laboral', null, array('placeholder' => 'Ingrese el contacto laboral del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('contacto_laboral') }}
	</div>
</div>
<div class="row">

	<div class="form-group col-md-4">
		
		{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary navbar-btn btn-sm','id'=>'id_guardar')) }}
	</div>
</div>
{{ Form::close() }}
@stop

@section('scripts')
<script>

	$(function() {

		$("input[name='dni").focusout(function(event) {
			var dni = $("input[name='dni']").val();
			$.ajax({
				type : 'POST',
				url : '{{ Request::root() }}/usuarios/dni',
				data : 'dni=' + dni,
				success : function(d) {
					var res = jQuery.parseJSON(d);
					if (res.respuesta) {
						$("#id_guardar").attr("disabled", "disabled");
						$("#errores").html("Ya Existe un Usuario con ese Nro Documento").show("slow");
					} else {
						$("#id_guardar").removeAttr("disabled");
						$("#errores").hide("slow");
					}
				}
			});
		});

		$("#id_perfil").change(function(){
			var id = $(this).val();
			//ministerio & ministerioDJ
			if(id==5 || id==6) {
				$('#id_fuerza').prop('disabled', 'disabled');
				$('#span-alert-fuerza').text('<No corresponde para el perfil Ministerio>');
			} else {
				$('#id_fuerza').prop('disabled', false);
				$('#span-alert-fuerza').text('');
			}
		});

		$("#id_perfil").change();

	}); 
</script>

@stop