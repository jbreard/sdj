<!DOCTYPE html>
<html>
<head>
    <title>@yield('title', 'Sistema de DDJJFF')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ URL::asset('bootstrap/images/favicon.png')}}">
    <!-- Bootstrap core CSS -->
    {{ HTML::style('assets/jquery-ui-1.11.4/jquery-ui.css') }}
    {{ HTML::style('assets/bootstrap-3.3.6-dist/css/bootstrap.css', array('media' => 'all')) }}
    {{ HTML::style('assets/bootstrap-3.3.6-dist/css/bootstrap-theme.min.css', array('media' => 'all')) }}
    {{ HTML::style('assets/select2-4.0.3/dist/css/select2.css', array('media' => 'all')) }}
    {{ HTML::style('assets/bootstrap-3.3.6-dist/css/bootstrap-datetimepicker.css', array('media' => 'all')) }}
    {{ HTML::style('assets/bootstrap-3.3.6-dist/css/docs.min.css', array('media' => 'all')) }}


    {{ HTML::style('assets/css/ladda-themeless.min.css') }}
    {{ HTML::style('assets/css/main.css', array('media' => 'all')) }}

    <style type="text/css" media="all">
        @font-face {
            font-family: 'Roboto Condensed';
            font-style: normal;
            font-weight: 700;
            src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url('{{ URL::asset("assets/fonts/b9QBgL0iMZfDSpmcXcE8nCSLrGe-fkSRw2DeVgOoWcQ.woff") }}') format('woff');
        }

        .table thead>tr>th, .table>tbody>tr>th , div#datos_personales>form>div.row>div.form-group>label.label_datos_personales {
            color: #777 !important;
            font-weight: normal !important;
        }

        .table caption>span.title {
            font-weight: bold !important;
        }

        .table>caption>div.bs-callout-info, .table>caption>div.bs-callout-info>h4 {
            font-size: 10px !important;
        }

    </style>
    @yield('styles')

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109290489-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109290489-1');
</script>



</head>
<body>

<div id="div-print-logo" class="row">
    <div class="col-md-12">
       <figure id="figure-print-logo">
           <img src="{{ URL::asset('bootstrap/images/logo-minseg.png'); }}" width="201" height="60">
       </figure>
    </div>
</div>

<!-- Static navbar -->
<div class="navbar-inverse navbar-default navbar-fixed-top not-view-print" role="navigation">
    <div class="container" style="width:100%">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"
               style="background-image:url({{ URL::asset('bootstrap/images/isologo.png') }}); background-position:center left; background-repeat:no-repeat; padding-left:60px; padding-top:7px">Sistema
                de DDJJFF <br/><span
                    style="font-family: Arial, Helvetica, sans-serif; font-size:10px;">DIRECCION DE INFORMATICA</span></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown"></li>

            </ul>
            <img src="{{ URL::asset('bootstrap/images/logo-minseg.png'); }}" width="201" height="60"
                 style="float: right;">

        </div>
        <!--/.nav-collapse -->
    </div>
</div>

<!-- Fin Static navbar -->
<div class="container contenedor">
    @yield('content')
</div>

<div id="footer" style="margin-bottom:10px" class="not-view-print">
    <div class="container">
        <p class="text-muted">MINISTERIO DE SEGURIDAD</p>
    </div>
</div>
{{ HTML::script('assets/js/jquery-v1.11.2.min.js') }}
{{ HTML::script('assets/js/masked-input-plugin/jquery.maskedinput.min.js') }}
{{ HTML::script('assets/js/jquery-validate/jquery.validate.min.js') }}
{{ HTML::script('assets/js/moment/moment-with-locales.min.js') }}
<!--{{ HTML::script('assets/jquery-ui-1.11.4/jquery-ui.min.js') }}-->
{{ HTML::script('assets/bootstrap-3.3.6-dist/js/transition.js') }}
{{ HTML::script('assets/bootstrap-3.3.6-dist/js/collapse.js') }}
{{ HTML::script('assets/bootstrap-3.3.6-dist/js/bootstrap.min.js') }}
{{ HTML::script('assets/bootstrap-3.3.6-dist/js/bootstrap-datetimepicker.js') }}
{{ HTML::script('assets/select2-4.0.3/dist/js/select2.full.min.js') }}
{{ HTML::script('assets/js/fullscreen.js') }}
{{ HTML::script('assets/js/spin.min.js') }}
{{ HTML::script('assets/js/ladda.min.js') }}

@yield('scripts')
</body>
</html>
