<!DOCTYPE html>
<html>
<head>
    <title>@yield('title', 'Sistema de DDJJFF')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{ URL::asset('bootstrap/images/favicon.ico')}}">
    <!-- Bootstrap core CSS -->
    {{ HTML::style('assets/jquery-ui-1.11.4/jquery-ui.css') }}
    {{ HTML::style('assets/bootstrap-3.3.6-dist/css/bootstrap.css', array('media' => 'all')) }}
    {{ HTML::style('assets/bootstrap-3.3.6-dist/css/bootstrap-theme.min.css', array('media' => 'all')) }}
    {{ HTML::style('assets/select2-4.0.3/dist/css/select2.css', array('media' => 'all')) }}
    {{ HTML::style('assets/bootstrap-3.3.6-dist/css/bootstrap-datetimepicker.css', array('media' => 'all')) }}

    {{ HTML::style('assets/css/ladda-themeless.min.css') }}
    {{ HTML::style('assets/css/main.css', array('media' => 'all')) }}

    <style type="text/css" media="all">
        @font-face {
            font-family: 'Roboto Condensed';
            font-style: normal;
            font-weight: 700;
            src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url('{{ URL::asset("assets/fonts/b9QBgL0iMZfDSpmcXcE8nCSLrGe-fkSRw2DeVgOoWcQ.woff") }}') format('woff');
        }
    </style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109290489-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-109290489-1');
</script>


</head>
<body>

<div id="div-print-logo" class="row">
    <div class="col-md-12">
        <figure id="figure-print-logo">
            <img src="{{ URL::asset('bootstrap/images/logo-minseg.png'); }}" width="201" height="60">
        </figure>
    </div>
</div>

<!-- Static navbar -->
<div class="navbar-inverse navbar-default navbar-fixed-top not-view-print" role="navigation">
    <div class="container" style="width:100%">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" style="background-image:url({{ URL::asset('bootstrap/images/isologo.png') }}); background-position:center left; background-repeat:no-repeat; padding-left:60px; padding-top:7px">
                Sistema de DDJJFF <br/>
                <span style="font-family: Arial, Helvetica, sans-serif; font-size:10px;">DIRECCION DE INFORMATICA</span>
            </a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        @if( Auth::user()->id_perfil==1 || Auth::user()->id_perfil == 6)
                        <li>
                            <a href="{{ route('usuarios.create') }}">
                                <span class="glyphicon glyphicon-user" style="margin-bottom:5px;"></span>
                                <span style="margin-left:5px;">Agregar Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('usuarios.listado') }}">
                                <span class="glyphicon glyphicon-th-list" style="margin-bottom:5px;"></span>
                                <span style="margin-left:5px">Listado de Usuarios</span>
                            </a>
                        </li>
                        <li>
                            <hr>
                        </li>
                        <li>
                            <a href="{{ route('usuariosFormulario.create') }}">
                                <span class="glyphicon glyphicon-user" style="margin-bottom:5px;"></span>
                                <span style="margin-left:5px;">Agregar Usuario de Formulario DDJJ</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('usuariosFormulario.listado') }}">
                                <span class="glyphicon glyphicon-th-list" style="margin-bottom:5px;"></span>
                                <span style="margin-left:5px">Listado de Usuarios Válidos <br>para Formulario DDJJ</span>
                            </a>
                        </li>
                        <li>
                            <hr>
                        </li>
                        @endif
                        @if(Auth::user()->id_perfil==1)
                        {{--<li>--}}
                            {{--<a href="{{ route('estadisticas.index') }}">--}}
                                {{--<span class="glyphicon glyphicon-stats" style="margin-bottom:5px;"></span>--}}
                                {{--<span style="margin-left:5px">Estadísticas</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<hr>--}}
                        {{--</li>--}}
                        @endif
                        <li>
                            <a href="{{ route('usuarios.clave') }}">
                                <span class="glyphicon glyphicon-lock" style="margin-bottom:5px;"></span>
                                <span style="margin-left:5px;">Cambiar Clave</span>
                            </a>
                        </li>
                        <!--                        <li><a href="#"><span class="glyphicon glyphicon-book" style="margin-bottom:5px;" /></span><span style="margin-left:5px;">Manual</span></a></li>-->
                    </ul>
                </li>
                @if(Auth::user()->id_perfil == 3 || Auth::user()->id_perfil == 4 || Auth::user()->id_perfil == 5 || Auth::user()->id_perfil == 6 || Auth::user()->id_perfil == 1)
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Listado<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('listado.index') }}">
                                <span class="glyphicon glyphicon-th-list" style="margin-bottom:5px;"></span>
                                <span style="margin-left:5px;">Listado</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://sdj-dmz-historico.minseg.gob.ar/login" target="_blank">
                                <span class="glyphicon glyphicon-fast-backward" style="margin-bottom:5px;"></span>
                                <span style="margin-left:5px;">Ir a Listado Período Fiscal 2015</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                <li>
                    <a href="{{ URL::to('logout') }}" style="background-image:url({{ URL::asset('bootstrap/images/boton-apagar.png'); }}); background-position:center left; background-repeat:no-repeat; padding-left:36px;">Salir</a>
                </li>
                <li>
                    <a href="#" style="background-position:center left; background-repeat:no-repeat; padding-left:36px;">{{Auth::user()->username}} {{ Auth::user()->perfil->descripcion }}</a>
                </li>
            </ul>
            <img src="{{ URL::asset('bootstrap/images/logo-minseg.png') }}" width="201" height="60" style="float: right;">
        </div>
        <!--/.nav-collapse -->
    </div>
</div>

<!-- Fin Static navbar -->
<div class="container contenedor">
    @yield('content')
</div>

<div id="footer" style="margin-bottom:10px" class="not-view-print">
    <div class="container">
        <p class="text-muted">MINISTERIO DE SEGURIDAD</p>
    </div>
</div>
{{ HTML::script('assets/js/jquery-v1.11.2.min.js') }}
{{ HTML::script('assets/js/masked-input-plugin/jquery.maskedinput.min.js') }}
{{ HTML::script('assets/js/jquery-validate/jquery.validate.min.js') }}
{{ HTML::script('assets/js/moment/moment-with-locales.min.js') }}
<!--{{ HTML::script('assets/jquery-ui-1.11.4/jquery-ui.min.js') }}-->
{{ HTML::script('assets/bootstrap-3.3.6-dist/js/transition.js') }}
{{ HTML::script('assets/bootstrap-3.3.6-dist/js/collapse.js') }}
{{ HTML::script('assets/bootstrap-3.3.6-dist/js/bootstrap.min.js') }}
{{ HTML::script('assets/bootstrap-3.3.6-dist/js/bootstrap-datetimepicker.js') }}
{{ HTML::script('assets/select2-4.0.3/dist/js/select2.full.min.js') }}
{{ HTML::script('assets/js/fullscreen.js') }}
{{ HTML::script('assets/js/spin.min.js') }}
{{ HTML::script('assets/js/ladda.min.js') }}
@yield('scripts')
</body>
</html>
