@extends('pdf.index')
@section('contenido')
<div class="table-responsive">
<h1 style="text-align: center; vertical-align: middle; " >PORTADA DE DECLARACIÓN JURADA</h1>

<p style="text-align: center; vertical-align: middle; font-size: 10px;">
    EL  DOCUMENTO QUE CONTIENE EL PRESENTE SOBRE TIENE CARÁCTER DE DECLARACIÓN JURADA EN LOS TÉRMINOS Y ALCANCES DE LA RESOLUCIÓN  MINISTERIAL 190/2016 Y DE LA DISPOSICIÓN DI-2017-1-APN-DNCIFS#MSG DEL MINISTERIO DE SEGURIDAD DE LA NACIÓN.
</p>
<br>
<!--<p style="text-align: left">Datos Personales</p>-->
<table class="table table-bordered table-condensed table-striped">
    <tbody>
    <tr>
        <td colspan="2">APELLIDO: {{$data->persona->apellido}}</td>
    </tr>
    <tr>
        <td colspan="2">NOMBRES: {{$data->persona->nombres}}</td>
    </tr>
    <tr>
        <td colspan="2">DNI: {{$data->persona->documento}}</td>
    </tr>
    <tr>
        <td colspan="2">CARACTER DE LA DDJJ: {{$data->caracter->descripcion or ''}}</td>
    </tr>
    <tr>
        <td colspan="2">PERIODO FISCAL: {{$data->periodo_fiscal}}</td>
    </tr>
    <tr>
        <td colspan="2">FUERZA: {{$data->persona->fuerza->descripcion or ''}}</td>
    </tr>
    <tr>
        <td colspan="2">PERSONAL: {{$data->persona->tipo_personal->descripcion or ''}}</td>
    </tr>
    <tr>
        <td colspan="2">GRADO: {{$data->persona->grado->descripcion or ''}}</td>
    </tr>
    <tr>
        <td colspan="2">LEGAJO: {{$data->persona->legajo_personal}}</td>
    </tr>
    <tr>
        <td colspan="2">N° DE FORMULARIO: {{$data->id}}</td>
    </tr>
    <tr>
        <td colspan="2">N° DE VERSIÓN: {{$data->version}}</td>
    </tr>
    <tr>
        <td colspan="2">FECHA Y HORA DE LA IMPRESIÓN: {{date("d-m-Y H:i") }}</td>
    </tr>
    <tr>
        <td colspan="2">
            <p style="text-align: center; vertical-align: middle; font-size: 10px">Los datos de esta portada deben coincidir con los que se visualizan en el Sistema Declaraciones Juradas.</p>
            <br>
        </td>
    </tr>
    <tr >
        <td style=" background-color:#ffffff; text-align:center; height: 100px !important; border: solid 1px;"  colspan="1">
            @for($i=0;$i<4;$i++)<br>@endfor
            Firma y aclaración del declarante.
        </td>
        <td style=" background-color:#ffffff; text-align:center; height: 100px !important; border: solid 1px;"  colspan="1">
            @for($i=0;$i<3;$i++)<br>@endfor
                Firma y sello aclaratorio del funcionario responsable y certificante.
        </td>
        <!--
        <td style="background-color:#ffffff; text-align:center; height: 100px !important; border: solid 1px;" >
            @for($i=0;$i<4;$i++)<br>@endfor
            Firma y sello aclaratorio del funcionario responsable y certificante.
        </td>
        -->
    </tr>
    </tbody>
</table>

<h2>Datos Personales</h2>
    <table class="table table-bordered table-condensed table-striped">
        <caption>{{$data->persona->apellido}} {{$data->persona->nombres}}</caption>
        <thead>
        <tr>
            <th>Fecha nacimiento</th>
            <th>DNI</th>
            <th>CUIL/CUIT</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$data->persona->nacimiento}}</td>
            <td>{{$data->persona->documento}}</td>
            <td>{{$data->persona->cuil_cuit}}</td>
        </tr>
        </tbody>
        <thead>
        <tr>
            <th>Sexo</th>
            <th>Estado Civil</th>
            <th>Domicilio real</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$data->persona->sexo->descripcion}}</td>
            <td>{{$data->persona->estado_civil->descripcion}}</td>
            <td>{{$data->persona->domicilio_real}}</td>
        </tr>
        </tbody>
        <thead>
        <tr>
            <th>Código postal</th>
            <th>Provincia</th>
            <th>Localidad</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$data->persona->codigo_postal}}</td>
            <td>{{$data->persona->provincia->descripcion or ''}}</td>
            <td>{{$data->persona->localidad->descripcion or ''}}</td>
        </tr>
        </tbody>
        <thead>
        <tr>
            <th>Telefono de contacto</th>
            <th>Correo electrónico</th>
            <th>Fuerza a la que pertenece</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$data->persona->telefono}}</td>
            <td>{{$data->persona->email}}</td>
            <td>{{$data->persona->fuerza->descripcion or ''}}</td>
        </tr>
        </tbody>
        <thead>
        <tr>

            <th>Destino de revista</th>
            <th>Personal</th>
            <th>Grado</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$data->persona->destino_revista}}</td>
            <td>{{$data->persona->tipo_personal->descripcion or ''}}</td>
            <td>{{$data->persona->grado->descripcion or ''}}</td>

        </tr>
        </tbody>
        <thead>
        <tr>
            <th>Legajo personal</th>
            <th>
                @if($data->persona->fuerza->id == 4)
                    Agrupamiento/Modalidad
                @else
                    Cuerpo/Agrupamiento
                @endif
            </th>
            <th>Cargo o función </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$data->persona->legajo_personal}}</td>
            <td>
                @if($data->persona->fuerza->id == 4)
			@if($data->version_formulario>0)
	                    {{$data->persona->cuerpo_agrupamiento}}
			@else
	                    {{$data->persona->tipo_agrupamiento_modalidad->descripcion or ''}}
			@endif
                @else
                    {{$data->persona->cuerpo_agrupamiento}}
                @endif
            </td>
            <td>{{$data->persona->cargo_funcion}}</td>
        </tr>
        </tbody>
        <thead>
        <tr>

            <th>Escalafón</th>
            <th colspan="2">Situación de revista</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$data->persona->escalafon}}</td>
            <td colspan="2">{{$data->persona->situacion_revista->descripcion or ''}}</td>
        </tr>
        </tbody>
    </table>

<h2>Datos Familiares</h2>
    @if(isset($data->persona->familiar->conyuge) && !empty($data->persona->familiar->conyuge))
        <table class="table table-bordered table-condensed table-striped">
            <caption>Conyuge / Convivientes</caption>
                <?php $i = 0;?>
                @foreach($data->persona->familiar->conyuge as $dataConyuges)
                <thead>
                <tr><td colspan="6" class="inmuebles_header_registry"># {{ $i }}</td></tr>
                <tr>
                    <th>Apellido</th>
                    <th>Nombres</th>
                    <th>Fecha de Nacimiento</th>
                    <th>Documento</th>
                    <th colspan="2">Ocupacion</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$dataConyuges->persona->apellido}}</td>
                    <td>{{$dataConyuges->persona->nombres}}</td>
                    <td>{{$dataConyuges->persona->nacimiento}}</td>
                    <td>{{$dataConyuges->persona->documento}}</td>
                    <td colspan="2">{{$dataConyuges->persona->ocupacion}}</td>
                </tr>
                </tbody>
                <thead>
                <tr>
                    <th>Vínculo</th>
                    <th>¿Trabaja?</th>
                    <th>¿Desde cuándo trabaja?</th>
                    <th>Ocupación</th>
                    <th>Ingreso neto anual</th>
                    <th>Categoría Tributaria</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$dataConyuges->persona->tipo_vinculo->descripcion}}</td>
                    <td>{{$dataConyuges->persona->trabaja}}</td>
                    <td>{{$dataConyuges->persona->desde_cuando_trabaja}}</td>
                    <td>{{$dataConyuges->persona->ocupacion}}</td>
                    <td>{{$dataConyuges->persona->ingreso_neto_anual}}</td>
                    <td>{{$dataConyuges->persona->categoria_tributaria->descripcion or ''}}</td>
                </tr>
                </tbody>
                <?php $i++;?>
                @endforeach

        </table>
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
                <tr>
                    <td>Declara no tener conviviente o cónyuge</td>
                </tr>
            </tbody>
        </table>
    @endif


    @if(isset($data->persona->familiar->hijo) && !empty($data->persona->familiar->hijo))
        <table class="table table-bordered table-condensed table-striped">
            <caption>Hijos/as</caption>
            <thead>
            <tr>
                <th>Apellido</th>
                <th>Nombres</th>
                <th>Fecha de Nacimiento</th>
                <th>Documento</th>
                <th>¿Convive con usted?</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data->persona->familiar->hijo as $dataHijos)
                <tr>
                    <td>{{$dataHijos->persona->apellido}}</td>
                    <td>{{$dataHijos->persona->nombres}}</td>
                    <td>{{$dataHijos->persona->nacimiento}}</td>
                    <td>{{$dataHijos->persona->documento}}</td>
                    <td>{{$dataHijos->persona->tipo_hijo_convive->descripcion or ''}}</td>

                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener hijas/os</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->persona->familiar->otra_persona) && !empty($data->persona->familiar->otra_persona))
        <table class="table table-bordered table-condensed table-striped">
            <caption>Otras Personas convivientes</caption>
            <thead>
            <tr>
                <th>Apellido</th>
                <th>Nombres</th>
                <th>Fecha de Nacimiento</th>
                <th>Parentesco</th>
                <th>Documento</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data->persona->familiar->otra_persona as $dataOtrasPersonas)
                <tr>
                    <td>{{$dataOtrasPersonas->persona->apellido}}</td>
                    <td>{{$dataOtrasPersonas->persona->nombres}}</td>
                    <td>{{$dataOtrasPersonas->persona->nacimiento}}</td>
                    <td>{{$dataOtrasPersonas->persona->vinculo_parentesco}}</td>
                    <td>{{$dataOtrasPersonas->persona->documento}}</td>

                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener otras personas convivientes</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->persona->familiar->padre_madre) && !empty($data->persona->familiar->padre_madre))
        <table class="table table-bordered table-condensed table-striped">
            <caption>Padre/Madre</caption>
            <thead>
            <tr>
                <th>Apellido</th>
                <th>Nombres</th>
                <th>Vínculo Parentesco</th>
                <th>Tipo De Documento</th>
                <th>Número</th>
                <th>Fecha de Nacimiento</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data->persona->familiar->padre_madre as $dataPadreMadre)
                <tr>
                    <td>{{$dataPadreMadre->persona->apellido}}</td>
                    <td>{{$dataPadreMadre->persona->nombres}}</td>
                    <td>{{$dataPadreMadre->persona->tipo_vinculo->descripcion or ''}}</td>
                    <td>{{$dataPadreMadre->persona->tipo_documento->descripcion or ''}}</td>
                    <td>{{$dataPadreMadre->persona->documento}}</td>
                    <td>{{$dataPadreMadre->persona->nacimiento}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener padre o madre</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->persona->familiar->conviviente_fuerza) && !empty($data->persona->familiar->conviviente_fuerza))
        <table class="table table-bordered table-condensed table-striped">
            <caption> Persona con vínculo conyugal o convivencial, de consanguinidad y/o vínculo por afinidad, que sea o a haya sido parte de alguna Fuerza Nacional, Provincial o Municipal.</caption>
            <thead>
            <tr>
                <th>Nombres</th>
                <th>Apellido</th>
                <th>Tipo de vínculo</th>
                <th>Fuerza</th>
                <th>En Actividad</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data->persona->familiar->conviviente_fuerza as $dataConvivienteFuerza)
                <tr>
                    <td>{{$dataConvivienteFuerza->persona->nombres}}</td>
                    <td>{{$dataConvivienteFuerza->persona->apellido}}</td>
                    <td>{{$dataConvivienteFuerza->persona->tipo_vinculo->descripcion or ''}}</td>
                    <td>{{$dataConvivienteFuerza->persona->fuerza->descripcion or ''}}</td>
                    <td>{{$dataConvivienteFuerza->persona->tipo_actividad_conviviente_fuerza->descripcion or ''}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener persona con vínculo conyugal o convivencial, de consanguinidad y/o vínculo por afinidad, que sea o a haya sido parte de alguna Fuerza Nacional, Provincial o Municipal.</td>
            </tr>
            </tbody>
        </table>
    @endif


    <h2>Datos Patrimoniales Propios</h2>

    @if(isset($data->inmueble->propio) && !empty($data->inmueble->propio))
        @include('pdf.partials.inmueble',['inmuebles'=>$data->inmueble->propio])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener inmuebles registrados propios</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->vehiculo->propio) && !empty($data->vehiculo->propio))
        @include('pdf.partials.vehiculos',['vehiculos'=>$data->vehiculo->propio])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener vehículos registrados</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->cuenta_bancaria) && !empty($data->cuenta_bancaria))
        @include('pdf.partials.cuenta_bancaria',['cuentas'=>$data->cuenta_bancaria])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener cuentas bancarias</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->cuenta_bancaria_exterior) && !empty($data->cuenta_bancaria_exterior))
        @include('pdf.partials.cuenta_bancaria_exterior',['cuentas'=>$data->cuenta_bancaria_exterior])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener cuentas bancarias en el exterior</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->bonos_titulos_acciones) && !empty($data->bonos_titulos_acciones))
        @include('pdf.partials.bonos_titulos_acciones',['bonosTitulosAcciones'=>$data->bonos_titulos_acciones])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener bonos, títulos valores y/o acciones</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->derecho_expectativa->propio) && !empty($data->derecho_expectativa->propio))
        @include('pdf.partials.derecho_expectativa',['derechos'=>$data->derecho_expectativa->propio])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener Derechos en Expectativa</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->bien->propio) && !empty($data->bien->propio))
        @include('pdf.partials.bienes',['bienes'=>$data->bien->propio])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener otros bienes registrados propios</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->dinero_efectivo) && !empty($data->dinero_efectivo))
        @include('pdf.partials.dinero_efectivo',['dineros'=>$data->dinero_efectivo])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener dinero en efectivo</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->acreencias) && !empty($data->acreencias))
        @include('pdf.partials.acreencias',['acreencias'=>$data->acreencias])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener acreencias</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->ingreso->fuerza) && !empty($data->ingreso->fuerza))
        @include('pdf.partials.ingreso',['ingresos'=>[$data->ingreso->fuerza]])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener Ingresos</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->ingreso_extraordinario) && !empty($data->ingreso_extraordinario))
        @include('pdf.partials.ingreso_extraordinario',['ingresos'=>$data->ingreso_extraordinario])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener Ingresos Extraordinarios</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->ingreso_externo) && !empty($data->ingreso_externo))
        @include('pdf.partials.ingreso_externo',['ingresos'=>$data->ingreso_externo])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener Ingresos por fuera de la fuerza</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->tarjeta_credito) && !empty($data->tarjeta_credito))
        @include('pdf.partials.tarjeta_credito',['cuentas'=>$data->tarjeta_credito])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener tarjetas credito</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->deuda->propio) && !empty($data->deuda->propio))
        @include('pdf.partials.deuda',['deudas'=>$data->deuda->propio])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener Deuda</td>
            </tr>
            </tbody>
        </table>
    @endif

    <h2>Datos Patrimoniales del Grupo Familiar</h2>

    <h2>Cónyuge o conviviente actual</h2>

    @if(isset($data->inmueble->conyuge) && !empty($data->inmueble->conyuge))
        @include('pdf.partials.inmueble',['inmuebles'=>$data->inmueble->conyuge,'titulo'=>'Conyuge'])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener inmuebles registrados</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->vehiculo->conyuge) && !empty($data->vehiculo->conyuge))
        @include('pdf.partials.vehiculos',['vehiculos'=>$data->vehiculo->conyuge,'titulo'=>'Conyuge'])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener vehiculos registrados</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->derecho_expectativa->conyuge) && !empty($data->derecho_expectativa->conyuge))
        @include('pdf.partials.derecho_expectativa',['derechos'=>$data->derecho_expectativa->conyuge,'titulo'=>'Conyuge'])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener Derechos en Expectativa</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->bien->conyuge) && !empty($data->bien->conyuge))
        @include('pdf.partials.bienes',['bienes'=>$data->bien->conyuge,'titulo'=>'Conyuge'])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener otros bienes registrados propios</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->deuda->conyuge) && !empty($data->deuda->conyuge))
        @include('pdf.partials.deuda',['deudas'=>$data->deuda->conyuge,'titulo'=>'Conyuge'])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener Deuda</td>
            </tr>
            </tbody>
        </table>
    @endif

    <h2>Hijos menores no emancipados</h2>

    @if(isset($data->inmueble->hijo) && !empty($data->inmueble->hijo))
        @include('pdf.partials.inmueble',['inmuebles'=>$data->inmueble->hijo,'titulo'=>'Hijo'])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener inmuebles registrados </td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->vehiculo->hijo) && !empty($data->vehiculo->hijo))
        @include('pdf.partials.vehiculos',['vehiculos'=>$data->vehiculo->hijo,'titulo'=>'Hijo'])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener vehiculos registrados</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->derecho_expectativa->conyuge) && !empty($data->derecho_expectativa->conyuge))
        @include('pdf.partials.derecho_expectativa',['derechos'=>$data->derecho_expectativa->conyuge,'titulo'=>'Conyuge'])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener Derechos en Expectativa</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->bien->hijo) && !empty($data->bien->hijo))
        @include('pdf.partials.bienes',['bienes'=>$data->bien->hijo,'titulo'=>'Hijo'])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener otros bienes registrados propios</td>
            </tr>
            </tbody>
        </table>
    @endif

    @if(isset($data->deuda->hijo) && !empty($data->deuda->hijo))
        @include('pdf.partials.deuda',['deudas'=>$data->deuda->hijo, 'titulo'=>'Hijo'])
    @else
        <table class="table table-bordered table-condensed table-striped">
            <tbody>
            <tr>
                <td>Declara no tener Deuda</td>
            </tr>
            </tbody>
        </table>
    @endif

    <div class="row">
        <div class="col-md-12">
            <p>
                *Afirmo y declaro que los datos consignados en este formulario son correctos y completos, sin omitir ni falsear dato alguno.  El presente documento tiene carácter de declaración jurada en los términos y alcances de la Resolución Ministerial 190/2016 del Ministerio de Seguridad de la Nación.
            </p>
            <p>Firma y aclaración del declarante_________________________________________________________</p>
            <p>Lugar y Fecha________________________________________________________________________</p>
            <!--<p>Firma y sello aclaratorio de</p><p>funcionario responsable/certificante_______________________________________________________</p>-->
        </div>
    </div>
</div>
@stop
