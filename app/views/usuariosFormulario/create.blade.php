@extends ('header')

@section ('title') {{$action}} usuario @stop

@section ('content')
<h1>{{$action}} Usuario de Formulario DDJJ</h1>
<div class="alert alert-danger fade in" style="display:none" id="errores">
      
     
</div>

{{ Form::model($user,$form_data, array('role' => 'form')) }}
<div class="row">
	<div class="form-group col-md-3">
		{{ Form::label('nombre', 'Nombre') }}
		{{ Form::text('nombre', null, array('placeholder' => 'Ingrese el nombre del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('nombre') }}
	</div>
	<div class="form-group col-md-3">
		{{ Form::label('apellido', 'Apellido') }}
		{{ Form::text('apellido', null, array('placeholder' => 'Ingrese el apellido del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('apellido') }}
	</div>
	<div class="form-group col-md-3">
		{{ Form::label('dni', 'Dni') }}
		{{ Form::text('dni', null, array('placeholder' => 'Ingrese el dni del usuario', 'class' => 'form-control masked-input-dni')) }}
		{{ $errors->first('dni') }}
	</div>
</div>
<div class="row">
	<div class="form-group col-md-3">
		{{ Form::label('nro_ord', 'Número de Orden') }}
		{{ Form::text('nro_ord', null, array('placeholder' => 'Ingrese el numero de orden del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('nro_ord') }}
	</div>
	<div class="form-group col-md-3">
		{{ Form::label('legajo', 'Legajo') }}
		{{ Form::text('legajo', null, array('placeholder' => 'Ingrese el legajo del usuario', 'class' => 'form-control masked-input-legajo')) }}
		{{ $errors->first('legajo') }}
	</div>
	<div class="form-group col-md-3">
		{{ Form::label('grado', 'Grado') }}
		{{ Form::text('grado', null, array('placeholder' => 'Ingrese el grado del usuario', 'class' => 'form-control')) }}
		{{ $errors->first('grado') }}
	</div>
</div>
<div class="row">
	<div class="form-group col-md-4">
		{{ Form::label('fuerza', 'Fuerza a la que pertenece') }} <small class="" id="span-alert-fuerza"></small>
		{{ Form::select('id_fuerza',$fuerzas ,null,array('class' => 'form-control','id'=>'id_fuerza')) }}
		{{ $errors->first('id_fuerza') }}
	</div>
	<div class="form-group col-md-4">
		{{ Form::label('tipo', 'Carácter de la declaración') }}
		{{ Form::select('tipo',$tiposFormulario ,null,array('class' => 'form-control','id'=>'tipo')) }}
		{{ $errors->first('tipo') }}
	</div>
</div>
<div class="row">
	<div class="form-group col-md-4">
		{{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary navbar-btn btn-sm','id'=>'id_guardar')) }}
	</div>
</div>
{{ Form::close() }}
@stop

@section('scripts')
<script>

	$(function() {

		$.mask.definitions['h'] = "[A-Za-z0-9 ]";
		$(".masked-input-dni").mask("9999999?9");
		$(".masked-input-legajo").mask("h?hhhhhhhhh");

		$("input[name='dni").focusout(function(event) {
			var dni = $("input[name='dni']").val();
			$.ajax({
				type : 'POST',
				url : '{{ Request::root() }}/usuarios/dni',
				data : 'dni=' + dni,
				success : function(d) {
					var res = jQuery.parseJSON(d);
					if (res.respuesta) {
						$("#id_guardar").attr("disabled", "disabled");
						$("#errores").html("Ya Existe un Usuario con ese Nro Documento").show("slow");
					} else {
						$("#id_guardar").removeAttr("disabled");
						$("#errores").hide("slow");
					}
				}
			});
		});

		$("#id_perfil").change(function(){
			var id = $(this).val();
			if(id==5) {
				$('#id_fuerza').prop('disabled', 'disabled');
				$('#span-alert-fuerza').text('<No corresponde para el perfil Ministerio>');
			} else {
				$('#id_fuerza').prop('disabled', false);
				$('#span-alert-fuerza').text('');
			}
		});

		$("#id_perfil").change();

	}); 
</script>

@stop