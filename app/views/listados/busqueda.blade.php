@extends('header')
@section('scripts')
    @include('listados.javascript.busqueda')
@endsection
@section('content')
<div class="row">
    <div class="col-md-12"><br></div>
</div>
{{ Form::open(array('id' => 'form-search')) }}
<div class="row">
    <div class="form-inline form-group col-md-12">
        @if(Auth::user()->id_perfil == 5 || Auth::user()->id_perfil == 6 || Auth::user()->id_perfil == 1)
            {{ Form::select('id_fuerza',$combo_fuerzas,null,array('class' => 'form-control select2 class_find','id' => 'id_fuerza')) }}
        @else
            {{ Form::select('id_fuerza',$combo_fuerzas,null,array('class' => 'form-control select2 class_find hidden','id' => 'id_fuerza')) }}
        @endif
        {{ Form::text('nro_formulario',null,['class' => 'form-control class_find','placeholder' => 'Nro Formulario','maxlength' => 10]) }}
        {{ Form::text('nro_version',null,['class' => 'form-control class_find','placeholder' => 'Nro Versión','maxlength' => 10]) }}
    </div>
</div>
<div class="row">
    <div class="form-inline form-group col-md-12">
        {{ Form::text('nombres',null,['class' => 'form-control class_find','placeholder' => 'Nombre','maxlength' => 100]) }}
        {{ Form::text('apellido',null,['class' => 'form-control class_find','placeholder' => 'Apellido','maxlength' => 100]) }}
        {{ Form::text('documento',null,['class' => 'form-control class_find','placeholder' => 'DNI','maxlength' => 8]) }}

    </div>
</div>
<div class="row">
    <div class="form-inline form-group col-md-12">
        Personal:
        {{ Form::select('id_tipo_personal',$comboPersonal,null,array('class' => 'form-control select2 select-personal','id'=>'id_tipo_personal')) }}
        Grado:
        {{ Form::select('id_grado',$comboGrado,null,array('class' => 'form-control select2 select-grado','id'=>'id_grado')); }}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <div class="row">
            <div class="form-inline form-group col-md-6">
                Creada:<br>
                {{ Form::text('fecha_creada_desde',null,['class' => 'form-control datetimepicker-fecha class_find','placeholder' => 'Desde','maxlength' => 10]) }}
                {{ Form::text('fecha_creada_hasta',null,['class' => 'form-control datetimepicker-fecha class_find','placeholder' => 'Hasta','maxlength' => 10]) }}
            </div>
            <div class="form-inline form-group col-md-6">
                Aceptada:<br>
                {{ Form::text('fecha_aceptada_desde',null,['class' => 'form-control datetimepicker-fecha class_find','placeholder' => 'Desde','maxlength' => 10]) }}
                {{ Form::text('fecha_aceptada_hasta',null,['class' => 'form-control datetimepicker-fecha class_find','placeholder' => 'Hasta','maxlength' => 10]) }}
            </div>
        </div>
        <div class="row">
            <div class="form-inline form-group col-md-6">
                Recibida:<br>
                {{ Form::text('fecha_recibida_desde',null,['class' => 'form-control datetimepicker-fecha class_find','placeholder' => 'Desde','maxlength' => 10]) }}
                {{ Form::text('fecha_recibida_hasta',null,['class' => 'form-control datetimepicker-fecha class_find','placeholder' => 'Hasta','maxlength' => 10]) }}
            </div>
            <div class="form-inline form-group col-md-6">
                Eliminada:<br>
                {{ Form::text('fecha_eliminada_desde',null,['class' => 'form-control datetimepicker-fecha class_find','placeholder' => 'Desde','maxlength' => 10]) }}
                {{ Form::text('fecha_eliminada_hasta',null,['class' => 'form-control datetimepicker-fecha class_find','placeholder' => 'Hasta','maxlength' => 10]) }}
            </div>
        </div>
    </div>
    <div class="form-group col-md-6">
        <div class="row">
            <div class="form-inline form-group col-md-12">
                Estados:<br>
                <select id="tipo_estado" name="tipo_estado" class="form-control select2 class_find" multiple size="6">
                    @foreach($estados as $estado)
                        <option value="{{$estado}}">{{$estado}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-inline form-group col-md-12">
        <button class="btn btn-primary navbar-btn ladda-button btn-sm btn-submit-form" data-style="expand-left" data-page="search"><span class="ladda-label">Buscar</span></button>
        @if(Auth::user()->id_perfil == 1)
            <button class="btn btn-primary navbar-btn ladda-button btn-sm btn-submit-form" data-style="expand-left" data-page="exportToExcel"><span class="ladda-label">Exportar</span></button>
            <div id="div-export-excel"></div>
        @endif
    </div>
</div>
<div class="row">
    <div class="form-inline form-group col-md-12 div-control-paginate">
        <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active">
                <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Página actual">
                    <span class="glyphicon glyphicon-home ladda-label" aria-hidden="true"></span>
                    <span id="span-current-page" class="badge"></span>
                </button>
            </li>
            <li role="presentation">
                <button class="btn btn-primary ladda-button btn-sm btn-submit-form" data-style="expand-left" id="btn-first" data-page="first" data-toggle="tooltip" data-placement="top" title="Página inicial" >
                    <span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
                    <span class="badge">1</span>
                </button>
            </li>
            <li role="presentation">
                <button class="btn btn-primary ladda-button btn-sm btn-submit-form" data-style="expand-left" id="btn-prev" data-page="prev" data-toggle="tooltip" data-placement="top" title="Anteriores">
                    <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
                    <span id="span-from" class="badge"></span>
                </button>
            </li>
            <li role="presentation">
                <button class="btn btn-primary ladda-button btn-sm btn-submit-form" data-style="expand-left" id="btn-next" data-page="next" data-toggle="tooltip" data-placement="top" title="Siguientes">
                    <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
                    <span id="span-to" class="badge"></span>
                </button>
            </li>
            <li role="presentation">
                <button class="btn btn-primary ladda-button btn-sm btn-submit-form" data-style="expand-left" id="btn-last" data-page="last" data-toggle="tooltip" data-placement="top" title="Última página">
                    <span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
                    <span id="span-last-page" class="badge"></span>
                </button>
            </li>
            <li role="presentation">
                <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Total Registros">
                    <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                    <span id="span-total" class="badge"></span>
                </button>
            </li>

        </ul>
    </div>
    {{ Form::hidden('hide-next',null) }}
    {{ Form::hidden('hide-prev',null) }}
    {{ Form::hidden('hide-first',1) }}
    {{ Form::hidden('hide-last',null) }}
</div>
{{ Form::close() }}
<div class="row" style="width: 100%;overflow-x: scroll">
    <div class="col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>DNI</th>
                <th>Fuerza</th>
                <th>Personal</th>
                <th>Grado</th>
                <th>Legajo</th>
                <th>Caracter declaración</th>
                <th>Periodo Fiscal</th>
                <th>Tipo Estado</th>
                <th>Fecha</th>
                <th>Fecha aceptado</th>
                <th>Fecha recibido</th>
                <th>Fecha eliminado</th>
                <th>Nro formulario</th>
                <th>Nro versión</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody id="table" >
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalSendEmail" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Atención</h4>
            </div>
            <div class="modal-body">
                ¿Confirma el envio del email?
            </div>
            <div class="modal-footer">
                <button id="modal-btn-cancel" data-dismiss="modal" class="btn btn-success btn-sm">Cancelar</button>
                <button id="modal-btn-confirm" class="btn btn-primary btn-sm" data-href="">Confirmar</button>
            </div>
        </div>
    </div>
</div>


@stop