@extends ('header')
@section('content')

<table class="table table-hover">
    <thead>
    <tr>
        <th>Titulo</th>
        <th>Informes</th>
        <th>Fecha</th>
        <th>Temario</th>
        <th>Dirección</th>
        <th colspan="2">Acciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($declaracionesJuradas as $key => $value)
    <tr>
        <td>{{ $value->titulo }}</td>
        <td>{{ $value->mesa->descripcion }}</td>
        <td>{{ $value->fecha   }}</td>
        <td>{{ substr($value->temario,0,140) }}...</td>
        <td>{{ $value->lugar->direccion or '' }}</td>
        <td>
            @if(Auth::user()->id_perfil != 1)
                <a class="glyphicon glyphicon-pencil"    href="{{ route('mesa.edit',$value->id) }}"></a>
                <a class="glyphicon glyphicon-remove"    href="{{ route('mesa.destroy_logic',$value->id) }}"></a>
            @endif
        </td>
        <td><a class="glyphicon glyphicon-download-alt" href="{{ route('mesa.pdf',$value->id) }}" target="_blank"></a></td>
    </tr>
    @endforeach
    </tbody>
</table>

{{ $mesa->links() }}

@stop