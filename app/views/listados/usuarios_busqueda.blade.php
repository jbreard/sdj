@extends('header')
@section('scripts')
    @include('listados.javascript.usuarios_busqueda')
@endsection
@section('content')
<style media="all" type="text/css">
    .usuario_0 {
        background-color: #c9302c !important;
    }

    .usuario_1 {
        background-color: #5cb85c !important;
    }
</style>
@if(Session::has('success'))
    <br>
    <div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
    <br>
@endif
<div class="row">
    <div class="col-md-12"><br></div>
</div>
{{ Form::open(array('id' => 'form-search')) }}
<div class="row">
    <div class="form-inline form-group col-md-12">
        {{ Form::text('nombre',null,['class' => 'form-control class_find','placeholder' => 'Nombre','maxlength' => 10]) }}
        {{ Form::text('apellido',null,['class' => 'form-control class_find','placeholder' => 'Apellido','maxlength' => 10]) }}
        {{ Form::text('dni',null,['class' => 'form-control class_find','placeholder' => 'DNI','maxlength' => 8]) }}
        {{ Form::text('username',null,['class' => 'form-control class_find','placeholder' => 'usuario','maxlength' => 100]) }}
        {{ Form::text('email',null,['class' => 'form-control class_find','placeholder' => 'email','maxlength' => 8]) }}
    </div>
</div>
<div class="row">
    <div class="form-inline form-group col-md-12">

        <div class="row">
            <div class="form-inline form-group col-md-5">
                {{ Form::select('id_fuerza',$combo_fuerzas,null,array('class' => 'form-control select2 class_find','id' => 'id_fuerza')) }}
                {{ Form::select('id_perfil',$perfiles,null,array('class' => 'form-control select2','id'=>'id_perfil')) }}
            </div>
            <div class="form-inline form-group col-md-2">
                <select id="activo" name="activo" class="form-control select2 class_find" multiple size="2">
                    @foreach($estados as $key=>$estado)
                        <option value="{{$key}}">{{$estado}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-inline form-group col-md-12">
	<span class="label label-danger">No Habilitado</span>
	<span class="label label-success">Habilitado</span>
    </div>
</div>
<div class="row">
    <div class="form-inline form-group col-md-12">
        <button class="btn btn-primary navbar-btn ladda-button btn-sm btn-submit-form" data-style="expand-left" data-page="search"><span class="ladda-label">Buscar</span></button>
    </div>
</div>
<div class="row">
    <div class="form-inline form-group col-md-12 div-control-paginate">
        <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active">
                <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Página actual">
                    <span class="glyphicon glyphicon-home ladda-label" aria-hidden="true"></span>
                    <span id="span-current-page" class="badge"></span>
                </button>
            </li>
            <li role="presentation">
                <button class="btn btn-primary ladda-button btn-sm btn-submit-form" data-style="expand-left" id="btn-first" data-page="first" data-toggle="tooltip" data-placement="top" title="Página inicial" >
                    <span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
                    <span class="badge">1</span>
                </button>
            </li>
            <li role="presentation">
                <button class="btn btn-primary ladda-button btn-sm btn-submit-form" data-style="expand-left" id="btn-prev" data-page="prev" data-toggle="tooltip" data-placement="top" title="Anteriores">
                    <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
                    <span id="span-from" class="badge"></span>
                </button>
            </li>
            <li role="presentation">
                <button class="btn btn-primary ladda-button btn-sm btn-submit-form" data-style="expand-left" id="btn-next" data-page="next" data-toggle="tooltip" data-placement="top" title="Siguientes">
                    <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
                    <span id="span-to" class="badge"></span>
                </button>
            </li>
            <li role="presentation">
                <button class="btn btn-primary ladda-button btn-sm btn-submit-form" data-style="expand-left" id="btn-last" data-page="last" data-toggle="tooltip" data-placement="top" title="Última página">
                    <span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
                    <span id="span-last-page" class="badge"></span>
                </button>
            </li>
            <li role="presentation">
                <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Total Registros">
                    <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                    <span id="span-total" class="badge"></span>
                </button>
            </li>

        </ul>
    </div>
    {{ Form::hidden('hide-next',null) }}
    {{ Form::hidden('hide-prev',null) }}
    {{ Form::hidden('hide-first',1) }}
    {{ Form::hidden('hide-last',null) }}
</div>
{{ Form::close() }}
<div class="row" style="width: 100%;overflow-x: scroll">
    <div class="col-md-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Estado</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>DNI</th>
                <th>Usuario</th>
                <th>Perfil</th>
                <th>Fuerza</th>
                <th>Email</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tbody id="table" >
            </tbody>
        </table>
    </div>
</div>
@stop
