<script>
    var ESTADO_CARGADO = '{{array_get(Config::get('app.declaracion_jurada.estados'),0)}}';//'Cargado',
    var ESTADO_MODIFICADO = '{{array_get(Config::get('app.declaracion_jurada.estados'),1)}}';//'Modificado',
    var ESTADO_ELIMINADO = '{{array_get(Config::get('app.declaracion_jurada.estados'),2)}}';//'Eliminado',
    var ESTADO_ENTREGADO_DEPENDENCIA = '{{array_get(Config::get('app.declaracion_jurada.estados'),3)}}';//'Entregado en Dependencia',
    var ESTADO_RECIBIDO = '{{array_get(Config::get('app.declaracion_jurada.estados'),4)}}';//'Recibido',
    var ESTADO_ENTREGADO_RRHH = '{{array_get(Config::get('app.declaracion_jurada.estados'),5)}}';//'Entregado a RRHH'
    var idPerfil = {{ Auth::user()->id_perfil }};

    function mostrarDatos(data)
    {
        $("#table").empty();

        if(data){
            var len = data.length;
            var innerHtml = "";
            if(len > 0){
                for(var i=0;i<len;i++)
                {
                    var url = "{{route('declaraciones_juradas.index')}}";
                    var urlPDF = url + "/" + data[i].hash + "/export_pdf";
                    var urlRecibido = url + "/" + data[i].hash + "/updateRecibida";
                    var urlAceptado = url + "/" + data[i].hash + "/updateAceptada";
                    var urlEntregadoARecursosHumanos = url + "/" + data[i].hash + "/updateEntregadoARecursosHumanos";
                    var urlEliminada = url + "/" + data[i].hash + "/updateEliminada";
                    var urlSendMail = "{{route('mail.sendMail')}}" + "?email=" + data[i].persona.email + "&hash=" + data[i].hash;
                    var eliminada = (data[i].deleted_at != "");
                    var linkSendMail = '';

//                    var grado = data[i].persona.grado.descripcion || '';

                    innerHtml += "<tr>";
                    innerHtml += "<td>"+data[i].persona.nombres+"</td>";
                    innerHtml += "<td>"+data[i].persona.apellido+"</td>";
                    innerHtml += "<td>"+data[i].persona.documento+"</td>";

                    if(data[i].persona.fuerza)
                        innerHtml += "<td>"+data[i].persona.fuerza.descripcion+"</td>";
                    else
                        innerHtml += "<td></td>";

                    if(data[i].persona.tipo_personal)
                        innerHtml += "<td>"+data[i].persona.tipo_personal.descripcion+"</td>";
                    else
                        innerHtml += "<td></td>";

                    if(data[i].persona.grado)
                        innerHtml += "<td>"+data[i].persona.grado.descripcion+"</td>";
                    else
                        innerHtml += "<td></td>";
                    innerHtml += "<td>"+data[i].persona.legajo_personal+"</td>";
                    if(data[i].caracter)
                        innerHtml += "<td>"+data[i].caracter.descripcion+"</td>";
                    else
                        innerHtml += "<td></td>";
                    innerHtml += "<td>"+data[i].periodo_fiscal+"</td>";
                    innerHtml += "<td>"+data[i].tipo_estado+"</td>";
                    innerHtml += "<td>"+data[i].created_at+"</td>";
                    innerHtml += "<td>"+data[i].accepted_at+"</td>";
                    innerHtml += "<td>"+data[i].received_at+"</td>";
                    innerHtml += "<td>"+data[i].deleted_at+"</td>";
                    innerHtml += "<td>"+data[i].id+"</td>";
                    innerHtml += "<td>"+data[i].version+"</td>";

                    @if(Auth::user()->id_perfil == 3)

                        if( data[i].tipo_estado == ESTADO_ELIMINADO || eliminada == true) {
                            innerHtml += "<td>" + "</td>";
                        }else if( data[i].tipo_estado == ESTADO_CARGADO || data[i].tipo_estado == ESTADO_MODIFICADO) {

                            innerHtml += "<td>" +
                                    "<a title='" + ESTADO_ENTREGADO_DEPENDENCIA + "' href='"+urlAceptado+"' style='margin-right: 10px'><i class='glyphicon glyphicon-ok' ></i></a>" +
                                    "<a title='" + ESTADO_ELIMINADO + "' href='"+urlEliminada+"' style='margin-right: 10px'><i class='glyphicon glyphicon-remove' ></i></a>" +
                                    "</td>";
                        } else if (data[i].tipo_estado == ESTADO_ENTREGADO_DEPENDENCIA) {

                            innerHtml += "<td>" +
                                    "<a title='" + ESTADO_ENTREGADO_RRHH + "' href='"+urlEntregadoARecursosHumanos+"' style='margin-right: 10px'><i class='glyphicon glyphicon-share-alt' ></i></a>" +
                                    "<a title='" + ESTADO_ELIMINADO + "' href='"+urlEliminada+"' style='margin-right: 10px'><i class='glyphicon glyphicon-remove' ></i></a>" +
                                    "</td>";

                        } else {
                            innerHtml += "<td>" + "</td>";
                        }

                    @elseif(Auth::user()->id_perfil == 4)
                        innerHtml += "<td>";

                        if( data[i].tipo_estado == ESTADO_ELIMINADO || eliminada == true) {
                            innerHtml += "<td>" + "</td>";
                        } else if(data[i].tipo_estado == ESTADO_ENTREGADO_RRHH ) {
                            innerHtml += "<a title='" + ESTADO_RECIBIDO + "' href='"+urlRecibido+"' style='margin-right: 10px'><i class='glyphicon glyphicon-certificate' ></i></a>";
                        }


                        innerHtml += "</td>";

                    @else
                            @if(Auth::user()->id_perfil == 5 || Auth::user()->id_perfil == 6)
                                linkSendMail = "<a class='link-send-email' title='sendEmail' href='#' data-href='"+urlSendMail+"'><i class='glyphicon glyphicon-send' ></i></a>"
                            @endif

                            innerHtml += "<td>" +
                            "<a title='Pdf' target='_blank' href='"+urlPDF+"'><i class='glyphicon glyphicon-file' ></i></a>" +
                                linkSendMail +
                            "</td>";

                    @endif

                            innerHtml += "</tr>";
                }

                if(innerHtml != ""){
                    $("#table").html(innerHtml).removeClass("hidden");
                }
            }
        }
    }


    function darParam(param)
    {
        if(param == "")
            return "";
        else
            return "&";

    }

    function getUrlToPost() {
        return "{{ Route('listado.buscar') }}";
    }

    $(function(){

        $('[data-toggle="tooltip"]').tooltip();
        $('form#form-search').on('submit', function(event){
            event.preventDefault(); return false;
        });
        $(".div-control-paginate").hide();

        $('.datetimepicker-fecha').datetimepicker({
            format: 'DD/MM/YYYY',
            locale:'es'
        });

        $("#table").on('click','.link-send-email', function(){
            var href = $(this).data('href');
            $("#modal-btn-confirm").data('href',href);
            $('#modalSendEmail').modal('show');
        });

        $("#modal-btn-confirm").on('click',function(){

            var urlGet = $(this).data('href');

            $.get( urlGet, function(response) {
                alert("El email fue enviado");
                $('#modalSendEmail').modal('hide');
            }).fail(function() {
                alert('Hubo un inconveniente con el envio del email.'); // or whatever
            });

        });

        $('.btn-submit-form').on('click',function(e){
            e.preventDefault();

            var data = "";
            var loaderLadda = Ladda.create(this);
            var type = $(this).data('page');


            if(type == 'search') $(".div-control-paginate").show();

            if(type == 'exportToExcel') {
                $(".div-control-paginate").hide();
                data += darParam(data)+"responseType=1";
            }

            loaderLadda.start();

            if($('input:text[name=nombres]').val() != "")
                data += darParam(data)+"nombres="+$('input:text[name=nombres]').val();

            if($('input:text[name=apellido]').val() != "")
                data += darParam(data)+"apellido="+$('input:text[name=apellido]').val();

            if($('input:text[name=documento]').val() != "")
                data += darParam(data)+"documento="+$('input:text[name=documento]').val();

            if($('input:text[name=fecha_creada_desde]').val() != "") {
                data += darParam(data)+"fecha_creada_desde="+$('input:text[name=fecha_creada_desde]').val();
                if($('input:text[name=fecha_creada_hasta]').val() != "")
                    data += darParam(data)+"fecha_creada_hasta="+$('input:text[name=fecha_creada_hasta]').val();
            }

            if($('input:text[name=fecha_aceptada_desde]').val() != "") {
                data += darParam(data)+"fecha_aceptada_desde="+$('input:text[name=fecha_aceptada_desde]').val();
                if($('input:text[name=fecha_aceptada_hasta]').val() != "")
                    data += darParam(data)+"fecha_aceptada_hasta="+$('input:text[name=fecha_aceptada_hasta]').val();
            }

            if($('input:text[name=fecha_recibida_desde]').val() != "") {
                data += darParam(data)+"fecha_recibida_desde="+$('input:text[name=fecha_recibida_desde]').val();
                if($('input:text[name=fecha_recibida_hasta]').val() != "")
                    data += darParam(data)+"fecha_recibida_hasta="+$('input:text[name=fecha_recibida_hasta]').val();
            }

            if($('input:text[name=fecha_eliminada_desde]').val() != "") {
                data += darParam(data)+"fecha_eliminada_desde="+$('input:text[name=fecha_eliminada_desde]').val();
                if($('input:text[name=fecha_eliminada_hasta]').val() != "")
                    data += darParam(data)+"fecha_eliminada_hasta="+$('input:text[name=fecha_eliminada_hasta]').val();
            }




            if($('#id_fuerza').val() != "")
                data += darParam(data)+"id_fuerza="+$('#id_fuerza').val();

            if($('#id_tipo_personal').val() != "")
                data += darParam(data)+"id_tipo_personal="+$('#id_tipo_personal').val();

            if($('#id_grado').val() != "")
                data += darParam(data)+"id_grado="+$('#id_grado').val();

            if($('#tipo_estado').val() != null)
                data += darParam(data)+"tipo_estado="+$('#tipo_estado').val();

            if($('input:text[name=nro_formulario]').val() != "")
                data += darParam(data)+"nro_formulario="+$('input:text[name=nro_formulario]').val();

            if($('input:text[name=nro_version]').val() != "")
                data += darParam(data)+"nro_version="+$('input:text[name=nro_version]').val();

            if(type =='next') {
                data += darParam(data)+"page="+$('input[name=hide-next]').val();
            } else if(type =='prev') {
                data += darParam(data)+"page="+$('input[name=hide-prev]').val();
            } else if(type =='last') {
                data += darParam(data)+"page="+$('input[name=hide-last]').val();
            }

            var url = getUrlToPost();

            if(type == 'exportToExcel') {
                $("#table").empty();

                if(idPerfil != 4)
                {
                    var selectFuerza = $('#id_fuerza').val();
                    if(selectFuerza == ''){
                        alert('Para exportar debe seleccionar una fuerza.');
                        loaderLadda.stop();
                        return false;
                    }
                }


                $.post(url,data).done(function(responseData){
                    var innerHtml = '<a href="' + responseData + '">Descargar Excel</a>';
                    $("#div-export-excel").html(innerHtml);
                }).always(function() {
                    loaderLadda.stop();
                }).fail(function( jqXHR, textStatus, errorThrown ) {
                    var innerHtml = '<p class="text-danger">' + textStatus + ' type: ' + errorThrown +'</p>';
                    $("#div-export-excel").html(innerHtml);
                });

            } else {
                $.post(url,data).done(function(responseData){

                    var currentPage = responseData.current_page;
                    var lastPage = responseData.last_page;
                    var itemsTotal = responseData.total;
                    var itemsFrom = responseData.from;
                    var itemsTo = responseData.to;
                    var nextPage = currentPage + 1;
                    var prevPage = currentPage - 1;

                    $("span#span-current-page").html(currentPage);
                    $("span#span-last-page").html(lastPage);
                    $("span#span-from").html(itemsFrom);
                    $("span#span-to").html(itemsTo);
                    $("span#span-total").html(itemsTotal);
                    $('input[name="hide-next"]').val(nextPage);
                    $('input[name="hide-prev"]').val(prevPage);
                    $('input[name="hide-last"]').val(lastPage);
                    $("#div-export-excel").html('');

                    mostrarDatos(responseData.data);
                }).always(function() { loaderLadda.stop(); });
            }


        });

    });


</script>