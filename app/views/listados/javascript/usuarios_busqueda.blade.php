<script>
    var idPerfil = {{ Auth::user()->id_perfil }};

    function mostrarDatos(data)
    {
        $("#table").empty();

        if(data){
            var len = data.length;
            var innerHtml = "";
            if(len > 0){
                for(var i=0;i<len;i++)
                {
                    var url = "{{route('usuarios.index')}}";
                    var row = data[i];
                    var urlActivar = url + "/enable" + "/" + row.id ;
                    var urlDesactivar = url + "/destroy" + "/" + row.id;
                    var urlEditar = url + "/" + row.id;
                    var actions = '';

                    innerHtml += "<tr>";
                    innerHtml += '<td  style ="word-break:break-all;" class="usuario_' + row.activo + '"></td>';
                    innerHtml += "<td style ='word-break:break-all; text-transform:uppercase;'>"+row.nombre+"</td>";
                    innerHtml += "<td style ='word-break:break-all; text-transform:uppercase;'>"+row.apellido+"</td>";
                    innerHtml += "<td style ='word-break:break-all;'>"+row.dni+"</td>";
                    innerHtml += "<td style ='word-break:break-all;'>"+row.username+"</td>";
                    innerHtml += "<td style ='word-break:break-all;'>"+row.perfil.descripcion+"</td>";

                    if(row.fuerza)
                        innerHtml += "<td style ='word-break:break-all;'>"+row.fuerza.descripcion+"</td>";
                    else
                        innerHtml += "<td></td>";

                    innerHtml += "<td style ='word-break:break-all;'>"+row.email+"</td>";

                    @if(Auth::user()->id_perfil==6 || Auth::user()->id_perfil==1 )
                    actions = '<a class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Editar" href="' +
                            urlEditar +
                            '"></a>';

                    if(row.activo == 1) {
                        actions +=" | <a title='Activar' href='"+urlDesactivar+"' style='margin-right: 10px'><i class='glyphicon glyphicon-remove' ></i></a>";
                    } else {
                        actions += " | <a title='Desactivar' href='"+urlActivar+"' style='margin-right: 10px'><i class='glyphicon glyphicon-ok' ></i></a>";
                    }
                    @endif

                    innerHtml += "<td style ='word-break:break-all;'>"+actions+"</td>";
                    innerHtml += "</tr>";
                }

                if(innerHtml != ""){
                    $("#table").html(innerHtml).removeClass("hidden");
                }
            }
        }
    }


    function darParam(param)
    {
        if(param == "")
            return "";
        else
            return "&";

    }

    function getUrlToPost() {
        return "{{ route('usuarios.buscar') }}";
    }

    $(function(){

        $('[data-toggle="tooltip"]').tooltip();
        $('form#form-search').on('submit', function(event){
            event.preventDefault(); return false;
        });
        $(".div-control-paginate").hide();

        $('.datetimepicker-fecha').datetimepicker({
            format: 'DD/MM/YYYY',
            locale:'es'
        });

        $('.btn-submit-form').on('click',function(e){
            e.preventDefault();

            var data = "";
            var loaderLadda = Ladda.create(this);
            var type = $(this).data('page');


            if(type == 'search') $(".div-control-paginate").show();

            if(type == 'exportToExcel') {
                $(".div-control-paginate").hide();
                data += darParam(data)+"responseType=1";
            }

            loaderLadda.start();

            if($('input:text[name=nombre]').val() != "")
                data += darParam(data)+"nombre="+$('input:text[name=nombre]').val();

            if($('input:text[name=username]').val() != "")
                data += darParam(data)+"username="+$('input:text[name=username]').val();

            if($('input:text[name=apellido]').val() != "")
                data += darParam(data)+"apellido="+$('input:text[name=apellido]').val();

            if($('input:text[name=email]').val() != "")
                data += darParam(data)+"email="+$('input:text[name=email]').val();

            if($('input:text[name=dni]').val() != "")
                data += darParam(data)+"dni="+$('input:text[name=dni]').val();

            if($('#id_fuerza').val() != "")
                data += darParam(data)+"id_fuerza="+$('#id_fuerza').val();

            if($('#id_perfil').val() != "")
                data += darParam(data)+"id_perfil="+$('#id_perfil').val();

            if($('#activo').val() != null)
                data += darParam(data)+"activo="+$('#activo').val();

            if(type =='next') {
                data += darParam(data)+"page="+$('input[name=hide-next]').val();
            } else if(type =='prev') {
                data += darParam(data)+"page="+$('input[name=hide-prev]').val();
            } else if(type =='last') {
                data += darParam(data)+"page="+$('input[name=hide-last]').val();
            }

            var url = getUrlToPost();

            $.post(url,data).done(function(responseData){

                var currentPage = responseData.current_page;
                var lastPage = responseData.last_page;
                var itemsTotal = responseData.total;
                var itemsFrom = responseData.from;
                var itemsTo = responseData.to;
                var nextPage = currentPage + 1;
                var prevPage = currentPage - 1;

                $("span#span-current-page").html(currentPage);
                $("span#span-last-page").html(lastPage);
                $("span#span-from").html(itemsFrom);
                $("span#span-to").html(itemsTo);
                $("span#span-total").html(itemsTotal);
                $('input[name="hide-next"]').val(nextPage);
                $('input[name="hide-prev"]').val(prevPage);
                $('input[name="hide-last"]').val(lastPage);
                $("#div-export-excel").html('');

                mostrarDatos(responseData.data);
            }).always(function() { loaderLadda.stop(); });


        });

    });


</script>