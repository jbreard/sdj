<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
</head>
<body>
<table>
    <tr>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Dni</th>
        <th>Fuerza</th>
        <th>Tipo Estado</th>
        <th>Fecha</th>
        <th>Fecha aceptado</th>
        <th>Fecha recibido</th>
        <th>Fecha eliminado</th>
        <th>Nro formulario</th>
        <th>Nro version</th>
    </tr>
    @foreach($items as $item)
        <tr>
            <td>{{ array_get($item,'nombres') }}</td>
            <td>{{ array_get($item,'apellido') }}</td>
            <td>{{ array_get($item,'documento') }}</td>
            <td>{{ array_get($item,'fuerza') }}</td>
            <td>{{ array_get($item,'tipo_estado') }}</td>
            <td>{{ array_get($item,'created_at') }}</td>
            <td>{{ array_get($item,'accepted_at') }}</td>
            <td>{{ array_get($item,'received_at') }}</td>
            <td>{{ array_get($item,'deleted_at') }}</td>
            <td>{{ array_get($item,'id') }}</td>
            <td>{{ array_get($item,'version') }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>