<script type="application/javascript">

    /**
     *
     * Formularios Bienes Propios
     *
     */



    var selectorForm3 = '#form-3';

    function form3CheckInmueblesPropios() {

        var tbody = $('.tbody-inmuebles_propios');

        tbody.find(".tr-1-datos-inmuebles_propios").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="inmuebles[propios]['+row+']"]' +
                '[name!="inmuebles[propios]['+row+'][id]"]' +
                '[name!="inmuebles[propios]['+row+'][otra_forma_pago]"]' +
                '[name!="inmuebles[propios]['+row+'][domicilio_piso]"]' +
                '[name!="inmuebles[propios]['+row+'][superficie_cubierta]"]' +
                '[name!="inmuebles[propios]['+row+'][superficie_descubierta]"]' +
                '[name!="inmuebles[propios]['+row+'][domicilio_depto]"]' +
                '[name!="inmuebles[propios]['+row+'][otro_pais_provincia]"]' +
                '[name!="inmuebles[propios]['+row+'][otro_pais_localidad]"]' +
                '[name!="inmuebles[propios]['+row+'][otro_tipo_moneda_valor_adquisicion]"]' +
                '[name!="inmuebles[propios]['+row+'][otro_tipo_moneda_valor_estimado]"]' +
                ',.select-tipo-inmueble' +
                ',.select-unidad-superficie' +
                ',.select-modo-adquisicion' +
                ',.select-tipo-moneda-valor-adquisicion' +
                ',.select-tipo-moneda-valor-estimado' +
                ',.select-caracter';

            var finds = element.find(selectorInputs);
            var tr2 = tbody.find('.tr-2-datos-inmuebles_propios-'+row);
            var findsTr2 = tr2.find(selectorInputs);

            var selectFormaPago = tr2.find('.select-forma-pago');
            var valFormaPago = selectFormaPago.val();

            var selectTipoMonedaValorAdquisicion = tr2.find('.select-tipo-moneda-valor-adquisicion');
            var valSelectTipoMonedaValorAdquisicion = selectTipoMonedaValorAdquisicion.val();

            var selectTipoMonedaValorEstimado = tr2.find('.select-tipo-moneda-valor-estimado');
            var valSelectTipoMonedaValorEstimado = selectTipoMonedaValorEstimado.val();

            var otraFormaPago = tr2.find('input[name="inmuebles[propios]['+row+'][otra_forma_pago]"]');
            var valOtraFormaPago = otraFormaPago.val();

            var otroTipoMonedaValorAdquisicion = tr2.find('input[name="inmuebles[propios]['+row+'][otro_tipo_moneda_valor_adquisicion]"]');
            var valOtroTipoMonedaValorAdquisicion = otroTipoMonedaValorAdquisicion.val();

            var otroTipoMonedaValorEstimado = tr2.find('input[name="inmuebles[propios]['+row+'][otro_tipo_moneda_valor_estimado]"]');
            var valOtroTipoMonedaValorEstimado = otroTipoMonedaValorEstimado.val();

            var totalSuperficieCubierta = element.find('input[name="inmuebles[propios]['+row+'][superficie_cubierta]"]');
            var valTotalSuperficieCubierta = totalSuperficieCubierta.val();

            var totalSuperficieDescubierta = element.find('input[name="inmuebles[propios]['+row+'][superficie_cubierta]"]');
            var valTotalSuperficieDescubierta = totalSuperficieDescubierta.val();

            var tr3 = tbody.find('.tr-3-datos-inmuebles_propios-'+row);
            var tr3DivPais = '.div-pais-'+row;
            var tr3DivProvincia = '.div-provincia-'+row;
            var tr3DivLocalidad = '.div-localidad-'+row;
            var labelErrorPais = '<label id="inmuebles[propios]['+row+'][id_pais]" class="error" for="inmuebles[propios]['+row+'][id_pais]">Completar país</label>';
            var labelErrorProvincia = '<label id="inmuebles[propios]['+row+'][id_provincia]" class="error" for="inmuebles[propios]['+row+'][id_provincia]">Completar provincia</label>';
            var labelErrorLocalidad = '<label id="inmuebles[propios]['+row+'][id_localidad]" class="error" for="inmuebles[propios]['+row+'][id_localidad]">Completar localidad</label>';

            var tr4 = tbody.find('.tr-4-datos-inmuebles_propios-'+row);

            var selectPaises = tr4.find('.select-paises-propios');
            var valPais = selectPaises.select2().val();

            var selectProvincias = tr4.find('.select2-provincias');
            var valProvincia = selectProvincias.select2().val();

            var selectLocalidad = tr4.find('.select-localidad');
            var valLocalidad = selectLocalidad.select2().val();

            var otroPaisProvincia = tr4.find('input[name="inmuebles[propios]['+row+'][otro_pais_provincia]"]');
            var valOtroPaisProvincia = otroPaisProvincia.val();

            var otroPaisLocalidad = tr4.find('input[name="inmuebles[propios]['+row+'][otro_pais_localidad]"]');
            var valOtroPaisLocalidad = otroPaisLocalidad.val();

            var tr5 = tbody.find('.tr-5-datos-inmuebles_propios-'+row);
            var findsTr5 = tr5.find(selectorInputs);

            if(!empty(valFormaPago)) completedFields = true;

            if(!empty(valSelectTipoMonedaValorAdquisicion)) completedFields = true;

            if(!empty(valSelectTipoMonedaValorEstimado)) completedFields = true;

            if(!empty(valPais)) completedFields = true;

            if(valTotalSuperficieCubierta != '') completedFields = true;

            if(valTotalSuperficieDescubierta != '') completedFields = true;

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr2.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr5.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(completedFields)
            {

                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if(valTotalSuperficieCubierta == ''){
                    setFormToNoValid(selectorForm3);
                    totalSuperficieCubierta.addClass('error');
                } else {
                    totalSuperficieCubierta.removeClass('error');
                }

                if(valTotalSuperficieDescubierta == ''){
                    setFormToNoValid(selectorForm3);
                    totalSuperficieDescubierta.addClass('error');
                } else {
                    totalSuperficieDescubierta.removeClass('error');
                }


                findsTr2.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valSelectTipoMonedaValorAdquisicion)) {
                    setFormToNoValid(selectorForm3);
                    selectTipoMonedaValorAdquisicion.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorAdquisicion == 4 && empty(valOtroTipoMonedaValorAdquisicion)) {
                        setFormToNoValid(selectorForm3);
                        otroTipoMonedaValorAdquisicion.addClass('error');
                    } else {
                        otroTipoMonedaValorAdquisicion.removeClass('error');
                    }

                    selectTipoMonedaValorAdquisicion.removeClass('error');

                }

                if (empty(valSelectTipoMonedaValorEstimado)) {
                    setFormToNoValid(selectorForm3);
                    selectTipoMonedaValorEstimado.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorEstimado == 4 && empty(valOtroTipoMonedaValorEstimado)) {
                        setFormToNoValid(selectorForm3);
                        otroTipoMonedaValorEstimado.addClass('error');
                    } else {
                        otroTipoMonedaValorEstimado.removeClass('error');
                    }

                    selectTipoMonedaValorEstimado.removeClass('error');

                }

                if (empty(valFormaPago)) {
                    setFormToNoValid(selectorForm3);
                    selectFormaPago.addClass('error');
                } else {

                    if (valFormaPago == 5 && empty(valOtraFormaPago)) {
                        setFormToNoValid(selectorForm3);
                        otraFormaPago.addClass('error');
                    } else {
                        otraFormaPago.removeClass('error');
                    }

                    selectFormaPago.removeClass('error');

                }



                findsTr5.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valPais)) {
                    setFormToNoValid(selectorForm3);
                    tr3.find(tr3DivPais).html(labelErrorPais);
                    tr3.find(tr3DivProvincia).html('');
                    tr3.find(tr3DivLocalidad).html('');
                } else {

                    if (valPais == 13) {//Pais = Argentina

                        if(empty(valProvincia)) {
                            setFormToNoValid(selectorForm3);
                            tr3.find(tr3DivProvincia).html(labelErrorProvincia);
                        } else {
                            tr3.find(tr3DivProvincia).html('');
                        }

                        if(empty(valLocalidad)) {
                            setFormToNoValid(selectorForm3);
                            tr3.find(tr3DivLocalidad).html(labelErrorLocalidad);
                        } else {
                            tr3.find(tr3DivLocalidad).html('');
                        }

                    } else {

                        if(empty(valOtroPaisProvincia)) {
                            setFormToNoValid(selectorForm3);
                            otroPaisProvincia.addClass('error');
                        } else {
                            otroPaisProvincia.removeClass('error');
                        }

                        if(empty(valOtroPaisLocalidad)) {
                            setFormToNoValid(selectorForm3);
                            otroPaisLocalidad.addClass('error');
                        } else {
                            otroPaisLocalidad.removeClass('error');
                        }

                    }

                    tr3.find(tr3DivPais).html('');

                }
            }

        });


    }

    /******************************************************************************************************************/

    function form3CheckVehiculosPropios() {
        var tbody = $('.tbody-vehiculos_propios');

        tbody.find(".tr-1-datos-vehiculos_propios").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="vehiculos[propios]['+row+']"]' +
                    '[name!="vehiculos[propios]['+row+'][id]"]' +
                    '[name!="vehiculos[propios]['+row+'][otro_pais_provincia]"]' +
                    '[name!="vehiculos[propios]['+row+'][otro_pais_localidad]"]' +
                    '[name!="vehiculos[propios]['+row+'][otro_tipo_moneda_valor_seguro]"]' +
                    '[name!="vehiculos[propios]['+row+'][otro_tipo_moneda_valor_estimado]"]' +
                    ',.select-tipo-vehiculo' +
                    ',.select-caracter' +
                    ',.select-modo-adquisicion' +
                    ',.select-tipo-moneda-valor-estimado' +
                    ',.select-tipo-moneda-valor-seguro' +
                    ',.select-forma-pago' +
                    '';

            var finds = element.find(selectorInputs);

            var tr2 = tbody.find('.tr-2-datos-vehiculos_propios-'+row);
            var findsTr2 = tr2.find(selectorInputs);

            var selectTipoMonedaValorSeguro = tr2.find('.select-tipo-moneda-valor-seguro');
            var valSelectTipoMonedaValorSeguro = selectTipoMonedaValorSeguro.val();

            var selectTipoMonedaValorEstimado = tr2.find('.select-tipo-moneda-valor-estimado');
            var valSelectTipoMonedaValorEstimado = selectTipoMonedaValorEstimado.val();

            var otroTipoMonedaValorSeguro = tr2.find('input[name="vehiculos[propios]['+row+'][otro_tipo_moneda_valor_seguro]"]');
            var valOtroTipoMonedaValorSeguro = otroTipoMonedaValorSeguro.val();

            var otroTipoMonedaValorEstimado = tr2.find('input[name="vehiculos[propios]['+row+'][otro_tipo_moneda_valor_estimado]"]');
            var valOtroTipoMonedaValorEstimado = otroTipoMonedaValorEstimado.val();

            var tr3 = tbody.find('.tr-3-datos-vehiculos_propios-'+row);
            var findsTr3 = tr3.find(selectorInputs);

            var tr5 = tbody.find('.tr-5-datos-vehiculos_propios-'+row);
            var tr5DivPais = '.div-pais-'+row;
            var tr5DivProvincia = '.div-provincia-'+row;
            var tr5DivLocalidad = '.div-localidad-'+row;
            var labelErrorPais = '<label id="vehiculos[propios]['+row+'][id_pais]" class="error" for="vehiculos[propios]['+row+'][id_pais]">Completar país</label>';
            var labelErrorProvincia = '<label id="vehiculos[propios]['+row+'][id_provincia]" class="error" for="vehiculos[propios]['+row+'][id_provincia]">Completar provincia</label>';
            var labelErrorLocalidad = '<label id="vehiculos[propios]['+row+'][id_localidad]" class="error" for="vehiculos[propios]['+row+'][id_localidad]">Completar localidad</label>';

            var tr4 = tbody.find('.tr-4-datos-vehiculos_propios-'+row);

            var selectPaises = tr4.find('.select-paises-vehiculos-propios');
            var valPais = selectPaises.select2().val();

            var selectProvincias = tr4.find('.select2-provincias');
            var valProvincia = selectProvincias.select2().val();

            var selectLocalidad = tr4.find('.select-localidad');
            var valLocalidad = selectLocalidad.select2().val();

            var otroPaisProvincia = tr4.find('input[name="vehiculos[propios]['+row+'][otro_pais_provincia]"]');
            var valOtroPaisProvincia = otroPaisProvincia.val();

            var otroPaisLocalidad = tr4.find('input[name="vehiculos[propios]['+row+'][otro_pais_localidad]"]');
            var valOtroPaisLocalidad = otroPaisLocalidad.val();

            if(!empty(valSelectTipoMonedaValorSeguro)) completedFields = true;

            if(!empty(valSelectTipoMonedaValorEstimado)) completedFields = true;

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr2.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr3.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(!empty(valPais)) completedFields = true;

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                findsTr2.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valSelectTipoMonedaValorSeguro)) {
                    setFormToNoValid(selectorForm3);
                    selectTipoMonedaValorSeguro.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorSeguro == 4 && empty(valOtroTipoMonedaValorSeguro)) {
                        setFormToNoValid(selectorForm3);
                        otroTipoMonedaValorSeguro.addClass('error');
                    } else {
                        otroTipoMonedaValorSeguro.removeClass('error');
                    }

                    selectTipoMonedaValorSeguro.removeClass('error');

                }

                if (empty(valSelectTipoMonedaValorEstimado)) {
                    setFormToNoValid(selectorForm3);
                    selectTipoMonedaValorEstimado.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorEstimado == 4 && empty(valOtroTipoMonedaValorEstimado)) {
                        setFormToNoValid(selectorForm3);
                        otroTipoMonedaValorEstimado.addClass('error');
                    } else {
                        otroTipoMonedaValorEstimado.removeClass('error');
                    }

                    selectTipoMonedaValorEstimado.removeClass('error');

                }

                findsTr3.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valPais)) {
                    setFormToNoValid(selectorForm3);
                    tr5.find(tr5DivPais).html(labelErrorPais);
                    tr5.find(tr5DivProvincia).html('');
                    tr5.find(tr5DivLocalidad).html('');
                } else {

                    if (valPais == 13) {//Pais = Argentina

                        if(empty(valProvincia)) {
                            setFormToNoValid(selectorForm3);
                            tr5.find(tr5DivProvincia).html(labelErrorProvincia);
                        } else {
                            tr5.find(tr5DivProvincia).html('');
                        }

                        if(empty(valLocalidad)) {
                            setFormToNoValid(selectorForm3);
                            tr5.find(tr5DivLocalidad).html(labelErrorLocalidad);
                        } else {
                            tr5.find(tr5DivLocalidad).html('');
                        }

                    } else {

                        if(empty(valOtroPaisProvincia)) {
                            setFormToNoValid(selectorForm3);
                            otroPaisProvincia.addClass('error');
                        } else {
                            otroPaisProvincia.removeClass('error');
                        }

                        if(empty(valOtroPaisLocalidad)) {
                            setFormToNoValid(selectorForm3);
                            otroPaisLocalidad.addClass('error');
                        } else {
                            otroPaisLocalidad.removeClass('error');
                        }

                    }

                    tr5.find(tr5DivPais).html('');

                }

            }

        });

    }

    /******************************************************************************************************************/

    function form3CheckCuentasBancarias() {
        var tbody = $('.tbody-cuentas_bancarias');

        tbody.find(".tr-datos-cuentas_bancarias").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="cuentas_bancarias['+row+']"]' +
                '[name!="cuentas_bancarias['+row+'][id]"]' +
                '[name!="cuentas_bancarias['+row+'][otro_tipo_moneda]"]' +
                ',.select-tipo-banco' +
                ',.select-tipo-moneda';

            var selectTipoMoneda = element.find('.select-tipo-moneda');
            var valTipoMoneda = selectTipoMoneda.val();

            var otroTipoMoneda = element.find('input[name="cuentas_bancarias['+row+'][otro_tipo_moneda]"]');
            var valOtroTipoMoneda = otroTipoMoneda.val();

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoMoneda == 4 && empty(valOtroTipoMoneda)) {
                    setFormToNoValid(selectorForm3);
                    otroTipoMoneda.addClass('error');
                } else {
                    otroTipoMoneda.removeClass('error');
                }

            }

        });
    }

    /******************************************************************************************************************/

    function form3CheckCuentasBancariasExterior() {
        var tbody = $('.tbody-cuentas_bancarias_exterior');

        tbody.find(".tr-datos-cuentas_bancarias_exterior").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="cuentas_bancarias_exterior['+row+']"]' +
                '[name!="cuentas_bancarias_exterior['+row+'][id]"]' +
                '[name!="cuentas_bancarias_exterior['+row+'][otro_tipo_moneda]"]' +
                ',.select-tipo-banco' +
                ',.select-tipo-moneda';

            var selectTipoMoneda = element.find('.select-tipo-moneda');
            var valTipoMoneda = selectTipoMoneda.val();

            var otroTipoMoneda = element.find('input[name="cuentas_bancarias_exterior['+row+'][otro_tipo_moneda]"]');
            var valOtroTipoMoneda = otroTipoMoneda.val();

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoMoneda == 4 && empty(valOtroTipoMoneda)) {
                    setFormToNoValid(selectorForm3);
                    otroTipoMoneda.addClass('error');
                } else {
                    otroTipoMoneda.removeClass('error');
                }

            }

        });
    }

    /******************************************************************************************************************/

    function form3CheckBonosTitulosAcciones() {
        var tbody = $('.tbody-bonos_titulos_acciones');

        tbody.find(".tr-datos-bonos_titulos_acciones").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="bonos_titulos_acciones['+row+']"]' +
                '[name!="bonos_titulos_acciones['+row+'][id]"]' +
                '[name!="bonos_titulos_acciones['+row+'][otro_tipo_bono]"]' +
                '[name!="bonos_titulos_acciones['+row+'][fecha_adquisicion]"]' +
                ',.select-tipo-bono' +
                '';

            var selectTipoBono = element.find('.select-tipo-bono');
            var valTipoBono = selectTipoBono.val();

            var otroTipoBono = element.find('input[name="bonos_titulos_acciones['+row+'][otro_tipo_bono]"]');
            var valOtroTipoBono = otroTipoBono.val();

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoBono == 4 && empty(valOtroTipoBono)) {
                    setFormToNoValid(selectorForm3);
                    otroTipoBono.addClass('error');
                } else {
                    otroTipoBono.removeClass('error');
                }

            }

        });
    }

    /******************************************************************************************************************/

    function form3CheckDerechosExpectativaPropios() {
        var tbody = $('.tbody-derecho_expectativa_propios');

        tbody.find(".tr-datos-derecho_expectativa_propios").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="derecho_expectativa[propios]['+row+']"]' +
                '[name!="derecho_expectativa[propios]['+row+'][id]"]' +
                '[name!="derecho_expectativa[propios]['+row+'][cantidad_cuotas_pagas]"]' +
                '[name!="derecho_expectativa[propios]['+row+'][cantidad_cuotas_restantes]"]' +
                ',.select-tipo-derecho-expectativa' +
                ',.select-tipo-fuente' +
                '';

            var selectTipoFuente = element.find('.select-tipo-fuente');
            var valTipoFuente = selectTipoFuente.val();

            var cantidadCuotasPagas = element.find('input[name="derecho_expectativa[propios]['+row+'][cantidad_cuotas_pagas]"]');
            var valCantidadCuotasPagas = cantidadCuotasPagas.val();

            var cantidadCuotasRestantes = element.find('input[name="derecho_expectativa[propios]['+row+'][cantidad_cuotas_restantes]"]');
            var valCantidadCuotasRestantes = cantidadCuotasRestantes.val();

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoFuente == 9 || valTipoFuente == 10) {

                    if(empty(valCantidadCuotasPagas)) {
                        setFormToNoValid(selectorForm3);
                        cantidadCuotasPagas.addClass('error');
                    } else {
                        cantidadCuotasPagas.removeClass('error');
                    }

                    if(empty(valCantidadCuotasRestantes)){
                        setFormToNoValid(selectorForm3);
                        cantidadCuotasRestantes.addClass('error');
                    } else {
                        cantidadCuotasRestantes.removeClass('error');
                    }

                } else {
                    cantidadCuotasPagas.removeClass('error');
                    cantidadCuotasRestantes.removeClass('error');
                }

            }

        });
    }

    /******************************************************************************************************************/

    function form3CheckOtrosBienes() {
        var tbody = $('.tbody-bienes_propios');

        tbody.find(".tr-datos-bienes_propios").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="bienes[propios]['+row+']"]' +
                '[name!="bienes[propios]['+row+'][id]"]' +
                ',.select-tipo-caracter' +
                ',.select-tipo-adquisicion' +
                '';

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });
            }

        });
    }

    /******************************************************************************************************************/

    function form3CheckDineroEfectivo() {
        var tbody = $('.tbody-dinero_efectivo');

        tbody.find(".tr-datos-dinero_efectivo").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="dinero_efectivo['+row+']"]' +
                '[name!="dinero_efectivo['+row+'][id]"]' +
                '[name!="dinero_efectivo['+row+'][otro_tipo_moneda]"]' +
                ',.select-tipo-moneda';

            var selectTipoMoneda = element.find('.select-tipo-moneda');
            var valTipoMoneda = selectTipoMoneda.val();

            var otroTipoMoneda = element.find('input[name="dinero_efectivo['+row+'][otro_tipo_moneda]"]');
            var valOtroTipoMoneda = otroTipoMoneda.val();

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoMoneda == 4 && empty(valOtroTipoMoneda)) {
                    setFormToNoValid(selectorForm3);
                    otroTipoMoneda.addClass('error');
                } else {
                    otroTipoMoneda.removeClass('error');
                }
            }

        });
    }

    /******************************************************************************************************************/

    function form3CheckAcreencias() {
        var tbody = $('.tbody-acreencias');

        tbody.find(".tr-datos-acreencias").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="acreencias['+row+']"]' +
                '[name!="acreencias['+row+'][id]"]' +
                '[name!="acreencias['+row+'][otro_tipo_moneda]"]' +
                '[name!="acreencias['+row+'][fecha]"]' +
                ',.select-tipo-moneda';

            var selectTipoMoneda = element.find('.select-tipo-moneda');
            var valTipoMoneda = selectTipoMoneda.val();

            var otroTipoMoneda = element.find('input[name="acreencias['+row+'][otro_tipo_moneda]"]');
            var valOtroTipoMoneda = otroTipoMoneda.val();

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm3);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoMoneda == 4 && empty(valOtroTipoMoneda)) {
                    setFormToNoValid(selectorForm3);
                    otroTipoMoneda.addClass('error');
                } else {
                    otroTipoMoneda.removeClass('error');
                }
            }

        });
    }


</script>