<script type="application/javascript">

    /**
     * Formulario Datos Familiares
     */

    function form2CheckCamposHijos() {

        var tbody = $('.tbody-hijo');

        tbody.find(".tr-datos-hijo").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="datos_hijo['+row+']"]' +
                '[name!="datos_hijo['+row+'][id]"]';

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            })

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });
            }

        });

    }

    function form2CheckConyugeNoTrabaja() {
        var selectsNoTrabaja = $('.tbody-conyuge').find(selectConyugeTrabaja);

        selectsNoTrabaja.each(function (index) {

            var element = $(this);
            var row = element.data('row');
            var tbodyConyuge = $('.tbody-conyuge');
            var selectPersonalFuerza = tbodyConyuge.find('.select-personal-fuerza-conyuge-' + row);
            var selectFuerza = tbodyConyuge.find('.select-fuerza-conyuge-' + row);
            var selectTipoCategoriaTributaria = tbodyConyuge.find('.select-tipo-categoria-tributaria-' + row);

            var selectorInputs = 'input[name*="datos_conyuge['+row+']"]' +
                '[name!="datos_conyuge['+row+'][id]"]' +
                ',.select-tipo-categoria-tributaria-' + row;

            var valTrabaja = element.val();
            var valPersonalFuerza = selectPersonalFuerza.val();
            var valFuerza = selectFuerza.val();

            if (valTrabaja == 2) {

                var trDatosConyugeTrabaja = $('.tbody-conyuge').find('.tr-3-datos-conyuge-'+row);

                trDatosConyugeTrabaja.find(selectorInputs).each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    }else {
                        input.removeClass('error');
                    }
                });


                if (empty(valPersonalFuerza)) {
                    setFormToNoValid('#form-2');
                    selectPersonalFuerza.addClass('error');
                } else {

                    if (valPersonalFuerza == 2 && empty(valFuerza)) {
                        setFormToNoValid('#form-2');
                        selectFuerza.addClass('error');
                    } else {
                        selectFuerza.removeClass('error');
                    }

                    selectPersonalFuerza.removeClass('error');

                }

            } else {
                selectPersonalFuerza.removeClass('error');
                selectFuerza.removeClass('error');
                selectTipoCategoriaTributaria.removeClass('error');
            }

        });
    }

    function form2checkCamposConyuge() {

        var tbody = $('.tbody-conyuge');

        tbody.find(".tr-1-datos-conyuge").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="datos_conyuge['+row+']"]' +
                '[name!="datos_conyuge['+row+'][id]"]' +
                ', .select-conyuge-vinculo';
            var trConyugeTrabaja = tbody.find('.tr-2-datos-conyuge-'+ row);

            var finds = element.find(selectorInputs);
            var findsTrConyugeTrabaja = trConyugeTrabaja.find(selectConyugeTrabaja);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTrConyugeTrabaja.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                findsTrConyugeTrabaja.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });
            }

        });

    }

    function form2CheckCamposOtrasPersonas() {
        var tbody = $('.tbody-otra_persona');

        tbody.find(".tr-datos-otra_persona").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="datos_otra_persona['+row+']"]' +
                '[name!="datos_otra_persona['+row+'][id]"]';

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });
            }

        });
    }
</script>