@include('formularios.declaracion_jurada.javascript.modulos.external_functions')
@include('formularios.declaracion_jurada.javascript.modulos.methods_jquery_validate')
@include('formularios.declaracion_jurada.javascript.modulos.on_changes_functions')
@include('formularios.declaracion_jurada.javascript.modulos.validations.form_2')
@include('formularios.declaracion_jurada.javascript.modulos.validations.form_3')
@include('formularios.declaracion_jurada.javascript.modulos.validations.form_4')
@include('formularios.declaracion_jurada.javascript.modulos.validations.form_7')
@include('formularios.declaracion_jurada.javascript.modulos.validations.form_8')
@include('formularios.declaracion_jurada.javascript.modulos.events')

<script type="application/javascript">

    var liSubmitForm = $('.li-submit-form');
    var selectorDateTimePicker = '.datetimepicker-fecha-nacimiento';
    var selectFuerzaPerteneciente = '.select2-fuerza-perteneciente';
    var selectProvincia = '.select2-provincias';
    var selectOrigen = '.select-origen';
    var selectTipoBanco = '.select-tipo-banco';
    var selectEntidadEmisora = '.select-entidad-emisora';
    var selectTipoMoneda = '.select-tipo-moneda';
    var selectTipoDeuda = '.select-tipo-deuda';
    var selectTipoBien = '.select-tipo-bien';
    var selectFormaPago = '.select-forma-pago';
    var selectorSelects2 = '.select2';
    var selectPersonal = '.select-personal';
    var selectSituacionRevista = '.select-situacion-revista';
    var selectTipoIngreso = '.select-tipo-ingreso';
    var selectTipoIngresoExterno = '.select-tipo-ingreso-externo';
    var selectTipoIngresoExtraordinario = '.select-ingreso-extraordinario';
    var selectTipoFuente = '.select-tipo-fuente';
    var selectTipoBono = '.select-tipo-bono';
    var selectCaracterDeclaracion = '.select2-caracter-declaracion';

    var selectPaisesInmueblesPropios = '.select-paises-propios';
    var selectPaisesInmueblesConyuge = '.select-paises-conyuge';
    var selectPaisesInmueblesHijos = '.select-paises-hijos';

    var selectPaisesVehiculosPropios = '.select-paises-vehiculos-propios';
    var selectPaisesVehiculosConyuge = '.select-paises-vehiculos-conyuge';
    var selectPaisesVehiculosHijos = '.select-paises-vehiculos-hijos';

    var objectDateTimePicker = $(formsToPost).find(selectorDateTimePicker);
    var objectSelect2 = $(selectorSelects2);

    /*
    *
    * Variables para validacion Jquery Validate
    *
    */

    var messagesForm1 = {
        "datos_personales[apellido]": '{{$validaciones['apellido']}}',
        "datos_personales[nombres]": '{{$validaciones['nombres']}}',
        "datos_personales[nacimiento]": '{{$validaciones['nacimiento']}}',
        "datos_personales[domicilio_real]": '{{$validaciones['domicilio_real']}}',
        "datos_personales[cuil_cuit]": '{{$validaciones['cuil_cuit']}}',
        "datos_personales[documento]": '{{$validaciones['documento']}}',
        "datos_personales[telefono]": '{{$validaciones['telefono']}}',
        "datos_personales[email]": '{{$validaciones['email']}}',
        "datos_personales[destino_revista]": '{{$validaciones['destino_revista']}}',
        "datos_personales[legajo_personal]": '{{$validaciones['legajo_personal']}}',
        "datos_personales[cargo_funcion]": '{{$validaciones['cargo_funcion']}}',
        "datos_personales[cuerpo_agrupamiento]": '{{$validaciones['cuerpo_agrupamiento']}}',
        "datos_personales[escalafon]": '{{$validaciones['escalafon']}}',
    };

    //messagesForm1 = {};

    var rulesForm1 = {
        "datos_personales[apellido]": {required: true},
        "datos_personales[nombres]": {required: true},
        "datos_personales[nacimiento]": {required: true},
        "datos_personales[id_estado_civil]": {checkEstadoCivil: true},
        "datos_personales[id_provincia]": {checkProvincia: true},
        "datos_personales[id_tipo_agrupamiento_modalidad]": {checkAgrupamientoModalidad: true},
        "datos_personales[id_localidad]": {checkLocalidad: true},
        "datos_personales[id_tipo_personal]": {checkTipoPersonal: true},
        "datos_personales[id_grado]": {checkGrado: true},
        "datos_personales[domicilio_real]": {required: true},
        "datos_personales[documento]": {required: true},
        "datos_personales[cuil_cuit]": {required: true,checkCuilCuit: true},
        "datos_personales[telefono]": {required: true},
        "datos_personales[legajo_personal]": {required: true},
        "datos_personales[id_situacion_revista]": {checkSelectDefault: true},
        "datos_personales[id_tipo_sexo]": {checkTipoGenero: true},
        "datos_personales[id_fuerza]": {checkFuerza: true},
        "datos_personales[email]": {required: true, email: true},
        "datos_personales[destino_revista]": {required: true},
        "datos_personales[cargo_funcion]": {required: true},
        "datos_personales[cuerpo_agrupamiento]": {checkCuerpoAgrupamiento: true},
        "datos_personales[escalafon]": {checkEscalafon: true},
        "datos_personales[llamados_servicio]": {checkLlamadosPrestarServicio: true},
    };

    //rulesForm1 = {};

    var messagesForm6 = {};
    var rulesForm6 = {
//        "caracter_declaracion[id_tipo_acto_administrativo]": {checkCaracterBaja: true},
//        "caracter_declaracion[numero_anio]": {checkCaracterBaja: true},
//        "caracter_declaracion[fecha_acto_administrativo]": {checkCaracterBaja: true}
    };

    var messagesForm2 = {};
    var rulesForm2 = {};

    var messagesForm8 = {};
    var rulesForm8 = {};

    var messagesForm7 = {};
    var rulesForm7 = {
        "ingresos[fuerza][monto_anual_neto]": {checkIsNotEmpty: true},
        "ingresos[fuerza][monto_anual_bruto]": {checkIsNotEmpty: true},
    };

    var validatorForms = {
        '#form-1':{
            rules:rulesForm1,
            messages:messagesForm1
        },
        '#form-2':{
            rules:rulesForm2,
            messages:messagesForm2
        },
        '#form-7':{
            rules:rulesForm7,
            messages:messagesForm7
        },
        '#form-8':{
            rules:rulesForm8,
            messages:messagesForm8
        },
        '#form-6':{
            rules:rulesForm6,
            messages:messagesForm6
        }
    };

    /*-----------------------------------------------------------------------------------------------------*/


    function eliminar(id,modelo,mensaje)
    {
        $("#contenido-pregunta-1").html("");
        $("#contenido-pregunta-1").append("<h3>"+mensaje+"</h3>");

        $("input:hidden[name=id_seleccionado]").val(id);
        $("input:hidden[name=modelo]").val(modelo);
        $("#pregunta-1").modal(function(){show:true});

    }

    /** FIN **/

    var setFormToNoValid = function (selectorForm) {
        var object = $(selectorForm);

        if(object.hasClass('no-valid')) {
            //nothing
        } else {
            object.addClass('no-valid');
            object.find('div.div-error-form').show();
        }

    };

    function setDataByAccessForm()
    {
        @if(isset($access_form_data))
        var idFuerza = '{{ array_get($access_form_data,'id_fuerza','')}}';
        var apellido  = '{{array_get($access_form_data,'apellido','')}}';
        var nombres  = '{{array_get($access_form_data,'nombres','')}}';
        var dni  = '{{array_get($access_form_data,'dni','')}}';
        var legajo  = '{{array_get($access_form_data,'legajo','')}}';
        var email  = '{{array_get($access_form_data,'email','')}}';


        $(selectFuerzaPerteneciente).select2().val(idFuerza).trigger("change");
        onChange($(selectFuerzaPerteneciente),changeFuerzaPerteneciente);

        $('input[name="datos_personales[apellido]"]').val(apellido);
        $('input[name="datos_personales[apellido_fake]"]').val(apellido);
        $('input[name="datos_personales[nombres]"]').val(nombres);
        $('input[name="datos_personales[nombres_fake]"]').val(nombres);
        $('input[name="datos_personales[documento]"]').val(dni);
        $('input[name="datos_personales[documento_fake]"]').val(dni);
        $('input[name="datos_personales[legajo_personal]"]').val(legajo);
        $('input[name="datos_personales[email]"]').val(email);

        @endif
    }

    function loadDateTimePicker(object) {
        object.datetimepicker({
            format: 'DD/MM/YYYY',
            locale: 'es'
        });
    }

    function loadJqueryMask() {
        $.mask.definitions['h'] = "[A-Za-z0-9 ]";
        $(".masked-input-fecha-nacimiento").mask("99/99/9999", {placeholder: "DD/MM/AAAA"});
        $(".masked-numero-anio").mask("9?999/9999");
        $(".masked-input-cuenta-bancaria-exterior").mask("h?hhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
        $(".masked-input-superficie").mask("9?9999999");
        $(".masked-input-dni").mask("9999999?9");
        $(".masked-input-anio").mask("9999");
        $(".masked-input-porcentaje").mask("9?99");
        $(".masked-input-cuil_cuit").mask("99-99999999-9");
    }

    function loadSelect2(object) {
        object.select2();
    }

    function onChange(object,method,launch) {
        var launch = launch || true;
        var onChange = object.on('change',method);
        if(launch) onChange.change();
    }

    $(function(){

        @if(is_null($declaracion_jurada))
            setDataByAccessForm();
        @endif

        loadEventSubmitForms();

        liSubmitForm.hide();

        $("#eliminar-1").click(function(){

            var id = $("input:hidden[name=id_seleccionado]").val();
            var modelo = $("input:hidden[name=modelo]").val();
            var urlDelete = "{{route('formulario.declaraciones_juradas.eliminar')}}";


            if($.isNumeric(id))
            {
                $.ajax({
                    method: "post",
                    data: "id="+id+"&modelo="+modelo,
                    url : urlDelete,
                    success:function()
                    {
                    }
                });
            }
            $("."+modelo+"-"+id).remove();
            $("#pregunta-1").modal("hide");
//            $("#contenido-modal-1").html("Se ha guardado correctamente");
//            $("#confirmacion-1").modal(function(){show:true});

        });

        $('.nav-tabs > li:not(.active)').hide();

        /**
         * BLOQUE PARA AGREGAR FORMULARIOS A VALIDAR
         */

        /** CARACTER DE LA DECLARACION **/
        $("#form-6").validate({
            ignore: [],
            rules: rulesForm6,
            messages: messagesForm6,
        });

        /** DATOS PERSONALES **/
        $("#form-1").validate({
            ignore: [],
            rules: rulesForm1,
            messages: messagesForm1,
        });

        /** DATOS PATRIMONIALES INGRESOS **/
        $("#form-7").validate({
            ignore: [],
            rules: rulesForm7,
            messages: messagesForm7
        });

        /** FIN **/

        loadDateTimePicker(objectDateTimePicker);
        loadJqueryMask();
        loadSelect2(objectSelect2);

        //todo Simplificar esta parte en un array
        onChange($(selectFuerzaPerteneciente),changeFuerzaPerteneciente);
        onChange($(selectPersonal),changePersonal,false);
        onChange($(selectTipoIngreso),changeSelectTipoIngresoFuerza);
        onChange($(selectTipoIngresoExterno),changeSelectTipoIngresoFuerzaExterno);
        onChange($(selectTipoIngresoExtraordinario),changeSelectTipoIngresoExternoExtraordinario);

        onChange($(selectProvincia),changeSelectProvincia);
        onChange($(selectOrigen),changeSelectOrigen);
        onChange($(selectTipoBanco),changeSelectTipoBanco);
        onChange($(selectEntidadEmisora),changeSelectEntidadEmisora);
        onChange($(selectTipoMoneda),changeSelectTipoMoneda);
        onChange($(selectTipoFuente),changeSelectTipoFuente);
        onChange($(selectTipoBono),changeSelectTipoBono);
        onChange($(selectTipoDeuda),changeSelectTipoDeuda);
        onChange($(selectTipoBien),changeSelectTipoBien);
        onChange($(selectFormaPago),changeSelectFormaPago);
        onChange($(selectSituacionRevista),changeSituacionRevista);
        onChange($(selectPersonalFuerzaConyuge),changeSelectPersonalFuerzaConyuge);
        onChange($(selectCaracterDeclaracion),changeSelectCaracterDeclaracion);

        onChange($(selectPaisesInmueblesPropios),changeSelectPaisesInmueblesPropios);
        onChange($(selectPaisesInmueblesConyuge),changeSelectPaisesInmueblesConyuge);
        onChange($(selectPaisesInmueblesHijos),changeSelectPaisesInmueblesHijos);

        onChange($(selectPaisesVehiculosPropios),changeSelectPaisesVehiculosPropios);
        onChange($(selectPaisesVehiculosConyuge),changeSelectPaisesVehiculosConyuge);
        onChange($(selectPaisesVehiculosHijos),changeSelectPaisesVehiculosHijos);

        onChange($(selectConyugeTrabaja),changeSelectConyugeTrabaja);


        /**
         *
         *  Funcion que se ejecuta al agregar un nuevo registro
         *
         */
        loadEventClickBtnAddRegistry();

        loadEventClickBtnPrevious();
        loadEventClickBtnConfirm();

        /**
         *
         * Chequeo de formularios validos para seguir a la siguiente pestaña
         *
         */

        loadEventClickBtnSubmitForm6();
        loadEventClickBtnSubmitForm1();
        loadEventClickBtnSubmitForm3();
        loadEventClickBtnSubmitForm7();
        loadEventClickBtnSubmitForm8();
        loadEventClickBtnSubmitForm2();
        loadEventClickBtnSubmitForm4();

        /** Fin **/

        loadEventClickBtnSubmitForm();

    });
</script>