<div class="row">
    <div class="form-group col-md-12">
        <br>
        <a class="btn btn-primary btnPrevious" >Anterior</a>
        -
        <a class="btn btn-primary btnNext" id="btn-submit-form-7" >Siguiente</a>
    </div>
    <div class="col-md-12">
        <div class="div-error-form alert alert-danger" role="alert" style="display: none;">
            <strong>Atención: </strong>Hay campos en el formulario que debe completar para continuar la carga.
        </div>
    </div>
</div>
<div id="ingresos">
<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Ingresos ordinarios percibidos por la fuerza</span>
        <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
            <h4>Notas de carga</h4>
            <p>{{ Config::get('app.textos.notas_carga.ingresos.a'); }}</p>
        </div>
    </caption>
    <tbody>
    <tr>
        <th>Salario percibido por</th>
        <th>Monto anual neto</th>
        <th>Monto anual bruto</th>
    </tr>
    @include('formularios.declaracion_jurada.modulos.ingesos_fuerza')
    </tbody>
</table>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Ingresos extraordinarios de la fuerza</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="ingresos_extraordinarios_v2" data-count="0">[Añadir registro]</button>
    </caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Tipo de ingreso</th>
        <th>Monto neto anual total</th>
        <th>Moneda</th>
    </tr>
    </thead>
    <tbody class="tbody-ingresos_extraordinarios_v2">
    @if(is_null($declaracion_jurada))
        @include('formularios.declaracion_jurada.modulos.ingresos_extraordinarios_v2',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_ingreso_extraordinario; $i++)
            @include('formularios.declaracion_jurada.modulos.ingresos_extraordinarios_v2',['i'=>$i])
        @endfor
    @endif

    </tbody>
</table>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Ingresos recibidos por fuera de la fuerza</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="ingresos_externos_v2" data-count="0">[Añadir registro]</button>
    </caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Tipo de ingreso</th>
        <th>Monto neto anual total</th>
        <th>Moneda</th>
    </tr>
    </thead>
    <tbody class="tbody-ingresos_externos_v2">
    @if(is_null($declaracion_jurada))
        @include('formularios.declaracion_jurada.modulos.ingresos_externos_v2',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_ingreso_externo; $i++)
            @include('formularios.declaracion_jurada.modulos.ingresos_externos_v2',['i'=>$i])
        @endfor
    @endif

    </tbody>
</table>
</div>