<div class="row">
    <div class="form-group col-md-12">
        <br>
        <a class="btn btn-primary btnPrevious" >Anterior</a>
        -
        <a class="btn btn-primary btnNext" id="btn-submit-form-2">Siguiente</a>
    </div>
    <div class="col-md-12">
        <div class="div-error-form alert alert-danger" role="alert" style="display: none;">
            <strong>Atención: </strong>Hay campos en el formulario que debe completar para continuar la carga.
        </div>
    </div>
</div>
<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Cónyuge o conviviente actual</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="conyuge" data-count="0">[Añadir registro]</button>
        {{--<button type="button" class="btn btn-link btn-remove-registry" data-type_registry="conyuge" >[Remover Ultimo registro]</button>--}}
    </caption>
    <caption>
        <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
            <h4>Notas de carga</h4>
            <p>
                {{ Config::get('app.textos.notas_carga.datos_familiares.a'); }}
                <br>
            </p>
        </div>
    </caption>

    <tbody class="tbody-conyuge">
    @if(is_null($declaracion_jurada))
        @include('formularios.declaracion_jurada.modulos.conyuge',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_conyuge; $i++)
            @include('formularios.declaracion_jurada.modulos.conyuge',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<table class="table table-condensed">
    <caption>
        <span class="title">Hijas/os no emancipados</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="hijo" data-count="0">[Añadir registro]</button>
    </caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Apellido</th>
        <th>Nombres</th>
        <th>Fecha de nacimiento</th>
        <th>DNI</th>
    </tr>
    </thead>
    <tbody class="tbody-hijo">
    @if(is_null($declaracion_jurada))
        @include('formularios.declaracion_jurada.modulos.hijo',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_hijo; $i++)
            @include('formularios.declaracion_jurada.modulos.hijo',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<table class="table table-condensed">
    <caption>
        <span class="title">Otras Personas convivientes</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="otra_persona" data-count="0">[Añadir registro]</button>
    </caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Apellido</th>
        <th>Nombres</th>
        <th>Fecha de Nacimiento</th>
        <th>Vínculo/Parentesco</th>
        <th>DNI</th>
    </tr>
    </thead>
    <tbody class="tbody-otra_persona">
    @if(is_null($declaracion_jurada))
        @include('formularios.declaracion_jurada.modulos.otra_persona',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_otra_persona; $i++)
            @include('formularios.declaracion_jurada.modulos.otra_persona',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>