<div class="row">
    <div class="form-group col-md-12">
        <br>
        <a class="btn btn-primary btnPrevious" >Anterior</a>
        -
        <a class="btn btn-primary btnNext" id="btn-submit-form-3" >Siguiente</a>
    </div>
    <div class="col-md-12">
        <div class="div-error-form alert alert-danger" role="alert" style="display: none;">
            <strong>Atención: </strong>Hay campos en el formulario que debe completar para continuar la carga.
        </div>
    </div>
</div>
<div id="inmuebles">
    <table class="table table-striped table-condensed">
        <caption><span class="title">Inmuebles</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="inmuebles_propios" data-count="0">[Añadir registro]</button>
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>Notas de carga</h4>
                <p>
                    {{ Config::get('app.textos.notas_carga.inmueble.a'); }}
                    <br>
                    <br>
                    {{ Config::get('app.textos.notas_carga.inmueble.b'); }}
                    <br>
                </p>
            </div>
        </caption>

        <tbody class="tbody-inmuebles_propios">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.inmuebles_propios',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_inmueble['propio']; $i++)
                @include('formularios.declaracion_jurada.modulos.inmuebles_propios',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>
</div>

<div id="vehiculos">
    <table class="table table-striped table-condensed">
        <caption><span class="title">Vehículos: Terrestres / Embarcaciones / Aeronaves</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="vehiculos_propios" data-count="0">[Añadir registro]</button>
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>Notas de carga</h4>
                <p>
                    {{ Config::get('app.textos.notas_carga.vehiculos.a'); }}
                    <br>
                    <br>
                    {{ Config::get('app.textos.notas_carga.vehiculos.b'); }}
                </p>
            </div>
        </caption>
        <tbody class="tbody-vehiculos_propios">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.vehiculos_propios',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_vehiculo['propio']; $i++)
                @include('formularios.declaracion_jurada.modulos.vehiculos_propios',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>
</div>

<div id="cuentas_bancarias">
    <table class="table table-striped table-condensed">
        <caption>
            <span class="title">Cuentas Bancarias</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="cuentas_bancarias" data-count="0">[Añadir registro]</button>
        </caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Banco</th>
            <th>Tipo</th>
            <th>Saldo</th>
            <th>Moneda</th>
        </tr>
        </thead>
        <tbody class="tbody-cuentas_bancarias">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.cuentas_bancarias',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_cuenta_bancaria; $i++)
                @include('formularios.declaracion_jurada.modulos.cuentas_bancarias',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>

    <table class="table table-striped table-condensed">
        <caption>
            <span class="title">Cuentas Bancarias en el exterior</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="cuentas_bancarias_exterior" data-count="0">[Añadir registro]</button>
        </caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Banco</th>
            <th>Tipo</th>
            <th>Saldo</th>
            <th>Moneda</th>
        </tr>
        </thead>
        <tbody class="tbody-cuentas_bancarias_exterior">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.cuentas_bancarias_exterior',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_cuenta_bancaria_exterior; $i++)
                @include('formularios.declaracion_jurada.modulos.cuentas_bancarias_exterior',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>
</div>

<div id="bonos">
    <table class="table table-striped table-condensed">
        <caption>
            <span class="title">Bonos, títulos valores y/o acciones</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="bonos_titulos_acciones" data-count="0">[Añadir registro]</button>
        </caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Tipo</th>
            <th>Descripción</th>
            <th>Cantidad</th>
            <th>Valor nominal</th>
            <th>Entidad emisora/otorgante</th>
            <th>Fecha de adquisición</th>
        </tr>
        </thead>
        <tbody class="tbody-bonos_titulos_acciones">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.bonos_titulos_acciones',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_bonos_titulos_acciones; $i++)
                @include('formularios.declaracion_jurada.modulos.bonos_titulos_acciones',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>
</div>

<div id="derechos">
    <table class="table table-striped table-condensed">
        <caption>
            <span class="title">Derechos en expectativa</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="derecho_expectativa_propios" data-count="0">[Añadir registro]</button>
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>Notas de carga</h4>
                <p>{{ Config::get('app.textos.notas_carga.derecho_expectativa.a'); }}</p>
            </div>
        </caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Tipo</th>
            <th>Fuente</th>
        </tr>
        </thead>
        <tbody class="tbody-derecho_expectativa_propios">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.derecho_expectativa_propios',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_derecho_expectativa['propio']; $i++)
                @include('formularios.declaracion_jurada.modulos.derecho_expectativa_propios',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>
</div>

<div id="otros_bienes">
    <table class="table table-striped table-condensed">
        <caption>
            <span class="title">Otros bienes</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="bienes_propios" data-count="0">[Añadir registro]</button>
        </caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Descripción</th>
            <th>Caracter</th>
            <th>Modo de Adquisición</th>
            <th>Monto</th>
        </tr>
        </thead>
        <tbody class="tbody-bienes_propios">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.bienes_propios',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_bien['propio']; $i++)
                @include('formularios.declaracion_jurada.modulos.bienes_propios',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>
</div>

<div id="dinero">
    <table class="table table-striped table-condensed">
        <caption>
            <span class="title">Dinero en efectivo</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="dinero_efectivo" data-count="0">[Añadir registro]</button>
        </caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Moneda</th>
            <th>Origen de fondos</th>
            <th>Monto</th>
        </tr>
        </thead>
        <tbody class="tbody-dinero_efectivo">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.dinero_efectivo',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_dinero_efectivo; $i++)
                @include('formularios.declaracion_jurada.modulos.dinero_efectivo',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>
</div>

<div id="acreencias">
    <table class="table table-striped table-condensed">
        <caption>
            <span class="title">Acreencias</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="acreencias" data-count="0">[Añadir registro]</button>
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>Notas de carga</h4>
                <p>{{ Config::get('app.textos.notas_carga.acreencias.a'); }}</p>
            </div>
        </caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Identidad del deudor (Consignar nombre y apellido)</th>
            <th>Moneda</th>
            <th>Monto</th>
            <th>Fecha</th>
        </tr>
        </thead>
        <tbody class="tbody-acreencias">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.acreencias',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_acreencias; $i++)
                @include('formularios.declaracion_jurada.modulos.acreencias',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>
</div>