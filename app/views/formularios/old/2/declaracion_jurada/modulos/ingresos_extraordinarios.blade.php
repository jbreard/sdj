<?php
$declaracion_jurada['ingreso'] = [];
$notExists = !array_key_exists('extraordinario',$declaracion_jurada['ingreso']);
$id_tipo_ingreso = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['extraordinario']['id_tipo_ingreso']);
$otro_tipo_ingreso = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['extraordinario']['otro_tipo_ingreso']);
$monto_anual_neto = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['extraordinario']['monto_anual_neto']);
$id_tipo_moneda = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['extraordinario']['id_tipo_moneda']);
$otro_tipo_moneda = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['extraordinario']['otro_tipo_moneda']);
$id_salario = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['extraordinario']['id_salario']);
$id = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['extraordinario']['id']);
?>
<tr>
    <td>
        <b><span class="span-fuerza-pertenece">XXXXXX</span></b>
    </td>
    <td>
        1{{ Form::select("ingresos[extraordinario][id_tipo_ingreso]",$combo_tipos_ingresos_extraordinario, $id_tipo_ingreso,array('class' => 'form-control select-tipo-ingreso')) }}
        <div>{{ Form::text("ingresos[extraordinario][otro_tipo_ingreso]", $otro_tipo_ingreso,array('class' => 'form-control input-otro')); }}</div>
    </td>
    <td>
        {{ Form::text("ingresos[extraordinario][monto_anual_neto]", $monto_anual_neto,array('class' => 'form-control')); }}
    </td>
    <td>
        {{ Form::select("ingresos[extraordinario][id_moneda]",$combo_tipos_moneda, $id_tipo_moneda,array('class' => 'form-control select-tipo-moneda')) }}
        <div>{{ Form::text("ingresos[extraordinario][otro_tipo_moneda]", $otro_tipo_moneda,array('class' => 'form-control input-otro')); }}</div>
        {{ Form::hidden("ingresos[extraordinario][id_salario]", $id_salario,array('class'=>'datos-propios-ingresos')) }}
        {{ Form::hidden("ingresos[extraordinario][tipo_ingreso]",4,array('class' => '')); }}
        {{ Form::hidden("ingresos[extraordinario][id]", $id) }}
    </td>
</tr>