<?php
$persona = isset($declaracion_jurada['persona']['familiar']['otra_persona'][$i]) ? $declaracion_jurada['persona']['familiar']['otra_persona'][$i] : null;
$isNull = (is_null($declaracion_jurada) && is_null($persona));
$apellido = ( $isNull ? null : $persona['persona']['apellido']);
$nombres = ($isNull ? null : $persona['persona']['nombres']);
$nacimiento = ($isNull ? null : $persona['persona']['nacimiento']);
$vinculoParentesco = ($isNull ? null : $persona['persona']['vinculo_parentesco']);
$documento = ($isNull ? null : $persona['persona']['documento']);
$id = ($isNull ? "new".$i : $persona['id_familiar']);
?>
<tr class="tr-datos-otra_persona otras_convivientes-{{$id}}" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','otras_convivientes','¿Confirma que desea eliminar registro de otras personas convivientes?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>{{ Form::text("datos_otra_persona[$i][apellido]",$apellido,array('class' => 'form-control')) }}</td>
    <td>{{ Form::text("datos_otra_persona[$i][nombres]",$nombres,array('class' => 'form-control')) }}</td>
    <td>{{ Form::text("datos_otra_persona[$i][nacimiento]",$nacimiento,array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento')) }}</td>
    <td>{{ Form::text("datos_otra_persona[$i][vinculo_parentesco]",$vinculoParentesco,array('class' => 'form-control')) }}</td>
    <td>
        {{ Form::text("datos_otra_persona[$i][documento]",$documento,array('class' => 'form-control masked-input-dni','size'=>'8')) }}
        {{ Form::hidden("datos_otra_persona[$i][id]", $id) }}
    </td>
</tr>