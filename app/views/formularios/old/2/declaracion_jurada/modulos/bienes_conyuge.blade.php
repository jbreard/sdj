<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['bien']['conyuge'][$i]['id'];
?>
<tr class="bienes_conyuge-{{$id}} tr-datos-bienes_conyuge" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','bienes_conyuge','¿Confirma que desea eliminar registro de otros bienes?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>{{ Form::text("bienes[conyuge][$i][descripcion]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['bien']['conyuge'][$i]['descripcion']),array('class' => 'form-control','maxlength'=>50)); }}</td>
    <td>{{ Form::select("bienes[conyuge][$i][id_tipo_caracter]",$combo_tipos_caracter,(is_null($declaracion_jurada) ? null : $declaracion_jurada['bien']['conyuge'][$i]['id_tipo_caracter']),array('class' => 'form-control select-tipo-caracter')) }}</td>
    <td>{{ Form::select("bienes[conyuge][$i][id_tipo_adquisicion]",$combo_tipos_modos_adquisicion,(is_null($declaracion_jurada) ? null : $declaracion_jurada['bien']['conyuge'][$i]['id_tipo_adquisicion']),array('class' => 'form-control select-tipo-adquisicion')) }}</td>
    <td>
        {{ Form::text("bienes[conyuge][$i][monto]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['bien']['conyuge'][$i]['monto']),array('class' => 'form-control')); }}
        {{ Form::hidden("bienes[conyuge][$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['bien']['conyuge'][$i]['id'])) }}
    </td>
</tr>