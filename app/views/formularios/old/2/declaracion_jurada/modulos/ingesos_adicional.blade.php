<tr>
    <td>
        <b>Adicional <span class="span-fuerza-pertenece">XXXXXX</span></b>
    </td>
    <td>
        {{ Form::select("ingresos[adicional][id_moneda]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : (!array_key_exists('adicional',$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso']['adicional']['id_moneda']),array('class' => 'form-control select-tipo-moneda')) }}
        <div>{{ Form::text("ingresos[adicional][otro_tipo_moneda]",(is_null($declaracion_jurada) ? null : (!array_key_exists('adicional',$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso']['adicional']['otro_tipo_moneda']),array('class' => 'form-control input-otro')); }}</div>
    </td>
    <td>
        {{ Form::text("ingresos[adicional][monto_anual_aproximado]",(is_null($declaracion_jurada) ? null : (!array_key_exists('adicional',$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso']['adicional']['monto_anual_aproximado']),array('class' => 'form-control')); }}
        {{ Form::hidden("ingresos[adicional][id]", (is_null($declaracion_jurada) ? null : (!array_key_exists('adicional',$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso']['adicional']['id'])) }}
        {{ Form::hidden("ingresos[adicional][id_salario]", (is_null($declaracion_jurada) ? null : (!array_key_exists('adicional',$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso']['adicional']['id_salario']),array('class'=>'datos-propios-ingresos')) }}
        {{ Form::hidden("ingresos[adicional][tipo_ingreso]",2,array('class' => '')); }}
    </td>
</tr>