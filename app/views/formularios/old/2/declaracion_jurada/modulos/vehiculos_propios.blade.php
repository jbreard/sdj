<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['vehiculo']['propio'][$i]['id'];
$fechaTransferenciaDominio = (is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['fecha_transferencia_dominio']);

?>
<tr class="vehiculos_propios-{{$id}}"><td colspan="8" class="inmuebles_header_registry"># {{ $i + 1 }}
    <a title="Eliminar" onclick="eliminar('{{ $id }}','vehiculos_propios','¿Confirma que desea eliminar el vehiculo propio?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a>
    </td></tr>
<tr class="vehiculos_propios-{{$id}}">
    <th>Tipo de vehículo</th>
    <th>Tipo</th>
    <th>Marca</th>
    <th>Modelo</th>
    <th>Año fabricación</th>
    <th colspan="3">Dominio/Matrícula</th>
</tr>
<tr class="vehiculos_propios-{{$id}} tr-1-datos-vehiculos_propios" data-row="{{$i}}">
    <td>{{ Form::select("vehiculos[propios][$i][id_tipo_vehiculo_nuevo]",$combo_tipos_vehiculo,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_tipo_vehiculo_nuevo']),array('class' => 'form-control select-tipo-vehiculo')) }}</td>
    <td>{{ Form::text("vehiculos[propios][$i][tipo]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['marca']),array('class' => 'form-control', 'maxlength' => 50)); }}</td>
    <td>{{ Form::text("vehiculos[propios][$i][marca]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['marca']),array('class' => 'form-control', 'maxlength' => 50)); }}</td>
    <td>{{ Form::text("vehiculos[propios][$i][modelo]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['modelo']),array('class' => 'form-control', 'maxlength' => 50)); }}</td>
    <td>{{ Form::text("vehiculos[propios][$i][anio]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['anio']),array('class' => 'form-control masked-input-anio')); }}</td>
    <td colspan="3">{{ Form::text("vehiculos[propios][$i][dominio]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['dominio']),array('class' => 'form-control')); }}</td>
</tr>
<tr class="vehiculos_propios-{{$id}}">
    <th>Fecha de transferencia del dominio</th>
    <th>Carácter</th>
    <th>% de titularidad</th>
    <th>Valor de adquisición</th>
    <th>Moneda</th>
    <th>Valuación según seguro</th>
    <th>Moneda</th>
    <th>Modo de adquisición</th>
</tr>
<tr class="vehiculos_propios-{{$id}} tr-2-datos-vehiculos_propios-{{$i}}"  data-row="{{$i}}">
    <td>{{ Form::text("vehiculos[propios][$i][fecha_transferencia_dominio]",$fechaTransferenciaDominio,array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento')); }}</td>
    <td>{{ Form::select("vehiculos[propios][$i][id_caracter]",$combo_tipos_caracter,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_caracter']),array('class' => 'form-control select-caracter')) }}</td>
    <td>{{ Form::text("vehiculos[propios][$i][porcentaje]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['porcentaje']),array('class' => 'form-control masked-input-porcentaje')); }}</td>
    <td>{{ Form::text("vehiculos[propios][$i][valor_adquisicion]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['valor_adquisicion']),array('class' => 'form-control')); }}</td>
    <td>
        {{ Form::select("vehiculos[propios][$i][id_tipo_moneda_valor_estimado]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_tipo_moneda_valor_estimado']),array('class' => 'form-control select-tipo-moneda select-tipo-moneda-valor-estimado')) }}
        <div>{{ Form::text("vehiculos[propios][$i][otro_tipo_moneda_valor_estimado]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['otro_tipo_moneda_valor_estimado']),array('class' => 'form-control input-otro')); }}</div>
    </td>

    <td>{{ Form::text("vehiculos[propios][$i][valor_seguro]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['valor_seguro']),array('class' => 'form-control')); }}</td>
    <td>
        {{ Form::select("vehiculos[propios][$i][id_tipo_moneda_valor_seguro]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_tipo_moneda_valor_seguro']),array('class' => 'form-control select-tipo-moneda select-tipo-moneda-valor-seguro')) }}
        <div>{{ Form::text("vehiculos[propios][$i][otro_tipo_moneda_valor_seguro]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['otro_tipo_moneda_valor_seguro']),array('class' => 'form-control input-otro')); }}</div>
    </td>
    <td>{{ Form::select("vehiculos[propios][$i][id_modo_adquisicion]",$combo_tipos_modos_adquisicion,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_modo_adquisicion']),array('class' => 'form-control select-modo-adquisicion')) }}</td>

</tr>
<tr class="vehiculos_propios-{{$id}}">
    <th colspan="8">Forma de pago</th>
</tr>
<tr class="vehiculos_propios-{{$id}} tr-3-datos-vehiculos_propios-{{$i}}" data-row="{{$i}}">
    <td colspan="8">{{ Form::select("vehiculos[propios][$i][id_forma_pago]",$combo_tipos_formas_pago_vehiculo,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_forma_pago']),array('class' => 'form-control select-forma-pago')) }}</td>
</tr>
<tr class="vehiculos_propios-{{$id}}">
    <th colspan="8">Radicación</th>
</tr>
<tr class="vehiculos_propios-{{$id}} tr-5-datos-vehiculos_propios-{{$i}}">
    <th>Pais <div class="div-pais-{{$i}}"></div></th>
    <th>Provincia/Estado <div class="div-provincia-{{$i}}"></div></th>
    <th>Localidad <div class="div-localidad-{{$i}}"></div></th>
    <th colspan="3"></th>
</tr>
<tr class="vehiculos_propios-{{$id}} tr-4-datos-vehiculos_propios-{{$i}}" data-row="{{$i}}">
    <td>
        {{ Form::select("vehiculos[propios][$i][id_pais]",$combo_paises,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_pais']),array(
        'class' => 'form-control select2 select-paises-vehiculos-propios',
        'data-row'=>$i,
        'data-id-provincia' => (is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_provincia']),
        'data-id-localidad' => (is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_localidad']),
        ))}}


    </td>
    <td>
        <div class="div-vehiculo-propios-provincia-{{$i}}">
            {{ Form::select("vehiculos[propios][$i][id_provincia]",$combo_provincias,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_provincia']),array('class' => 'form-control select2 select2-provincias','data-localidad'=>"vehiculo_localidad_$i", 'data-id-localidad'=>(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_localidad']) )) }}
        </div>
        <div class="div-provincia-otro-pais-{{$i}}">
            {{ Form::text("vehiculos[propios][$i][otro_pais_provincia]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['otro_pais_provincia']),array('class' => 'form-control')); }}
        </div>
    </td>
    <td>
        <div class="div-vehiculo-propios-localidad-{{$i}}">
            {{ Form::select("vehiculos[propios][$i][id_localidad]",[],(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id_localidad']),array('class' => "form-control select2 vehiculo_localidad_$i select-localidad")) }}
        </div>
        <div class="div-localidad-otro-pais-{{$i}}">
            {{ Form::text("vehiculos[propios][$i][otro_pais_localidad]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['otro_pais_localidad']),array('class' => 'form-control')); }}
        </div>
    </td>
    <td colspan="3">
        {{ Form::hidden("vehiculos[propios][$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['propio'][$i]['id'])) }}
    </td>
</tr>