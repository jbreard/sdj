<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['inmueble']['propio'][$i]['id'];
?>
<tr class="inmuebles_propios-{{$id}}"><td colspan="7" class="inmuebles_header_registry"># {{ $i + 1 }}
    <a title="Eliminar" onclick="eliminar('{{ $id }}','inmuebles_propios','¿Confirma que desea eliminar el inmueble propio?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a>
    </td></tr>
<tr class="inmuebles_propios-{{$id}}">
    <th>Tipo</th>
    <th>Total de superficie cubierta</th>
    <th>Total de superficie descubierta</th>
    <th>Unidad</th>
    <th>Carácter</th>
    <th colspan="2">% de titularidad</th>
</tr>
<tr class="inmuebles_propios-{{$id}} tr-1-datos-inmuebles_propios" data-row="{{$i}}">
    <td>{{ Form::select("inmuebles[propios][$i][id_tipo_inmueble]",$combo_tipos_inmueble,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_tipo_inmueble']),array('class' => 'form-control select-tipo-inmueble')) }}</td>
    <td>{{ Form::text("inmuebles[propios][$i][superficie_cubierta]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['superficie_cubierta']),array('class' => 'form-control masked-input-superficie')); }}</td>
    <td>{{ Form::text("inmuebles[propios][$i][superficie_descubierta]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['superficie_descubierta']),array('class' => 'form-control masked-input-superficie')); }}</td>
    <td>{{ Form::select("inmuebles[propios][$i][id_unidad_superficie]",$combo_unidades_superficie,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_unidad_superficie']),array('class' => 'form-control select-unidad-superficie')) }}</td>
    <td>{{ Form::select("inmuebles[propios][$i][id_caracter]",$combo_tipos_caracter,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_caracter']),array('class' => 'form-control select-caracter')) }}</td>
    <td colspan="2">{{ Form::text("inmuebles[propios][$i][porcentaje]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['porcentaje']),array('class' => 'form-control masked-input-porcentaje ')); }}</td>
</tr>
<tr class="inmuebles_propios-{{$id}}">
    <th>Fecha de escritura</th>
    <th>Valor de adquisición</th>
    <th>Moneda</th>
    <th>Valor estimado de mercado</th>
    <th>Moneda</th>
    <th>Modo de adquisición</th>
    <th>Forma de pago</th>
</tr>
<tr class="inmuebles_propios-{{$id}} tr-2-datos-inmuebles_propios-{{$i}}" data-row="{{$i}}">
    <td>{{ Form::text("inmuebles[propios][$i][fecha_escritura]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['fecha_escritura']),array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento')); }}</td>
    <td>{{ Form::text("inmuebles[propios][$i][valor_adquisicion]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['valor_adquisicion']),array('class' => 'form-control')); }}</td>
    <td>
        {{ Form::select("inmuebles[propios][$i][id_tipo_moneda_valor_adquisicion]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_tipo_moneda_valor_adquisicion']),array('class' => 'form-control select-tipo-moneda select-tipo-moneda-valor-adquisicion')) }}
        <div>{{ Form::text("inmuebles[propios][$i][otro_tipo_moneda_valor_adquisicion]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['otro_tipo_moneda_valor_adquisicion']),array('class' => 'form-control input-otro')); }}</div>

    </td>
    <td>{{ Form::text("inmuebles[propios][$i][valor_estimado]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['valor_estimado']),array('class' => 'form-control')); }}</td>
    <td>
        {{ Form::select("inmuebles[propios][$i][id_tipo_moneda_valor_estimado]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_tipo_moneda_valor_estimado']),array('class' => 'form-control select-tipo-moneda select-tipo-moneda-valor-estimado')) }}
        <div>{{ Form::text("inmuebles[propios][$i][otro_tipo_moneda_valor_estimado]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['otro_tipo_moneda_valor_estimado']),array('class' => 'form-control input-otro')); }}</div>

    </td>
    <td>{{ Form::select("inmuebles[propios][$i][id_modo_adquisicion]",$combo_tipos_modos_adquisicion_inmueble,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_modo_adquisicion']),array('class' => 'form-control select-modo-adquisicion')) }}</td>
    <td colspan="2">
        {{ Form::select("inmuebles[propios][$i][id_forma_pago]",$combo_tipos_formas_pago,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_forma_pago']),array('class' => 'form-control select-forma-pago')) }}
        <div>
            {{ Form::text("inmuebles[propios][$i][otra_forma_pago]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['otra_forma_pago']),array('class' => 'form-control input-otro')); }}
        </div>
    </td>
</tr>
<tr class="inmuebles_propios-{{$id}}">
    <th colspan="6">Radicación</th>
</tr>
<tr class="inmuebles_propios-{{$id}} tr-3-datos-inmuebles_propios-{{$i}}">
    <th>Pais <div class="div-pais-{{$i}}"></div></th>
    <th>Provincia/Estado <div class="div-provincia-{{$i}}"></div></th>
    <th>Localidad <div class="div-localidad-{{$i}}"></div></th>
    <th colspan="4"></th>
</tr>
<tr class="inmuebles_propios-{{$id}} tr-4-datos-inmuebles_propios-{{$i}}" data-row="{{$i}}">
    <td>
        {{ Form::select("inmuebles[propios][$i][id_pais]",$combo_paises,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_pais']),array(
        'class' => 'form-control select2 select-paises-propios',
        'data-row'=>$i,
        'data-id-provincia' => (is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_provincia']),
        'data-id-localidad' => (is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_localidad']),
        ))}}
    </td>
    <td>
        <div class="div-inmueble-propios-provincia-{{$i}}">
        {{ Form::select("inmuebles[propios][$i][id_provincia]",$combo_provincias,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_provincia']),array('class' => "form-control select2 select2-provincias",'data-localidad'=>"inmueble_localidad_$i", 'data-id-localidad'=>(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_localidad']) )) }}
        </div>
        <div class="div-provincia-otro-pais-{{$i}}">
            {{ Form::text("inmuebles[propios][$i][otro_pais_provincia]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['otro_pais_provincia']),array('class' => 'form-control')); }}
        </div>
    </td>
    <td>
        <div class="div-inmueble-propios-localidad-{{$i}}">
            {{ Form::select("inmuebles[propios][$i][id_localidad]",[],(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id_localidad']),array('class' => "form-control select2 select-localidad inmueble_localidad_$i")) }}
        </div>
        <div class="div-localidad-otro-pais-{{$i}}">
            {{ Form::text("inmuebles[propios][$i][otro_pais_localidad]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['otro_pais_localidad']),array('class' => 'form-control')); }}
        </div>
    </td>
    <td colspan="3">{{ Form::hidden("inmuebles[propios][$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['id'])) }}</td>
</tr>
<tr class="inmuebles_propios-{{$id}}">
    <th colspan="4">Ruta/Avenida/Calle <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a></th>
    <th>Número/KM <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a></th>
    <th>Piso</th>
    <th colspan="2">Depto</th>
</tr>
<tr class="inmuebles_propios-{{$id}} tr-5-datos-inmuebles_propios-{{$i}}" data-row="{{$i}}">
    <td colspan="4">{{ Form::text("inmuebles[propios][$i][domicilio_calle]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['domicilio_calle']),array('class' => 'form-control')); }}</td>
    <td>{{ Form::text("inmuebles[propios][$i][domicilio_numero]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['domicilio_numero']),array('class' => 'form-control')); }}</td>
    <td>{{ Form::text("inmuebles[propios][$i][domicilio_piso]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['domicilio_piso']),array('class' => 'form-control')); }}</td>
    <td>{{ Form::text("inmuebles[propios][$i][domicilio_depto]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['propio'][$i]['domicilio_depto']),array('class' => 'form-control')); }}</td>
</tr>