<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['acreencias'][$i]['id'];
?>
<tr class="acreencias-{{$id}} tr-datos-acreencias" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','acreencias','¿Confirma que desea eliminar el registro de acreencia?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>
        {{ Form::text("acreencias[$i][identidad_deudor]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['acreencias'][$i]['identidad_deudor']),array('class' => 'form-control')); }}
    </td>
    <td>
        {{ Form::select("acreencias[$i][id_tipo_moneda]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['acreencias'][$i]['id_tipo_moneda']),array('class' => 'form-control select-tipo-moneda')) }}
        <div>{{ Form::text("acreencias[$i][otro_tipo_moneda]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['acreencias'][$i]['otro_tipo_moneda']),array('class' => 'form-control input-otro')); }}</div>
    </td>
    <td>
        {{ Form::text("acreencias[$i][monto]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['acreencias'][$i]['monto']),array('class' => 'form-control')); }}
    </td>
    <td>
        {{ Form::text("acreencias[$i][fecha]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['acreencias'][$i]['fecha']),array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento')); }}
        {{ Form::hidden("acreencias[$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['acreencias'][$i]['id'])) }}
    </td>
</tr>