<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['dinero_efectivo'][$i]['id'];
?>
<tr class="dinero_efectivo-{{$id}} tr-datos-dinero_efectivo" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','dinero_efectivo','¿Confirma que desea eliminar el registro dinero en efectivo?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
        <td>{{ Form::select("dinero_efectivo[$i][id_tipo_moneda]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['dinero_efectivo'][$i]['id_tipo_moneda']),array('class' => 'form-control select-tipo-moneda')) }}
        <div>{{ Form::text("dinero_efectivo[$i][otro_tipo_moneda]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['dinero_efectivo'][$i]['otro_tipo_moneda']),array('class' => 'form-control input-otro')); }}</div>
    </td>
    <td>
        {{ Form::text("dinero_efectivo[$i][origen_fondo]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['dinero_efectivo'][$i]['origen_fondo']),array('class' => 'form-control','maxlength'=>50)); }}
    </td>
    <td>
        {{ Form::text("dinero_efectivo[$i][monto]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['dinero_efectivo'][$i]['monto']),array('class' => 'form-control')); }}
        {{ Form::hidden("dinero_efectivo[$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['dinero_efectivo'][$i]['id'])) }}
    </td>
</tr>