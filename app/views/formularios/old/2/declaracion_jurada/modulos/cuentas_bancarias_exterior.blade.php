<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['cuenta_bancaria_exterior'][$i]['id'];
?>
    <tr class="cuenta_bancaria_exterior-{{$id}} tr-datos-cuentas_bancarias_exterior" data-row="{{$i}}">
        <td><a title="Eliminar" onclick="eliminar('{{ $id }}','cuenta_bancaria_exterior','¿Confirma que desea eliminar registro de cuenta bancaria en el exterior?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
        <td>{{ Form::text("cuentas_bancarias_exterior[$i][banco]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['cuenta_bancaria_exterior'][$i]['banco']),array('class' => 'form-control')); }}</td>
        <td>{{ Form::select("cuentas_bancarias_exterior[$i][id_tipo_banco]",$combo_tipos_cuenta_bancaria,(is_null($declaracion_jurada) ? null : $declaracion_jurada['cuenta_bancaria_exterior'][$i]['id_tipo_banco']),array('class' => 'form-control select-tipo-banco')) }}</td>

        <td>{{ Form::text("cuentas_bancarias_exterior[$i][saldo]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['cuenta_bancaria_exterior'][$i]['saldo']),array('class' => 'form-control','size'=>'30')); }}</td>
        <td>
            {{ Form::select("cuentas_bancarias_exterior[$i][id_tipo_moneda]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['cuenta_bancaria_exterior'][$i]['id_tipo_moneda']),array('class' => 'form-control select-tipo-moneda')) }}
            <div>{{ Form::text("cuentas_bancarias_exterior[$i][otro_tipo_moneda]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['cuenta_bancaria_exterior'][$i]['otro_tipo_moneda']),array('class' => 'form-control input-otro')); }}</div>
            {{ Form::hidden("cuentas_bancarias_exterior[$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['cuenta_bancaria_exterior'][$i]['id'])) }}
        </td>
    </tr>