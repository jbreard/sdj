<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['bien']['propio'][$i]['id'];
?>
<tr class="otros_bienes-{{$id}} tr-datos-bienes_propios" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','otros_bienes','¿Confirma que desea eliminar el registro de otros bienes?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>{{ Form::text("bienes[propios][$i][descripcion]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['bien']['propio'][$i]['descripcion']),array('class' => 'form-control','maxlength'=>50)); }}</td>
    <td>{{ Form::select("bienes[propios][$i][id_tipo_caracter]",$combo_tipos_caracter,(is_null($declaracion_jurada) ? null : $declaracion_jurada['bien']['propio'][$i]['id_tipo_caracter']),array('class' => 'form-control select-tipo-caracter')) }}</td>
    <td>{{ Form::select("bienes[propios][$i][id_tipo_adquisicion]",$combo_tipos_modos_adquisicion,(is_null($declaracion_jurada) ? null : $declaracion_jurada['bien']['propio'][$i]['id_tipo_adquisicion']),array('class' => 'form-control select-tipo-adquisicion')) }}</td>
    <td>
        {{ Form::text("bienes[propios][$i][monto]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['bien']['propio'][$i]['monto']),array('class' => 'form-control')); }}
        {{ Form::hidden("bienes[propios][$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['bien']['propio'][$i]['id'])) }}
    </td>
</tr>