<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['deuda']['propio'][$i]['id'];
?>
    <tr class="deudas_propias-{{$id}} tr-datos-deudas_propios deudas-{{$i}}" data-row="{{$i}}">
        <td><a title="Eliminar" onclick="eliminar('{{ $id }}','deudas_propias','¿Confirma que desea eliminar registro de deudas?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
        <td>
            {{ Form::select("deudas[propios][$i][id_tipo_deuda]",$combo_tipos_deuda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['deuda']['propio'][$i]['id_tipo_deuda']),array('class' => 'form-control select-tipo-deuda','data-row'=>$i)) }}
            <div class="div-otra-deuda">
                Otro tipo de deuda:<br>
                {{ Form::text("deudas[propios][$i][otro_tipo_deuda]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['deuda']['propio'][$i]['otro_tipo_deuda']),array('class' => 'form-control input-otro')); }}
            </div>
            <div class="div-credito-personal-otro">
                Otro crédito presonal:<br>
                {{ Form::text("deudas[propios][$i][otro_credito_personal]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['deuda']['propio'][$i]['otro_credito_personal']),array('class' => 'form-control input-otro')); }}
            </div>
            <div class="div-cuotas-restantes">
                Cuotas restantes:<br>
                {{ Form::text("deudas[propios][$i][cuotas_restantes]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['deuda']['propio'][$i]['cuotas_restantes']),array('class' => 'form-control input-cuotas-restantes')); }}
            </div>
        </td>
        <td>{{ Form::text("deudas[propios][$i][acreedor]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['deuda']['propio'][$i]['acreedor']),array('class' => 'form-control')); }}</td>
        <td>
            {{ Form::select("deudas[propios][$i][id_tipo_moneda]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['deuda']['propio'][$i]['id_tipo_moneda']),array('class' => 'form-control select-tipo-moneda')) }}
            <div>{{ Form::text("deudas[propios][$i][otro_tipo_moneda]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['deuda']['propio'][$i]['otro_tipo_moneda']),array('class' => 'form-control input-otro')); }}</div>
        </td>
        <td>
            {{ Form::text("deudas[propios][$i][monto]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['deuda']['propio'][$i]['monto']),array('class' => 'form-control')); }}
            {{ Form::hidden("deudas[propios][$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['deuda']['propio'][$i]['id'])) }}
        </td>
    </tr>