<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['derecho_expectativa']['conyuge'][$i]['id'];
?>
<tr class="derecho_expectativa_conyuge-{{$id}} tr-datos-derecho_expectativa_conyuge" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','derecho_expectativa_conyuge','¿Confirma que desea eliminar registro de derechos en expectativa?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>{{ Form::select("derecho_expectativa[conyuge][$i][id_tipo_derechos_expectativa]",$combo_tipos_derecho_expectativa,(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['conyuge'][$i]['id_tipo_derechos_expectativa']),array('class' => 'form-control select-tipo-derecho-expectativa')) }}</td>
    <td>
        {{ Form::select("derecho_expectativa[conyuge][$i][id_tipo_fuente]",$combo_tipos_fuentes,(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['conyuge'][$i]['id_tipo_fuente']),array('class' => 'form-control select-tipo-fuente')) }}
        <div class="div-fuente-tipo-cuotas">
            Cantidad cuotas pagas:
            {{ Form::text("derecho_expectativa[conyuge][$i][cantidad_cuotas_pagas]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['conyuge'][$i]['cantidad_cuotas_pagas']),array('class' => 'form-control input-otro')); }}
            Cantidad cuotas restantes a pagar:
            {{ Form::text("derecho_expectativa[conyuge][$i][cantidad_cuotas_restantes]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['conyuge'][$i]['cantidad_cuotas_restantes']),array('class' => 'form-control input-otro')); }}
        </div>
        {{ Form::hidden("derecho_expectativa[conyuge][$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['conyuge'][$i]['id'])) }}
    </td>
</tr>