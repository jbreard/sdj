<?php
$id_tipo_ingreso = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo'][$i]['id_tipo_ingreso'];
$monto_neto_anual_total = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo'][$i]['monto_neto_anual_total'];
$id_tipo_moneda = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo'][$i]['id_tipo_moneda'];
$otro_tipo_ingreso = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo'][$i]['otro_tipo_ingreso'];
$otro_tipo_ingreso_extraordinario = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo'][$i]['otro_tipo_ingreso_extraordinario'];
$otro_tipo_moneda = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo'][$i]['otro_tipo_moneda'];
$id_fuerza = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo'][$i]['id_fuerza'];
$id_ingreso_extraordinario = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo'][$i]['id_ingreso_extraordinario'];
$id_ingreso_otra_actividad = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo'][$i]['id_ingreso_otra_actividad'];
$id_categoria_tributaria = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo'][$i]['id_categoria_tributaria'];
$id = (is_null($declaracion_jurada)) ? "new".$i : $declaracion_jurada['ingreso_externo'][$i]['id'];
?>
<tr class="ingresos_externos-{{$id}} tr-datos-ingresos_externos" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','ingresos_externos','¿Confirma que desea eliminar el ingreso por fuera de la fuerza?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td class="td-ingresos-externos">
        {{ Form::select("ingresos_externos[$i][id_tipo_ingreso]",$combo_tipos_ingresos_externos,$id_tipo_ingreso,array('class' => 'form-control select-tipo-ingreso-externo')); }}

        <div class="div-otro-ingreso">
            {{ Form::text("ingresos_externos[$i][otro_tipo_ingreso]", $otro_tipo_ingreso,array('class' => 'form-control input-otro')); }}
        </div>

        <div class="div-ingreso-extraordinario">
            Tipos ingresos extraordinarios:<br>
            {{ Form::select("ingresos_externos[$i][id_ingreso_extraordinario]",$combo_tipos_ingresos_extraordinario_externo,$id_ingreso_extraordinario,array('class' => 'form-control select-ingreso-extraordinario')); }}
            <div class="div-ingreso-extraordinario-otros">
                Otros tipos de ingresos extraordinarios:<br>
                {{ Form::text("ingresos_externos[$i][otro_tipo_ingreso_extraordinario]", $otro_tipo_ingreso_extraordinario,array('class' => 'form-control input-otro')); }}
            </div>
        </div>

        <div class="div-ingreso-actividad">
            Tipos ingresos:<br>
            {{ Form::select("ingresos_externos[$i][id_ingreso_otra_actividad]",$combo_tipos_ingresos_actividades,$id_ingreso_otra_actividad,array('class' => 'form-control select-ingreso-otra-actividad')); }}
        </div>
        <div class="div-categoria-tributaria">
            Categorias tributarias:<br>
            {{ Form::select("ingresos_externos[$i][id_categoria_tributaria]",$combo_tipos_categorias_tributarias,$id_categoria_tributaria,array('class' => 'form-control select-categoria-tributaria')); }}
        </div>
    </td>
    <td>
        {{ Form::text("ingresos_externos[$i][monto_neto_anual_total]", $monto_neto_anual_total,array('class' => 'form-control')); }}
    </td>
    <td>
        {{ Form::select("ingresos_externos[$i][id_tipo_moneda]",$combo_tipos_moneda,$id_tipo_moneda,array('class' => 'form-control select-tipo-moneda')); }}
        <div>{{ Form::text("ingresos_externos[$i][otro_tipo_moneda]", $otro_tipo_moneda,array('class' => 'form-control input-otro')); }}</div>
        {{ Form::hidden("ingresos_externos[$i][id_fuerza]", $id_fuerza,array('class'=>'datos-propios-ingresos')) }}
        {{ Form::hidden("ingresos_externos[$i][id]", $id) }}
    </td>
</tr>