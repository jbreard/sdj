<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['tarjeta_credito'][$i]['id'];
?>
<tr class="tarjeta_credito-{{$id}} tr-datos-tarjetas_credito" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','tarjeta_credito','¿Confirma que desea eliminar registro de tarjetas de crédito?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>{{ Form::text("tarjetas_credito[$i][banco]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['tarjeta_credito'][$i]['banco']),array('class' => 'form-control')); }}</td>
    <td>
        {{ Form::select("tarjetas_credito[$i][id_entidad_emisora]",$combo_entidades_emisora_tc,(is_null($declaracion_jurada) ? null : $declaracion_jurada['tarjeta_credito'][$i]['id_entidad_emisora']),array('class' => 'form-control select-entidad-emisora')) }}
        <div>
            {{ Form::text("tarjetas_credito[$i][otra_entidad_emisora]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['tarjeta_credito'][$i]['otra_entidad_emisora']),array('class' => 'form-control input-otro')); }}
        </div>
    </td>
    <td>
        {{ Form::select("tarjetas_credito[$i][id_tipo_titular]",$combo_tipo_titular,(is_null($declaracion_jurada) ? null : $declaracion_jurada['tarjeta_credito'][$i]['id_tipo_titular']),array('class' => 'form-control select-tipo-titular')) }}
    </td>
    <td>
        {{ Form::text("tarjetas_credito[$i][cantidad_extension]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['tarjeta_credito'][$i]['cantidad_extension']),array('class' => 'form-control')); }}
        {{ Form::hidden("tarjetas_credito[$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['tarjeta_credito'][$i]['id'])) }}
    </td>
</tr>