<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['ingreso_extraordinario'][$i]['id'];
?>
<tr class="ingreso_extraordinario-{{$id}} tr-datos-ingresos_extraordinarios" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','ingreso_extraordinario','¿Confirma que desea eliminar el registro ingreso extraordinario?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>
        {{ Form::select("ingresos_extraordinarios[$i][id_tipo_ingreso]",$combo_tipos_ingresos_extraordinario, (is_null($declaracion_jurada) ? null : $declaracion_jurada['ingreso_extraordinario'][$i]['id_tipo_ingreso']),array('class' => 'form-control select-tipo-ingreso')) }}
        <div>{{ Form::text("ingresos_extraordinarios[$i][otro_tipo_ingreso]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['ingreso_extraordinario'][$i]['otro_tipo_ingreso']),array('class' => 'form-control input-otro')); }}</div>
    </td>
    <td>
        {{ Form::text("ingresos_extraordinarios[$i][monto_anual_neto]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['ingreso_extraordinario'][$i]['monto_anual_neto']),array('class' => 'form-control')); }}
    </td>
    <td>
        {{ Form::select("ingresos_extraordinarios[$i][id_tipo_moneda]",$combo_tipos_moneda, (is_null($declaracion_jurada) ? null : $declaracion_jurada['ingreso_extraordinario'][$i]['id_tipo_moneda']),array('class' => 'form-control select-tipo-moneda')) }}
        <div>
            {{ Form::text("ingresos_extraordinarios[$i][otro_tipo_moneda]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['ingreso_extraordinario'][$i]['otro_tipo_moneda']),array('class' => 'form-control input-otro')); }}
        </div>
        {{ Form::hidden("ingresos_extraordinarios[$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['ingreso_extraordinario'][$i]['id'])) }}
    </td>
</tr>