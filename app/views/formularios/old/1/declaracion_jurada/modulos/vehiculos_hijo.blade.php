<?php
$isNull = (is_null($declaracion_jurada));
$id = ($isNull ? "new".$i : $declaracion_jurada['vehiculo']['hijo'][$i]['id']);
?>

<tr class="vehiculos_hijos-{{$id}}"><td colspan="8" class="inmuebles_header_registry"># {{ $i + 1 }}
        <a title="Eliminar" onclick="eliminar('{{ $id }}','vehiculos_hijos','¿Confirma que desea eliminar el vehiculo del hijo?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a>
    </td></tr>
<tr class="vehiculos_hijos-{{$id}}">
    <th>Tipo de vehículo</th>
    <th>Tipo</th>
    <th>Marca</th>
    <th>Modelo</th>
    <th>Año fabricación</th>
    <th colspan="3">Dominio/Matrícula</th>
</tr>
<tr class="vehiculos_hijos-{{$id}} tr-1-datos-vehiculos_hijos" data-row="{{$i}}">
    <td>{{ Form::select("vehiculos[hijos_menores][$i][id_tipo_vehiculo_nuevo]",$combo_tipos_vehiculo,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_tipo_vehiculo_nuevo']),array('class' => 'form-control select-tipo-vehiculo')) }}</td>
    <td>{{ Form::text("vehiculos[hijos_menores][$i][tipo]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['marca']),array('class' => 'form-control', 'maxlength' => 50)); }}</td>
    <td>{{ Form::text("vehiculos[hijos_menores][$i][marca]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['marca']),array('class' => 'form-control', 'maxlength' => 50)); }}</td>
    <td>{{ Form::text("vehiculos[hijos_menores][$i][modelo]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['modelo']),array('class' => 'form-control', 'maxlength' => 50)); }}</td>
    <td>{{ Form::text("vehiculos[hijos_menores][$i][anio]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['anio']),array('class' => 'form-control masked-input-anio')); }}</td>
    <td colspan="3">{{ Form::text("vehiculos[hijos_menores][$i][dominio]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['dominio']),array('class' => 'form-control')); }}</td>
</tr>
<tr class="vehiculos_hijos-{{$id}}">
    <th>Fecha de transferencia del dominio</th>
    <th>Carácter</th>
    <th>% de titularidad</th>
    <th>Valor de adquisición</th>
    <th>Moneda</th>
    <th>Valuación según seguro</th>
    <th>Moneda</th>
    <th>Modo de adquisición</th>
</tr>
<tr class="vehiculos_hijos-{{$id}} tr-2-datos-vehiculos_hijos-{{$i}}"  data-row="{{$i}}">
    <td>{{ Form::text("vehiculos[hijos_menores][$i][fecha_transferencia_dominio]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['fecha_transferencia_dominio']),array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento')); }}</td>
    <td>{{ Form::select("vehiculos[hijos_menores][$i][id_caracter]",$combo_tipos_caracter,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_caracter']),array('class' => 'form-control select-caracter')) }}</td>
    <td>{{ Form::text("vehiculos[hijos_menores][$i][porcentaje]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['porcentaje']),array('class' => 'form-control masked-input-porcentaje')); }}</td>
    <td>{{ Form::text("vehiculos[hijos_menores][$i][valor_adquisicion]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['valor_adquisicion']),array('class' => 'form-control')); }}</td>
    <td>
        {{ Form::select("vehiculos[hijos_menores][$i][id_tipo_moneda_valor_estimado]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_tipo_moneda_valor_estimado']),array('class' => 'form-control select-tipo-moneda select-tipo-moneda-valor-estimado')) }}
        <div>{{ Form::text("vehiculos[hijos_menores][$i][otro_tipo_moneda_valor_estimado]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['otro_tipo_moneda_valor_estimado']),array('class' => 'form-control input-otro')); }}</div>
    </td>
    <td>{{ Form::text("vehiculos[hijos_menores][$i][valor_seguro]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['valor_seguro']),array('class' => 'form-control')); }}</td>
    <td>
        {{ Form::select("vehiculos[hijos_menores][$i][id_tipo_moneda_valor_seguro]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_tipo_moneda_valor_seguro']),array('class' => 'form-control select-tipo-moneda select-tipo-moneda-valor-seguro')) }}
        <div>{{ Form::text("vehiculos[hijos_menores][$i][otro_tipo_moneda_valor_seguro]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['otro_tipo_moneda_valor_seguro']),array('class' => 'form-control input-otro')); }}</div>
    </td>
    <td>{{ Form::select("vehiculos[hijos_menores][$i][id_modo_adquisicion]",$combo_tipos_modos_adquisicion,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_modo_adquisicion']),array('class' => 'form-control select-modo-adquisicion')) }}</td>

</tr>
<tr class="vehiculos_hijos-{{$id}}">
    <th colspan="8">Forma de pago</th>
</tr>
<tr class="vehiculos_hijos-{{$id}} tr-3-datos-vehiculos_hijos-{{$i}}" data-row="{{$i}}">
    <td colspan="8">{{ Form::select("vehiculos[hijos_menores][$i][id_forma_pago]",$combo_tipos_formas_pago_vehiculo,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_forma_pago']),array('class' => 'form-control  select-forma-pago')) }}</td>
</tr>
<tr class="vehiculos_hijos-{{$id}}">
    <th colspan="8">Radicación</th>
</tr>
<tr class="vehiculos_hijos-{{$id}} tr-5-datos-vehiculos_hijos-{{$i}}">
    <th>Pais <div class="div-pais-{{$i}}"></div></th>
    <th>Provincia/Estado <div class="div-provincia-{{$i}}"></div></th>
    <th>Localidad <div class="div-localidad-{{$i}}"></div></th>
    <th colspan="3"></th>
</tr>
<tr class="vehiculos_hijos-{{$id}} tr-4-datos-vehiculos_hijos-{{$i}}" data-row="{{$i}}">
    <td>{{ Form::select("vehiculos[hijos_menores][$i][id_pais]",$combo_paises,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_pais']),array(
    'class' => 'form-control select2 select-paises-vehiculos-hijos',
    'data-row'=>$i,
    'data-id-provincia' => (is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_provincia']),
    'data-id-localidad' => (is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_localidad']),
    )) }}</td>
    <td>
        <div class="div-vehiculo-hijos-provincia-{{$i}}">
            {{ Form::select("vehiculos[hijos_menores][$i][id_provincia]",$combo_provincias,(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_provincia']),array('class' => 'form-control select2 select2-provincias','data-localidad'=>"vehiculo_hijo_localidad_$i", 'data-id-localidad'=>(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_localidad']) )) }}
        </div>
        <div class="div-provincia-otro-pais-{{$i}}">
            {{ Form::text("vehiculos[hijos_menores][$i][otro_pais_provincia]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['otro_pais_provincia']),array('class' => 'form-control')); }}
        </div>
    </td>
    <td>
        <div class="div-vehiculo-hijos-localidad-{{$i}}">
            {{ Form::select("vehiculos[hijos_menores][$i][id_localidad]",[],(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id_localidad']),array('class' => "form-control select2 vehiculo_hijo_localidad_$i  select-localidad")) }}
        </div>
        <div class="div-localidad-otro-pais-{{$i}}">
            {{ Form::text("vehiculos[hijos_menores][$i][otro_pais_localidad]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['otro_pais_localidad']),array('class' => 'form-control')); }}
        </div>
    </td>
    <td colspan="3">
        {{ Form::hidden("vehiculos[hijos_menores][$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['vehiculo']['hijo'][$i]['id'])) }}
    </td>
</tr>