<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['derecho_expectativa']['hijo'][$i]['id'];
?>
<tr class="derecho_expectativa_hijo-{{$id}} tr-datos-derecho_expectativa_hijo" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','derecho_expectativa_hijo','¿Confirma que desea eliminar registro de derechos en expectativa?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>{{ Form::select("derecho_expectativa[hijos_menores][$i][id_tipo_derechos_expectativa]",$combo_tipos_derecho_expectativa,(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['hijo'][$i]['id_tipo_derechos_expectativa']),array('class' => 'form-control select-tipo-derecho-expectativa')) }}</td>
    <td>
        {{ Form::select("derecho_expectativa[hijos_menores][$i][id_tipo_fuente]",$combo_tipos_fuentes,(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['hijo'][$i]['id_tipo_fuente']),array('class' => 'form-control select-tipo-fuente')) }}
        <div class="div-fuente-tipo-cuotas">
            Cantidad cuotas pagas:
            {{ Form::text("derecho_expectativa[hijos_menores][$i][cantidad_cuotas_pagas]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['hijo'][$i]['cantidad_cuotas_pagas']),array('class' => 'form-control input-otro')); }}
            Cantidad cuotas restantes a pagar:
            {{ Form::text("derecho_expectativa[hijos_menores][$i][cantidad_cuotas_restantes]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['hijo'][$i]['cantidad_cuotas_restantes']),array('class' => 'form-control input-otro')); }}
        </div>
        {{ Form::hidden("derecho_expectativa[hijos_menores][$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['hijo'][$i]['id'])) }}
    </td>
</tr>