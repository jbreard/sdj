<tr>
    <td>
        <b>Otros Ingresos</b>
    </td>
    <td>
        {{ Form::select("ingresos[otros][id_moneda]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : (!array_key_exists('otros',$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso']['otros']['id_moneda']),array('class' => 'form-control select-tipo-moneda')) }}
        <div>{{ Form::text("ingresos[otros][otro_tipo_moneda]",(is_null($declaracion_jurada) ? null : (!array_key_exists('otros',$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso']['otros']['otro_tipo_moneda']),array('class' => 'form-control input-otro')); }}</div>
    </td>
    <td>
        {{ Form::text("ingresos[otros][monto_anual_aproximado]",(is_null($declaracion_jurada) ? null : (!array_key_exists('otros',$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso']['otros']['monto_anual_aproximado']),array('class' => 'form-control')); }}
        {{ Form::hidden("ingresos[otros][id]", (is_null($declaracion_jurada) ? null : (!array_key_exists('otros',$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso']['otros']['id'])) }}
        {{ Form::hidden("ingresos[otros][id_salario]", (is_null($declaracion_jurada) ? null : (!array_key_exists('otros',$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso']['otros']['id_salario']),array('class'=>'datos-propios-ingresos')) }}
        {{ Form::hidden("ingresos[otros][tipo_ingreso]",3,array('class' => '')); }}
    </td>
</tr>