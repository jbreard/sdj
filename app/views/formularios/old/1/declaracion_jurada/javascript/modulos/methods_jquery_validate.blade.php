<script type="application/javascript">

    /**
     * Solo se validara este campo si la Fuerza a la que pertenece es PFA(id:1)
     * @param value
     * @returns {boolean}
     */
    function validarCheckSituacionRevista(value) {
        var selectSitutacionRevista = $('.select-situacion-revista');
        var idSituacionRevista = parseInt(selectSitutacionRevista.val());

        var selectFuerzaPerteneciente = $('.select2-fuerza-perteneciente');
        var idFuerza = parseInt(selectFuerzaPerteneciente.select2().val());

        var response = (idFuerza == 1 && idSituacionRevista  == 14 && empty(value));

        return !response;
    }

    function validarCuit(cuit)
    {
        if (typeof (cuit) == 'undefined')
            return true;
        cuit = cuit.toString().replace(/[-_]/g, "");
        if (cuit == '')
            return true; //No estamos validando si el campo esta vacio, eso queda para el "required"
        if (cuit.length != 11)
            return false;
        else {
            var mult = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
            var total = 0;
            for (var i = 0; i < mult.length; i++) {
                total += parseInt(cuit[i]) * mult[i];
            }
            var mod = total % 11;
            var digito = mod == 0 ? 0 : mod == 1 ? 9 : 11 - mod;
        }
        return digito == parseInt(cuit[10]);
    }

    function validarCuerpoAgrupamiento(value) {
        var fuerzaPerteneciente = $('.select2-fuerza-perteneciente');
        var idFuerza = fuerzaPerteneciente.select2().val();

        //GNA, PFA, PSA
        return !(idFuerza != 4 && idFuerza != 2 && idFuerza != 1 && idFuerza != 0 && empty(value));
    }

    function validarFuerza(value) {
        if(value == '' || value <= 0)
            return false;

        return true;
    }

    function validarEstadoCivil(value) {
        if(value == '' || value <= 0)
            return false;

        return true;
    }

    function validarSelectProvincia(value) {
        if(value == '' || value <= 0)
            return false;

        return true;
    }

    function validarSelectTipoPersonal(value) {
        if(value == '' || value <= 0)
            return false;

        return true;
    }

    function validarSelectGrado(value) {
        var fuerzaPerteneciente = $('.select2-fuerza-perteneciente');
        var idFuerza = fuerzaPerteneciente.select2().val();
        var tipoPersonal = $('.select-personal');
        var idTipoPersonal = tipoPersonal.select2().val();

        if(idFuerza == 4 && idTipoPersonal == 2) return true;

        if(value == '' || value <= 0)
            return false;

        return true;
    }

    function validarSelect(value) {
        if(empty(value) || value <= 0)
            return false;

        return true;
    }

    function validarCaracterBaja(value) {
        var selectCaracterDeclaracion = $('.select2-caracter-declaracion');
        var optionSelected = selectCaracterDeclaracion.val();

        return !(optionSelected == 5 && empty(value));
    }

    $.validator.addMethod("checkCuilCuit",
        function (value, element) {
            return this.optional(element) || validarCuit(value);
        },
        '{{$validaciones['cuil_cuit']}}'
    );

    $.validator.addMethod("checkFuerza",
        function (value, element) {
            return this.optional(element) || validarFuerza(value);
        },
        '{{$validaciones['fuerza_perteneciente']}}'
    );

    $.validator.addMethod("checkCuerpoAgrupamiento",
        function (value, element) {
            return validarCuerpoAgrupamiento(value);
        },
        '{{$validaciones['cuerpo_agrupamiento']}}'
    );

    $.validator.addMethod("checkEstadoCivil",
        function (value, element) {
            return this.optional(element) || validarEstadoCivil(value);
        },
        '{{$validaciones['estado_civil']}}'
    );

    $.validator.addMethod("checkLlamadosPrestarServicio",
        function (value, element) {
            return validarCheckSituacionRevista(value);
        },
        '{{$validaciones['llamados_servicio']}}'
    );

    $.validator.addMethod("checkProvincia",
        function (value, element) {
            return this.optional(element) || validarSelectProvincia(value);
        },
        '{{$validaciones['provincia']}}'
    );

    $.validator.addMethod("checkLocalidad",
        function (value, element) {
            return validarSelect(value);
        },
        '{{$validaciones['localidad']}}'
    );

    $.validator.addMethod("checkTipoPersonal",
            function (value, element) {
                return validarSelectTipoPersonal(value);
            },
            '{{$validaciones['id_tipo_personal']}}'
    );

    $.validator.addMethod("checkGrado",
            function (value, element) {
                return validarSelectGrado(value);
            },
            '{{$validaciones['id_grado']}}'
    );

    $.validator.addMethod("checkTipoGenero",
        function (value, element) {
            return this.optional(element) || validarSelect(value);
        },
        '{{$validaciones['id_tipo_sexo']}}'
    );

    $.validator.addMethod("checkSelectDefault",
        function (value, element) {
            return this.optional(element) || validarSelect(value);
        },
        '{{$validaciones['select_default']}}'
    );

    $.validator.addMethod("checkCaracterBaja",
        function (value, element) {
            return validarCaracterBaja(value);
        },
        '{{$validaciones['campo_requerido']}}'
    );

    $.validator.addMethod("checkIsNotEmpty",
        function (value, element) {
            var isEmtpy = empty(value);

            if(isEmtpy) {
                setFormToNoValid('#form-7');
                return false;
            }

            return true;
        },
        '{{$validaciones['campo_requerido']}}'
    );

    var validatorMessage = {
        required: "Este campo es requerido.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: $.validator.format("Please enter no more than {0} characters."),
        minlength: $.validator.format("Please enter at least {0} characters."),
        rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
        range: $.validator.format("Please enter a value between {0} and {1}."),
        max: $.validator.format("Please enter a value less than or equal to {0}."),
        min: $.validator.format("Please enter a value greater than or equal to {0}.")
    };

    $.extend($.validator.messages, validatorMessage);

</script>