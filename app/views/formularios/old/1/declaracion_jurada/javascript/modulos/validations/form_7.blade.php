<script type="application/javascript">

    /**
     *
     * Formulario Ingresos Propios
     *
     */

    var selectorForm7 = '#form-7';

    function form7CheckIngresosExtraordinarios(){
        var tbody = $('.tbody-ingresos_extraordinarios_v2');

        tbody.find(".tr-datos-ingresos_extraordinarios").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="ingresos_extraordinarios['+row+']"]' +
                '[name!="ingresos_extraordinarios['+row+'][id]"]' +
                '[name!="ingresos_extraordinarios['+row+'][otro_tipo_moneda]"]' +
                '[name!="ingresos_extraordinarios['+row+'][otro_tipo_ingreso]"]' +
                ',.select-tipo-moneda' +
                ',.select-tipo-ingreso';

            var selectTipoMoneda = element.find('.select-tipo-moneda');
            var valTipoMoneda = selectTipoMoneda.val();

            var otroTipoMoneda = element.find('input[name="ingresos_extraordinarios['+row+'][otro_tipo_moneda]"]');
            var valOtroTipoMoneda = otroTipoMoneda.val();

            var selectTipoIngreso = element.find('.select-tipo-ingreso');
            var valTipoIngreso = selectTipoIngreso.val();

            var otroTipoIngreso = element.find('input[name="ingresos_extraordinarios['+row+'][otro_tipo_ingreso]"]');
            var valOtroTipoIngreso = otroTipoIngreso.val();

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm7);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoMoneda == 4 && empty(valOtroTipoMoneda)) {
                    setFormToNoValid(selectorForm7);
                    otroTipoMoneda.addClass('error');
                } else {
                    otroTipoMoneda.removeClass('error');
                }

                if(valTipoIngreso == 9 && empty(valOtroTipoIngreso)) {
                    setFormToNoValid(selectorForm7);
                    otroTipoIngreso.addClass('error');
                } else {
                    otroTipoIngreso.removeClass('error');
                }
            }

        });
    }

    function form7CheckIngresosExternos(){
        var tbody = $('.tbody-ingresos_externos_v2');

        tbody.find(".tr-datos-ingresos_externos").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="ingresos_externos['+row+']"]' +
                '[name!="ingresos_externos['+row+'][id]"]' +
                '[name!="ingresos_externos['+row+'][otro_tipo_moneda]"]' +
                '[name!="ingresos_externos['+row+'][otro_tipo_ingreso]"]' +
                '[name!="ingresos_externos['+row+'][otro_tipo_ingreso_extraordinario]"]' +
                '[name!="ingresos_externos['+row+'][id_fuerza]"]' +
                ',.select-tipo-moneda' +
                ',.select-tipo-ingreso-externo';

            var selectTipoMoneda = element.find('.select-tipo-moneda');
            var valTipoMoneda = selectTipoMoneda.val();

            var otroTipoMoneda = element.find('input[name="ingresos_externos['+row+'][otro_tipo_moneda]"]');
            var valOtroTipoMoneda = otroTipoMoneda.val();

            var selectTipoIngresoExterno = element.find('.select-tipo-ingreso-externo');
            var valTipoIngresoExterno = selectTipoIngresoExterno.val();

            var otroTipoIngreso = element.find('input[name="ingresos_externos['+row+'][otro_tipo_ingreso]"]');
            var valOtroTipoIngreso = otroTipoIngreso.val();

            var selectIngresoExtraordinario = element.find('.select-ingreso-extraordinario');
            var valIngresoExtraordinario = selectIngresoExtraordinario.val();

            var otroTipoIngresoExtraordinario = element.find('input[name="ingresos_externos['+row+'][otro_tipo_ingreso_extraordinario]"]');
            var valOtroTipoIngresoExtraordinario = otroTipoIngresoExtraordinario.val();

            var selectIngresoOtraActividad = element.find('.select-ingreso-otra-actividad');
            var valIngresoOtraActividad = selectIngresoOtraActividad.val();

            var selectCategoriaTributaria = element.find('.select-categoria-tributaria');
            var valCategoriaTributaria = selectCategoriaTributaria.val();

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();
                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm7);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoMoneda == 4 && empty(valOtroTipoMoneda)) {
                    setFormToNoValid(selectorForm7);
                    otroTipoMoneda.addClass('error');
                } else {
                    otroTipoMoneda.removeClass('error');
                }

                if(valTipoIngresoExterno == 4 && empty(valOtroTipoIngreso)) {
                    setFormToNoValid(selectorForm7);
                    otroTipoIngreso.addClass('error');
                } else {

                    if(valTipoIngresoExterno == 2) {
                        if(empty(valIngresoExtraordinario)) {
                            setFormToNoValid(selectorForm7);
                            selectIngresoExtraordinario.addClass('error');
                        } else {

                            if(valIngresoExtraordinario == 3 && empty(valOtroTipoIngresoExtraordinario)) {
                                setFormToNoValid(selectorForm7);
                                otroTipoIngresoExtraordinario.addClass('error');
                            } else {
                                otroTipoIngresoExtraordinario.removeClass('error');
                            }

                            selectIngresoExtraordinario.removeClass('error');
                        }
                    } else if(valTipoIngresoExterno == 3) {
                        if(empty(valCategoriaTributaria)) {
                            setFormToNoValid(selectorForm7);
                            selectCategoriaTributaria.addClass('error');
                        } else {
                            selectCategoriaTributaria.removeClass('error');
                        }

                        if(empty(valIngresoOtraActividad)) {
                            setFormToNoValid(selectorForm7);
                            selectIngresoOtraActividad.addClass('error');
                        } else {
                            selectIngresoOtraActividad.removeClass('error');
                        }
                    }

                    otroTipoIngreso.removeClass('error');
                }
            }

        });
    }

</script>