<script type="application/javascript">

    /**
     *
     * Formulario Deudas Propios
     *
     */

    var selectorForm8 = '#form-8';

    function form8CheckTarjetasCredito() {
        var tbody = $('.tbody-tarjetas_credito');

        tbody.find(".tr-datos-tarjetas_credito").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="tarjetas_credito['+row+']"]' +
                '[name!="tarjetas_credito['+row+'][id]"]' +
                '[name!="tarjetas_credito['+row+'][otra_entidad_emisora]"]' +
                '[name!="tarjetas_credito['+row+'][cantidad_extension]"]' +
                ',.select-entidad-emisora' +
                ',.select-tipo-titular';

            var finds = element.find(selectorInputs);

            var selectEntidadEmisora = element.find('.select-entidad-emisora');
            var valEntidadEmisora = selectEntidadEmisora.val();

            var otraEntidadEmisora = element.find('input[name="tarjetas_credito['+row+'][otra_entidad_emisora]"]');
            var valOtraEntidadEmisora = otraEntidadEmisora.val();

            var selectTipoTitular = element.find('.select-tipo-titular');
            var valTipoTitular = selectTipoTitular.val();

            var cantidadExtensiones = element.find('input[name="tarjetas_credito['+row+'][cantidad_extension]"]');
            var valCantidadExtensiones = cantidadExtensiones.val();

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    var a = input.attr('name');
                    console.log(a);

                    if(empty(value)) {
                        setFormToNoValid(selectorForm8);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valEntidadEmisora == 6 && empty(valOtraEntidadEmisora)) {
                    setFormToNoValid(selectorForm8);
                    otraEntidadEmisora.addClass('error');
                } else {
                    otraEntidadEmisora.removeClass('error');
                }

                if(valTipoTitular == 2 && ( valCantidadExtensiones === '0' || valCantidadExtensiones === 0 )) {
                    cantidadExtensiones.removeClass('error');
                }else {
                    if(valTipoTitular == 2 && empty(valCantidadExtensiones)) {
                        setFormToNoValid(selectorForm8);
                        cantidadExtensiones.addClass('error');
                    } else {
                        cantidadExtensiones.removeClass('error');
                    }
                }


            }

        });
    }
    function form8CheckDeudas() {
        var tbody = $('.tbody-deudas_propios');

        tbody.find(".tr-datos-deudas_propios").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="deudas[propios]['+row+']"]' +
                '[name!="deudas[propios]['+row+'][id]"]' +
                '[name!="deudas[propios]['+row+'][otro_tipo_deuda]"]' +
                '[name!="deudas[propios]['+row+'][otro_tipo_moneda]"]' +
                '[name!="deudas[propios]['+row+'][cuotas_restantes]"]' +
                '[name!="deudas[propios]['+row+'][otro_credito_personal]"]' +
                ',.select-tipo-deuda' +
                ',.select-tipo-moneda';

            var finds = element.find(selectorInputs);

            var selectTipoDeuda = element.find('.select-tipo-deuda');
            var valTipoDeuda = selectTipoDeuda.val();

            var otroTipoDeuda = element.find('input[name="deudas[propios]['+row+'][otro_tipo_deuda]"]');
            var valOtroTipoDeuda = otroTipoDeuda.val();

            var selectTipoMoneda = element.find('.select-tipo-moneda');
            var valTipoMoneda = selectTipoMoneda.val();

            var otroTipoMoneda = element.find('input[name="deudas[propios]['+row+'][otro_tipo_moneda]"]');
            var valOtroTipoMoneda = otroTipoMoneda.val();

            var cuotasRestantes = element.find('input[name="deudas[propios]['+row+'][cuotas_restantes]"]');
            var valCuotasRestantes = cuotasRestantes.val();

            var otroCreditoPersonal = element.find('input[name="deudas[propios]['+row+'][otro_credito_personal]"]');
            var valOtroCreditoPersonal = otroCreditoPersonal.val();

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            })

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm8);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                /**
                 * Otras especificar
                 */
                if(valTipoDeuda == 4 && empty(valOtroTipoDeuda)) {
                    setFormToNoValid(selectorForm8);
                    otroTipoDeuda.addClass('error');
                } else {
                    otroTipoDeuda.removeClass('error');
                }

                /**
                 * Plan de vivienda adjudicado
                 * Plan de ahorro adjudicado
                 */
                if( (valTipoDeuda == 9 || valTipoDeuda == 10) && empty(valCuotasRestantes)) {
                    setFormToNoValid(selectorForm8);
                    cuotasRestantes.addClass('error');
                } else {
                    cuotasRestantes.removeClass('error');
                }

                /**
                 * Credito personal -otros especificar-
                 */
                if( (valTipoDeuda == 12 ) && empty(valOtroCreditoPersonal)) {
                    setFormToNoValid(selectorForm8);
                    otroCreditoPersonal.addClass('error');
                } else {
                    otroCreditoPersonal.removeClass('error');
                }

                if(valTipoMoneda == 4 && empty(valOtroTipoMoneda)) {
                    setFormToNoValid(selectorForm8);
                    otroTipoMoneda.addClass('error');
                } else {
                    otroTipoMoneda.removeClass('error');
                }
            }

        });
    }
</script>