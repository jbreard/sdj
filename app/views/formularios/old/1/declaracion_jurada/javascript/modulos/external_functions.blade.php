<script type="application/javascript">
    function ucfirst (str) {
        //  discuss at: http://locutus.io/php/ucfirst/
        // original by: Kevin van Zonneveld (http://kvz.io)
        // bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
        // improved by: Brett Zamir (http://brett-zamir.me)
        //   example 1: ucfirst('kevin van zonneveld')
        //   returns 1: 'Kevin van zonneveld'

        str += ''
        var f = str.charAt(0)
            .toUpperCase()
        return f + str.substr(1)
    }

    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    function empty (mixedVar) {
        //  discuss at: http://locutus.io/php/empty/
        // original by: Philippe Baumann
        //    input by: Onno Marsman (https://twitter.com/onnomarsman)
        //    input by: LH
        //    input by: Stoyan Kyosev (http://www.svest.org/)
        // bugfixed by: Kevin van Zonneveld (http://kvz.io)
        // improved by: Onno Marsman (https://twitter.com/onnomarsman)
        // improved by: Francesco
        // improved by: Marc Jansen
        // improved by: Rafał Kukawski (http://blog.kukawski.pl)
        //   example 1: empty(null)
        //   returns 1: true
        //   example 2: empty(undefined)
        //   returns 2: true
        //   example 3: empty([])
        //   returns 3: true
        //   example 4: empty({})
        //   returns 4: true
        //   example 5: empty({'aFunc' : function () { alert('humpty'); } })
        //   returns 5: false

        var undef
        var key
        var i
        var len
        var emptyValues = [undef, null, false, 0, '', '0']

        for (i = 0, len = emptyValues.length; i < len; i++) {
            if (mixedVar === emptyValues[i]) {
                return true
            }
        }

        if (typeof mixedVar === 'object') {
            for (key in mixedVar) {
                if (mixedVar.hasOwnProperty(key)) {
                    return false
                }
            }
            return true
        }

        return false
    }
</script>