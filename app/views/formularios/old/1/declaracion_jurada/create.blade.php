@extends ('header_dmz')
@section('scripts')
    @include('formularios.old.1.declaracion_jurada.javascript.create')
@stop

@section('styles')
    <style>
        form label.error, p.error, .asterisk-required, .text-important {
            color:red !important;
        }
        form input.error, form select.error, p.error {
            border:1px solid red !important;
        }
    </style>
@stop


@section('content')
    <div class="row">
        <div class="form-group col-md-12" id="div-message-error">
            {{ $errors->first('redirect') }}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-12">
            <br>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#datos_caracter_declaracion" data-toggle="tab" class="link-nav-tab">Carácter de la Declaración</a></li>
                <li><a href="#datos_personales" data-toggle="tab" class="link-nav-tab">Datos Personales</a></li>
                <li><a href="#datos_familiares" data-toggle="tab" class="link-nav-tab">Datos Familiares</a></li>
                <li><a href="#datos_patrimoniales_bienes" data-toggle="tab" class="link-nav-tab">Bienes</a></li>
                <li><a href="#datos_patrimoniales_ingresos" data-toggle="tab" class="link-nav-tab">Ingresos</a></li>
                <li><a href="#datos_patrimoniales_deudas" data-toggle="tab" class="link-nav-tab">Deudas</a></li>
                <li><a href="#datos_patrimoniales_familiares" data-toggle="tab" class="link-nav-tab">Datos Patrimoniales del Grupo Familiar</a></li>
                <li class="li-submit-form" style="display: none !important;">
                    {{ Form::model(isset($decaracion_jurada)? $decaracion_jurada : null,array('id' => 'form-5'),array('role' => 'form')) }}
                        {{Form::captcha()}}
                    {{ Form::close() }}
                    <button class="btn btn-primary navbar-btn ladda-button btn-sm" id="btn-submit-form" data-style="expand-left"><span class="ladda-label">Guardar</span></button>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active " id="datos_caracter_declaracion">
                    {{ Form::model(isset($decaracion_jurada)? $decaracion_jurada : null,array('id' => 'form-6'),array('role' => 'form')) }}
                    @include('formularios.old.1.declaracion_jurada.solapas.datos_caracter_declaracion')
                    {{ Form::close() }}
                </div>
                <div class="tab-pane" id="datos_personales">
                    {{ Form::model(isset($decaracion_jurada)? $decaracion_jurada : null,array('id' => 'form-1'),array('role' => 'form')) }}
                    @include('formularios.old.1.declaracion_jurada.solapas.datos_personales')

                        @if(!is_null($declaracion_jurada))
                            <input type="hidden" name="_method" value="PATCH">
                        @endif

                    {{ Form::close() }}
                </div>
                <div class="tab-pane" id="datos_familiares">
                    {{ Form::model(isset($decaracion_jurada)? $decaracion_jurada : null,array('id' => 'form-2'),array('role' => 'form')) }}
                    @include('formularios.old.1.declaracion_jurada.solapas.datos_familiares')
                    {{ Form::close() }}
                </div>
                <div class="tab-pane" id="datos_patrimoniales_bienes">
                    {{ Form::model(isset($decaracion_jurada)? $decaracion_jurada : null,array('id' => 'form-3'),array('role' => 'form')) }}
                    @include('formularios.old.1.declaracion_jurada.solapas.datos_patrimoniales_bienes')
                    {{ Form::close() }}
                </div>
                <div class="tab-pane" id="datos_patrimoniales_ingresos">
                    {{ Form::model(isset($decaracion_jurada)? $decaracion_jurada : null,array('id' => 'form-7'),array('role' => 'form')) }}
                    @include('formularios.old.1.declaracion_jurada.solapas.datos_patrimoniales_ingresos')
                    {{ Form::close() }}
                </div>
                <div class="tab-pane" id="datos_patrimoniales_deudas">
                    {{ Form::model(isset($decaracion_jurada)? $decaracion_jurada : null,array('id' => 'form-8'),array('role' => 'form')) }}
                    @include('formularios.old.1.declaracion_jurada.solapas.datos_patrimoniales_deudas')
                    {{ Form::close() }}
                </div>
                <div class="tab-pane" id="datos_patrimoniales_familiares">
                    {{ Form::model(isset($decaracion_jurada)? $decaracion_jurada : null,array('id' => 'form-4'),array('role' => 'form')) }}
                    @include('formularios.old.1.declaracion_jurada.solapas.datos_patrimoniales_grupo_familiar')
                    {{ Form::close() }}
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-12">
            <p class="bg-danger">
                <a class="text-important"><i class='glyphicon glyphicon-check' ></i></a> {{ Config::get('app.textos.declaracion_legal'); }}
            </p>
        </div>
    </div>

    <input type="hidden" name="id_seleccionado" value="" />
    <input type="hidden" name="modelo" value="" />
    @include('components.modal',['accion' => 'Eliminar','id' => 1])

@stop