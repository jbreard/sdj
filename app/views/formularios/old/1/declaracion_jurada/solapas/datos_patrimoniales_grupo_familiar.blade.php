<div class="row">
    <div class="form-group col-md-12">
        <br>
        <a class="btn btn-primary btnPrevious" >Anterior</a>
        -
        <a class="btn btn-success btnConfirm" id="btn-submit-form-4" >Confirmar</a>
    </div>
    <div class="col-md-12">
        <div class="div-error-form alert alert-danger" role="alert" style="display: none;">
            <strong>Atención: </strong>Hay campos en el formulario que debe completar para continuar la carga.
        </div>
    </div>
</div>
<h2>Cónyuge o conviviente actual</h2>
<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Inmuebles</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="inmuebles_conyuge" data-count="0">[Añadir registro]</button>
        <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
            <h4>Notas de carga</h4>
            <p>
                {{ Config::get('app.textos.notas_carga.inmueble.a'); }}
                <br>
                <br>
                {{ Config::get('app.textos.notas_carga.inmueble.b'); }}
            </p>
        </div>
    </caption>

    <tbody class="tbody-inmuebles_conyuge">
    @if(is_null($declaracion_jurada))
        @include('formularios.old.1.declaracion_jurada.modulos.inmuebles_conyuge',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_inmueble['conyuge']; $i++)
            @include('formularios.old.1.declaracion_jurada.modulos.inmuebles_conyuge',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Vehículos: Terrestres / Embarcaciones / Aeronaves</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="vehiculos_conyuge" data-count="0">[Añadir registro]</button>
        <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
            <h4>Notas de carga</h4>
            <p>
                {{ Config::get('app.textos.notas_carga.vehiculos.a'); }}
                <br>
                <br>
                {{ Config::get('app.textos.notas_carga.vehiculos.b'); }}
            </p>
        </div>
    </caption>
    <tbody class="tbody-vehiculos_conyuge">
    @if(is_null($declaracion_jurada))
        @include('formularios.old.1.declaracion_jurada.modulos.vehiculos_conyuge',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_vehiculo['conyuge']; $i++)
            @include('formularios.old.1.declaracion_jurada.modulos.vehiculos_conyuge',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Otros bienes</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="bienes_conyuge" data-count="0">[Añadir registro]</button>
    </caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Descripción</th>
        <th>Carácter</th>
        <th>Modo de adquisición</th>
        <th>Monto</th>
    </tr>
    </thead>
    <tbody class="tbody-bienes_conyuge">
    @if(is_null($declaracion_jurada))
        @include('formularios.old.1.declaracion_jurada.modulos.bienes_conyuge',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_bien['conyuge']; $i++)
            @include('formularios.old.1.declaracion_jurada.modulos.bienes_conyuge',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Derechos en expectativa</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="derecho_expectativa_conyuge" data-count="0">[Añadir registro]</button>
        <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
            <h4>Notas de carga</h4>
            <p>{{ Config::get('app.textos.notas_carga.derecho_expectativa.a'); }}</p>
        </div>
    </caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Tipo</th>
        <th>Fuente</th>
    </tr>
    </thead>
    <tbody class="tbody-derecho_expectativa_conyuge">
    @if(is_null($declaracion_jurada))
        @include('formularios.old.1.declaracion_jurada.modulos.derecho_expectativa_conyuge',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_derecho_expectativa['conyuge']; $i++)
            @include('formularios.old.1.declaracion_jurada.modulos.derecho_expectativa_conyuge',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Deudas</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="deudas_conyuge" data-count="0">[Añadir registro]</button>
    </caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Tipo de deuda</th>
        <th>Acreedor</th>
        <th>Moneda</th>
        <th>Saldo que adeuda</th>
    </tr>
    </thead>
    <tbody class="tbody-deudas_conyuge">
    @if(is_null($declaracion_jurada))
        @include('formularios.old.1.declaracion_jurada.modulos.deudas_conyuge',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_deuda['conyuge']; $i++)
            @include('formularios.old.1.declaracion_jurada.modulos.deudas_conyuge',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<h2>Hijos menores no emancipados</h2>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Inmuebles</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="inmuebles_hijo" data-count="0">[Añadir registro]</button>
        <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
            <h4>Notas de carga</h4>
            <p>
                {{ Config::get('app.textos.notas_carga.inmueble.a'); }}
                <br>
                <br>
                {{ Config::get('app.textos.notas_carga.inmueble.b'); }}
            </p>
        </div>
    </caption>
    <tbody class="tbody-inmuebles_hijo">
    @if(is_null($declaracion_jurada))
        @include('formularios.old.1.declaracion_jurada.modulos.inmuebles_hijo',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_inmueble['hijo']; $i++)
            @include('formularios.old.1.declaracion_jurada.modulos.inmuebles_hijo',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Vehículos: Terrestres / Embarcaciones / Aeronaves</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="vehiculos_hijo" data-count="0">[Añadir registro]</button>
        <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
            <h4>Notas de carga</h4>
            <p>
                {{ Config::get('app.textos.notas_carga.vehiculos.a'); }}
                <br>
                <br>
                {{ Config::get('app.textos.notas_carga.vehiculos.b'); }}
            </p>
        </div>
    </caption>
    <tbody class="tbody-vehiculos_hijo">
    @if(is_null($declaracion_jurada))
        @include('formularios.old.1.declaracion_jurada.modulos.vehiculos_hijo',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_vehiculo['hijo']; $i++)
            @include('formularios.old.1.declaracion_jurada.modulos.vehiculos_hijo',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Otros bienes</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="bienes_hijo" data-count="0">[Añadir registro]</button>
    </caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Descripción</th>
        <th>Carácter</th>
        <th>Modo de adquisición</th>
        <th>Monto</th>
    </tr>
    </thead>
    <tbody class="tbody-bienes_hijo">
    @if(is_null($declaracion_jurada))
        @include('formularios.old.1.declaracion_jurada.modulos.bienes_hijo',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_bien['hijo']; $i++)
            @include('formularios.old.1.declaracion_jurada.modulos.bienes_hijo',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Derechos en expectativa</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="derecho_expectativa_hijo" data-count="0">[Añadir registro]</button>
        <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
            <h4>Notas de carga</h4>
            <p>{{ Config::get('app.textos.notas_carga.derecho_expectativa.a'); }}</p>
        </div>
    </caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Tipo</th>
        <th>Fuente</th>
    </tr>
    </thead>
    <tbody class="tbody-derecho_expectativa_hijo">
    @if(is_null($declaracion_jurada))
        @include('formularios.old.1.declaracion_jurada.modulos.derecho_expectativa_hijo',['i'=>0])
    @else
        @for ($i = 0; $i <  $cantidad_derecho_expectativa['hijo']; $i++)
            @include('formularios.old.1.declaracion_jurada.modulos.derecho_expectativa_hijo',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>

<table class="table table-striped table-condensed">
    <caption>
        <span class="title">Deudas</span>
        <button type="button" class="btn btn-link btn-add-registry" data-type_registry="deudas_hijo" data-count="0">[Añadir registro]</button>
    </caption>
    <thead>
    <tr>
        <th>#</th>
        <th>Tipo de deuda</th>
        <th>Acreedor</th>
        <th>Moneda</th>
        <th>Saldo que adeuda</th>
    </tr>
    </thead>
    <tbody class="tbody-deudas_hijo">
    @if(is_null($declaracion_jurada))
        @include('formularios.old.1.declaracion_jurada.modulos.deudas_hijo',['i'=>0])
    @else
        @for ($i = 0; $i < $cantidad_deuda['hijo']; $i++)
            @include('formularios.old.1.declaracion_jurada.modulos.deudas_hijo',['i'=>$i])
        @endfor
    @endif
    </tbody>
</table>