@for ($i = 0; $i < $cantidad_ingreso; $i++)
    <tr>
        <td>
            @if($i==0)
                <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
                <b><span class="span-fuerza-pertenece">XXXXXX</span></b>
            @elseif($i==1)
                <b>Adicional <span class="span-fuerza-pertenece">XXXXXX</span></b>
            @elseif($i==2)
                <b>Otros Ingresos</b>
            @endif
        </td>
        <td>
            {{ Form::select("ingresos[$i][id_moneda]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : (!array_key_exists($i,$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso'][$i]['id_moneda']),array('class' => 'form-control select-tipo-moneda')) }}
            <div>{{ Form::text("ingresos[$i][otro_tipo_moneda]",(is_null($declaracion_jurada) ? null : (!array_key_exists($i,$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso'][$i]['otro_tipo_moneda']),array('class' => 'form-control input-otro')); }}</div>
        </td>
        <td>
            {{ Form::text("ingresos[$i][monto_anual_aproximado]",(is_null($declaracion_jurada) ? null : (!array_key_exists($i,$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso'][$i]['monto_anual_aproximado']),array('class' => 'form-control')); }}
            {{ Form::hidden("ingresos[$i][id]", (is_null($declaracion_jurada) ? null : (!array_key_exists($i,$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso'][$i]['id'])) }}
            {{ Form::hidden("ingresos[$i][id_salario]", (is_null($declaracion_jurada) ? null : (!array_key_exists($i,$declaracion_jurada['ingreso'])) ? null : $declaracion_jurada['ingreso'][$i]['id_salario']),array('class'=>'datos-propios-ingresos')) }}
            @if($i==0)
                {{ Form::hidden("ingresos[$i][tipo_ingreso]",1,array('class' => '')); }}
                {{ $errors->first('monto_anual_aproximado') }}
            @elseif($i==1)
                {{ Form::hidden("ingresos[$i][tipo_ingreso]",2,array('class' => '')); }}
            @elseif($i==2)
                {{ Form::hidden("ingresos[$i][tipo_ingreso]",3,array('class' => '')); }}
            @endif
        </td>
    </tr>
@endfor