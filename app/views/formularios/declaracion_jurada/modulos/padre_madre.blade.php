<?php
$persona = isset($declaracion_jurada['persona']['familiar']['padre_madre'][$i]) ? $declaracion_jurada['persona']['familiar']['padre_madre'][$i] : null;
$isNull = (is_null($declaracion_jurada) && is_null($persona));
$apellido = ( $isNull ? null : $persona['persona']['apellido']);
$nombres = ($isNull ? null : $persona['persona']['nombres']);
$nacimiento = ($isNull ? null : $persona['persona']['nacimiento']);
$tiposVinculo = ($isNull ? null : $persona['persona']['id_tipo_vinculo']);
$tiposDocumento = ($isNull ? null : $persona['persona']['id_tipo_documento']);
$documento = ($isNull ? null : $persona['persona']['documento']);
$id = ($isNull ? "new".$i : $persona['id_familiar']);
?>
<tr class="tr-datos-padre_madre padre_madre-{{$id}}" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','padre_madre','¿Confirma que desea eliminar registro de padre y madre?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>{{ Form::text("datos_padre_madre[$i][apellido]",$apellido,array('class' => 'form-control')) }}</td>
    <td>{{ Form::text("datos_padre_madre[$i][nombres]",$nombres,array('class' => 'form-control')) }}</td>
    <td>{{ Form::select("datos_padre_madre[$i][id_tipo_vinculo]",$combo_tipos_vinculos_padre_madre,($isNull ? null : $tiposVinculo),array('class' => 'form-control select-padre_madre-tipo_vinculo')) }}</td>
    <td>{{ Form::select("datos_padre_madre[$i][id_tipo_documento]",$combo_tipos_documentos,($isNull ? null : $tiposDocumento),array('class' => 'form-control select-padre_madre-tipo_documento')) }}</td>
    <td>{{ Form::text("datos_padre_madre[$i][documento]",$documento,array('class' => 'form-control masked-input-dni','size'=>'8')) }}</td>
    <td>
        {{ Form::text("datos_padre_madre[$i][nacimiento]",$nacimiento,array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento')) }}
        {{ Form::hidden("datos_padre_madre[$i][id]", $id) }}
    </td>
</tr>