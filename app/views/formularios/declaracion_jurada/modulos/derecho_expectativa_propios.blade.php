<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['derecho_expectativa']['propio'][$i]['id'];
?>
<tr class="derechos_expectativas_propios-{{$id}} tr-datos-derecho_expectativa_propios" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','derechos_expectativas_propios','¿Confirma que desea eliminar el derecho en expectativa propio?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>{{ Form::select("derecho_expectativa[propios][$i][id_tipo_derechos_expectativa]",$combo_tipos_derecho_expectativa,(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['propio'][$i]['id_tipo_derechos_expectativa']),array('class' => 'form-control select-tipo-derecho-expectativa')) }}</td>
    <td>
        {{ Form::select("derecho_expectativa[propios][$i][id_tipo_fuente]",$combo_tipos_fuentes,(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['propio'][$i]['id_tipo_fuente']),array('class' => 'form-control select-tipo-fuente')) }}
        <div class="div-fuente-tipo-cuotas">
            Cantidad cuotas pagas:
            {{ Form::text("derecho_expectativa[propios][$i][cantidad_cuotas_pagas]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['propio'][$i]['cantidad_cuotas_pagas']),array('class' => "form-control input-otro")); }}
            Cantidad cuotas restantes a pagar:
            {{ Form::text("derecho_expectativa[propios][$i][cantidad_cuotas_restantes]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['propio'][$i]['cantidad_cuotas_restantes']),array('class' => "form-control input-otro")); }}
        </div>
        {{ Form::hidden("derecho_expectativa[propios][$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['derecho_expectativa']['propio'][$i]['id'])) }}
    </td>
</tr>