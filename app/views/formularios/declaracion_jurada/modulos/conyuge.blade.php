 <?php
$persona = isset($declaracion_jurada['persona']['familiar']['conyuge'][$i]) ? $declaracion_jurada['persona']['familiar']['conyuge'][$i] : null;
$isNull = (is_null($declaracion_jurada) && is_null($persona));
$apellido = ( $isNull ? null : $persona['persona']['apellido']);
$nombres = ($isNull ? null : $persona['persona']['nombres']);
$nacimiento = ($isNull ? null : $persona['persona']['nacimiento']);
$documento = ($isNull ? null : $persona['persona']['documento']);
$ocupacion = ($isNull ? null : $persona['persona']['ocupacion']);
$desde_cuando_trabaja = ($isNull ? null : $persona['persona']['desde_cuando_trabaja']);
$domicilioLaboral = ($isNull ? null : $persona['persona']['domicilio_laboral']);
$tiposCategoriasTributarias = ($isNull ? null : $persona['persona']['id_tipo_categoria_tributaria']);
$tiposVinculo = ($isNull ? null : $persona['persona']['id_tipo_vinculo']);
$personalFuerza = ($isNull ? null : $persona['persona']['personal_fuerza']);
$ingreso_neto_anual = ($isNull ? null : $persona['persona']['ingreso_neto_anual']);
$idFuerza = ($isNull ? null : $persona['persona']['id_fuerza']);
$id = ($isNull ? "new".$i : $persona['id_familiar']);

 if($isNull) {
     $trabaja = null;
 }else {
     if($persona['persona']['trabaja'] == 'Si'){
         $trabaja = 2;
     }else if($persona['persona']['trabaja'] == 'No'){
         $trabaja = 1;
     }else {
         $trabaja = 0;
     }
 }


?>
<tr class="conyuge-{{$id}}">
    <td colspan="5" class="inmuebles_header_registry"># {{ $i }}
        <a title="Eliminar" onclick="eliminar('{{ $id }}','conyuge','¿Confirma que desea eliminar el conyuge o conviviente actual?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a>
    </td>
</tr>
<tr class="conyuge-{{$id}}">
    <th>Apellido</th>
    <th>Nombres</th>
    <th>Fecha de nacimiento</th>
    <th>DNI</th>
    <th>Vínculo</th>
</tr>
 <tr class="conyuge-{{$id}} tr-1-datos-conyuge" data-row="{{$i}}">
    <td>{{ Form::text("datos_conyuge[$i][apellido]",($isNull ? null : $apellido),array('class' => 'form-control')); }}</td>
    <td>{{ Form::text("datos_conyuge[$i][nombres]",($isNull ? null : $nombres),array('class' => 'form-control')); }}</td>
    <td>{{ Form::text("datos_conyuge[$i][nacimiento]",($isNull ? null : $nacimiento),array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento')); }}</td>
    <td>{{ Form::text("datos_conyuge[$i][documento]",($isNull ? null : $documento),array('class' => 'form-control masked-input-dni','size'=>'8')); }}</td>
    <td>{{ Form::select("datos_conyuge[$i][id_tipo_vinculo]",$combo_tipos_vinculos_conviviente,($isNull ? null : $tiposVinculo),array('class' => 'form-control select-conyuge-vinculo')); }}</td>
</tr>
 <tr class="conyuge-{{$id}} tr-2-datos-conyuge-{{$i}}">
    <th>Trabaja</th>
    <th>{{ Form::select("datos_conyuge[$i][trabaja]",$combo_tipo_trabaja,($isNull ? null : $trabaja),array('class' => 'form-control select-conyuge-trabaja', "data-row"=>"$i")); }}</th>
    <th colspan="3"></th>
</tr>
 <tr class="conyuge-{{$id}} tr-datos-conyuge-trabaja-{{$i}}">
    <th>¿Desde cuando trabaja?</th>
    <th>Ingreso neto anual</th>
    <th>Ocupación</th>
    <th>Categoría tributaria</th>
</tr>
 <tr class="conyuge-{{$id}} tr-datos-conyuge-trabaja-{{$i}} tr-3-datos-conyuge-{{$i}}" data-row="{{$i}}">
    <td>{{ Form::text("datos_conyuge[$i][desde_cuando_trabaja]",($isNull ? null : $desde_cuando_trabaja),array('class' => "form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento required-conyuge-trabaja-{$i}")); }}</td>
    <td>{{ Form::text("datos_conyuge[$i][ingreso_neto_anual]",($isNull ? null : $ingreso_neto_anual),array('class' => "form-control required-conyuge-trabaja-{$i}")); }}</td>
    <td>
        {{ Form::text("datos_conyuge[$i][ocupacion]",($isNull ? null : $ocupacion),array('class' => "form-control required-conyuge-trabaja-{$i}")); }}
    </td>
    <td>{{ Form::select("datos_conyuge[$i][id_tipo_categoria_tributaria]",$combo_tipos_categorias_tributarias,($isNull ? null : $tiposCategoriasTributarias),array('class' => "form-control select-tipo-categoria-tributaria-{$i} ")); }}
        {{ Form::hidden("datos_conyuge[$i][id]", ($isNull ? null : $id)) }}
    </td>
</tr>
