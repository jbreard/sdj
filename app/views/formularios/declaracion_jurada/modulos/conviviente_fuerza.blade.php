<?php
$persona = isset($declaracion_jurada['persona']['familiar']['conviviente_fuerza'][$i]) ? $declaracion_jurada['persona']['familiar']['conviviente_fuerza'][$i] : null;
$isNull = (is_null($declaracion_jurada) && is_null($persona));
$apellido = ( $isNull ? null : $persona['persona']['apellido']);
$nombres = ($isNull ? null : $persona['persona']['nombres']);
$nacimiento = ($isNull ? null : $persona['persona']['nacimiento']);
$tiposFuerza = ($isNull ? null : $persona['persona']['id_fuerza']);
$tiposVinculo = ($isNull ? null : $persona['persona']['id_tipo_vinculo']);
$documento = ($isNull ? null : $persona['persona']['documento']);
$tipoActividad = ($isNull ? null : $persona['persona']['id_tipo_actividad']);
$id = ($isNull ? "new".$i : $persona['id_familiar']);

?>
<tr class="tr-datos-conviviente_fuerza conviviente_fuerza-{{$id}}" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','conviviente_fuerza','¿Confirma que desea eliminar registro de otras personas convivientes?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>{{ Form::text("datos_conviviente_fuerza[$i][nombres]",$nombres,array('class' => 'form-control')) }}</td>
    <td>{{ Form::text("datos_conviviente_fuerza[$i][apellido]",$apellido,array('class' => 'form-control')) }}</td>
    <td>{{ Form::select("datos_conviviente_fuerza[$i][id_tipo_vinculo]",$combo_tipos_vinculos_fuerza,($isNull ? null : $tiposVinculo),array('class' => 'form-control select-conviviente-tipo_vinculo')) }}</td>
    <td>{{ Form::select("datos_conviviente_fuerza[$i][id_fuerza]",$combo_tipos_fuerza_conviviente,($isNull ? null : $tiposFuerza),array('class' => 'form-control select-conviviente_fuerza-fuerzas')) }}</td>
    <td>
        {{ Form::select("datos_conviviente_fuerza[$i][id_tipo_actividad]",$combo_tipo_actividad,($isNull ? null : $tipoActividad),array('class' => 'form-control select-conviviente_fuerza-actividad')) }}
        {{ Form::hidden("datos_conviviente_fuerza[$i][id]", $id) }}
    </td>
</tr>