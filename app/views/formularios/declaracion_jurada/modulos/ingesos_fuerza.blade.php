<?php
$isNull = (is_null($declaracion_jurada));
$existsIngreso = ($isNull) ? false : array_key_exists('ingreso',$declaracion_jurada);
$notExists = ($existsIngreso) ? !array_key_exists('fuerza',$declaracion_jurada['ingreso']) : true;
$monto_anual_neto = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['fuerza']['monto_anual_neto']);
$monto_anual_bruto = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['fuerza']['monto_anual_bruto']);
$id_salario = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['fuerza']['id_salario']);
$id = (is_null($declaracion_jurada) ? null : ($notExists) ? null : $declaracion_jurada['ingreso']['fuerza']['id']);
?>
<tr>
    <td>
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        <b><span class="span-fuerza-pertenece">XXXXXX</span></b>
    </td>
    <td>
        {{ Form::text("ingresos[fuerza][monto_anual_neto]",$monto_anual_neto,array('class' => 'form-control')); }}
    </td>
    <td>
        {{ Form::text("ingresos[fuerza][monto_anual_bruto]",$monto_anual_bruto,array('class' => 'form-control')); }}
        {{ Form::hidden("ingresos[fuerza][id_salario]", $id_salario,array('class'=>'datos-propios-ingresos')) }}
        {{ Form::hidden("ingresos[fuerza][tipo_ingreso]",1,array('class' => '')); }}
        {{ Form::hidden("ingresos[fuerza][id]", $id) }}
        {{ $errors->first('monto_anual_neto') }}
    </td>
</tr>