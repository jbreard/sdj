<?php
$id = is_null($declaracion_jurada) ? "new".$i : $declaracion_jurada['bonos_titulos_acciones'][$i]['id'];
?>
<tr class="cuenta_bancaria_bonos_titulos_acciones-{{$id}} tr-datos-bonos_titulos_acciones" data-row="{{$i}}">
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','cuenta_bancaria_bonos_titulos_acciones','¿Confirma que desea eliminar registro de bonos, títulos valores y/o acciones?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>
        {{ Form::select("bonos_titulos_acciones[$i][id_tipo_bono]",$combo_tipos_bonos,(is_null($declaracion_jurada) ? null : $declaracion_jurada['bonos_titulos_acciones'][$i]['id_tipo_bono']),array('class' => 'form-control select-tipo-bono')) }}
        <div>
            {{ Form::text("bonos_titulos_acciones[$i][otro_tipo_bono]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['bonos_titulos_acciones'][$i]['otro_tipo_bono']),array('class' => 'form-control input-otro')); }}
        </div>
    </td>
    <td>{{ Form::text("bonos_titulos_acciones[$i][descripcion]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['bonos_titulos_acciones'][$i]['descripcion']),array('class' => 'form-control','maxlength' => 50)); }}</td>
    <td>{{ Form::text("bonos_titulos_acciones[$i][cantidad]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['bonos_titulos_acciones'][$i]['cantidad']),array('class' => 'form-control','maxlength' => 50)); }}</td>
    <td>{{ Form::text("bonos_titulos_acciones[$i][valor_nominal]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['bonos_titulos_acciones'][$i]['valor_nominal']),array('class' => 'form-control','maxlength' => 50)); }}</td>
    <td>{{ Form::text("bonos_titulos_acciones[$i][entidad_emisora_otorgante]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['bonos_titulos_acciones'][$i]['entidad_emisora_otorgante']),array('class' => 'form-control','maxlength' => 50)); }}</td>
    <td>
        {{ Form::text("bonos_titulos_acciones[$i][fecha_adquisicion]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['bonos_titulos_acciones'][$i]['fecha_adquisicion']),array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento')); }}
        {{ Form::hidden("bonos_titulos_acciones[$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['bonos_titulos_acciones'][$i]['id'])) }}
    </td>
</tr>