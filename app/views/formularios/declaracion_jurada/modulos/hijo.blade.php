<?php
$persona = isset($declaracion_jurada['persona']['familiar']['hijo'][$i]) ? $declaracion_jurada['persona']['familiar']['hijo'][$i] : null;
$isNull = (is_null($declaracion_jurada) && is_null($persona));
$apellido = ( $isNull ? null : $persona['persona']['apellido']);
$nombres = ($isNull ? null : $persona['persona']['nombres']);
$nacimiento = ($isNull ? null : $persona['persona']['nacimiento']);
$documento = ($isNull ? null : $persona['persona']['documento']);
$idHijoConvive = ($isNull ? null : $persona['persona']['id_tipo_hijo_convive']);
$id = ($isNull ? "new".$i : $persona['id_familiar']);
?>
<tr class="tr-datos-hijo hijo-{{$id}}" data-row="{{$i}}" >
    <td><a title="Eliminar" onclick="eliminar('{{ $id }}','hijo','¿Confirma que desea eliminar registro de hijos no emancipados?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a></td>
    <td>{{ Form::text("datos_hijo[$i][apellido]",($isNull ? null : $apellido),array('class' => 'form-control input-required apellido')); }}</td>
    <td>{{ Form::text("datos_hijo[$i][nombres]",($isNull ? null : $nombres),array('class' => 'form-control input-required nombre')); }}</td>
    <td>{{ Form::text("datos_hijo[$i][nacimiento]",($isNull ? null : $nacimiento),array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento input-required')); }}</td>
    <td>{{ Form::text("datos_hijo[$i][documento]",($isNull ? null : $documento),array('class' => 'form-control masked-input-dni input-required','size'=>'8')); }}</td>
    <td>
        {{ Form::select("datos_hijo[$i][id_tipo_hijo_convive]",$combo_hijo_convive,($isNull ? null : $idHijoConvive),array('class' => 'form-control input-required select-hijo-convive')) }}
        {{ Form::hidden("datos_hijo[$i][id]", ($isNull ? null : $id)) }}
    </td>
</tr>