<?php
$id_tipo_ingreso = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['id_tipo_ingreso'];
$monto_anual_neto = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['monto_anual_neto'];
$id_tipo_moneda = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['id_tipo_moneda'];
$otro_tipo_ingreso = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['otro_tipo_ingreso'];
$otro_tipo_ingreso_extraordinario = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['otro_tipo_ingreso_extraordinario'];
$otro_tipo_moneda = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['otro_tipo_moneda'];
$id_salario = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['id_salario'];
$id_ingreso_extraordinario = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['id_ingreso_extraordinario'];
$id_ingreso_otra_actividad = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['id_ingreso_otra_actividad'];
$id_categoria_tributaria = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['id_categoria_tributaria'];
$id = (is_null($declaracion_jurada)) ? null : $declaracion_jurada['ingreso_externo']['id'];
?>
<tr>
    <td>
        <b><span class="span-fuerza-pertenece">XXXXXX</span></b>
    </td>
    <td class="td-ingresos-externos">
        {{ Form::select("ingresos_externos[id_tipo_ingreso]",$combo_tipos_ingresos_externos,$id_tipo_ingreso,array('class' => 'form-control select-tipo-ingreso-externo')); }}
        <div class="div-otro-ingreso">
            {{ Form::text("ingresos_externos[otro_tipo_ingreso]", $otro_tipo_ingreso,array('class' => 'form-control input-otro')); }}
        </div>

        <div class="div-ingreso-extraordinario">
            {{ Form::select("ingresos_externos[id_ingreso_extraordinario]",$combo_tipos_ingresos_extraordinario_externo,$id_ingreso_extraordinario,array('class' => 'form-control select-ingreso-extraordinario')); }}
            <div class="div-ingreso-extraordinario-otros">{{ Form::text("ingresos_externos[otro_tipo_ingreso_extraordinario]", $otro_tipo_ingreso_extraordinario,array('class' => 'form-control input-otro')); }}</div>
        </div>

        <div class="div-ingreso-actividad">
            {{ Form::select("ingresos_externos[id_ingreso_otra_actividad]",$combo_tipos_ingresos_actividades,$id_ingreso_otra_actividad,array('class' => 'form-control select-ingreso-otra-actividad input-otro')); }}
        </div>
        <div class="div-categoria-tributaria">
            {{ Form::select("ingresos_externos[id_categoria_tributaria]",$combo_tipos_categorias_tributarias,$id_categoria_tributaria,array('class' => 'form-control select-categoria-tributaria input-otro')); }}
        </div>
    </td>
    <td>
        {{ Form::text("ingresos_externos[monto_neto_anual_total]", $monto_anual_neto,array('class' => 'form-control')); }}
    </td>
    <td>
        {{ Form::select("ingresos_externos[id_tipo_moneda]",$combo_tipos_moneda,$id_tipo_moneda,array('class' => 'form-control select-tipo-moneda')); }}
        <div>{{ Form::text("ingresos_externos[otro_tipo_moneda]", $otro_tipo_moneda,array('class' => 'form-control input-otro')); }}</div>
        {{ Form::hidden("ingresos_externos[id_fuerza]", $id_salario,array('class'=>'datos-propios-ingresos')) }}
        {{ Form::hidden("ingresos_externos[id]", $id) }}
    </td>
</tr>