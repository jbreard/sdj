<?php
$isNull = (is_null($declaracion_jurada));
$id = ($isNull ? "new".$i : $declaracion_jurada['inmueble']['conyuge'][$i]['id']);
?>



<tr class="inmuebles_conyuge-{{$id}}">
    <td colspan="7" class="inmuebles_header_registry"># {{ $i + 1 }}
    <a title="Eliminar" onclick="eliminar('{{ $id }}','inmuebles_conyuge','¿Confirma que desea eliminar el inmueble del conyuge o conviviente actual?')" style="color: red; cursor:pointer"><i class='glyphicon glyphicon-remove' ></i></a>
    </td></tr>
<tr class="inmuebles_conyuge-{{$id}}">
    <th>Tipo</th>
    <th>Total de superficie cubierta</th>
    <th>Total de superficie descubierta</th>
    <th>Unidad</th>
    <th>Carácter</th>
    <th colspan="2">% de titularidad</th>
</tr>
<tr class="inmuebles_conyuge-{{$id}} tr-1-datos-inmuebles_conyuge" data-row="{{$i}}">
    <td>{{ Form::select("inmuebles[conyuge][$i][id_tipo_inmueble]",$combo_tipos_inmueble,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_tipo_inmueble']),array('class' => 'form-control select-tipo-inmueble')) }}</td>
    <td>{{ Form::text("inmuebles[conyuge][$i][superficie_cubierta]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['superficie_cubierta']),array('class' => 'form-control masked-input-superficie')); }}</td>
    <td>{{ Form::text("inmuebles[conyuge][$i][superficie_descubierta]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['superficie_descubierta']),array('class' => 'form-control masked-input-superficie')); }}</td>
    <td>{{ Form::select("inmuebles[conyuge][$i][id_unidad_superficie]",$combo_unidades_superficie,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_unidad_superficie']),array('class' => 'form-control select-unidad-superficie')) }}</td>
    <td>{{ Form::select("inmuebles[conyuge][$i][id_caracter]",$combo_tipos_caracter,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_caracter']),array('class' => 'form-control select-caracter')) }}</td>
    <td colspan="2">{{ Form::text("inmuebles[conyuge][$i][porcentaje]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['porcentaje']),array('class' => 'form-control masked-input-porcentaje ')); }}</td>
</tr>

<tr class="inmuebles_conyuge-{{$id}}">
    <th>Fecha de escritura</th>
    <th>Valor de adquisición</th>
    <th>Moneda</th>
    <th>Valor estimado de mercado</th>
    <th>Moneda</th>
    <th>Modo de adquisición</th>
    <th colspan="2">Forma de pago</th>
</tr>

<tr class="inmuebles_conyuge-{{$id}} tr-2-datos-inmuebles_conyuge-{{$i}}" data-row="{{$i}}">
    <td>{{ Form::text("inmuebles[conyuge][$i][fecha_escritura]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['fecha_escritura']),array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento')); }}</td>
    <td>{{ Form::text("inmuebles[conyuge][$i][valor_adquisicion]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['valor_adquisicion']),array('class' => 'form-control')); }}</td>
    <td>
        {{ Form::select("inmuebles[conyuge][$i][id_tipo_moneda_valor_adquisicion]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_tipo_moneda_valor_adquisicion']),array('class' => 'form-control select-tipo-moneda select-tipo-moneda-valor-adquisicion')) }}
        <div>{{ Form::text("inmuebles[conyuge][$i][otro_tipo_moneda_valor_adquisicion]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['otro_tipo_moneda_valor_adquisicion']),array('class' => 'form-control input-otro')); }}</div>

    </td>
    <td>{{ Form::text("inmuebles[conyuge][$i][valor_estimado]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['valor_estimado']),array('class' => 'form-control')); }}</td>
    <td>
        {{ Form::select("inmuebles[conyuge][$i][id_tipo_moneda_valor_estimado]",$combo_tipos_moneda,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_tipo_moneda_valor_estimado']),array('class' => 'form-control select-tipo-moneda select-tipo-moneda-valor-estimado')) }}
        <div>{{ Form::text("inmuebles[conyuge][$i][otro_tipo_moneda_valor_estimado]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['otro_tipo_moneda_valor_estimado']),array('class' => 'form-control input-otro')); }}</div>

    </td>
    <td>{{ Form::select("inmuebles[conyuge][$i][id_modo_adquisicion]",$combo_tipos_modos_adquisicion_inmueble,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_modo_adquisicion']),array('class' => 'form-control select-modo-adquisicion')) }}</td>
    <td colspan="2">
        {{ Form::select("inmuebles[conyuge][$i][id_forma_pago]",$combo_tipos_formas_pago,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_forma_pago']),array('class' => 'form-control select-forma-pago')) }}
        <div>
            {{ Form::text("inmuebles[conyuge][$i][otra_forma_pago]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['otra_forma_pago']),array('class' => 'form-control input-otro')); }}
        </div>
    </td>
</tr>
<tr class="inmuebles_conyuge-{{$id}}">
    <th colspan="6">Radicación</th>
</tr>
<tr class="inmuebles_conyuge-{{$id}} tr-3-datos-inmuebles_conyuge-{{$i}}">
    <th>Pais <div class="div-pais-{{$i}}"></div></th>
    <th>Provincia/Estado <div class="div-provincia-{{$i}}"></div></th>
    <th>Localidad <div class="div-localidad-{{$i}}"></div></th>
    <th colspan="3"></th>
</tr>
<tr class="inmuebles_conyuge-{{$id}} tr-4-datos-inmuebles_conyuge-{{$i}}" data-row="{{$i}}">
    <td>
        {{ Form::select("inmuebles[conyuge][$i][id_pais]",$combo_paises,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_pais']),array(
        'class' => 'form-control select2 select-paises-conyuge',
        'data-row'=>$i,
        'data-id-provincia' => (is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_provincia']),
        'data-id-localidad' => (is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_localidad']),
        ))}}
    </td>
    <td>
        <div class="div-inmueble-conyuge-provincia-{{$i}}">
            {{ Form::select("inmuebles[conyuge][$i][id_provincia]",$combo_provincias,(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_provincia']),array('class' => 'form-control select2 select2-provincias','data-localidad'=>"inmuebles_conyuge_localidad_$i", 'data-id-localidad'=>(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_localidad']))) }}
        </div>
        <div class="div-provincia-otro-pais-{{$i}}">
            {{ Form::text("inmuebles[conyuge][$i][otro_pais_provincia]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['otro_pais_provincia']),array('class' => 'form-control')); }}
        </div>
    </td>
    <td>
        <div class="div-inmueble-conyuge-localidad-{{$i}}">
            {{ Form::select("inmuebles[conyuge][$i][id_localidad]",[],(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id_localidad']),array('class' => "form-control inmuebles_conyuge_localidad_$i  select-localidad")) }}
        </div>
        <div class="div-localidad-otro-pais-{{$i}}">
            {{ Form::text("inmuebles[conyuge][$i][otro_pais_localidad]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['otro_pais_localidad']),array('class' => 'form-control')); }}
        </div>
    </td>
    <td colspan="3">
        {{ Form::hidden("inmuebles[conyuge][$i][id]", (is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['id'])) }}
    </td>
</tr>
<tr class="inmuebles_conyuge-{{$id}}">
    <th colspan="3">Ruta/Avenida/Calle <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a></th>
    <th>Número/KM <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a></th>
    <th>Piso</th>
    <th>Depto</th>
</tr>
<tr class="inmuebles_conyuge-{{$id}} tr-5-datos-inmuebles_conyuge-{{$i}}" data-row="{{$i}}">
    <td colspan="3">{{ Form::text("inmuebles[conyuge][$i][domicilio_calle]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['domicilio_calle']),array('class' => 'form-control')); }}</td>
    <td>{{ Form::text("inmuebles[conyuge][$i][domicilio_numero]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['domicilio_numero']),array('class' => 'form-control')); }}</td>
    <td>{{ Form::text("inmuebles[conyuge][$i][domicilio_piso]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['domicilio_piso']),array('class' => 'form-control')); }}</td>
    <td>{{ Form::text("inmuebles[conyuge][$i][domicilio_depto]",(is_null($declaracion_jurada) ? null : $declaracion_jurada['inmueble']['conyuge'][$i]['domicilio_depto']),array('class' => 'form-control')); }}</td>
</tr>