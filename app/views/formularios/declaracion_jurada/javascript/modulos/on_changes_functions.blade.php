<script type="application/javascript">

    var selectLlamadosPrestarServicio = '.select-llamados-servicio';
    var selectFuerzaPerteneciente = '.select2-fuerza-perteneciente';
    var selectTipoIngresoExtraordinario = '.select-ingreso-extraordinario';
    var selectGrado = '.select-grado';
    var selectConyugeTrabaja = '.select-conyuge-trabaja';
    var selectPersonalFuerzaConyuge = '.select-personal-fuerza-conyuge';

    /******************************************************************************************************************/

    function changeSelectOrigen(e) {
        e.preventDefault();
        var object = $(this);
        var idOrigen = object.val();
        if (idOrigen == 5){
            object.next('div').show();
        } else {
            object.next('div').hide();
            object.next('div').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/


    function changeSelectTipoBanco(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();
        if (id == 4){
            object.next('div').show();
        } else {
            object.next('div').hide();
            object.next('div').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/


    function changeSelectEntidadEmisora(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();
        if (id == 6){
            object.next('div').show();
        } else {
            object.next('div').hide();
            object.next('div').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/


    function changeSelectTipoMoneda(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();
        if (id == 4){
            object.next('div').show();
        } else {
            object.next('div').hide();
            object.next('div').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/


    function changeSelectTipoIngresoFuerza(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();
        if (id == 9){
            object.next('div').show();
        } else {
            object.next('div').hide();
            object.next('div').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/


    function changeSelectTipoIngresoExternoExtraordinario(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();
        if (id == 3){
            object.next('div').show();
        } else {
            object.next('div').hide();
            object.next('div').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/


    function changeSelectTipoFuente(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();
        if (id == 9 || id == 10){
            object.next('div.div-fuente-tipo-cuotas').show();
        } else {
            object.next('div.div-fuente-tipo-cuotas').hide();
            object.next('div.div-fuente-tipo-cuotas').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/


    function changeSelectTipoBono(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();
        if (id == 4) {
            object.next('div').show();
        } else {
            object.next('div').hide();
            object.next('div').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/

    function showHideLlamadosPrestarSevicio(value) {
        if(value == 1)
            $(selectLlamadosPrestarServicio).removeClass("hidden");
        else
            $(selectLlamadosPrestarServicio).addClass("hidden");
    }

    function changeSituacionRevista(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();

        if (id == 14) {
            object.parent().next('div').show();
            showHideLlamadosPrestarSevicio(1);
        } else {
            object.parent().next('div').hide();
            object.parent().next('div').find('.select-llamados-servicio').val('');
            showHideLlamadosPrestarSevicio(0);
        }
    }

    /******************************************************************************************************************/


    function changeSelectTipoDeuda(e) {
        e.preventDefault();
        var object = $(this);
        var row = object.data('row');
        var id = object.val();
        var tbodyParent = object.parents('tbody');
        var tr = tbodyParent.find('tr.deudas-'+row);
        var selectorOtroTipoDeuda = '.div-otra-deuda';
        var selectorCuotasRestantes = '.div-cuotas-restantes';
        var selectorCreditoPersonalOtro = '.div-credito-personal-otro';


        if (id == 4){
            //Otros (especificar)
            tr.find(selectorCuotasRestantes).hide();
            tr.find(selectorCuotasRestantes).find('.input-cuotas-restantes').val('');
            
            tr.find(selectorCreditoPersonalOtro).hide();
            tr.find(selectorCreditoPersonalOtro).find('.input-otro').val('');

            tr.find(selectorOtroTipoDeuda).show();
        } else if(id == 9 || id == 10) {
            //9 - Plan de ahorro adjudicado - cuotas restantes / 10 - Plan de vivienda adjudicado - cuotas restantes
            tr.find(selectorOtroTipoDeuda).hide();
            tr.find(selectorOtroTipoDeuda).find('.input-otro').val('');

            tr.find(selectorCreditoPersonalOtro).hide();
            tr.find(selectorCreditoPersonalOtro).find('.input-otro').val('');

            tr.find(selectorCuotasRestantes).show();
        } else if(id == 12) {
            //Crédito personal -otros especificar-
            tr.find(selectorCuotasRestantes).hide();
            tr.find(selectorCuotasRestantes).find('.input-cuotas-restantes').val('');

            tr.find(selectorOtroTipoDeuda).hide();
            tr.find(selectorOtroTipoDeuda).find('.input-otro').val('');

            tr.find(selectorCreditoPersonalOtro).show();
        } else {
            tr.find(selectorOtroTipoDeuda).hide();
            tr.find(selectorOtroTipoDeuda).find('.input-otro').val('');

            tr.find(selectorCuotasRestantes).hide();
            tr.find(selectorCuotasRestantes).find('.input-cuotas-restantes').val('');

            tr.find(selectorCreditoPersonalOtro).hide();
            tr.find(selectorCreditoPersonalOtro).find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/


    function changeSelectTipoBien(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();
        if (id == 4){
            object.next('div').show();
        } else {
            object.next('div').hide();
            object.next('div').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/


    function changeSelectFormaPago(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();
        if (id == 5){
            object.next('div').show();
        } else {
            object.next('div').hide();
            object.next('div').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/


    function changeSelectPersonalFuerzaConyuge() {
        var object = $(this);
        var id = object.val();

        if (id == 2){
            object.next('div').show();
        } else {
            object.next('div').hide();
            object.next('div').find('.input-otro').val('');
        }
    }

    /******************************************************************************************************************/

    function changeSelectCaracterDeclaracion() {
        var object = $(this);
        var id = object.val();

        switch(id) {
            case 5:
                $('div.div-caracter-baja').show();
                $("#periodo_fiscal").val('2018');
                break;
            case 2:
                $('div.div-caracter-baja').show();
                $("#periodo_fiscal").val('2019');
                break;
            default:
                $('div.div-caracter-baja').hide();
                $('div.div-caracter-baja').find('.input-caracter-baja').val('');
                $("#periodo_fiscal").val('2018');
        }


    }

    /******************************************************************************************************************/

    function getUrlGetLocalidad(idProvincia) {
        return "{{route('localidades.getByProvincia')}}?idProvincia=" + idProvincia;
    }

    function getLocalidad() {
        var object = $(this);
        var idProvincia = object.val();
        var classLocalidad = object.data('localidad');
        var idLocalidad = object.data('id-localidad');

        if(!empty(idProvincia)) {

            var url = getUrlGetLocalidad(idProvincia);

            $.getJSON(url, function (data) {
                var select = $('.' + classLocalidad);
                select.html("").select2({data: data.data});
                if (!empty(idLocalidad )) select.select2().val(idLocalidad).trigger("change");
            });

        }

    }

    function changeSelectProvincia(e) {
        e.preventDefault();
        getLocalidad.call(this);
    }

    /******************************************************************************************************************/

    function changeSelectConyugeTrabaja() {
        var object = $(this);
        var id = object.val();
        var row = object.data('row');
        var elements = $('.tbody-conyuge').find('.tr-datos-conyuge-trabaja-' + row);

        if(id == 2) {
            elements.show();
        } else {

            var selectorInputs = 'input[name*="datos_conyuge['+row+']"]' +
                '[name!="datos_conyuge['+row +'][id]"]' +
                ',.select-personal-fuerza-conyuge-' + row +
                ',.select-fuerza-conyuge-' + row +
                ',.select-tipo-categoria-tributaria-' + row;

            elements.find(selectorInputs).val('');
            onChange($(selectPersonalFuerzaConyuge),changeSelectPersonalFuerzaConyuge);
            elements.hide();
        }

    }

    /******************************************************************************************************************/

    function changeSelectPaisesVehiculosPropios() {
        var object = $(this);
        changeSelectPaises(object, 'div.div-vehiculo-propios-provincia-', 'div.div-vehiculo-propios-localidad-');
    }

    function changeSelectPaisesVehiculosConyuge() {
        var object = $(this);
        changeSelectPaises(object, 'div.div-vehiculo-conyuge-provincia-', 'div.div-vehiculo-conyuge-localidad-');
    }

    function changeSelectPaisesVehiculosHijos() {
        var object = $(this);
        changeSelectPaises(object, 'div.div-vehiculo-hijos-provincia-', 'div.div-vehiculo-hijos-localidad-');
    }

    function changeSelectPaisesInmueblesPropios() {
        var object = $(this);
        changeSelectPaises(object, 'div.div-inmueble-propios-provincia-', 'div.div-inmueble-propios-localidad-');
    }

    function changeSelectPaisesInmueblesConyuge() {
        var object = $(this);
        changeSelectPaises(object, 'div.div-inmueble-conyuge-provincia-', 'div.div-inmueble-conyuge-localidad-');
    }

    function changeSelectPaisesInmueblesHijos() {
        var object = $(this);
        changeSelectPaises(object, 'div.div-inmueble-hijos-provincia-', 'div.div-inmueble-hijos-localidad-');
    }

    function changeSelectPaises(object, selectorDivProvincia, selectorDivLocalidad)
    {
        var id = object.val();
        var idLocalidad = object.data('id-localidad');
        var idProvincia = object.data('id-provincia');
        var row = object.data('row');
        var divProvincia = $(selectorDivProvincia + row);
        var divLocalidad = $(selectorDivLocalidad + row);

        var trParent = object.parents('tr');
        var divProvinciaOtroPais = trParent.find('.div-provincia-otro-pais-' + row);
        var divLocalidadOtroPais = trParent.find('.div-localidad-otro-pais-' + row);

        if (id == 13){//Argentina

            divProvincia.show();
            divLocalidad.show();

            if(!empty(idLocalidad)) {
                divLocalidad.find('.select-localidad').select2().val(idLocalidad).trigger("change");
            }else {
                divLocalidad.find('.select-localidad').select2().val('').trigger("change");
            }

            if(!empty(idProvincia)) {
                divProvincia.find('.select2-provincias').select2().val(idProvincia).trigger("change");
            } else {
                divProvincia.find('.select2-provincias').select2().val('').trigger("change");
            }

            divProvinciaOtroPais.hide();
            divProvinciaOtroPais.find(':input').val('');
            divLocalidadOtroPais.hide();
            divLocalidadOtroPais.find(':input').val('');

        } else if(!empty(id)){//Otro Pais

            divProvincia.hide();
            divLocalidad.hide();

            divProvincia.find('.select2-provincias').select2().val('').trigger("change");
            divLocalidad.find('.select-localidad').html("").select2({data: ''});

            divProvinciaOtroPais.show();
            divLocalidadOtroPais.show();

        } else {

            divProvincia.hide();
            divLocalidad.hide();

            divProvincia.find('.select2-provincias').select2().val('').trigger("change");
            divLocalidad.find('.select-localidad').html("").select2({data: ''});

            divProvinciaOtroPais.hide();
            divProvinciaOtroPais.find(':input').val('');
            divLocalidadOtroPais.hide();
            divLocalidadOtroPais.find(':input').val('');

        }
    }

    /******************************************************************************************************************/

    function getUrlGetAgrupamientoModalidad(idFuerza, idTipoPersonal) {
        return "{{route('agrupamiento_modalidad.getByFuerzaAndTipoPersonal')}}?id_fuerza=" + idFuerza + "&id_tipo_personal=" + idTipoPersonal;
    }

    function updateSelectAgrupamientoModalidad(idFuerza, idTipoPersonal, idAgrupamientoModalidad) {

        var idAgrupamientoModalidad = idAgrupamientoModalidad || '';
        var object = $('#form-1').find('.select-agrupamiento-modalidad');

        if(idFuerza == 4) {
            $(".div-agrup-cuerpo").hide();
            $(".div-agrup-cuerpo").find(':input').val('');
            $(".div-agrup-modalidad").show();

            if(!empty(idFuerza) && !empty(idTipoPersonal)) {
                var url = getUrlGetAgrupamientoModalidad(idFuerza, idTipoPersonal);

                $.getJSON(url, function (data) {

                    object.html("").select2({data: data.data});

                    if (!empty(idAgrupamientoModalidad)) object.select2().val(idAgrupamientoModalidad).trigger("change");

                });
            } else {
                object.html("").select2({data: ''})
            }

        } else {
            $(".div-agrup-cuerpo").show();
            $(".div-agrup-modalidad").hide();

            object.html("").select2({data: ''})
        }

    }


    /******************************************************************************************************************/

    function getUrlGetGrados(idFuerza, idTipoPersonal) {
        return "{{route('grado.getByFuerzaAndTipoPersonal')}}?id_fuerza=" + idFuerza + "&id_tipo_personal=" + idTipoPersonal;
    }

    function updateSelectGrado(idFuerza, idTipoPersonal, idGrado) {

        var idGrado = idGrado || '';
        var object = $('#form-1').find('.select-grado');

        if(!empty(idFuerza) && !empty(idTipoPersonal)) {

            if(idFuerza == 4 && idTipoPersonal == 2){

                $(".div-grado-fuerza").hide();

                object.html("").select2({data: ''})

            }else {

                $(".div-grado-fuerza").show();

                var url = getUrlGetGrados(idFuerza, idTipoPersonal);

                $.getJSON(url, function (data) {

                    object.html("").select2({data: data.data});

                    if (!empty(idGrado)) object.select2().val(idGrado).trigger("change");

                });
            }
        }
    }

    function getUrlGetFuerzas(idFuerza) {
        return "{{route('personal.getByFuerza')}}?id_fuerza=" + idFuerza;
    }

    function updateSelectPersonal(idFuerza, idTipoPersonal) {

        var idTipoPersonal = idTipoPersonal || '';


        if(!empty(idFuerza)) {

            var url = getUrlGetFuerzas(idFuerza);

            $.getJSON(url, function (data) {

                var select = $('#form-1').find('.select-personal');
                var idGrado = select.data('id-grado');
                var idAgrupamientoModalidad = select.data('id-agrupamiento-modalidad');

                select.html("").select2({data: data.data});//

                if (!empty(idTipoPersonal)){
                    select.select2().val(idTipoPersonal).trigger("change");
                } else {
                    idTipoPersonal = select.val();
                }

                updateSelectGrado(idFuerza, idTipoPersonal, idGrado);
                updateSelectAgrupamientoModalidad(idFuerza, idTipoPersonal, idAgrupamientoModalidad);

            });

        }
    }

    function showHideCuerpoAgrupamiento(idFuerza) {
        //GNA, PFA
        if( idFuerza != 1 && idFuerza != 2  && idFuerza != 0)
            $(".cuerpo_agrupamiento").removeClass("hidden");
        else
            $(".cuerpo_agrupamiento").addClass("hidden");
    }

    function showHideEscalafon(idFuerza) {

        var div = $(".div-escalafon");

        //PSA
        if(idFuerza == 4) {
            div.addClass("hidden");
            div.find(':input').val('');
        } else{
            div.removeClass("hidden");
        }

    }

    function changeFuerzaPerteneciente(e) {
        e.preventDefault();

        var object = $(this);
        var data = object.select2('data');
        var idFuerza = object.val();
        var fuerza = data[0].text;
        var idTipoPersonal = object.data('id-tipo-personal');

        $('.span-fuerza-pertenece').html(fuerza);
        $('.datos-propios-ingresos').val(idFuerza);

        updateSelectPersonal(idFuerza, idTipoPersonal);

        showHideCuerpoAgrupamiento(idFuerza);
        showHideEscalafon(idFuerza);

    }

    function changePersonal() {
        var idTipoPersonal = $(selectPersonal).val();
        var idGrado = $(selectPersonal).data('id_grado');
        var idAgrupamientoModalidad = $(selectPersonal).data('id-agrupamiento-modalidad');
        var idFuerza = $(selectFuerzaPerteneciente).val();

        updateSelectGrado(idFuerza, idTipoPersonal, idGrado);
        updateSelectAgrupamientoModalidad(idFuerza, idTipoPersonal, idAgrupamientoModalidad);
    }

    /******************************************************************************************************************/

    //todo Simplificar esta function
    function changeSelectTipoIngresoFuerzaExterno(e) {
        e.preventDefault();
        var object = $(this);
        var id = object.val();
        var div;
        var td = object.parent('td.td-ingresos-externos');

        if (id == 4){//Otros - especificar

            div = td.find('div.div-otro-ingreso');
            div.show();
            div.find('.input-otro').val('');

            div = td.find('div.div-ingreso-extraordinario');
            div.hide();
            div.find('.select-ingreso-extraordinario').prop('selectedIndex',0);
            div.find('.input-otro').val('');
            $(selectTipoIngresoExtraordinario).change();

            div = td.find('div.div-ingreso-actividad');
            div.find('.select-ingreso-otra-actividad').prop('selectedIndex',0);
            div.hide();

            div = td.find('div.div-categoria-tributaria');
            div.find('.select-categoria-tributaria').prop('selectedIndex',0);
            div.hide();
            div.find('.input-otro').val('');

        }else if (id == 2){//Ingresos Extraordinarios

            div = td.find('div.div-otro-ingreso');
            div.hide();
            div.find('.input-otro').val('');

            div = td.children('.div-ingreso-extraordinario');
            div.show();
            div.find('.input-otro').val('');
            $(selectTipoIngresoExtraordinario).change();


            div = td.find('div.div-ingreso-actividad');
            div.find('.select-ingreso-otra-actividad').prop('selectedIndex',0);
            div.hide();

            div = td.find('div.div-categoria-tributaria');
            div.find('.select-categoria-tributaria').prop('selectedIndex',0);
            div.hide();
            div.find('.input-otro').val('');

        }else if (id == 3){//Ingresos por otra actividad

            div = td.find('div.div-otro-ingreso');
            div.hide();
            div.find('.input-otro').val('');

            div = td.find('div.div-ingreso-extraordinario');
            div.find('.select-ingreso-extraordinario').prop('selectedIndex',0);
            div.hide();
            div.find('.input-otro').val('');
            $(selectTipoIngresoExtraordinario).change();

            div = td.find('div.div-ingreso-actividad');

            var selectIngresoOtraActividad = div.find('.select-ingreso-otra-actividad');

            if(empty(selectIngresoOtraActividad.val())){
                selectIngresoOtraActividad.prop('selectedIndex',0);
            }

            div.show();

            div = td.find('div.div-categoria-tributaria');

            var selectCategoriaTributaria = div.find('.select-categoria-tributaria');
            var valSelectCategoriaTributaria = selectCategoriaTributaria.val();

            if(empty(valSelectCategoriaTributaria)){
                selectCategoriaTributaria.prop('selectedIndex',0);
            }

            div.show();
            div.find('.input-otro').val('');

        } else {

            div = td.find('div.div-otro-ingreso');
            div.hide();
            div.find('.input-otro').val('');

            div = td.find('div.div-ingreso-extraordinario');
            div.hide();
            div.find('.select-ingreso-extraordinario').prop('selectedIndex',0);
            div.find('.input-otro').val('');
            $(selectTipoIngresoExtraordinario).change();

            div = td.find('div.div-ingreso-actividad');
            div.find('.select-ingreso-otra-actividad').prop('selectedIndex',0);
            div.hide();

            div = td.find('div.div-categoria-tributaria');
            div.find('.select-categoria-tributaria').prop('selectedIndex',0);
            div.hide();
            div.find('.input-otro').val('');

        }
    }

    /******************************************************************************************************************/


</script>