<script type="application/javascript">

    /** Aqui se debe agregar los formularios a enviar al backend. */
    var formsToPost = '#form-1,#form-2,#form-3,#form-4,#form-5,#form-6,#form-7,#form-8';

    /******************************************************************************************************************/

    function getUrlGetRegistry() {
        return "{{route('formulario.registro.getRegistry')}}";
    }

    function getLenght(object) {
        @if( !is_null( $declaracion_jurada ) )
            var lengthTr = object.parents('table').children('tbody').children('tr').length;
            return parseInt(lengthTr) - 1 ;
        @else
            return object.data('count');
        @endif
    }

    /**
     *
     *  Funcion que se ejecuta al agregar un nuevo registro
     *
     */
    function loadEventClickBtnAddRegistry(selector) {

        var selector = selector || '.btn-add-registry';

        $(selector).on('click',function(e){
            e.preventDefault();

            var object = $(this);
            var typeRegistry = object.data('type_registry');
            var tbody = $('.tbody-' + typeRegistry);
            var lenght = getLenght(object);
            var count = lenght + 1;
            var url = getUrlGetRegistry();

            url = url + "?count=" + count + "&template=" + typeRegistry;
            object.data('count',count);


            $.get(url,function(data){
                tbody.append(data);
            }).done(function(){

                loadDateTimePicker(tbody.find(selectorDateTimePicker));
                loadJqueryMask();
                loadSelect2(tbody.find(selectorSelects2));

                //todo Simplificar esta parte en un array
                /**
                 * Se cargan y ejecutan los changes() de los selects
                 */
                onChange(tbody.find(selectTipoIngreso),changeSelectTipoIngresoFuerza);
                onChange(tbody.find(selectTipoIngresoExterno),changeSelectTipoIngresoFuerzaExterno);
                onChange(tbody.find(selectTipoIngresoExtraordinario),changeSelectTipoIngresoExternoExtraordinario);

                onChange(tbody.find(selectProvincia),changeSelectProvincia);
                onChange(tbody.find(selectOrigen),changeSelectOrigen);
                onChange(tbody.find(selectTipoBanco),changeSelectTipoBanco);
                onChange(tbody.find(selectEntidadEmisora),changeSelectEntidadEmisora);
                onChange(tbody.find(selectTipoMoneda),changeSelectTipoMoneda);
                onChange(tbody.find(selectTipoFuente),changeSelectTipoFuente);
                onChange(tbody.find(selectTipoBono),changeSelectTipoBono);
                onChange(tbody.find(selectTipoDeuda),changeSelectTipoDeuda);
                onChange(tbody.find(selectTipoBien),changeSelectTipoBien);
                onChange(tbody.find(selectConyugeTrabaja),changeSelectConyugeTrabaja);
                onChange(tbody.find(selectFormaPago),changeSelectFormaPago);
                onChange(tbody.find(selectPersonalFuerzaConyuge),changeSelectPersonalFuerzaConyuge);

                onChange(tbody.find(selectPaisesInmueblesPropios),changeSelectPaisesInmueblesPropios);
                onChange(tbody.find(selectPaisesInmueblesConyuge),changeSelectPaisesInmueblesConyuge);
                onChange(tbody.find(selectPaisesInmueblesHijos),changeSelectPaisesInmueblesHijos);

                onChange(tbody.find(selectPaisesVehiculosPropios),changeSelectPaisesVehiculosPropios);
                onChange(tbody.find(selectPaisesVehiculosConyuge),changeSelectPaisesVehiculosConyuge);
                onChange(tbody.find(selectPaisesVehiculosHijos),changeSelectPaisesVehiculosHijos);
            });

        });
    }

    /******************************************************************************************************************/

    function triggerNextTab() {
        var navTabsActive = $('.nav-tabs > .active');
        var li = navTabsActive.next('li');
        var link = li.find('a');
        li.show();
        link.trigger('click');
        loadSelect2(objectSelect2);
    }

    /******************************************************************************************************************/
    /**
     * Formulario Caracter de la declaracion
     */
    function loadEventClickBtnSubmitForm6(selector) {
        var selector = selector || '#btn-submit-form-6';
        $(selector).on('click',function(){
            var isValidForm = $('#form-6').valid();
            if(isValidForm) triggerNextTab();
        });
    }

    /******************************************************************************************************************/
    /**
     * Formulario Datos Patrimoniales Familiares
     */
    function loadEventClickBtnSubmitForm4(selector) {
        var selector = selector || '#btn-submit-form-4';
        $(selector).on('click',function(){

            var formSelector = '#form-4';
            var form = $(formSelector);

            form.removeClass('no-valid');//limpio el form para pasar las validaciones

            form4ConyugeCheckInmuebles();
            form4ConyugeCheckVehiculos();
            form4ConyugeCheckDerechosExpectativa();
            form4ConyugeCheckOtrosBienes();
            form4ConyugeCheckDeudas();

            form4HijoCheckInmuebles();
            form4HijoCheckVehiculos();
            form4HijoCheckDerechosExpectativa();
            form4HijoCheckOtrosBienes();
            form4HijoCheckDeudas();


            if(!form.hasClass('no-valid')) {
                form.find('div.div-error-form').hide();

                var validForm2 = !$('#form-2').hasClass('no-valid');/*Datos Familiares*/
                var validForm3 = !$('#form-3').hasClass('no-valid');/*Bienes*/
                var validForm4 = !$('#form-4').hasClass('no-valid');/*Datos Patrimoniales Familiares*/
                var validForm7 = !$('#form-7').hasClass('no-valid');/*Ingresos*/
                var validForm8 = !$('#form-8').hasClass('no-valid');/*Deudas*/

                if(validForm2 && validForm3 && validForm4 && validForm7 && validForm8)
                {
                    liSubmitForm.show();
                } else {
                    liSubmitForm.hide();
                    alert('Existen campos vacios que no se han completado, revise las solapas por favor.');
                }

            }else {
                liSubmitForm.hide();
            }

        });
    }

    /******************************************************************************************************************/

    /**
     * Formulario Datos Personales
     */
    function loadEventClickBtnSubmitForm1(selector) {
        var selector = selector || '#btn-submit-form-1';
        $(selector).on('click',function(){
            var isValidForm = $('#form-1').valid();
            if(isValidForm) triggerNextTab();
        });
    }

    /******************************************************************************************************************/

    /**
     * Formulario Bienes
     */
    function loadEventClickBtnSubmitForm3(selector) {

        var selector = selector || '#btn-submit-form-3';

        $(selector).on('click',function(){

            var formSelector = '#form-3';
            var form = $(formSelector);

            form.removeClass('no-valid');//limpio el form para pasar las validaciones

            form3CheckInmueblesPropios();

            form3CheckVehiculosPropios();

            form3CheckCuentasBancarias();

            form3CheckCuentasBancariasExterior();

            form3CheckBonosTitulosAcciones();

            form3CheckDerechosExpectativaPropios();

            form3CheckOtrosBienes();

            form3CheckDineroEfectivo();

            form3CheckAcreencias();

            if(!form.hasClass('no-valid')) {
                form.find('div.div-error-form').hide();
                triggerNextTab();
            }

        });
    }

    /******************************************************************************************************************/

    /**
     * Formulario Ingresos
     */
    function loadEventClickBtnSubmitForm7(selector) {
        var selector = selector || '#btn-submit-form-7';
        $(selector).on('click',function(){
            var isValidForm = $('#form-7').valid();
            var formSelector = '#form-7';
            var form = $(formSelector);

            form.removeClass('no-valid');//limpio el form para pasar las validaciones

            form7CheckIngresosExtraordinarios();

            form7CheckIngresosExternos();

            if(!form.hasClass('no-valid') && isValidForm) {
                form.find('div.div-error-form').hide();
                triggerNextTab();
            }

        });
    }

    /******************************************************************************************************************/
    /**
     * Formulario Deudas
     */
    function loadEventClickBtnSubmitForm8(selector) {
        var selector = selector || '#btn-submit-form-8';
        $(selector).on('click',function(){
            var formSelector = '#form-8';
            var form = $(formSelector);

            form.removeClass('no-valid');//limpio el form para pasar las validaciones

            form8CheckTarjetasCredito();

            form8CheckDeudas();

            if(!form.hasClass('no-valid')) {
                form.find('div.div-error-form').hide();
                triggerNextTab();
            }
        });
    }

    /******************************************************************************************************************/

    /**
     * Formulario Datos Familiares
     */
    function loadEventClickBtnSubmitForm2(selector) {

        var selector = selector || '#btn-submit-form-2';

        $(selector).on('click',function(){

            var formSelector = '#form-2';
            var form = $(formSelector);

            form.removeClass('no-valid');//limpio el form para pasar las validaciones

            form2checkCamposConyuge();

            form2CheckConyugeNoTrabaja();

            form2CheckCamposHijos();

            form2CheckCamposOtrasPersonas();

            form2CheckCamposPadreMadre();

            form2CheckCamposConvivienteFuerza();

            if(!form.hasClass('no-valid')) {
                form.find('div.div-error-form').hide();
                triggerNextTab();
            }

        });
    }


    /******************************************************************************************************************/

    function loadEventClickBtnPrevious(selector) {

        var selector = selector || '.btnPrevious';

        $(selector).click(function(){
            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
            loadSelect2(objectSelect2);
        });
    }

    /******************************************************************************************************************/

    function loadEventSubmitForms() {
        $(formsToPost).on('submit',function(e) {
            e.preventDefault();
            return false;
        });
    }

    /******************************************************************************************************************/

    function loadEventClickBtnConfirm(selector) {
        var selector = selector || '.btnConfirm';
        $(selector).on('click',function(){
            var validForm2 = !$('#form-2').hasClass('no-valid');/*Datos Familiares*/
            var validForm3 = !$('#form-3').hasClass('no-valid');/*Bienes*/
            var validForm4 = !$('#form-4').hasClass('no-valid');/*Datos Patrimoniales Familiares*/
            var validForm7 = !$('#form-7').hasClass('no-valid');/*Ingresos*/
            var validForm8 = !$('#form-8').hasClass('no-valid');/*Deudas*/

            if(validForm2 && validForm3 && validForm4 && validForm7 && validForm8)
            {
                liSubmitForm.show();
            } else {
                if(!validForm4) {
                   setFormToNoValid('#form-4'); 
                }else {
                    alert('Existen campos vacios que no se han completado, revise las solapas por favor.');
                }
            }

        });
    }

    /******************************************************************************************************************/

    var isValid = true;

    function getUrlToPost() {

        @if(is_null($declaracion_jurada))
            return "{{route('formulario.declaraciones_juradas.store')}}";
        @else
            return "{{route('formulario.declaraciones_juradas.update',$hashId)}}";
        @endif

    }

    function loadEventClickBtnSubmitForm(selector) {

        var selector = selector || '#btn-submit-form';

        $(selector).on('click',function(e){

            e.preventDefault();
            var loaderLadda = Ladda.create(this);
            loaderLadda.start();

            var divMessagesError = '#div-message-error';
            var messagesError = '';
            var url = '';

            $(divMessagesError).html('').hide();

            $.each(validatorForms,function(formKey,formValue){

                var validator = $(formKey).validate();

                var messages = validatorForms[formKey].messages;

                $.each(messages,function(messagesKey,messagesValue){
                    isValid = validator.element('input[name="' + messagesKey + '"]');
                });

            });

            if(!isValid) {

                loaderLadda.stop();
                $(divMessagesError).html('').hide();

            } else {

                var serialize = $(formsToPost).serialize();
                var data = JSON.stringify(serialize);

                data = JSON.parse(data);
                url = getUrlToPost();

                $.post(url,data).done(function(responseData){
                    if(responseData.error === false){
                        $(divMessagesError).html('').hide();
                        window.location.replace(responseData.data.url);
                    } else {
                        var messages = '';
                        messagesError = responseData.messages ;

                        if(!empty(messagesError))
                        {
                            $.each(messagesError,function(messagesKey,messagesValue){
                                $.each(messagesValue,function(messageKey,messageValue){
                                    messages += '<p class="bg-danger error">' + messageValue + '</p>';
                                });
                            });

                            $(divMessagesError).html(messages).show();
                        }

                    }

                }).always(function() { loaderLadda.stop(); });

            }

        });
    }

</script>