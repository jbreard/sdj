<script type="application/javascript">

    /**
     * Formulario Datos Familiares
     */

    function form2CheckCamposHijos() {

        var tbody = $('.tbody-hijo');

        tbody.find(".tr-datos-hijo").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="datos_hijo['+row+']"]' +
                '[name!="datos_hijo['+row+'][id]"]' +
                ',.select-hijo-convive';

            var finds = element.find(selectorInputs);

            var selectTipoHijoConvive = element.find('.select-hijo-convive');
            var valTipoHijoConvive = selectTipoHijoConvive.val();

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            })

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(!empty(valTipoHijoConvive)) completedFields = true;

                if (empty(valTipoHijoConvive)) {
                    setFormToNoValid('#form-2');
                    selectTipoHijoConvive.addClass('error');
                } else {
                    selectTipoHijoConvive.removeClass('error');
                }
            }

        });

    }

    function form2CheckConyugeNoTrabaja() {
        var selectsNoTrabaja = $('.tbody-conyuge').find(selectConyugeTrabaja);

        selectsNoTrabaja.each(function (index) {

            var element = $(this);
            var row = element.data('row');
            var tbodyConyuge = $('.tbody-conyuge');

            var selectTipoCategoriaTributaria = tbodyConyuge.find('.select-tipo-categoria-tributaria-' + row);

            var selectorInputs = 'input[name*="datos_conyuge['+row+']"]' +
                '[name!="datos_conyuge['+row+'][id]"]' +
                ',.select-tipo-categoria-tributaria-' + row;

            var valTrabaja = element.val();

            if (valTrabaja == 2) {

                var trDatosConyugeTrabaja = $('.tbody-conyuge').find('.tr-3-datos-conyuge-'+row);

                trDatosConyugeTrabaja.find(selectorInputs).each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    }else {
                        input.removeClass('error');
                    }
                });

            } else {
                selectTipoCategoriaTributaria.removeClass('error');
            }

        });
    }

    function form2checkCamposConyuge() {

        var tbody = $('.tbody-conyuge');

        tbody.find(".tr-1-datos-conyuge").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="datos_conyuge['+row+']"]' +
                '[name!="datos_conyuge['+row+'][id]"]' +
                ', .select-conyuge-vinculo';
            var trConyugeTrabaja = tbody.find('.tr-2-datos-conyuge-'+ row);

            var finds = element.find(selectorInputs);
            var findsTrConyugeTrabaja = trConyugeTrabaja.find(selectConyugeTrabaja);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTrConyugeTrabaja.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                findsTrConyugeTrabaja.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });
            }

        });

    }

    function form2CheckCamposOtrasPersonas() {
        var tbody = $('.tbody-otra_persona');

        tbody.find(".tr-datos-otra_persona").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="datos_otra_persona['+row+']"]' +
                '[name!="datos_otra_persona['+row+'][id]"]';

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });
            }

        });
    }

    function form2CheckCamposPadreMadre() {
        var tbody = $('.tbody-padre_madre');

        tbody.find(".tr-datos-padre_madre").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="datos_padre_madre['+row+']"]' +
                '[name!="datos_padre_madre['+row+'][id]"]' +
		'[name!="datos_padre_madre['+row+'][nacimiento]"]' +
                ',.select-padre_madre-tipo_vinculo' +
                ',.select-padre_madre-tipo_documento';

            var finds = element.find(selectorInputs);

            var selectTipoVinculo = element.find('.select-padre_madre-tipo_vinculo');
            var valTipoVinculo = selectTipoVinculo.val();

            var selectTipoDocumento = element.find('.select-padre_madre-tipo_documento');
            var valTipoDocumento = selectTipoDocumento.val();

            finds.each(function(index){
                var input = $(this);
                var value = input.val();
                console.log(input.attr("name") + " " + value);

                if(!empty(value)) completedFields = true;
            });

            if(completedFields) {

                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(!empty(valTipoVinculo)) completedFields = true;

                if (empty(valTipoVinculo)) {
                    setFormToNoValid('#form-2');
                    selectTipoVinculo.addClass('error');
                } else {
                    selectTipoVinculo.removeClass('error');
                }

                if(!empty(valTipoDocumento)) completedFields = true;

                if (empty(valTipoDocumento)) {
                    setFormToNoValid('#form-2');
                    selectTipoDocumento.addClass('error');
                } else {
                    selectTipoDocumento.removeClass('error');
                }
            }

        });
    }

    function form2CheckCamposConvivienteFuerza() {
        var tbody = $('.tbody-conviviente_fuerza');

        tbody.find(".tr-datos-conviviente_fuerza").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="datos_conviviente_fuerza['+row+']"]' +
                '[name!="datos_conviviente_fuerza['+row+'][id]"]' +
                ',.select-conviviente-tipo_vinculo' +
                ',.select-conviviente_fuerza-fuerzas' +
                ',.select-conviviente_fuerza-actividad';

            var finds = element.find(selectorInputs);

            var selectTipoVinculo = element.find('.select-conviviente-tipo_vinculo');
            var valTipoVinculo = selectTipoVinculo.val();

            var selectFuerzas = element.find('.select-conviviente_fuerza-fuerzas');
            var valFuerza = selectFuerzas.val();

            var selectActividad = element.find('.select-conviviente_fuerza-actividad');
            var valActividad = selectActividad.val();

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid('#form-2');
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(!empty(valTipoVinculo)) completedFields = true;

                if (empty(valTipoVinculo)) {
                    setFormToNoValid('#form-2');
                    selectTipoVinculo.addClass('error');
                } else {
                    selectTipoVinculo.removeClass('error');
                }

                if(!empty(valFuerza)) completedFields = true;

                if (empty(valFuerza)) {
                    setFormToNoValid('#form-2');
                    selectFuerzas.addClass('error');
                } else {
                    selectFuerzas.removeClass('error');
                }

                if(!empty(valActividad)) completedFields = true;

                if (empty(valActividad)) {
                    setFormToNoValid('#form-2');
                    selectActividad.addClass('error');
                } else {
                    selectActividad.removeClass('error');
                }
            }

        });
    }

</script>
