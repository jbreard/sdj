<script type="application/javascript">

    /**
     *
     * Formulario Patrimonio Familiares
     */

    var selectorForm4 = '#form-4';


    function form4ConyugeCheckInmuebles() {
        var tbody = $('.tbody-inmuebles_conyuge');

        tbody.find(".tr-1-datos-inmuebles_conyuge").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="inmuebles[conyuge]['+row+']"]' +
                '[name!="inmuebles[conyuge]['+row+'][id]"]' +
                '[name!="inmuebles[conyuge]['+row+'][otra_forma_pago]"]' +
                '[name!="inmuebles[conyuge]['+row+'][superficie_cubierta]"]' +
                '[name!="inmuebles[conyuge]['+row+'][superficie_descubierta]"]' +
                '[name!="inmuebles[conyuge]['+row+'][domicilio_piso]"]' +
                '[name!="inmuebles[conyuge]['+row+'][domicilio_depto]"]' +
                '[name!="inmuebles[conyuge]['+row+'][otro_pais_provincia]"]' +
                '[name!="inmuebles[conyuge]['+row+'][otro_pais_localidad]"]' +
                '[name!="inmuebles[conyuge]['+row+'][otro_tipo_moneda_valor_adquisicion]"]' +
                '[name!="inmuebles[conyuge]['+row+'][otro_tipo_moneda_valor_estimado]"]' +
                ',.select-tipo-inmueble' +
                ',.select-unidad-superficie' +
                ',.select-modo-adquisicion' +
                ',.select-tipo-moneda-valor-adquisicion' +
                ',.select-tipo-moneda-valor-estimado' +
                ',.select-caracter';

            var finds = element.find(selectorInputs);
            var tr2 = tbody.find('.tr-2-datos-inmuebles_conyuge-'+row);
            var findsTr2 = tr2.find(selectorInputs);

            var selectFormaPago = tr2.find('.select-forma-pago');
            var valFormaPago = selectFormaPago.val();

            var selectTipoMonedaValorAdquisicion = tr2.find('.select-tipo-moneda-valor-adquisicion');
            var valSelectTipoMonedaValorAdquisicion = selectTipoMonedaValorAdquisicion.val();

            var selectTipoMonedaValorEstimado = tr2.find('.select-tipo-moneda-valor-estimado');
            var valSelectTipoMonedaValorEstimado = selectTipoMonedaValorEstimado.val();

            var totalSuperficieCubierta = element.find('input[name="inmuebles[conyuge]['+row+'][superficie_cubierta]"]');
            var valTotalSuperficieCubierta = totalSuperficieCubierta.val();

            var totalSuperficieDescubierta = element.find('input[name="inmuebles[conyuge]['+row+'][superficie_cubierta]"]');
            var valTotalSuperficieDescubierta = totalSuperficieDescubierta.val();

            var otraFormaPago = tr2.find('input[name="inmuebles[conyuge]['+row+'][otra_forma_pago]"]');
            var valOtraFormaPago = otraFormaPago.val();

            var otroTipoMonedaValorAdquisicion = tr2.find('input[name="inmuebles[conyuge]['+row+'][otro_tipo_moneda_valor_adquisicion]"]');
            var valOtroTipoMonedaValorAdquisicion = otroTipoMonedaValorAdquisicion.val();

            var otroTipoMonedaValorEstimado = tr2.find('input[name="inmuebles[conyuge]['+row+'][otro_tipo_moneda_valor_estimado]"]');
            var valOtroTipoMonedaValorEstimado = otroTipoMonedaValorEstimado.val();

            var tr3 = tbody.find('.tr-3-datos-inmuebles_conyuge-'+row);
            var tr3DivPais = '.div-pais-'+row;
            var tr3DivProvincia = '.div-provincia-'+row;
            var tr3DivLocalidad = '.div-localidad-'+row;
            var labelErrorPais = '<label id="inmuebles[conyuge]['+row+'][id_pais]" class="error" for="inmuebles[conyuge]['+row+'][id_pais]">Completar país</label>';
            var labelErrorProvincia = '<label id="inmuebles[conyuge]['+row+'][id_provincia]" class="error" for="inmuebles[conyuge]['+row+'][id_provincia]">Completar provincia</label>';
            var labelErrorLocalidad = '<label id="inmuebles[conyuge]['+row+'][id_localidad]" class="error" for="inmuebles[conyuge]['+row+'][id_localidad]">Completar localidad</label>';

            var tr4 = tbody.find('.tr-4-datos-inmuebles_conyuge-'+row);

            var selectPaises = tr4.find('.select-paises-conyuge');
            var valPais = selectPaises.select2().val();

            var selectProvincias = tr4.find('.select2-provincias');
            var valProvincia = selectProvincias.select2().val();

            var selectLocalidad = tr4.find('.select-localidad');
            var valLocalidad = selectLocalidad.select2().val();

            var otroPaisProvincia = tr4.find('input[name="inmuebles[conyuge]['+row+'][otro_pais_provincia]"]');
            var valOtroPaisProvincia = otroPaisProvincia.val();

            var otroPaisLocalidad = tr4.find('input[name="inmuebles[conyuge]['+row+'][otro_pais_localidad]"]');
            var valOtroPaisLocalidad = otroPaisLocalidad.val();


            var tr5 = tbody.find('.tr-5-datos-inmuebles_conyuge-'+row);
            var findsTr5 = tr5.find(selectorInputs);

            if(!empty(valFormaPago)) completedFields = true;

            if(!empty(valSelectTipoMonedaValorAdquisicion)) completedFields = true;

            if(!empty(valSelectTipoMonedaValorEstimado)) completedFields = true;

            if(!empty(valPais)) completedFields = true;

            if(valTotalSuperficieCubierta != '') completedFields = true;

            if(valTotalSuperficieDescubierta != '') completedFields = true;

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr2.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr5.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(completedFields)
            {

                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if(valTotalSuperficieCubierta == ''){
                    setFormToNoValid(selectorForm4);
                    totalSuperficieCubierta.addClass('error');
                } else {
                    totalSuperficieCubierta.removeClass('error');
                }

                if(valTotalSuperficieDescubierta == ''){
                    setFormToNoValid(selectorForm4);
                    totalSuperficieDescubierta.addClass('error');
                } else {
                    totalSuperficieDescubierta.removeClass('error');
                }

                findsTr2.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valSelectTipoMonedaValorAdquisicion)) {
                    setFormToNoValid(selectorForm4);
                    selectTipoMonedaValorAdquisicion.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorAdquisicion == 4 && empty(valOtroTipoMonedaValorAdquisicion)) {
                        setFormToNoValid(selectorForm4);
                        otroTipoMonedaValorAdquisicion.addClass('error');
                    } else {
                        otroTipoMonedaValorAdquisicion.removeClass('error');
                    }

                    selectTipoMonedaValorAdquisicion.removeClass('error');

                }

                if (empty(valSelectTipoMonedaValorEstimado)) {
                    setFormToNoValid(selectorForm4);
                    selectTipoMonedaValorEstimado.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorEstimado == 4 && empty(valOtroTipoMonedaValorEstimado)) {
                        setFormToNoValid(selectorForm4);
                        otroTipoMonedaValorEstimado.addClass('error');
                    } else {
                        otroTipoMonedaValorEstimado.removeClass('error');
                    }

                    selectTipoMonedaValorEstimado.removeClass('error');

                }


                if (empty(valFormaPago)) {
                    setFormToNoValid(selectorForm4);
                    selectFormaPago.addClass('error');
                } else {

                    if (valFormaPago == 5 && empty(valOtraFormaPago)) {
                        setFormToNoValid(selectorForm4);
                        otraFormaPago.addClass('error');
                    } else {
                        otraFormaPago.removeClass('error');
                    }

                    selectFormaPago.removeClass('error');

                }

                findsTr5.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valPais)) {
                    setFormToNoValid(selectorForm4);
                    tr3.find(tr3DivPais).html(labelErrorPais);
                    tr3.find(tr3DivProvincia).html('');
                    tr3.find(tr3DivLocalidad).html('');
                } else {

                    if (valPais == 13) {//Pais = Argentina

                        if(empty(valProvincia)) {
                            setFormToNoValid(selectorForm4);
                            tr3.find(tr3DivProvincia).html(labelErrorProvincia);
                        } else {
                            tr3.find(tr3DivProvincia).html('');
                        }

                        if(empty(valLocalidad)) {
                            setFormToNoValid(selectorForm4);
                            tr3.find(tr3DivLocalidad).html(labelErrorLocalidad);
                        } else {
                            tr3.find(tr3DivLocalidad).html('');
                        }

                    } else {

                        if(empty(valOtroPaisProvincia)) {
                            setFormToNoValid(selectorForm4);
                            otroPaisProvincia.addClass('error');
                        } else {
                            otroPaisProvincia.removeClass('error');
                        }

                        if(empty(valOtroPaisLocalidad)) {
                            setFormToNoValid(selectorForm4);
                            otroPaisLocalidad.addClass('error');
                        } else {
                            otroPaisLocalidad.removeClass('error');
                        }

                    }

                    tr3.find(tr3DivPais).html('');

                }
            }

        });   
    }
    
    
    function form4ConyugeCheckVehiculos() {
        var tbody = $('.tbody-vehiculos_conyuge');

        tbody.find(".tr-1-datos-vehiculos_conyuge").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="vehiculos[conyuge]['+row+']"]' +
                '[name!="vehiculos[conyuge]['+row+'][id]"]' +
                '[name!="vehiculos[conyuge]['+row+'][domicilio_piso]"]' +
                '[name!="vehiculos[conyuge]['+row+'][domicilio_depto]"]' +
                '[name!="vehiculos[conyuge]['+row+'][otro_pais_provincia]"]' +
                '[name!="vehiculos[conyuge]['+row+'][otro_pais_localidad]"]' +
                '[name!="vehiculos[conyuge]['+row+'][otro_tipo_moneda_valor_seguro]"]' +
                '[name!="vehiculos[conyuge]['+row+'][otro_tipo_moneda_valor_estimado]"]' +
                ',.select-tipo-vehiculo' +
                ',.select-caracter' +
                ',.select-modo-adquisicion' +
                ',.select-forma-pago' +
                '';

            var finds = element.find(selectorInputs);

            var tr2 = tbody.find('.tr-2-datos-vehiculos_conyuge-'+row);
            var findsTr2 = tr2.find(selectorInputs);

            var selectTipoMonedaValorSeguro = tr2.find('.select-tipo-moneda-valor-seguro');
            var valSelectTipoMonedaValorSeguro = selectTipoMonedaValorSeguro.val();

            var selectTipoMonedaValorEstimado = tr2.find('.select-tipo-moneda-valor-estimado');
            var valSelectTipoMonedaValorEstimado = selectTipoMonedaValorEstimado.val();

            var otroTipoMonedaValorSeguro = tr2.find('input[name="vehiculos[conyuge]['+row+'][otro_tipo_moneda_valor_seguro]"]');
            var valOtroTipoMonedaValorSeguro = otroTipoMonedaValorSeguro.val();

            var otroTipoMonedaValorEstimado = tr2.find('input[name="vehiculos[conyuge]['+row+'][otro_tipo_moneda_valor_estimado]"]');
            var valOtroTipoMonedaValorEstimado = otroTipoMonedaValorEstimado.val();

            var tr3 = tbody.find('.tr-3-datos-vehiculos_conyuge-'+row);
            var findsTr3 = tr3.find(selectorInputs);

            var tr5 = tbody.find('.tr-5-datos-vehiculos_conyuge-'+row);
            var tr5DivPais = '.div-pais-'+row;
            var tr5DivProvincia = '.div-provincia-'+row;
            var tr5DivLocalidad = '.div-localidad-'+row;
            var labelErrorPais = '<label id="vehiculos[conyuge]['+row+'][id_pais]" class="error" for="vehiculos[conyuge]['+row+'][id_pais]">Completar país</label>';
            var labelErrorProvincia = '<label id="vehiculos[conyuge]['+row+'][id_provincia]" class="error" for="vehiculos[conyuge]['+row+'][id_provincia]">Completar provincia</label>';
            var labelErrorLocalidad = '<label id="vehiculos[conyuge]['+row+'][id_localidad]" class="error" for="vehiculos[conyuge]['+row+'][id_localidad]">Completar localidad</label>';

            var tr4 = tbody.find('.tr-4-datos-vehiculos_conyuge-'+row);

            var selectPaises = tr4.find('.select-paises-vehiculos-conyuge');
            var valPais = selectPaises.select2().val();

            var selectProvincias = tr4.find('.select2-provincias');
            var valProvincia = selectProvincias.select2().val();

            var selectLocalidad = tr4.find('.select-localidad');
            var valLocalidad = selectLocalidad.select2().val();

            var otroPaisProvincia = tr4.find('input[name="vehiculos[conyuge]['+row+'][otro_pais_provincia]"]');
            var valOtroPaisProvincia = otroPaisProvincia.val();

            var otroPaisLocalidad = tr4.find('input[name="vehiculos[conyuge]['+row+'][otro_pais_localidad]"]');
            var valOtroPaisLocalidad = otroPaisLocalidad.val();

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr2.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr3.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(!empty(valPais)) completedFields = true;

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                findsTr2.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valSelectTipoMonedaValorSeguro)) {
                    setFormToNoValid(selectorForm4);
                    selectTipoMonedaValorSeguro.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorSeguro == 4 && empty(valOtroTipoMonedaValorSeguro)) {
                        setFormToNoValid(selectorForm4);
                        otroTipoMonedaValorSeguro.addClass('error');
                    } else {
                        otroTipoMonedaValorSeguro.removeClass('error');
                    }

                    selectTipoMonedaValorSeguro.removeClass('error');

                }

                if (empty(valSelectTipoMonedaValorEstimado)) {
                    setFormToNoValid(selectorForm4);
                    selectTipoMonedaValorEstimado.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorEstimado == 4 && empty(valOtroTipoMonedaValorEstimado)) {
                        setFormToNoValid(selectorForm4);
                        otroTipoMonedaValorEstimado.addClass('error');
                    } else {
                        otroTipoMonedaValorEstimado.removeClass('error');
                    }

                    selectTipoMonedaValorEstimado.removeClass('error');

                }

                findsTr3.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valPais)) {
                    setFormToNoValid(selectorForm4);
                    tr5.find(tr5DivPais).html(labelErrorPais);
                    tr5.find(tr5DivProvincia).html('');
                    tr5.find(tr5DivLocalidad).html('');
                } else {

                    if (valPais == 13) {//Pais = Argentina

                        if(empty(valProvincia)) {
                            setFormToNoValid(selectorForm4);
                            tr5.find(tr5DivProvincia).html(labelErrorProvincia);
                        } else {
                            tr5.find(tr5DivProvincia).html('');
                        }

                        if(empty(valLocalidad)) {
                            setFormToNoValid(selectorForm4);
                            tr5.find(tr5DivLocalidad).html(labelErrorLocalidad);
                        } else {
                            tr5.find(tr5DivLocalidad).html('');
                        }

                    } else {

                        if(empty(valOtroPaisProvincia)) {
                            setFormToNoValid(selectorForm4);
                            otroPaisProvincia.addClass('error');
                        } else {
                            otroPaisProvincia.removeClass('error');
                        }

                        if(empty(valOtroPaisLocalidad)) {
                            setFormToNoValid(selectorForm4);
                            otroPaisLocalidad.addClass('error');
                        } else {
                            otroPaisLocalidad.removeClass('error');
                        }

                    }

                    tr5.find(tr5DivPais).html('');

                }

            }

        });
        
    }
    
    
    function form4ConyugeCheckDerechosExpectativa() {
        var tbody = $('.tbody-derecho_expectativa_conyuge');

        tbody.find(".tr-datos-derecho_expectativa_conyuge").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="derecho_expectativa[conyuge]['+row+']"]' +
                '[name!="derecho_expectativa[conyuge]['+row+'][id]"]' +
                '[name!="derecho_expectativa[conyuge]['+row+'][cantidad_cuotas_pagas]"]' +
                '[name!="derecho_expectativa[conyuge]['+row+'][cantidad_cuotas_restantes]"]' +
                ',.select-tipo-derecho-expectativa' +
                ',.select-tipo-fuente' +
                '';

            var selectTipoFuente = element.find('.select-tipo-fuente');
            var valTipoFuente = selectTipoFuente.val();

            var cantidadCuotasPagas = element.find('input[name="derecho_expectativa[conyuge]['+row+'][cantidad_cuotas_pagas]"]');
            var valCantidadCuotasPagas = cantidadCuotasPagas.val();

            var cantidadCuotasRestantes = element.find('input[name="derecho_expectativa[conyuge]['+row+'][cantidad_cuotas_restantes]"]');
            var valCantidadCuotasRestantes = cantidadCuotasRestantes.val();

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoFuente == 9 || valTipoFuente == 10) {

                    if(empty(valCantidadCuotasPagas)) {
                        setFormToNoValid(selectorForm4);
                        cantidadCuotasPagas.addClass('error');
                    } else {
                        cantidadCuotasPagas.removeClass('error');
                    }

                    if(empty(valCantidadCuotasRestantes)){
                        setFormToNoValid(selectorForm4);
                        cantidadCuotasRestantes.addClass('error');
                    } else {
                        cantidadCuotasRestantes.removeClass('error');
                    }

                } else {
                    cantidadCuotasPagas.removeClass('error');
                    cantidadCuotasRestantes.removeClass('error');
                }

            }

        });
    }
    
    
    function form4ConyugeCheckOtrosBienes() {
        var tbody = $('.tbody-bienes_conyuge');

        tbody.find(".tr-datos-bienes_conyuge").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="bienes[conyuge]['+row+']"]' +
                '[name!="bienes[conyuge]['+row+'][id]"]' +
                ',.select-tipo-caracter' +
                ',.select-tipo-adquisicion' +
                '';

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });
            }

        });
    }
    
    
    function form4ConyugeCheckDeudas() {
        var tbody = $('.tbody-deudas_conyuge');

        tbody.find(".tr-datos-deudas_conyuge").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="deudas[conyuge]['+row+']"]' +
                '[name!="deudas[conyuge]['+row+'][id]"]' +
                '[name!="deudas[conyuge]['+row+'][otro_tipo_deuda]"]' +
                '[name!="deudas[conyuge]['+row+'][otro_tipo_moneda]"]' +
                '[name!="deudas[conyuge]['+row+'][cuotas_restantes]"]' +
                '[name!="deudas[conyuge]['+row+'][otro_credito_personal]"]' +
                ',.select-tipo-deuda' +
                ',.select-tipo-moneda';

            var finds = element.find(selectorInputs);

            var selectTipoDeuda = element.find('.select-tipo-deuda');
            var valTipoDeuda = selectTipoDeuda.val();

            var otroTipoDeuda = element.find('input[name="deudas[conyuge]['+row+'][otro_tipo_deuda]"]');
            var valOtroTipoDeuda = otroTipoDeuda.val();

            var selectTipoMoneda = element.find('.select-tipo-moneda');
            var valTipoMoneda = selectTipoMoneda.val();

            var otroTipoMoneda = element.find('input[name="deudas[conyuge]['+row+'][otro_tipo_moneda]"]');
            var valOtroTipoMoneda = otroTipoMoneda.val();

            var cuotasRestantes = element.find('input[name="deudas[conyuge]['+row+'][cuotas_restantes]"]');
            var valCuotasRestantes = cuotasRestantes.val();

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            })

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoDeuda == 4 && empty(valOtroTipoDeuda)) {
                    setFormToNoValid(selectorForm4);
                    otroTipoDeuda.addClass('error');
                } else {
                    otroTipoDeuda.removeClass('error');
                }

                if( (valTipoDeuda == 9 || valTipoDeuda == 10) && empty(valCuotasRestantes)) {
                    setFormToNoValid(selectorForm4);
                    cuotasRestantes.addClass('error');
                } else {
                    cuotasRestantes.removeClass('error');
                }

                if(valTipoMoneda == 4 && empty(valOtroTipoMoneda)) {
                    setFormToNoValid(selectorForm4);
                    otroTipoMoneda.addClass('error');
                } else {
                    otroTipoMoneda.removeClass('error');
                }
            }

        });
        
    }
    
    
    function form4HijoCheckInmuebles() {
        var tbody = $('.tbody-inmuebles_hijo');

        tbody.find(".tr-1-datos-inmuebles_hijo").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="inmuebles[hijos_menores]['+row+']"]' +
                '[name!="inmuebles[hijos_menores]['+row+'][id]"]' +
                '[name!="inmuebles[hijos_menores]['+row+'][superficie_cubierta]"]' +
                '[name!="inmuebles[hijos_menores]['+row+'][superficie_descubierta]"]' +
                '[name!="inmuebles[hijos_menores]['+row+'][otra_forma_pago]"]' +
                '[name!="inmuebles[hijos_menores]['+row+'][domicilio_piso]"]' +
                '[name!="inmuebles[hijos_menores]['+row+'][domicilio_depto]"]' +
                '[name!="inmuebles[hijos_menores]['+row+'][otro_pais_provincia]"]' +
                '[name!="inmuebles[hijos_menores]['+row+'][otro_pais_localidad]"]' +
                '[name!="inmuebles[hijos_menores]['+row+'][otro_tipo_moneda_valor_adquisicion]"]' +
                '[name!="inmuebles[hijos_menores]['+row+'][otro_tipo_moneda_valor_estimado]"]' +
                ',.select-tipo-inmueble' +
                ',.select-unidad-superficie' +
                ',.select-modo-adquisicion' +
                ',.select-caracter';

            var finds = element.find(selectorInputs);
            var tr2 = tbody.find('.tr-2-datos-inmuebles_hijo-'+row);
            var findsTr2 = tr2.find(selectorInputs);

            var selectFormaPago = tr2.find('.select-forma-pago');
            var valFormaPago = selectFormaPago.val();

            var selectTipoMonedaValorAdquisicion = tr2.find('.select-tipo-moneda-valor-adquisicion');
            var valSelectTipoMonedaValorAdquisicion = selectTipoMonedaValorAdquisicion.val();

            var selectTipoMonedaValorEstimado = tr2.find('.select-tipo-moneda-valor-estimado');
            var valSelectTipoMonedaValorEstimado = selectTipoMonedaValorEstimado.val();

            var otraFormaPago = tr2.find('input[name="inmuebles[hijos_menores]['+row+'][otra_forma_pago]"]');
            var valOtraFormaPago = otraFormaPago.val();

            var otroTipoMonedaValorAdquisicion = tr2.find('input[name="inmuebles[hijos_menores]['+row+'][otro_tipo_moneda_valor_adquisicion]"]');
            var valOtroTipoMonedaValorAdquisicion = otroTipoMonedaValorAdquisicion.val();

            var otroTipoMonedaValorEstimado = tr2.find('input[name="inmuebles[hijos_menores]['+row+'][otro_tipo_moneda_valor_estimado]"]');
            var valOtroTipoMonedaValorEstimado = otroTipoMonedaValorEstimado.val();

            var totalSuperficieCubierta = element.find('input[name="inmuebles[hijos_menores]['+row+'][superficie_cubierta]"]');
            var valTotalSuperficieCubierta = totalSuperficieCubierta.val();

            var totalSuperficieDescubierta = element.find('input[name="inmuebles[hijos_menores]['+row+'][superficie_cubierta]"]');
            var valTotalSuperficieDescubierta = totalSuperficieDescubierta.val();

            var tr3 = tbody.find('.tr-3-datos-inmuebles_hijo-'+row);
            var tr3DivPais = '.div-pais-'+row;
            var tr3DivProvincia = '.div-provincia-'+row;
            var tr3DivLocalidad = '.div-localidad-'+row;
            var labelErrorPais = '<label id="inmuebles[hijos_menores]['+row+'][id_pais]" class="error" for="inmuebles[hijos_menores]['+row+'][id_pais]">Completar país</label>';
            var labelErrorProvincia = '<label id="inmuebles[hijos_menores]['+row+'][id_provincia]" class="error" for="inmuebles[hijos_menores]['+row+'][id_provincia]">Completar provincia</label>';
            var labelErrorLocalidad = '<label id="inmuebles[hijos_menores]['+row+'][id_localidad]" class="error" for="inmuebles[hijos_menores]['+row+'][id_localidad]">Completar localidad</label>';

            var tr4 = tbody.find('.tr-4-datos-inmuebles_hijo-'+row);

            var selectPaises = tr4.find('.select-paises-hijos');
            var valPais = selectPaises.select2().val();

            var selectProvincias = tr4.find('.select2-provincias');
            var valProvincia = selectProvincias.select2().val();

            var selectLocalidad = tr4.find('.select-localidad');
            var valLocalidad = selectLocalidad.select2().val();

            var otroPaisProvincia = tr4.find('input[name="inmuebles[hijos_menores]['+row+'][otro_pais_provincia]"]');
            var valOtroPaisProvincia = otroPaisProvincia.val();

            var otroPaisLocalidad = tr4.find('input[name="inmuebles[hijos_menores]['+row+'][otro_pais_localidad]"]');
            var valOtroPaisLocalidad = otroPaisLocalidad.val();

            var tr5 = tbody.find('.tr-5-datos-inmuebles_hijo-'+row);
            var findsTr5 = tr5.find(selectorInputs);

            if(!empty(valFormaPago)) completedFields = true;

            if(!empty(valSelectTipoMonedaValorAdquisicion)) completedFields = true;

            if(!empty(valSelectTipoMonedaValorEstimado)) completedFields = true;

            if(!empty(valPais)) completedFields = true;

            if(valTotalSuperficieCubierta != '') completedFields = true;

            if(valTotalSuperficieDescubierta != '') completedFields = true;

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr2.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr5.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(completedFields)
            {

                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if(valTotalSuperficieCubierta == ''){
                    setFormToNoValid(selectorForm4);
                    totalSuperficieCubierta.addClass('error');
                } else {
                    totalSuperficieCubierta.removeClass('error');
                }

                if(valTotalSuperficieDescubierta == ''){
                    setFormToNoValid(selectorForm4);
                    totalSuperficieDescubierta.addClass('error');
                } else {
                    totalSuperficieDescubierta.removeClass('error');
                }

                findsTr2.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valSelectTipoMonedaValorAdquisicion)) {
                    setFormToNoValid(selectorForm4);
                    selectTipoMonedaValorAdquisicion.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorAdquisicion == 4 && empty(valOtroTipoMonedaValorAdquisicion)) {
                        setFormToNoValid(selectorForm4);
                        otroTipoMonedaValorAdquisicion.addClass('error');
                    } else {
                        otroTipoMonedaValorAdquisicion.removeClass('error');
                    }

                    selectTipoMonedaValorAdquisicion.removeClass('error');

                }

                if (empty(valSelectTipoMonedaValorEstimado)) {
                    setFormToNoValid(selectorForm4);
                    selectTipoMonedaValorEstimado.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorEstimado == 4 && empty(valOtroTipoMonedaValorEstimado)) {
                        setFormToNoValid(selectorForm4);
                        otroTipoMonedaValorEstimado.addClass('error');
                    } else {
                        otroTipoMonedaValorEstimado.removeClass('error');
                    }

                    selectTipoMonedaValorEstimado.removeClass('error');

                }

                if (empty(valFormaPago)) {
                    setFormToNoValid(selectorForm4);
                    selectFormaPago.addClass('error');
                } else {

                    if (valFormaPago == 5 && empty(valOtraFormaPago)) {
                        setFormToNoValid(selectorForm4);
                        otraFormaPago.addClass('error');
                    } else {
                        otraFormaPago.removeClass('error');
                    }

                    selectFormaPago.removeClass('error');

                }

                findsTr5.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valPais)) {
                    setFormToNoValid(selectorForm4);
                    tr3.find(tr3DivPais).html(labelErrorPais);
                    tr3.find(tr3DivProvincia).html('');
                    tr3.find(tr3DivLocalidad).html('');
                } else {

                    if (valPais == 13) {//Pais = Argentina

                        if(empty(valProvincia)) {
                            setFormToNoValid(selectorForm4);
                            tr3.find(tr3DivProvincia).html(labelErrorProvincia);
                        } else {
                            tr3.find(tr3DivProvincia).html('');
                        }

                        if(empty(valLocalidad)) {
                            setFormToNoValid(selectorForm4);
                            tr3.find(tr3DivLocalidad).html(labelErrorLocalidad);
                        } else {
                            tr3.find(tr3DivLocalidad).html('');
                        }

                    } else {

                        if(empty(valOtroPaisProvincia)) {
                            setFormToNoValid(selectorForm4);
                            otroPaisProvincia.addClass('error');
                        } else {
                            otroPaisProvincia.removeClass('error');
                        }

                        if(empty(valOtroPaisLocalidad)) {
                            setFormToNoValid(selectorForm4);
                            otroPaisLocalidad.addClass('error');
                        } else {
                            otroPaisLocalidad.removeClass('error');
                        }

                    }

                    tr3.find(tr3DivPais).html('');

                }
            }

        });

    }
    
    
    function form4HijoCheckVehiculos() {
        var tbody = $('.tbody-vehiculos_hijo');

        tbody.find(".tr-1-datos-vehiculos_hijos").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="vehiculos[hijos_menores]['+row+']"]' +
                '[name!="vehiculos[hijos_menores]['+row+'][id]"]' +
                '[name!="vehiculos[hijos_menores]['+row+'][domicilio_piso]"]' +
                '[name!="vehiculos[hijos_menores]['+row+'][domicilio_depto]"]' +
                '[name!="vehiculos[hijos_menores]['+row+'][otro_pais_provincia]"]' +
                '[name!="vehiculos[hijos_menores]['+row+'][otro_pais_localidad]"]' +
                '[name!="vehiculos[hijos_menores]['+row+'][otro_tipo_moneda_valor_seguro]"]' +
                '[name!="vehiculos[hijos_menores]['+row+'][otro_tipo_moneda_valor_estimado]"]' +
                ',.select-tipo-vehiculo' +
                ',.select-caracter' +
                ',.select-modo-adquisicion' +
                ',.select-forma-pago' +
                '';

            var finds = element.find(selectorInputs);

            var tr2 = tbody.find('.tr-2-datos-vehiculos_hijos-'+row);
            var findsTr2 = tr2.find(selectorInputs);

            var selectTipoMonedaValorSeguro = tr2.find('.select-tipo-moneda-valor-seguro');
            var valSelectTipoMonedaValorSeguro = selectTipoMonedaValorSeguro.val();

            var selectTipoMonedaValorEstimado = tr2.find('.select-tipo-moneda-valor-estimado');
            var valSelectTipoMonedaValorEstimado = selectTipoMonedaValorEstimado.val();

            var otroTipoMonedaValorSeguro = tr2.find('input[name="vehiculos[hijos_menores]['+row+'][otro_tipo_moneda_valor_seguro]"]');
            var valOtroTipoMonedaValorSeguro = otroTipoMonedaValorSeguro.val();

            var otroTipoMonedaValorEstimado = tr2.find('input[name="vehiculos[hijos_menores]['+row+'][otro_tipo_moneda_valor_estimado]"]');
            var valOtroTipoMonedaValorEstimado = otroTipoMonedaValorEstimado.val();

            var tr3 = tbody.find('.tr-3-datos-vehiculos_hijos-'+row);
            var findsTr3 = tr3.find(selectorInputs);

            var tr5 = tbody.find('.tr-5-datos-vehiculos_hijos-'+row);
            var tr5DivPais = '.div-pais-'+row;
            var tr5DivProvincia = '.div-provincia-'+row;
            var tr5DivLocalidad = '.div-localidad-'+row;
            var labelErrorPais = '<label id="vehiculos[hijos_menores]['+row+'][id_pais]" class="error" for="vehiculos[hijos_menores]['+row+'][id_pais]">Completar país</label>';
            var labelErrorProvincia = '<label id="vehiculos[hijos_menores]['+row+'][id_provincia]" class="error" for="vehiculos[hijos_menores]['+row+'][id_provincia]">Completar provincia</label>';
            var labelErrorLocalidad = '<label id="vehiculos[hijos_menores]['+row+'][id_localidad]" class="error" for="vehiculos[hijos_menores]['+row+'][id_localidad]">Completar localidad</label>';

            var tr4 = tbody.find('.tr-4-datos-vehiculos_hijos-'+row);

            var selectPaises = tr4.find('.select-paises-vehiculos-hijos');
            var valPais = selectPaises.select2().val();

            var selectProvincias = tr4.find('.select2-provincias');
            var valProvincia = selectProvincias.select2().val();

            var selectLocalidad = tr4.find('.select-localidad');
            var valLocalidad = selectLocalidad.select2().val();

            var otroPaisProvincia = tr4.find('input[name="vehiculos[hijos_menores]['+row+'][otro_pais_provincia]"]');
            var valOtroPaisProvincia = otroPaisProvincia.val();

            var otroPaisLocalidad = tr4.find('input[name="vehiculos[hijos_menores]['+row+'][otro_pais_localidad]"]');
            var valOtroPaisLocalidad = otroPaisLocalidad.val();


            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr2.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            findsTr3.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;
            });

            if(!empty(valPais)) completedFields = true;

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                findsTr2.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valSelectTipoMonedaValorSeguro)) {
                    setFormToNoValid(selectorForm3);
                    selectTipoMonedaValorSeguro.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorSeguro == 4 && empty(valOtroTipoMonedaValorSeguro)) {
                        setFormToNoValid(selectorForm3);
                        otroTipoMonedaValorSeguro.addClass('error');
                    } else {
                        otroTipoMonedaValorSeguro.removeClass('error');
                    }

                    selectTipoMonedaValorSeguro.removeClass('error');

                }

                if (empty(valSelectTipoMonedaValorEstimado)) {
                    setFormToNoValid(selectorForm3);
                    selectTipoMonedaValorEstimado.addClass('error');
                } else {

                    if (valSelectTipoMonedaValorEstimado == 4 && empty(valOtroTipoMonedaValorEstimado)) {
                        setFormToNoValid(selectorForm3);
                        otroTipoMonedaValorEstimado.addClass('error');
                    } else {
                        otroTipoMonedaValorEstimado.removeClass('error');
                    }

                    selectTipoMonedaValorEstimado.removeClass('error');

                }

                findsTr3.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }
                });

                if (empty(valPais)) {
                    setFormToNoValid(selectorForm4);
                    tr5.find(tr5DivPais).html(labelErrorPais);
                    tr5.find(tr5DivProvincia).html('');
                    tr5.find(tr5DivLocalidad).html('');
                } else {

                    if (valPais == 13) {//Pais = Argentina

                        if(empty(valProvincia)) {
                            setFormToNoValid(selectorForm4);
                            tr5.find(tr5DivProvincia).html(labelErrorProvincia);
                        } else {
                            tr5.find(tr5DivProvincia).html('');
                        }

                        if(empty(valLocalidad)) {
                            setFormToNoValid(selectorForm4);
                            tr5.find(tr5DivLocalidad).html(labelErrorLocalidad);
                        } else {
                            tr5.find(tr5DivLocalidad).html('');
                        }

                    } else {

                        if(empty(valOtroPaisProvincia)) {
                            setFormToNoValid(selectorForm4);
                            otroPaisProvincia.addClass('error');
                        } else {
                            otroPaisProvincia.removeClass('error');
                        }

                        if(empty(valOtroPaisLocalidad)) {
                            setFormToNoValid(selectorForm4);
                            otroPaisLocalidad.addClass('error');
                        } else {
                            otroPaisLocalidad.removeClass('error');
                        }

                    }

                    tr5.find(tr5DivPais).html('');

                }

            }

        });
    }
    
    
    function form4HijoCheckDerechosExpectativa() {
        var tbody = $('.tbody-derecho_expectativa_hijo');

        tbody.find(".tr-datos-derecho_expectativa_hijo").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="derecho_expectativa[hijos_menores]['+row+']"]' +
                '[name!="derecho_expectativa[hijos_menores]['+row+'][id]"]' +
                '[name!="derecho_expectativa[hijos_menores]['+row+'][cantidad_cuotas_pagas]"]' +
                '[name!="derecho_expectativa[hijos_menores]['+row+'][cantidad_cuotas_restantes]"]' +
                ',.select-tipo-derecho-expectativa' +
                ',.select-tipo-fuente' +
                '';

            var selectTipoFuente = element.find('.select-tipo-fuente');
            var valTipoFuente = selectTipoFuente.val();

            var cantidadCuotasPagas = element.find('input[name="derecho_expectativa[hijos_menores]['+row+'][cantidad_cuotas_pagas]"]');
            var valCantidadCuotasPagas = cantidadCuotasPagas.val();

            var cantidadCuotasRestantes = element.find('input[name="derecho_expectativa[hijos_menores]['+row+'][cantidad_cuotas_restantes]"]');
            var valCantidadCuotasRestantes = cantidadCuotasRestantes.val();

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoFuente == 9 || valTipoFuente == 10) {

                    if(empty(valCantidadCuotasPagas)) {
                        setFormToNoValid(selectorForm4);
                        cantidadCuotasPagas.addClass('error');
                    } else {
                        cantidadCuotasPagas.removeClass('error');
                    }

                    if(empty(valCantidadCuotasRestantes)){
                        setFormToNoValid(selectorForm4);
                        cantidadCuotasRestantes.addClass('error');
                    } else {
                        cantidadCuotasRestantes.removeClass('error');
                    }

                } else {
                    cantidadCuotasPagas.removeClass('error');
                    cantidadCuotasRestantes.removeClass('error');
                }

            }

        });
    }
    
    
    function form4HijoCheckOtrosBienes() {
        var tbody = $('.tbody-bienes_hijo');

        tbody.find(".tr-datos-bienes_hijo").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="bienes[hijos_menores]['+row+']"]' +
                '[name!="bienes[hijos_menores]['+row+'][id]"]' +
                ',.select-tipo-caracter' +
                ',.select-tipo-adquisicion' +
                '';

            var finds = element.find(selectorInputs);

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            });

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });
            }

        });
    }
    
    
    function form4HijoCheckDeudas() {
        var tbody = $('.tbody-deudas_hijo');

        tbody.find(".tr-datos-deudas_hijo").each(function(index){

            var completedFields = false;
            var element = $(this);
            var row = element.data('row');
            var selectorInputs = 'input[name*="deudas[hijos_menores]['+row+']"]' +
                '[name!="deudas[hijos_menores]['+row+'][id]"]' +
                '[name!="deudas[hijos_menores]['+row+'][otro_tipo_deuda]"]' +
                '[name!="deudas[hijos_menores]['+row+'][otro_tipo_moneda]"]' +
                '[name!="deudas[hijos_menores]['+row+'][cuotas_restantes]"]' +
                '[name!="deudas[hijos_menores]['+row+'][otro_credito_personal]"]' +
                ',.select-tipo-deuda' +
                ',.select-tipo-moneda';

            var finds = element.find(selectorInputs);

            var selectTipoDeuda = element.find('.select-tipo-deuda');
            var valTipoDeuda = selectTipoDeuda.val();

            var otroTipoDeuda = element.find('input[name="deudas[hijos_menores]['+row+'][otro_tipo_deuda]"]');
            var valOtroTipoDeuda = otroTipoDeuda.val();

            var selectTipoMoneda = element.find('.select-tipo-moneda');
            var valTipoMoneda = selectTipoMoneda.val();

            var otroTipoMoneda = element.find('input[name="deudas[hijos_menores]['+row+'][otro_tipo_moneda]"]');
            var valOtroTipoMoneda = otroTipoMoneda.val();

            var cuotasRestantes = element.find('input[name="deudas[hijos_menores]['+row+'][cuotas_restantes]"]');
            var valCuotasRestantes = cuotasRestantes.val();

            finds.each(function(index){
                var input = $(this);
                var value = input.val();

                if(!empty(value)) completedFields = true;

            })

            if(completedFields) {
                finds.each(function(index){
                    var input = $(this);
                    var value = input.val();

                    if(empty(value)) {
                        setFormToNoValid(selectorForm4);
                        input.addClass('error');
                    } else {
                        input.removeClass('error');
                    }

                });

                if(valTipoDeuda == 4 && empty(valOtroTipoDeuda)) {
                    setFormToNoValid(selectorForm4);
                    otroTipoDeuda.addClass('error');
                } else {
                    otroTipoDeuda.removeClass('error');
                }

                if( (valTipoDeuda == 9 || valTipoDeuda == 10) && empty(valCuotasRestantes)) {
                    setFormToNoValid(selectorForm4);
                    cuotasRestantes.addClass('error');
                } else {
                    cuotasRestantes.removeClass('error');
                }

                if(valTipoMoneda == 4 && empty(valOtroTipoMoneda)) {
                    setFormToNoValid(selectorForm4);
                    otroTipoMoneda.addClass('error');
                } else {
                    otroTipoMoneda.removeClass('error');
                }
            }

        });
    }
    
    
    
</script>