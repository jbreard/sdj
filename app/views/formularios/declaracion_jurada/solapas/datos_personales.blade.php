<div class="row">
    <div class="form-group col-md-12">
        <br>
        <a class="btn btn-primary btnPrevious" >Anterior</a>
        -
        <a class="btn btn-primary btnNext" id="btn-submit-form-1">Siguiente</a>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[apellido]', 'Apellido',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[apellido_fake]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['apellido']),array('class' => 'form-control','readonly'=>'')); }}
        {{ $errors->first('apellido') }}
    </div>
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[nombres]', 'Nombres',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[nombres_fake]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['nombres']),array('class' => 'form-control','readonly'=>'')); }}
        {{ $errors->first('nombres') }}
    </div>
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[nacimiento]', 'Fecha de nacimiento',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[nacimiento]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['nacimiento']),array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento')); }}
        {{ $errors->first('nacimiento') }}
    </div>
</div>


<div class="row">
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[id_tipo_sexo]', 'Género',array('class'=>'label_datos_personales')) }}
        {{ Form::select('datos_personales[id_tipo_sexo]',$combo_tipos_sexo,(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['id_tipo_sexo']),array('class' => 'form-control')) }}
    </div>
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[documento]', 'DNI',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[documento_fake]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['documento']),array('class' => 'form-control masked-input-dni','size'=>'8','readonly'=>'')); }}
        {{ $errors->first('documento') }}
    </div>
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[cuil_cuit]', 'CUIL/CUIT',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[cuil_cuit]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['cuil_cuit']),array('class' => 'form-control masked-input-cuil_cuit')); }}
        {{ $errors->first('nacimiento') }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[id_estado_civil]', 'Estado civil',array('class'=>'label_datos_personales')) }}
        {{ Form::select('datos_personales[id_estado_civil]',$combo_estado_civil,(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['id_estado_civil']),array('class' => 'form-control')) }}
        {{ $errors->first('estado_civil') }}
    </div>
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[domicilio_real]', 'Domicilio real',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[domicilio_real]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['domicilio_real']),array('class' => 'form-control')); }}
        {{ $errors->first('domicilio_real') }}
    </div>
    <div class="form-group col-md-4">
        {{ Form::label('datos_personales[codigo_postal]', 'Código postal',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[codigo_postal]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['codigo_postal']),array('class' => 'form-control')); }}
    </div>
</div>


<div class="row">
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[id_provincia]', 'Provincia',array('class'=>'label_datos_personales')) }}
        {{ Form::select('datos_personales[id_provincia]',$combo_provincias,(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['id_provincia']),array('class' => 'form-control select2 select2-provincias','data-localidad'=>'datos_personales_localidad', 'data-id-localidad'=>(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['id_localidad']) )) }}
        {{ $errors->first('id_provincia') }}
    </div>
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[id_localidad]', 'Localidad',array('class'=>'label_datos_personales')) }}
        {{ Form::select('datos_personales[id_localidad]',[],null,array('class' => 'form-control select2 datos_personales_localidad')) }}
        {{ $errors->first('id_localidad') }}
    </div>
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[telefono]', 'Telefono de contacto',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[telefono]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['telefono']),array('class' => 'form-control')); }}
        {{ $errors->first('telefono') }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[email]', 'Correo electrónico',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[email]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['email']),array('class' => 'form-control')); }}
        {{ $errors->first('email') }}
    </div>
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[destino_revista]', 'Destino de revista (Sin abreviaturas)',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[destino_revista]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['destino_revista']),array('class' => 'form-control')); }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[id_fuerza]', 'Fuerza a la que pertenece',array('class'=>'label_datos_personales')) }}
        {{ Form::select('datos_personales[id_fuerza]',$combo_fuerzas,(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['id_fuerza']),array('class' => 'form-control select2 select2-fuerza-perteneciente','data-id-tipo-personal'=>(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['id_tipo_personal'])  )) }}
        {{ $errors->first('id_fuerza') }}
    </div>
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[id_tipo_personal]', 'Personal',array('class'=>'label_datos_personales')) }}
        {{ Form::select('datos_personales[id_tipo_personal]',[],null,array(
            'class' => 'form-control select2 select-personal',
            'data-id-grado'=>(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['id_grado']),
            'data-id-agrupamiento-modalidad'=>(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['id_tipo_agrupamiento_modalidad'])
        )) }}
        {{ $errors->first('id_tipo_personal') }}
    </div>
    <div class="form-group col-md-4 div-grado-fuerza">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[id_grado]', 'Grado',array('class'=>'label_datos_personales')) }}
        {{ Form::select('datos_personales[id_grado]',[],null,array('class' => 'form-control select2 select-grado')); }}
        {{ $errors->first('id_grado') }}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[legajo_personal]', 'Legajo Personal',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[legajo_personal]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['legajo_personal']),array('class' => 'form-control')); }}
        {{ $errors->first('legajo_personal') }}
    </div>
    <div class="form-group col-md-4 cuerpo_agrupamiento div-agrup-cuerpo">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[cuerpo_agrupamiento]', 'Cuerpo/Agrupamiento (Sin abreviaturas)',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[cuerpo_agrupamiento]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['cuerpo_agrupamiento']),array('class' => 'form-control')); }}
        {{ $errors->first('cuerpo_agrupamiento') }}
    </div>
    <div class="form-group col-md-4 div-agrup-modalidad">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[id_tipo_agrupamiento_modalidad]', 'Agrupamiento/Modalidad',array('class'=>'label_datos_personales')) }}
        {{ Form::select('datos_personales[id_tipo_agrupamiento_modalidad]',[],null,array('class' => 'form-control select2 select-agrupamiento-modalidad' )) }}
        {{ $errors->first('id_tipo_agrupamiento_modalidad') }}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[cargo_funcion]', 'Cargo o función (Sin abreviaturas)',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[cargo_funcion]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['cargo_funcion']),array('class' => 'form-control','maxlength'=>'60')); }}
        {{ $errors->first('cargo_funcion') }}
    </div>
    <div class="form-group col-md-6 div-escalafon">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[escalafon]', 'Escalafón (Sin abreviaturas)',array('class'=>'label_datos_personales')) }}
        {{ Form::text('datos_personales[escalafon]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['escalafon']),array('class' => 'form-control')); }}
        {{ $errors->first('escalafon') }}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[id_situacion_revista]', 'Situación de revista',array('class'=>'label_datos_personales')) }}
        {{ Form::select('datos_personales[id_situacion_revista]',$combo_tipos_situaciones_revista,(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['id_situacion_revista']),array('class' => 'form-control select-situacion-revista')); }}
        {{ $errors->first('id_situacion_revista') }}
    </div>
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('datos_personales[llamados_servicio]', 'Llamados a prestar servicio',array('class'=>'label_datos_personales')) }}
        {{ Form::select('datos_personales[llamados_servicio]',$combo_llamados_prestar_servicio,(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['llamados_servicio']),array('class' => 'form-control select-llamados-servicio','id'=>'datos_personales[llamados_servicio]')) }}
        {{ $errors->first('llamados_servicio') }}
    </div>
</div>
{{ Form::hidden('datos_personales[apellido]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['apellido']),array('class' => 'form-control')); }}
{{ Form::hidden('datos_personales[nombres]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['nombres']),array('class' => 'form-control')); }}
{{ Form::hidden('datos_personales[documento]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['persona']['documento']),array('class' => 'form-control masked-input-dni','size'=>'8')); }}