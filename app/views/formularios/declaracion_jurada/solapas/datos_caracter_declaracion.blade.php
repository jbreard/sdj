@if($cartel<>'')
<div class="alert alert-warning" role="alert">
    {{$cartel}}
</div>
@endif
<div class="row"><div class="form-group col-md-12"></div><br></div>
<div class="row">
    <div class="form-group col-md-12">
        <a class="btn btn-primary btnNext" id="btn-submit-form-6" >Siguiente</a>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-4">
        <a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>
        {{ Form::label('caracter_declaracion[id_tipo_caracter]', 'Caracter de la declaración') }}
        {{ Form::select('caracter_declaracion[id_tipo_caracter]',$combo_caracter_declaracion,(is_null($declaracion_jurada) ? null : $declaracion_jurada['id_tipo_caracter']),array('class' => 'form-control select2-caracter-declaracion')) }}
    </div>
    <div class="form-group col-md-4">
        {{ Form::label('caracter_declaracion[periodo_fiscal]', 'Periodo Fiscal') }}
        {{ Form::select('caracter_declaracion[periodo_fiscal]',$comboPeriodoFiscal ,null,array('class' => 'form-control','id'=>'periodo_fiscal')) }}
        {{ $errors->first('caracter_declaracion[periodo_fiscal]') }}
    </div>
</div>
<div class="row div-caracter-baja">
    <div class="form-group col-md-4">
        {{--<a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>--}}
        {{ Form::label('caracter_declaracion[id_tipo_acto_administrativo]', 'Acto Admnistrativo') }}
        {{ Form::select('caracter_declaracion[id_tipo_acto_administrativo]',$combo_actos_administrativos,(is_null($declaracion_jurada) ? null : $declaracion_jurada['id_tipo_acto_administrativo']),array('class' => 'form-control input-caracter-baja')) }}
    </div>
    <div class="form-group col-md-4">
        {{--<a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>--}}
        {{ Form::label('caracter_declaracion[numero_anio]', 'Número/Año') }}
        {{ Form::text('caracter_declaracion[numero_anio]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['numero_anio']),array('class' => 'form-control masked-numero-anio input-caracter-baja')); }}
    </div>
    <div class="form-group col-md-4">
        {{--<a style='margin-right: 10px'><i class='glyphicon glyphicon-asterisk asterisk-required' ></i></a>--}}
        {{ Form::label('caracter_declaracion[fecha_acto_administrativo]', 'Fecha del acto administrativo') }}
        {{ Form::text('caracter_declaracion[fecha_acto_administrativo]',(is_null($declaracion_jurada) ? null : $declaracion_jurada['fecha_acto_administrativo']),array('class' => 'form-control datetimepicker-fecha-nacimiento masked-input-fecha-nacimiento input-caracter-baja')); }}
    </div>
</div>
