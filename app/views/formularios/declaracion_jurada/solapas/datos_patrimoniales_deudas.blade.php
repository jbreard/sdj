<div class="row">
    <div class="form-group col-md-12">
        <br>
        <a class="btn btn-primary btnPrevious" >Anterior</a>
        -
        <a class="btn btn-primary btnNext" id="btn-submit-form-8" >Siguiente</a>
    </div>
</div>
<div id="tarjetas_credito">
    <table class="table table-striped table-condensed">
        <caption>
            <span class="title">Tarjetas de crédito</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="tarjetas_credito" data-count="0">[Añadir registro]</button>
            <div class="bs-callout bs-callout-info" id="callout-navbar-breakpoint">
                <h4>Notas de carga</h4>
                <p>
                    {{ Config::get('app.textos.notas_carga.tarjetas_credito.a'); }}
                    <br>
                </p>
            </div>
        </caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Banco</th>
            <th>Entidad emisora</th>
            <th>Titular</th>
            <th>Cantidad de extensiones</th>
        </tr>
        </thead>
        <tbody class="tbody-tarjetas_credito">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.tarjetas_credito',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_tarjeta_credito; $i++)
                @include('formularios.declaracion_jurada.modulos.tarjetas_credito',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>
</div>

<div id="deudas">
    <table class="table table-striped table-condensed">
        <caption>
            <span class="title">Deudas</span>
            <button type="button" class="btn btn-link btn-add-registry" data-type_registry="deudas_propios" data-count="0">[Añadir registro]</button>
        </caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Tipo de deuda</th>
            <th>Acreedor</th>
            <th>Moneda</th>
            <th>Saldo que adeuda</th>
        </tr>
        </thead>
        <tbody class="tbody-deudas_propios">
        @if(is_null($declaracion_jurada))
            @include('formularios.declaracion_jurada.modulos.deudas_propios',['i'=>0])
        @else
            @for ($i = 0; $i < $cantidad_deuda['propio']; $i++)
                @include('formularios.declaracion_jurada.modulos.deudas_propios',['i'=>$i])
            @endfor
        @endif
        </tbody>
    </table>
</div>
