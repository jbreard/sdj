@extends('header_dmz')

@section('styles')
<style>

    .error{
        color: red;
        font-weight: bold;
        font-size: 11px;
    }

</style>
@endsection
@section('scripts')
<script>

    function darMensaje()
    {
        $("#contenido-modal-1").html("No se puede pegar en este campo");
        $("#confirmacion-1").modal(function(){show:true});
    }

    $(document).ready(function(){
        $(".dni").mask("9999999?9");

    });

</script>
@endsection


@section('content')
    <div class="row">
        <div class="form-group col-md-12" id="div-message-error">
            {{ $errors->first('redirect') }}
        </div>
    </div>

    {{ Form::model(null,array('route' => 'accessForm.validateFormAccess'), ['role' => 'form']) }}

    <div class="row" style="margin-top: 20px">
        <div class="form-group col-md-4 col-md-offset-4">
            {{ Form::label('id_fuerza', 'Fuerza') }}
            {{ Form::select('id_fuerza',$combo_fuerzas,Input::old('id_fuerza'),array('class' => 'form-control','id' => 'id_fuerza')) }}
            <span class="error">{{ $errors->first('id_fuerza') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-4 col-md-offset-4">
            {{ Form::label('dni', 'DNI') }}
            {{ Form::text('dni',Input::old('dni'),array('class'=>'form-control dni','placeholder' => '')) }}
            <span class="error">{{ $errors->first('dni') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-4 col-md-offset-4">
            {{ Form::label('legajo', 'Legajo  (en caso de no tener legajo, se repite DNI) ') }}
            {{ Form::text('legajo',Input::old('legajo'),array('class'=>'form-control')) }}
            <span class="error">{{ $errors->first('legajo') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-4 col-md-offset-4">
            {{ Form::label('email', 'Email') }}
            {{ Form::text('email',Input::old('email'),array('class'=>'form-control')) }}
            <span class="error">{{ $errors->first('email') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-4 col-md-offset-4">
            {{ Form::label('confirm_email', 'Reingrese mail') }}
            {{ Form::text('confirm_email',Input::old('confirm_email'),array('class'=>'form-control','onpaste'=>"darMensaje(); return false;")) }}
            <span class="error">{{ $errors->first('confirm_email') }}</span>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-4 col-md-offset-4">
            @if(Session::has('user_doesnt_exist'))
                <span class="error">{{ Session::get('user_doesnt_exist') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-4 col-md-offset-4">
            {{Form::captcha()}}
            <span class="error">{{ $errors->first('recaptcha_response_field')  }}</span>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-offset-4">
            <div class="row">
                <div class="form-group col-md-4">
                    <a id="btnTutorial" href="{{ route('archivos.descargar')}}" class="" target="_blank" style="margin-right: 10px">{{ Config::get('app.textos.guia_pdf') }}</a>
                </div>
                <div class="form-group col-md-4">
                    {{ Form::button("Ingresar al formulario", ['type' => 'submit', 'class' => 'btn btn-primary pull-right']) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

    @include('components.modal_confirmation',['accion' => 'Atención','id' => 1])

@endsection