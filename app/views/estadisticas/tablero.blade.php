@extends('header')

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script type="application/javascript">
        var chartDdjj = new Chart(document.getElementById('chart_ddjj').getContext('2d'), {
            // The type of chart we want to create
            type: 'bar',

            // The data for our dataset
            data: {
                labels: ["Creadas", "Recibidas", "Aceptadas", "Eliminadas"],
                datasets: [{
                    label: "Declaraciones Juradas",
                    backgroundColor: [
                        '#253544',
                        '#0c93bf',
                        '#27ae60',
                        '#c54133',
                    ],
                    borderColor: '#000000',
                    data: [
                        {{number_format($count_creadas,0,',','.')}},
                        {{number_format($count_recibidas,0,',','.')}},
                        {{number_format($count_aceptadas,0,',','.')}},
                        {{number_format($count_eliminadas,0,',','.')}}
                    ],
                }]
            },

            // Configuration options go here
            options: {animation : false}
        });

        var chartPfa = new Chart(document.getElementById('chart_pfa_ubicacion').getContext('2d'), {
            // The type of chart we want to create
            type: 'bar',

            // The data for our dataset
            data: {
                labels: [{{$ubicacion_pfa_data['keys']}}],
                datasets: [{
                    label: "Policía Federal Argentina  {{number_format($ubicacion_pfa_data['total'],0,',','.')}}",
                    backgroundColor:'#0040ff',
                    borderColor: '#000000',
                    data: [{{$ubicacion_pfa_data['counts']}}],
                }]
            },

            // Configuration options go here
            options: {animation : false}
        });

        var chartGna = new Chart(document.getElementById('chart_gna_ubicacion').getContext('2d'), {
            // The type of chart we want to create
            type: 'bar',

            // The data for our dataset
            data: {
                labels: [{{$ubicacion_gna_data['keys']}}],
                datasets: [{
                    label: "Gendarmeria Nacional Argentina  {{number_format($ubicacion_gna_data['total'],0,',','.')}}",
                    backgroundColor:'#4d6600',
                    borderColor: '#000000',
                    data: [
                        {{$ubicacion_gna_data['counts']}}
                    ],
                }]
            },

            // Configuration options go here
            options: {animation : false}
        });

        var chartPna = new Chart(document.getElementById('chart_pna_ubicacion').getContext('2d'), {
            // The type of chart we want to create
            type: 'bar',

            // The data for our dataset
            data: {
                labels: [{{$ubicacion_pna_data['keys']}}],
                datasets: [{
                    label: "Prefectura Naval Argentina  {{number_format($ubicacion_pna_data['total'],0,',','.')}}",
                    backgroundColor:'#00bfff',
                    borderColor: '#000000',
                    data: [
                        {{$ubicacion_pna_data['counts']}}
                    ],
                }]
            },

            // Configuration options go here
            options: {animation : false}
        });

        var chartPsa = new Chart(document.getElementById('chart_psa_ubicacion').getContext('2d'), {
            // The type of chart we want to create
            type: 'bar',
            // The data for our dataset
            data: {
                labels: [{{$ubicacion_psa_data['keys']}}],
                datasets: [{
                    label: "Policía de Seguridad Aeroportuaria  {{number_format($ubicacion_psa_data['total'],0,',','.')}}",
                    backgroundColor:'#000000',
                    borderColor: '#000000',
                    data: [{{$ubicacion_psa_data['counts']}}],
                }]
            },

            // Configuration options go here
            options: {animation : false}
        });

        var dataInmueblesPfa = {
            labels: [{{$inmueble_pfa_data['keys']}}],
            @foreach($inmueble_pfa_data['labels'] as $label=>$array)
            datasets: [{
                label: '{{$label}}',
                backgroundColor: "#27ae60",
                data: []
            },
            @endforeach
        };

        window.myBar = new Chart(document.getElementById("chart_pfa_inmueble").getContext("2d"), {
            type: 'bar',
            data: dataInmueblesPfa,
            options: {
                title:{
                    display:true,
                    text:"Chart.js Bar Chart - Stacked"
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });


    </script>
@endsection

@section('content')
<style>
    .iframe-mapa {
        width: 100%;
        height: 600px;
        border: none;
    }
</style>


<div class="row">
    <div class="col-md-12">
        <h2>Estadísticas <small>Datos al {{date("Y-m-d H:i:s")}}</small></h2>
    </div>

    <!--<div class="col-md-12">
        <h4>Mapa cantidad DDJJFF por fuerzas <small>Fecha del dato: {{Config::get('app.textos.mapa_cantidad_ddjjff.fecha_dato')}}</small></h4>
        <iframe class="iframe-mapa" src="http://173.10.0.96/qgis2web2"></iframe>
    </div>-->

</div>
<div class="row">
    <div class="col-md-12">
        <h4>Total de Declaraciones Juradas: <b>{{number_format($count_total,0,',','.')}}</b> </h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10">
        <canvas id="chart_ddjj"></canvas>
    </div>
    <div class="col-md-12">
        <h4>Total de Declaraciones Juradas por Fuerzas y Provincias </h4>
    </div>
    <div class="col-md-10">
        <canvas id="chart_pfa_ubicacion"></canvas>
    </div>
    <div class="col-md-10">
        <canvas id="chart_gna_ubicacion"></canvas>
    </div>
    <div class="col-md-10">
        <canvas id="chart_pna_ubicacion"></canvas>
    </div>
    <div class="col-md-10">
        <canvas id="chart_psa_ubicacion"></canvas>
    </div>
    <div class="col-md-10">
        <canvas id="chart_pfa_inmueble"></canvas>
    </div>

</div>
@endsection