{{ HTML::style('assets/jquery-ui-1.11.4/jquery-ui.css') }}
{{ HTML::style('assets/bootstrap-3.3.6-dist/css/bootstrap.css', array('media' => 'all')) }}
{{ HTML::style('assets/bootstrap-3.3.6-dist/css/bootstrap-theme.min.css', array('media' => 'all')) }}
{{ HTML::style('assets/select2-4.0.3/dist/css/select2.css', array('media' => 'all')) }}
{{ HTML::style('assets/bootstrap-3.3.6-dist/css/bootstrap-datetimepicker.css', array('media' => 'all')) }}

{{ HTML::style('assets/css/ladda-themeless.min.css') }}
{{ HTML::style('assets/css/main.css', array('media' => 'all')) }}

<style type="text/css" media="all">
    @font-face {
        font-family: 'Roboto Condensed';
        font-style: normal;
        font-weight: 700;
        src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url('{{ URL::asset("assets/fonts/b9QBgL0iMZfDSpmcXcE8nCSLrGe-fkSRw2DeVgOoWcQ.woff") }}') format('woff');
    }
</style>
<style>
    #contenido{
        margin-top:25px;
    }
</style>
<div id="top-nav" class="skin-6">
    <!-- logo -->

    <h1>Ministerio de Seguridad</h1>
    <div class="brand">
        <span>
            {{Config::get('app.nombre_sistema')}}
        </span>
    </div>

</div>

<div id="contenido" class="main-container">
    <div class="col-md-2">
        {{$remitente}}
        <br>

    </div>
    <div class="col-md-8">
        {{$contenido}}
    </div>
    <div class="col-md-2"></div>

</div>

