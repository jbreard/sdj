<!DOCTYPE html>
<html>
    <head>
        <title>{{Config::get('app.nombre_sistema')}}</title>
        <meta charset="UTF-8">

        <style>
            /*
            Tables
            ---------------------------------------------------------------------------------------------------- */

            table {
                max-width: 100%;
                font-size: small !important;
            }

            th {
                text-align: left;
            }

            .table {
                width: 100%;
                margin-bottom: 20px;
            }

            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                padding: 8px;
                line-height: 1.428571429;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }

            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #ddd;
            }

            .table > caption + thead > tr:first-child > th,
            .table > colgroup + thead > tr:first-child > th,
            .table > thead:first-child > tr:first-child > th,
            .table > caption + thead > tr:first-child > td,
            .table > colgroup + thead > tr:first-child > td,
            .table > thead:first-child > tr:first-child > td {
                border-top: 0;
            }

            .table > tbody + tbody {
                border-top: 2px solid #ddd;
            }

            .table .table {
                background-color: #fff;
            }

            .table-condensed > thead > tr > th,
            .table-condensed > tbody > tr > th,
            .table-condensed > tfoot > tr > th,
            .table-condensed > thead > tr > td,
            .table-condensed > tbody > tr > td,
            .table-condensed > tfoot > tr > td {
                padding: 5px;
            }

            .table-bordered {
                border: 1px solid #ddd;
            }

            .table-bordered > thead > tr > th,
            .table-bordered > tbody > tr > th,
            .table-bordered > tfoot > tr > th,
            .table-bordered > thead > tr > td,
            .table-bordered > tbody > tr > td,
            .table-bordered > tfoot > tr > td {
                border: 1px solid #ddd;
            }

            .table-bordered > thead > tr > th,
            .table-bordered > thead > tr > td {
                border-bottom-width: 2px;
            }

            .table-striped > tbody > tr:nth-child(odd) > td,
            .table-striped > tbody > tr:nth-child(odd) > th {
                background-color: #eaeaea;
            }

            .table-hover > tbody > tr:hover > td,
            .table-hover > tbody > tr:hover > th {
                background-color: #f5f5f5;
            }

            tr > .title {
                text-align: center;
                background: #0a4aa8;
            }

            h2{
                text-align: center;
            }

            @page {
                margin: 180px 50px;
                counter-increment: page;
                content: counter(page);
            }
            header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; text-align: center; }
            /*header p{ text-align: left;}*/

            footer {
                position: fixed; left: 0px; bottom: -180px; right: 0px; height: 100px;
            }
            footer p {
                width:960px;
                margin:0 auto;
            }

            footer .page:after {
                content: " Página: " counter(page);
            }

            @media print {
                h2,table {page-break-after: always;}
            }

            .inmuebles_header_registry {
                color: #FFFFFF;
                background-color: #1b809e;
                border: 0 !important;
            }
        </style>

    </head>
    <body>
        <header>
            <div align="left" style="font-size:13px;">Número de Formulario: {{$data->id}}</div>
            <div align="left" style="font-size:13px;">Número de Versión: {{$data->version}}</div>
            <center><img src="{{ URL::asset('bootstrap/images/logo-minseg2.png')}}" /></center>
        </header>
        @yield('contenido')
        <footer>
            <p>DECLARANTE:__________________________ {{$data->persona->nombres}}, {{$data->persona->apellido}}, {{$data->persona->documento}}</p>
            <br>
            {{--<p>DEPENDENCIA:_________________________ NOMBRE_________ APELLIDO_________ DNI_________</p>--}}
            <div align="right" class="page" style="font-size:13px;">Fecha,Hora:{{date("d-m-Y H:i")}}. </div>

        </footer>

    </body>
</html>