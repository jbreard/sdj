<table class="table table-striped table-bordered">
    <caption>Ingreso</caption>
    <thead>
    <tr>
        <th>Salario percibido por</th>
        <th>Monto anual neto</th>
        <th>Monto anual bruto</th>
    </tr>
    </thead>
    <tbody>
        @foreach($ingresos as $ingreso)
        <tr>
            <td>{{$ingreso->salario->descripcion or ''}}</td>
            <td>{{$ingreso->monto_anual_neto}}</td>
            <td>{{$ingreso->monto_anual_bruto}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
