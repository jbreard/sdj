<table class="table table-striped table-bordered">
    <caption>Bienes</caption>
    <thead>
    <tr>
        <th>Descripción</th>
        <th>Caracter</th>
        <th>Modo de Adquisición</th>
        <th>Monto</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bienes as $bien)
        <tr>
            <td>{{$bien->descripcion or ''}}</td>
            <td>{{$bien->tipo_caracter->descripcion or ''}}</td>
            <td>{{$bien->tipo_adquisicion->descripcion or ''}}</td>
            <td>{{$bien->monto}}</td>
        </tr>
    @endforeach
    </tbody>
</table>