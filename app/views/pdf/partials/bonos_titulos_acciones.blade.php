<table class="table table-striped table-bordered">
    <caption>Bonos, títulos valores y/o acciones</caption>
    <thead>
    <tr>
        <th>Tipo</th>
        <th>Otro Tipo</th>
        <th>Descripción</th>
        <th>Cantidad</th>
        <th>Valor nominal</th>
        <th>Entidad emisora/otorgante</th>
        <th>Fecha de adquisición</th>
    </tr>
    </thead>
    <tbody>
    @foreach($bonosTitulosAcciones as $bonoTituloAccion)
        <tr>
            <td>{{$bonoTituloAccion->tipo_bono->descripcion or ''}}</td>
            <td>{{$bonoTituloAccion->otro_tipo_bono}}</td>
            <td>{{$bonoTituloAccion->descripcion or ''}}</td>
            <td>{{$bonoTituloAccion->cantidad}}</td>
            <td>{{$bonoTituloAccion->valor_nominal}}</td>
            <td>{{$bonoTituloAccion->entidad_emisora_otorgante}}</td>
            <td>{{$bonoTituloAccion->fecha_adquisicion}}</td>
        </tr>
    @endforeach
    </tbody>
</table>


