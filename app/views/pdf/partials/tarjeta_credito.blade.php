<table class="table table-striped table-bordered">
    <caption>Tarjeta de Credito</caption>
    <thead>
    <tr>
        <th>Banco</th>
        <th>Entidad emisora</th>
        <th>Otra entidad emisora</th>
        <th>Titular</th>
        <th>Cantidad de extensiones</th>
    </tr>
    </thead>
    <tbody>
        @foreach($cuentas as $cuenta)
        <tr>
            <td>{{$cuenta->banco}}</td>
            <td>{{$cuenta->entidad_emisora->descripcion or ''}}</td>
            <td>{{$cuenta->otra_entidad_emisora}}</td>
            <td>{{$cuenta->tipo_titular->descripcion or ''}}</td>
            <td>{{$cuenta->cantidad_extension}}</td>
        </tr>
        @endforeach
    </tbody>
</table>




