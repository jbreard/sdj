<table class="table table-striped table-bordered">
    <caption>Deuda {{$titulo or ''}}</caption>
    <thead>
    <tr>
        <th>Tipo de deuda</th>
        <th>Otro tipo de deuda</th>
        <th>Otro crédito personal</th>
        <th>Cuotas restantes a pagar</th>
        <th>Acreedor</th>
        <th>Moneda</th>
        <th>Otra Moneda</th>
        <th>Saldo que adeuda</th>
    </tr>
    </thead>
    <tbody>
        @foreach($deudas as $deuda)
        <tr>
            <td>{{$deuda->tipo_deuda->descripcion or ''}}</td>
            <td>{{$deuda->otro_tipo_deuda}}</td>
            <td>{{$deuda->otro_credito_personal}}</td>
            <td>{{$deuda->cuotas_restantes}}</td>
            <td>{{$deuda->acreedor}}</td>
            <td>{{$deuda->tipo_moneda->descripcion or ''}}</td>
            <td>{{$deuda->otro_tipo_moneda}}</td>
            <td>{{$deuda->monto}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
