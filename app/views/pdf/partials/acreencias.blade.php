<table class="table table-striped table-bordered">
    <caption>Acreencias</caption>
    <thead>
    <tr>
        <th>Identidad del deudor</th>
        <th>Moneda</th>
        <th>Otra Moneda</th>
        <th>Monto</th>
        <th>Fecha</th>
    </tr>
    </thead>
    <tbody>
    @foreach($acreencias as $acreencia)
        <tr>
            <td>{{$acreencia->identidad_deudor}}</td>
            <td>{{$acreencia->tipo_moneda->descripcion or ''}}</td>
            <td>{{$acreencia->otro_tipo_moneda}}</td>
            <td>{{$acreencia->monto}}</td>
            <td>{{$acreencia->fecha}}</td>
        </tr>
    @endforeach
    </tbody>
</table>


