<table class="table table-striped table-bordered">
    <caption>Vehículos {{ $titulo or ''  }}</caption>

    <?php $i=0;?>
    @foreach($vehiculos as $vehiculo)
        <thead>
        <tr><td colspan="4" class="inmuebles_header_registry"># {{ $i }}</td></tr>
        <tr>
            <th>Tipo de vehículo</th>
            <th>Tipo</th>
            <th colspan="2">Marca</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$vehiculo->tipo_vehiculo_nuevo->descripcion or ''}}</td>
            <td>{{$vehiculo->tipo}}</td>
            <td colspan="2">{{$vehiculo->marca}}</td>
        </tr>
        </tbody>
        <thead>
        <tr><td colspan="4" class="inmuebles_header_registry"># {{ $i }}</td></tr>
        <tr>
            <th>Modelo</th>
            <th>Año fabricación</th>
            <th colspan="2">Dominio/Matrícula</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$vehiculo->modelo}}</td>
            <td>{{$vehiculo->anio}}</td>
            <td colspan="2">{{$vehiculo->dominio}}</td>
        </tr>
        </tbody>

        <thead>
        <tr>
            <th>Fecha de transferencia del dominio</th>
            <th>Carácter</th>
            <th>% de titularidad</th>
            <th>Valor de adquisición</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$vehiculo->fecha_transferencia_dominio}}</td>
            <td>{{$vehiculo->tipo_caracter->descripcion or ''}}</td>
            <td>{{$vehiculo->porcentaje}}</td>
            <td>{{$vehiculo->valor_adquisicion}}</td>
        </tr>
        </tbody>

        <thead>
        <tr>
            <th>Moneda</th>
            <th>Valuación según seguro</th>
            <th>Moneda</th>
            <th>Modo de adquisición</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            @if($vehiculo->id_tipo_moneda_valor_estimado != 4)
                <td>{{$vehiculo->moneda_valor_estimado->descripcion or ''}}</td>
            @else
                <td>{{$vehiculo->otro_tipo_moneda_valor_estimado or ''}}</td>
            @endif

            <td>{{$vehiculo->valor_seguro}}</td>
            @if($vehiculo->id_tipo_moneda_valor_seguro != 4)
                <td>{{$vehiculo->moneda_valor_seguro->descripcion or ''}}</td>
            @else
                <td>{{$vehiculo->otro_tipo_moneda_valor_seguro or ''}}</td>
            @endif
            <td>{{$vehiculo->tipo_modo_adquisicion->descripcion or ''}}</td>
        </tr>
        </tbody>
        <thead>
        <tr>
            <th>Forma de pago</th>
            <th>Pais</th>
            <th>Provincia/Estado</th>
            <th>Localidad</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$vehiculo->forma_pago->descripcion or ''}}</td>
            <td>{{$vehiculo->pais->descripcion or ''}}</td>
            <td>{{$vehiculo->provincia->descripcion or $vehiculo->otro_pais_provincia}}</td>
            <td>{{$vehiculo->localidad->descripcion or $vehiculo->otro_pais_localidad}}</td>
        </tr>
        </tbody>
        <?php $i++;?>
    @endforeach
</table>