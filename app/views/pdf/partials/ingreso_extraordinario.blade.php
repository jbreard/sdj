<table class="table table-striped table-bordered">
    <caption>Ingreso Extraordinario</caption>
    <thead>
    <tr>
        <th>Tipo de ingreso</th>
        <th>Otro tipo de ingreso</th>
        <th>Monto neto anual total</th>
        <th>Moneda</th>
        <th>Otra Moneda</th>
    </tr>
    </thead>
    <tbody>
    @foreach($ingresos as $ingreso)
        <tr>
            <td>{{$ingreso->tipo_ingreso->descripcion or ''}}</td>
            <td>{{$ingreso->otro_tipo_ingreso or ''}}</td>
            <td>{{$ingreso->monto_anual_neto or ''}}</td>
            <td>{{$ingreso->moneda->descripcion or ''}}</td>
            <td>{{$ingreso->otro_tipo_moneda or ''}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
