<table class="table table-striped table-bordered">
    <caption>Dinero en efectivo</caption>
    <thead>
    <tr>
        <th>Moneda</th>
        <th>Otra Moneda</th>
        <th>Origen de fondos</th>
        <th>Monto</th>
    </tr>
    </thead>
    <tbody>
    @foreach($dineros as $dinero)
        <tr>
            <td>{{$dinero->tipo_moneda->descripcion or ''}}</td>
            <td>{{$dinero->otro_tipo_moneda}}</td>
            <td>{{$dinero->origen_fondo}}</td>
            <td>{{$dinero->monto}}</td>
        </tr>
    @endforeach
    </tbody>
</table>


