<table class="table table-striped table-bordered">
    <caption>Derchos en expectativas {{$titulo or ''}}</caption>
    <thead>
    <tr>
        <th>Tipo</th>
        <th>Fuente</th>
        <th>Cantidad cuotas pagas</th>
        <th>Cantidad cuotas restantes a pagar</th>
    </tr>
    </thead>
    <tbody>
    @foreach($derechos as $derecho)
        <tr>
            <td>{{$derecho->tipo_derecho_expectativa->descripcion or ''}}</td>
            <td>{{$derecho->tipo_fuente->descripcion or ''}}</td>
            <td>{{$derecho->cantidad_cuotas_pagas}}</td>
            <td>{{$derecho->cantidad_cuotas_restantes}}</td>
        </tr>
    @endforeach
    </tbody>

</table>