<table class="table table-striped table-bordered">
    <caption>Cuentas Bancarias en Exterior</caption>
    <thead>
    <tr>
        <th>Banco</th>
        <th>Tipo</th>
        <th>Saldo</th>
        <th>Moneda</th>
        <th>Otra Moneda</th>
    </tr>
    </thead>
    <tbody>
    @foreach($cuentas as $cuenta)
        <tr>
            <td>{{$cuenta->banco}}</td>
            <td>{{$cuenta->tipo_cuenta_bancaria_exterior->descripcion or ''}}</td>
            <td>{{$cuenta->saldo}}</td>
            <td>{{$cuenta->tipo_moneda->descripcion or ''}}</td>
            <td>{{$cuenta->otro_tipo_moneda}}</td>
        </tr>
    @endforeach
</table>

