<table class="table table-striped table-bordered">
    <caption>Inmuebles {{$titulo or ''}}</caption>
    <?php $i = 0; ?>
    @foreach($inmuebles as $inmueble)
        <thead>
        <tr><td colspan="8" class="inmuebles_header_registry"># {{ $i }}</td></tr>
        <tr>
            <th>Tipo</th>
            <th>Total de superficie cubierta</th>
            <th>Total de superficie descubierta</th>
            <th>Unidad</th>
            <th>Carácter</th>
            <th colspan="3">% de titularidad</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$inmueble->tipo_inmueble->descripcion or ''}}</td>
            <td>{{$inmueble->superficie_cubierta}}</td>
            <td>{{$inmueble->superficie_descubierta}}</td>
            <td>{{$inmueble->unidad_superficie->descripcion or ''}}</td>
            <td>{{$inmueble->tipo_caracter->descripcion or ''}}</td>
            <td colspan="3">{{$inmueble->porcentaje}}</td>
        </tr>
        </tbody>
        <thead>
        <tr>
            <th>Fecha de escritura</th>
            <th>Valor de adquisición</th>
            <th>Moneda</th>
            <th>Valor estimado de mercado</th>
            <th>Moneda</th>
            <th>Modo de adquisición</th>
            <th>Forma de pago</th>
            <th>Otra forma de pago</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$inmueble->fecha_escritura}}</td>
            <td>{{$inmueble->valor_adquisicion}}</td>
            <td>{{$inmueble->moneda_valor_adquisicion->descripcion or ''}}</td>
            <td>{{$inmueble->valor_estimado}}</td>
            <td>{{$inmueble->moneda_valor_estimado->descripcion or ''}}</td>
            <td>{{$inmueble->tipo_modo_adquisicion->descripcion or ''}}</td>
            <td>{{$inmueble->tipo_forma_pago->descripcion or ''}}</td>
            <td>{{$inmueble->otra_forma_pago}}</td>
        </tr>
        </tbody>
        <thead>
        <tr>
            <th>País</th>
            <th>Provincia</th>
            <th>Localidad</th>
            <th>Ruta/Avenida/Calle</th>
            <th>Número/KM </th>
            <th>Piso</th>
            <th>Depto</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$inmueble->pais->descripcion or ''}}</td>
            <td>{{$inmueble->provincia->descripcion or $inmueble->otro_pais_provincia}}</td>
            <td>{{$inmueble->localidad->descripcion or $inmueble->otro_pais_localidad}}</td>
            <td>{{$inmueble->domicilio_calle}}</td>
            <td>{{$inmueble->domicilio_numero}}</td>
            <td>{{$inmueble->domicilio_piso}}</td>
            <td>{{$inmueble->domicilio_depto}}</td>
        </tr>
        </tbody>
        <?php $i++;?>
    @endforeach

</table>