<table class="table table-striped table-bordered">
    <caption>Ingreso por fuera de la fuerza</caption>
    <thead>
    <tr>
        <th>Tipo de ingreso</th>
        <th>Otro tipo de ingreso</th>
        <th>Tipo Ingreso Extraordinario</th>
        <th>Otro tipo Ingreso Extraordinario</th>
        <th>Ingreso Otra Actividad</th>
        <th>Categoria tributaria</th>
        <th>Monto Neto Anual Total</th>
        <th>Moneda</th>
        <th>Otra Moneda</th>
    </tr>
    </thead>
    <tbody>
        @foreach($ingresos as $ingreso)
        <tr>
            <td>{{$ingreso->tipo_ingreso->descripcion or ''}}</td>
            <td>{{$ingreso->otro_tipo_ingreso}}</td>
            <td>{{$ingreso->ingreso_extraordinario->descripcion or ''}}</td>
            <td>{{$ingreso->otro_tipo_ingreso_extraordinario}}</td>
            <td>{{$ingreso->ingreso_otra_actividad->descripcion or ''}}</td>
            <td>{{$ingreso->categoria_tributaria->descripcion or ''}}</td>
            <td>{{$ingreso->monto_neto_anual_total}}</td>
            <td>{{$ingreso->moneda->descripcion or ''}}</td>
            <td>{{$ingreso->otro_tipo_moneda}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
