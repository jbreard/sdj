<?php


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

class LocalidadesController extends \BaseController {

    public function getLocalidadesByProvincia()
    {
        $remember = Config::get('app.declaracion_jurada.cache.combos');
        $empty = [0 => ''];
        $data = Input::all();
        $idProvincia =$data['idProvincia'];
        $localidades = Localidad::remember($remember)->where('id_provincia',$idProvincia)->get(['id','descripcion as text'])->toArray();

        return array(
            'data' => array_merge($empty,$localidades)
        );
    }

}
