<?php

use DeclaracionJurada\Managers\MailEnvioMasivo;
use Illuminate\Support\Facades\Input;

class MailController extends \BaseController {

    public function sendMail()
    {
        $data = Input::all();
        $email = array_get($data,'email');
        $idDeclaracionJurada = array_get($data,'hash');

        $mailEnvioMasivo = new MailEnvioMasivo();
        $mailEnvioMasivo->sendMailDeclaracionJurada($email, $idDeclaracionJurada, false);
    }

}