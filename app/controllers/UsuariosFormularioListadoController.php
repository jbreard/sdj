<?php

use DeclaracionJurada\Managers\UsuarioFormularioManager;
use DeclaracionJurada\Repository\ValidacionUsersRepo;
use DeclaracionJurada\Transformers\ValidacionUsersTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class UsuariosFormularioListadoController extends \BaseController {

    private $validacionUsersRepo;
    private $validacionUsersTransformer;

    public function __construct(ValidacionUsersRepo $validacionUsersRepo,
                                ValidacionUsersTransformer $validacionUsersTransformer)
    {
        $this->validacionUsersRepo = $validacionUsersRepo;
        $this->validacionUsersTransformer = $validacionUsersTransformer;
    }

    public function index(){
        $remember = Config::get('app.declaracion_jurada.cache.combos');

        return View::make('listados.usuarios_formulario_busqueda',[
            'combo_fuerzas' => ['' => 'Seleccione Fuerza'] + Fuerza::remember($remember)->whereIn('id',[1,2,3,4])->lists('descripcion','id'),
            'estados' => Config::get('app.usuarios.estados'),
            'tiposFormulario' => [
                'ANUAL'=>'ANUAL',
                'BAJA'=>'BAJA',
                'ALTA'=>'ALTA'
            ],
        ]);
    }


    public function create() {

        $remember = Config::get('app.declaracion_jurada.cache.combos');
        $fuerzas = Fuerza::remember($remember)->whereIn('id',[1,2,3,4])->lists('descripcion', 'id');
        $user = $this->validacionUsersRepo->getModel();

        if (is_null($user)) App::abort(404);

        return View::make("usuariosFormulario.create", array(
            'user' => $user,
            'tiposFormulario' => [
                'ANUAL'=>'ANUAL',
                'BAJA'=>'BAJA',
                'ALTA'=>'ALTA',
            ],
            'fuerzas' => $fuerzas,
            'form_data' => array(
                'route' => 'usuariosFormulario.store',
                'method' => 'POST'
            ),
            'action' => 'Crear'
        ));

    }

    public function store() {
        $data = Input::all();

        $user = $this->validacionUsersRepo->getModel();

        $usuarioFormularioManager = new UsuarioFormularioManager($user, $data);

        if (is_null($user)) App::abort(404);

        if ($usuarioFormularioManager -> isValid($data)) {

            $data['id_usuario'] =  Auth::user()->id;
            $data['backup'] = '';

            $usuarioFormularioManager -> setData($data);

            $usuarioFormularioManager -> save();

            return Redirect::route('usuariosFormulario.listado')->with(
                'success',
                "Se ha creado el usuario: {$user->nombre} {$user->apellido}"
            );

        } else {

            return Redirect::route('usuariosFormulario.create')
                -> withInput()
                -> withErrors($usuarioFormularioManager -> getErrors());

        }
    }

    public function show($id) {
        $remember = Config::get('app.declaracion_jurada.cache.combos');
        $fuerzas = Fuerza::remember($remember)->lists('descripcion', 'id');
        $user = $this->validacionUsersRepo->find($id);

        if (is_null($user)) App::abort(404);

        return View::make("usuariosFormulario.create", array(
            'user' => $user,
            'tiposFormulario' => [
                'ANUAL'=>'ANUAL',
                'BAJA'=>'BAJA',
                'ALTA'=>'ALTA',
            ],
            'fuerzas' => $fuerzas,
            'form_data' => array('route' => array('usuariosFormulario.update', $user->id), 'method' => 'PATCH'),
            'action' => 'Editar'
        ));
    }

    public function update($id) {

        $data = Input::all();

        $user = $this->validacionUsersRepo->find($id);

        $usuarioFormularioManager = new UsuarioFormularioManager($user, $data);

        if (is_null($user)) App::abort(404);

        if ($usuarioFormularioManager -> isValid($data)) {

            $data['id_usuario'] =  Auth::user()->id;
            $data['backup'] = $usuarioFormularioManager->backup();

            $usuarioFormularioManager -> setData($data);

            $usuarioFormularioManager -> save();

            return Redirect::route('usuariosFormulario.listado')->with(
                'success',"Se actualizó el usuario: {$user->nombre} {$user->apellido}"
            );

        } else {

            return Redirect::route('usuariosFormulario.show', $id)
                -> withInput()
                -> withErrors($usuarioFormularioManager -> getErrors());

        }

    }

    public function buscar()
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 600);
        ini_set('set_time_limit', 0);

        $data = $this->validacionUsersTransformer->prepareData();

        $responseType = array_get($data,'responseType',0);
        
        $response = [
            0 => 'getSearch',
            1 => 'getExcel',
        ];

        return call_user_func([$this,$response[$responseType]],$data);

    }

    public function getSearch($data)
    {
        #Para transformar la respuesta si es que se necesita:
        #$paginate = $this->usuarioRepo->getData($data);
        #$response = $paginate->toArray();
        #$response = $this->usuarioListadoTransformer->prepareResponse($response);
        #return $response;

        return $this->validacionUsersRepo->getData($data);
    }

    public function getExcel($data)
    {
        return [];
    }
    

} 
