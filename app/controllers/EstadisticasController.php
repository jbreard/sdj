<?php

use DeclaracionJurada\Repository\ContabilizadorRepo;

class EstadisticasController extends BaseController {


    private $contabilizadorRepo;


    public function __construct()
    {
        $this->contabilizadorRepo = new ContabilizadorRepo();
    }


    public function index()
    {
        $countTotal = $this->contabilizadorRepo->getCountTotal();

        $countEliminadas = $this->contabilizadorRepo->getCountEliminadas();
        $countCreadas = $this->contabilizadorRepo->getCountCreadas();
        $countAceptadas = $this->contabilizadorRepo->getCountAceptadas();
        $countRecibidas = $this->contabilizadorRepo->getCountRecibidas();

        $dataUbicacionByPfa = $this->contabilizadorRepo->getCountUbicacionByIdFuerza(1);
        $dataUbicacionByGna = $this->contabilizadorRepo->getCountUbicacionByIdFuerza(2);
        $dataUbicacionByPna = $this->contabilizadorRepo->getCountUbicacionByIdFuerza(3);
        $dataUbicacionByPsa = $this->contabilizadorRepo->getCountUbicacionByIdFuerza(4);

        $dataInmuebleByPfa = $this->contabilizadorRepo->getCountInmueblesByIdFuerza(1);
        $dataInmuebleByGna = $this->contabilizadorRepo->getCountInmueblesByIdFuerza(2);
        $dataInmuebleByPna = $this->contabilizadorRepo->getCountInmueblesByIdFuerza(3);
        $dataInmuebleByPsa = $this->contabilizadorRepo->getCountInmueblesByIdFuerza(4);

        #echo "<pre>" . print_r($dataInmuebleByPfa,true) . "</pre>";
        #dd();
        $data = [
            'count_eliminadas' => $countEliminadas[0]->count
            ,'count_creadas' => $countCreadas[0]->count
            ,'count_aceptadas' => $countAceptadas[0]->count
            ,'count_recibidas' => $countRecibidas[0]->count
            ,'count_total' => $countTotal[0]->count
            ,'ubicacion_pfa_data' => $dataUbicacionByPfa
            ,'ubicacion_gna_data' => $dataUbicacionByGna
            ,'ubicacion_pna_data' => $dataUbicacionByPna
            ,'ubicacion_psa_data' => $dataUbicacionByPsa

            ,'inmueble_pfa_data' => $dataInmuebleByPfa
            ,'inmueble_gna_data' => $dataInmuebleByGna
            ,'inmueble_pna_data' => $dataInmuebleByPna
            ,'inmueble_psa_data' => $dataInmuebleByPsa
        ];

        return View::make('estadisticas.tablero',$data);
    }

}
