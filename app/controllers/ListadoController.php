<?php

use DeclaracionJurada\Managers\ReporteManager;
use DeclaracionJurada\Repository\DeclaracionJuradaRepo;
use DeclaracionJurada\Transformers\ListadoTransformer;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;


/**
 * Created by PhpStorm.
 * User: damian
 * Date: 30/06/16
 * Time: 14:01
 */

class ListadoController extends \BaseController {

    private $declaracionJuradaRepo;
    private $listadoTransformer;
    protected $reporteManager;

    public function __construct(DeclaracionJuradaRepo $declaracionJuradaRepo, 
                                ListadoTransformer $listadoTransformer)
    {
        $this->declaracionJuradaRepo = $declaracionJuradaRepo;
        $this->listadoTransformer = $listadoTransformer;
    }

    public function index(){
        $remember = Config::get('app.declaracion_jurada.cache.combos');
        $combo_fuerzas = ['' => 'Seleccione'] + Fuerza::remember($remember)->whereIn('id',[1,2,3,4])->lists('descripcion','id');
        $estados = Config::get('app.declaracion_jurada.estados');

        $comboPersonal = [''=>''] + TipoPersonal::orderBy('descripcion','ASC')->lists('descripcion', 'id');
        $comboGrado = [''=>''] + Grado::orderBy('descripcion','ASC')->lists('descripcion', 'id');


        return View::make('listados.busqueda',compact('combo_fuerzas','estados','comboPersonal','comboGrado'));
    }

    public function buscar()
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 600);
        ini_set('set_time_limit', 0);

        $data = $this->listadoTransformer->prepareData();        

        $responseType = array_get($data,'responseType',0);
        
        $response = [
            0 => 'getSearch',
            1 => 'getExcel',
        ];

        return call_user_func([$this,$response[$responseType]],$data);

    }

    public function getSearch($data)
    {        
        $paginate = $this->declaracionJuradaRepo->getData($data);
        $response = $paginate->toArray();
        $response['data'] = $this->listadoTransformer->prepareResponse($response['data']);

//        dd($response);
        return $response;
    }

    public function getExcel($data)
    {
        $idFuerza = array_get($data, 'id_fuerza');
        $fuerza = Fuerza::find($idFuerza);
        $descripcionFuerza = $fuerza->descripcion;

        $now = Carbon::now();
        $fileType = 'xlsx';
        $hashRand = md5(rand(0, 99) . time());
        $fileName = str_replace(' ','_',($descripcionFuerza . "_" . $now . "_" . $hashRand));
        $publicPath = public_path('assets/excel/');
        $pathResponse = asset('assets/excel') . "/" . $fileName . "." . $fileType;
        $pathSave = $publicPath . $fileName . "." . $fileType;

        $response = $this->declaracionJuradaRepo->getData($data, 'excel');
        $processedData = $this->listadoTransformer->prepareResponse($response);

        $reporte = new Reporte();
        $reporteManager = new ReporteManager($reporte, []);
        $reporteManager->getReport($processedData, $pathSave);
        
        return $pathResponse;
    }
    

} 