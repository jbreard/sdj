<?php
use DeclaracionJurada\Managers\MailEnvioMasivo;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;

class UsuariosController extends BaseController {

	protected $perfiles;
	protected $exceptPerfiles = [1,2];//Administrador y Operador
	protected $fuerzas;

	/**
	 * UsuariosController constructor.
	 */
	public function __construct()
	{
		$remember = Config::get('app.declaracion_jurada.cache.combos');
		$this->perfiles = array_except(Perfil::remember($remember) -> lists('descripcion', 'id'), $this->exceptPerfiles);
		$this->fuerzas = Fuerza::remember($remember)->whereIn('id',[1,2,3,4]) -> lists('descripcion', 'id');
	}

	public function index() {
		return Redirect::route('usuarios.listado');
	}

	public function create() {

		$user = new User;

		return View::make("usuarios.create", array(
			'user' => $user,
			'perfiles' => $this->perfiles,
			'fuerzas' => $this->fuerzas,
			)
		);
	}

	public function show($id) {

		$remember = Config::get('app.declaracion_jurada.cache.combos');
		$fuerzas = Fuerza::remember($remember)->lists('descripcion', 'id');
		$user = User::find($id);

		if (is_null($user)) {
			App::abort(404);
		}

		return View::make("usuarios.create", array(
			'user' => $user,
			'perfiles' => $this->perfiles,
			'fuerzas' => $fuerzas,
		));

	}

	public function update($id) {


		$user = User::find($id);

		if (is_null($user)) {
			App::abort(404);
		}

		// Obtenemos la data enviada por el usuario
		$data = Input::all();
		$sendMail = false;


		// Revisamos si la data es válido
		if ($user -> isValid($data)) {

			if(!empty($data['password_new'])) {
				$data['password'] = Hash::make($data['password_new']);
				$sendMail = true;
			}

			// Si la data es valida se la asignamos al usuario
			$user -> fill($data);
			// Guardamos el usuario
			$user -> save();


			if($sendMail) {
				$email = (empty($data['email'])) ? $user->email : $data['email'];
				$mailMasivo = new MailEnvioMasivo();
				$mailMasivo->sendMailUser($email, $data['username'], $data['password_new']);
			}

			// Y Devolvemos una redirección a la acción show para mostrar el usuario
			return Redirect::route('usuarios.show', array($user -> id))->with('success', 'Se actualizó el usuario: ' . $user->username);;
		} else {
			// En caso de error regresa a la acción edit con los datos y los errores encontrados
			return Redirect::route('usuarios.show', $user -> id) -> withInput() -> withErrors($user -> errors);
		}

	}

	public function validarDni() {

		if (Request::ajax()) {// it's an ajax request

			$input = Input::all();
			$usuarios = DB::table('usuarios') -> where('dni', '=', $input['dni']) -> get();

			if (count($usuarios)) {
				$response['respuesta'] = true;
			} else {
				$response['respuesta'] = false;
			}

			return json_encode((object)$response);
		}

	}

	public function store() {

		$user = new User();
		$data = Input::all();
		$isValid = $user -> isValid($data);

		if ($isValid) {

			if(!empty($data['password_new'])) {
				$data['password'] = Hash::make($data['password_new']);
			} else {
				$data['password'] = Hash::make($data['email']);
			}

			$data['remember_token'] = '';

			$user -> fill($data);
			$user -> save();

			$pass = (!empty($data['password_new'])) ? $data['password_new']: $data['email'];
			$mailMasivo = new MailEnvioMasivo();
			$mailMasivo->sendMailUser($data['email'], $data['username'], $pass);

			return Redirect::route('usuarios.index')->with('success', 'Se creo el usuario: ' . $user->username);

		} else {
			return Redirect::route('usuarios.create') -> with('user', $user) -> withInput() -> withErrors($user -> errors);
		}

	}


	public function comprobarUsuario() {
		if (Request::ajax()) {// it's an ajax request
			echo Auth::user() -> testigo;
		}
	}

	public function getCambiarClave() {

		return View::Make("usuarios.cambiar_clave") -> with("clave", null);
	}

	public function postCambiarClave() {

		$user = new User;
		$data = Input::all();

		// Revisamos si la data es válido
		if ($user -> isValidClave($data)) {

			$user = $user -> find(Auth::user() -> id);
			$clave_old = $data['clave_old'];
			$clave_nueva = $data['clave_nueva'];

			if (Hash::check($clave_old, $user -> getAuthPassword())) {
				$user -> password = Hash::make($clave_nueva);
				if ($user -> save()) {
					return Redirect::to('usuarios') -> with('global', 'Se cambio tu clave');
				}
			} else {
				return View::Make("usuarios.cambiar_clave") -> with("clave", null) -> with("mensa", "La Clave No Es Correcta");
			}
		} else {
			// En caso de error regresa a la acción create con los datos y los errores encontrados

			return Redirect::to('usuarios/clave') -> with('user', $user) -> withInput() -> withErrors($user -> errors);
		}

	}

	public function destroy($id)
	{
		$user = User::find($id);
		$user->activo = 0;
		$user->save();

		return Redirect::route('usuarios.listado')->with('success', 'Se desactivo el usuario: ' . $user->username) -> withInput();
	}

	public function enable($id)
	{
		$user = User::find($id);
		$user->activo = 1;
		$user->save();

		return Redirect::route('usuarios.listado')->with('success', 'Se activo el usuario: ' . $user->username) -> withInput();
	}

	

}
?>
