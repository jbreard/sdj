<?php


use Carbon\Carbon;
use DeclaracionJurada\Managers\DeclaracionJuradaManager;
use DeclaracionJurada\Managers\NotificationManager;
use DeclaracionJurada\Managers\PersonaManager;
use DeclaracionJurada\Managers\ResponseManager;
use DeclaracionJurada\Repository\DeclaracionJuradaRepo;
use DeclaracionJurada\Repository\PersonaRepo;
use DeclaracionJurada\Repository\SelectOptionsRepo;
use DeclaracionJurada\Transformers\CaracterDeclaracionTransformer;
use DeclaracionJurada\Transformers\DeclaracionJuradaTransformer;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Debug\Exception\FatalErrorException;


class DeclaracionJuradaController extends \BaseController
{

    protected $dataInput;
    protected $datosConyuge = [];
    protected $datosHijo = [];
    protected $datosOtraPersona = [];
    protected $datosDeclaracionJurada;
    protected $datosCuentasBancarias = [];
    protected $datosCuentasBancariasExterior = [];
    protected $datosTarjetasCredito = [];
    protected $datosDeudas;
    protected $datosDeudaPropios = [];
    protected $datosDeudaConyuge = [];
    protected $datosDeudaHijosMenores = [];
    protected $datosIngresos;
    protected $datosIngresosFuerza;
    protected $datosIngresosAdicional;
    protected $datosIngresosOtros;
    protected $datosIngresosExtraordinarios = [];
    protected $datosInmuebles;
    protected $datosInmueblesPropios = [];
    protected $datosInmueblesConyuge = [];
    protected $datosInmueblesHijosMenores = [];
    protected $datosVehiculos;
    protected $datosVehiculosPropios = [];
    protected $datosVehiculosConyuge = [];
    protected $datosVehiculosHijosMenores = [];
    protected $datosBienes;
    protected $datosBienesPropios = [];
    protected $datosBienesConyuge = [];
    protected $datosBienesHijosMenores = [];
    protected $datosDerechoExpectativa;
    protected $datosDerechoExpectativaPropios = [];
    protected $datosDerechoExpectativaConyuge = [];
    protected $datosDerechoExpectativaHijosMenores = [];
    protected $datosIngresosExternos = [];
    protected $datosBonosTitulosAcciones = [];
    protected $datosDineroEfectivo = [];
    protected $datosAcreencias = [];
    protected $datosFamiliares;
    protected $conyuges;
    protected $hijos;
    protected $otrasPersonas;
    protected $idPersona;
    protected $idDeclaracionJurada;
    protected $declaracionJuradaRepo;
    protected $errorMessages = [];
    protected $selectOptionsRepo;
    protected $declaracionJuradaTransformer;
    protected $responseManager;
    protected $notifactionManager;
    protected $caracterDeclaracionTransformer;
    protected $convivientesFuerza;
    protected $datosPadreMadre = [];
    protected $datosConvivienteFuerza = [];


    public function __construct(DeclaracionJuradaRepo $declaracionJuradaRepo,
                                SelectOptionsRepo $selectOptionsRepo,
                                ResponseManager $responseManager,
                                NotificationManager $notifactionManager,
                                DeclaracionJuradaTransformer $declaracionJuradaTransformer,
                                CaracterDeclaracionTransformer $caracterDeclaracionTransformer)
    {
        $this->declaracionJuradaRepo = $declaracionJuradaRepo;
        $this->selectOptionsRepo = $selectOptionsRepo;
        $this->declaracionJuradaTransformer = $declaracionJuradaTransformer;
        $this->responseManager = $responseManager;
        $this->notifactionManager = $notifactionManager;
        $this->caracterDeclaracionTransformer = $caracterDeclaracionTransformer;
    }

    public function getDatosIngresosFuerza()
    {
        return $this->datosIngresosFuerza;
    }

    public function setDatosIngresosFuerza($datosIngresosFuerza)
    {
        $this->datosIngresosFuerza = $datosIngresosFuerza;
    }

    public function getDatosIngresosExtraordinarios()
    {
        return $this->datosIngresosExtraordinarios;
    }

    public function setDatosIngresosExtraordinarios($datosIngresosExtraordinarios)
    {
        $this->datosIngresosExtraordinarios = $datosIngresosExtraordinarios;
    }

    public function getDatosIngresosAdicional()
    {
        return $this->datosIngresosAdicional;
    }

    public function setDatosIngresosAdicional($datosIngresosAdicional)
    {
        $this->datosIngresosAdicional = $datosIngresosAdicional;
    }

    public function getDatosIngresosOtros()
    {
        return $this->datosIngresosOtros;
    }

    public function setDatosIngresosOtros($datosIngresosOtros)
    {
        $this->datosIngresosOtros = $datosIngresosOtros;
    }

    public function getDatosVehiculosPropios()
    {
        return $this->datosVehiculosPropios;
    }

    public function setDatosVehiculosPropios($datosVehiculosPropios)
    {
        $this->datosVehiculosPropios = $datosVehiculosPropios;
    }

    public function addDatosVehiculosPropios($datosVehiculosPropios)
    {
        $this->datosVehiculosPropios = array_merge($this->datosVehiculosPropios, $datosVehiculosPropios);
    }

    public function getDatosVehiculosConyuge()
    {
        return $this->datosVehiculosConyuge;
    }

    public function setDatosVehiculosConyuge($datosVehiculosConyuge)
    {
        $this->datosVehiculosConyuge = $datosVehiculosConyuge;
    }

    public function addDatosVehiculosConyuge($datosVehiculosConyuge)
    {
        $this->datosVehiculosConyuge = array_merge($this->datosVehiculosConyuge, $datosVehiculosConyuge);
    }

    public function getDatosVehiculosHijosMenores()
    {
        return $this->datosVehiculosHijosMenores;
    }

    public function setDatosVehiculosHijosMenores($datosVehiculosHijosMenores)
    {
        $this->datosVehiculosHijosMenores = $datosVehiculosHijosMenores;
    }

    public function addDatosVehiculosHijosMenores($datosVehiculosHijosMenores)
    {
        $this->datosVehiculosHijosMenores = array_merge($this->datosVehiculosHijosMenores, $datosVehiculosHijosMenores);
    }

    public function getDatosBienesPropios()
    {
        return $this->datosBienesPropios;
    }

    public function setDatosBienesPropios($datosBienesPropios)
    {
        $this->datosBienesPropios = $datosBienesPropios;
    }

    public function addDatosBienesPropios($datosBienesPropios)
    {
        $this->datosBienesPropios = array_merge($this->datosBienesPropios, $datosBienesPropios);
    }

    public function getDatosBienesConyuge()
    {
        return $this->datosBienesConyuge;
    }

    public function setDatosBienesConyuge($datosBienesConyuge)
    {
        $this->datosBienesConyuge = $datosBienesConyuge;
    }

    public function addDatosBienesConyuge($datosBienesConyuge)
    {
        $this->datosBienesConyuge = array_merge($this->datosBienesConyuge, $datosBienesConyuge);
    }

    public function getDatosBienesHijosMenores()
    {
        return $this->datosBienesHijosMenores;
    }

    public function setDatosBienesHijosMenores($datosBienesHijosMenores)
    {
        $this->datosBienesHijosMenores = $datosBienesHijosMenores;
    }

    public function addDatosBienesHijosMenores($datosBienesHijosMenores)
    {
        $this->datosBienesHijosMenores = array_merge($this->datosBienesHijosMenores, $datosBienesHijosMenores);
    }

    public function getDatosInmueblesPropios()
    {
        return $this->datosInmueblesPropios;
    }

    public function setDatosInmueblesPropios($datosInmueblesPropios)
    {
        $this->datosInmueblesPropios = $datosInmueblesPropios;
    }

    public function addDatosInmueblesPropios($datosInmueblesPropios)
    {
        $this->datosInmueblesPropios = array_merge($this->datosInmueblesPropios, $datosInmueblesPropios);
    }

    public function getDatosInmueblesConyuge()
    {
        return $this->datosInmueblesConyuge;
    }

    public function setDatosInmueblesConyuge($datosInmueblesConyuge)
    {
        $this->datosInmueblesConyuge = $datosInmueblesConyuge;
    }

    public function addDatosInmueblesConyuge($datosInmueblesConyuge)
    {
        $this->datosInmueblesConyuge = array_merge($this->datosInmueblesConyuge, $datosInmueblesConyuge);
    }

    public function getDatosInmueblesHijosMenores()
    {
        return $this->datosInmueblesHijosMenores;
    }

    public function setDatosInmueblesHijosMenores($datosInmueblesHijosMenores)
    {
        $this->datosInmueblesHijosMenores = $datosInmueblesHijosMenores;
    }

    public function addDatosInmueblesHijosMenores($datosInmueblesHijosMenores)
    {
        $this->datosInmueblesHijosMenores = array_merge($this->datosInmueblesHijosMenores, $datosInmueblesHijosMenores);
    }

    public function getDatosDerechoExpectativaPropios()
    {
        return $this->datosDerechoExpectativaPropios;
    }

    public function setDatosDerechoExpectativaPropios($datosDerechoExpectativaPropios)
    {
        $this->datosDerechoExpectativaPropios = $datosDerechoExpectativaPropios;
    }

    public function addDatosDerechoExpectativaPropios($datosDerechoExpectativaPropios)
    {
        $this->datosDerechoExpectativaPropios = array_merge($this->datosDerechoExpectativaPropios, $datosDerechoExpectativaPropios);
    }

    public function getDatosDerechoExpectativaConyuge()
    {
        return $this->datosDerechoExpectativaConyuge;
    }

    public function setDatosDerechoExpectativaConyuge($datosDerechoExpectativaConyuge)
    {
        $this->datosDerechoExpectativaConyuge = $datosDerechoExpectativaConyuge;
    }

    public function addDatosDerechoExpectativaConyuge($datosDerechoExpectativaConyuge)
    {
        $this->datosDerechoExpectativaConyuge = array_merge($this->datosDerechoExpectativaConyuge, $datosDerechoExpectativaConyuge);
    }

    public function getDatosDerechoExpectativaHijosMenores()
    {
        return $this->datosDerechoExpectativaHijosMenores;
    }

    public function setDatosDerechoExpectativaHijosMenores($datosDerechoExpectativaHijosMenores)
    {
        $this->datosDerechoExpectativaHijosMenores = $datosDerechoExpectativaHijosMenores;
    }

    public function addDatosDerechoExpectativaHijosMenores($datosDerechoExpectativaHijosMenores)
    {
        $this->datosDerechoExpectativaHijosMenores = array_merge($this->datosDerechoExpectativaHijosMenores, $datosDerechoExpectativaHijosMenores);
    }

    public function getDatosDeudaPropios()
    {
        return $this->datosDeudaPropios;
    }

    public function setDatosDeudaPropios($datosDeudaPropios)
    {
        $this->datosDeudaPropios = $datosDeudaPropios;
    }

    public function addDatosDeudaPropios($datosDeudaPropios)
    {
        $this->datosDeudaPropios = array_merge($this->datosDeudaPropios, $datosDeudaPropios);
    }

    public function getDatosDeudaConyuge()
    {
        return $this->datosDeudaConyuge;
    }

    public function setDatosDeudaConyuge($datosDeudaConyuge)
    {
        $this->datosDeudaConyuge = $datosDeudaConyuge;
    }

    public function addDatosDeudaConyuge($datosDeudaConyuge)
    {
        $this->datosDeudaConyuge = array_merge($this->datosDeudaConyuge, $datosDeudaConyuge);
    }

    public function getDatosDeudaHijosMenores()
    {
        return $this->datosDeudaHijosMenores;
    }

    public function setDatosDeudaHijosMenores($datosDeudaHijosMenores)
    {
        $this->datosDeudaHijosMenores = $datosDeudaHijosMenores;
    }

    public function addDatosDeudaHijosMenores($datosDeudaHijosMenores)
    {
        $this->datosDeudaHijosMenores = array_merge($this->datosDeudaHijosMenores, $datosDeudaHijosMenores);
    }

    public function getDatosConyuge()
    {
        return $this->datosConyuge;
    }

    public function setDatosConyuge($datosConyuge)
    {
        $this->datosConyuge = $datosConyuge;
    }

    public function addDatosConyuge($datosConyuge)
    {
        $this->datosConyuge = array_merge($this->datosConyuge, $datosConyuge);
    }

    public function getDatosConvivienteFuerza()
    {
        return $this->datosConvivienteFuerza;
    }

    public function setDatosConvivienteFuerza($datosConvivienteFuerza)
    {
        $this->datosConvivienteFuerza = $datosConvivienteFuerza;
    }

    public function addDatosConvivienteFuerza($datosConvivienteFuerza)
    {
        $this->datosConvivienteFuerza = array_merge($this->datosConvivienteFuerza, $datosConvivienteFuerza);
    }

    public function getDatosPadreMadre()
    {
        return $this->datosPadreMadre;
    }

    public function setDatosPadreMadre($datosPadreMadre)
    {
        $this->datosPadreMadre = $datosPadreMadre;
    }

    public function addDatosPadreMadre($datosPadreMadre)
    {
        $this->datosPadreMadre = array_merge($this->datosPadreMadre, $datosPadreMadre);
    }

    public function getDatosInmuebles()
    {
        return $this->datosInmuebles;
    }

    public function setDatosInmuebles($datosInmuebles)
    {
        $this->datosInmuebles = $datosInmuebles;
    }

    public function getDataInput()
    {
        return $this->dataInput;
    }

    public function setDataInput($dataInput)
    {
        $this->dataInput = $dataInput;
    }

    public function getDatosFamiliares()
    {
        return $this->datosFamiliares;
    }

    public function setDatosFamiliares($datosFamiliares)
    {
        $this->datosFamiliares = $datosFamiliares;
    }

    private function getSelectOptions()
    {
        return $this->selectOptionsRepo->getSelectOptions();
    }

    private function setIdPersona($id)
    {
        $this->idPersona = $id;
    }

    private function addErrorMessages($errorMessages)
    {
        $this->errorMessages = array_merge($this->errorMessages, $errorMessages);
    }

    private function getErrorMessages()
    {
        return $this->errorMessages;
    }

    private function getIdPersona()
    {
        return $this->idPersona;
    }

    private function setIdDeclaracionJurada($id)
    {
        $this->idDeclaracionJurada = $id;
    }

    private function getIdDeclaracionJurada()
    {
        return $this->idDeclaracionJurada;
    }

    public function getDatosDeclaracionJurada()
    {
        return $this->datosDeclaracionJurada;
    }

    public function setDatosDeclaracionJurada($datosDeclaracionJurada)
    {
        $this->datosDeclaracionJurada = $datosDeclaracionJurada;
    }

    public function getDatosHijo()
    {
        return $this->datosHijo;
    }

    public function setDatosHijo($datosHijo)
    {
        $this->datosHijo = $datosHijo;
    }

    public function addDatosHijo($datosHijo)
    {
        $this->datosHijo = array_merge($this->datosHijo, $datosHijo);
    }

    public function getDatosCuentasBancarias()
    {
        return $this->datosCuentasBancarias;
    }

    public function setDatosCuentasBancarias($datosCuentasBancarias)
    {
        $this->datosCuentasBancarias = $datosCuentasBancarias;
    }

    public function addDatosCuentasBancarias($datosCuentasBancarias)
    {
        $this->datosCuentasBancarias = array_merge($this->datosCuentasBancarias, $datosCuentasBancarias);
    }

    public function getDatosBienes()
    {
        return $this->datosBienes;
    }

    public function setDatosBienes($datosBienes)
    {
        $this->datosBienes = $datosBienes;
    }

    public function getDatosTarjetasCredito()
    {
        return $this->datosTarjetasCredito;
    }

    public function setDatosTarjetasCredito($datosTarjetasCredito)
    {
        $this->datosTarjetasCredito = $datosTarjetasCredito;
    }
    public function getDatosBonosTitulosAcciones()
    {
        return $this->datosBonosTitulosAcciones;
    }

    public function setDatosBonosTitulosAcciones($datosBonosTitulosAcciones)
    {
        $this->datosBonosTitulosAcciones = $datosBonosTitulosAcciones;
    }
    public function getDatosDerechoExpectativa()
    {
        return $this->datosDerechoExpectativa;
    }

    public function setDatosDerechoExpectativa($datosDerechoExpectativa)
    {
        $this->datosDerechoExpectativa = $datosDerechoExpectativa;
    }

    public function getDatosDineroEfectivo()
    {
        return $this->datosDineroEfectivo;
    }

    public function setDatosDineroEfectivo($datosDineroEfectivo)
    {
        $this->datosDineroEfectivo = $datosDineroEfectivo;
    }

    public function getDatosAcreencias()
    {
        return $this->datosAcreencias;
    }

    public function setDatosAcreencias($datosAcreencias)
    {
        $this->datosAcreencias = $datosAcreencias;
    }

    public function addDatosTarjetasCredito($datosTarjetasCredito)
    {
        $this->datosTarjetasCredito = array_merge($this->datosTarjetasCredito, $datosTarjetasCredito);
    }

    public function addDatosIngresoExterno($datosIngesosExternos)
    {
        $this->datosIngresosExternos = array_merge($this->datosIngresosExternos, $datosIngesosExternos);
    }

    public function addDatosIngresoExtraordinario($datosIngesosExtraordinario)
    {
        $this->datosIngresosExtraordinarios = array_merge($this->datosIngresosExtraordinarios, $datosIngesosExtraordinario);
    }

    public function getDatosDeudas()
    {
        return $this->datosDeudas;
    }

    public function setDatosDeudas($datosDeudas)
    {
        $this->datosDeudas = $datosDeudas;
    }

    public function addDatosDeudas($datosDeudas)
    {
        $this->datosDeudas = array_merge($this->datosDeudas, $datosDeudas);
    }

    public function getDatosIngresos()
    {
        return $this->datosIngresos;
    }

    public function setDatosIngresos($datosIngresos)
    {
        $this->datosIngresos = $datosIngresos;
    }

    public function getDatosIngresosExternos()
    {
        return $this->datosIngresosExternos;
    }

    public function setDatosIngresosExternos($datosIngresosExternos)
    {
        $this->datosIngresosExternos = $datosIngresosExternos;
    }

    public function getDatosVehiculos()
    {
        return $this->datosVehiculos;
    }

    public function setDatosVehiculos($datosVehiculos)
    {
        $this->datosVehiculos = $datosVehiculos;
    }

    public function getDatosCuentasBancariasExterior()
    {
        return $this->datosCuentasBancariasExterior;
    }

    public function setDatosCuentasBancariasExterior($datosCuentasBancariasExterior)
    {
        $this->datosCuentasBancariasExterior = $datosCuentasBancariasExterior;
    }

    public function addDatosCuentasBancariasExterior($datosCuentasBancariasExterior)
    {
        $this->datosCuentasBancariasExterior = array_merge($this->datosCuentasBancariasExterior, $datosCuentasBancariasExterior);
    }

    public function addDatosBonosTitulosAcciones($datosBonosTitulosAcciones)
    {
        $this->datosBonosTitulosAcciones = array_merge($this->datosBonosTitulosAcciones, $datosBonosTitulosAcciones);
    }

    public function addDatosDineroEfectivo($datosDineroEfectivo)
    {
        $this->datosDineroEfectivo = array_merge($this->datosDineroEfectivo, $datosDineroEfectivo);
    }

    public function addDatosAcreencias($datosAcreencias)
    {
        $this->datosAcreencias = array_merge($this->datosAcreencias, $datosAcreencias);
    }

    public function getDatosOtraPersona()
    {
        return $this->datosOtraPersona;
    }

    public function setDatosOtraPersona($datosOtraPersona)
    {
        $this->datosOtraPersona = $datosOtraPersona;
    }

    public function addDatosOtraPersona($datosOtraPersona)
    {
        $this->datosOtraPersona = array_merge($this->datosOtraPersona, $datosOtraPersona);
    }

    public function getRegistry()
    {
        $data = Input::all();
        $template = array_get($data,'template');
        $view = "formularios.declaracion_jurada.modulos.{$template}";
        $datos = $this->getSelectOptions();
        $datos["declaracion_jurada"] = null;
        $datos["i"] = array_get($data,'count');
        return View::make($view)->with($datos);
    }

    private function setDataForStore()
    {
        $data = $this->getDataInput();

        $this->setDatosDeclaracionJurada(array(
            'id_persona' => $this->getIdPersona(),
            'id_declaracion_jurada' => $this->getIdDeclaracionJurada()
        ));

        $datosConyuge = array_get($data, 'datos_conyuge');
        $this->setDatosConyuge($datosConyuge);

        $datosConvivienteFuerza = array_get($data, 'datos_conviviente_fuerza');
        $this->setDatosConvivienteFuerza($datosConvivienteFuerza);

        $datosPadreMadre = array_get($data, 'datos_padre_madre');
        $this->setDatosPadreMadre($datosPadreMadre);

        $datosHijo = array_get($data, 'datos_hijo');
        $this->setDatosHijo($datosHijo);

        $datosOtraPersona = array_get($data, 'datos_otra_persona');
        $this->setDatosOtraPersona($datosOtraPersona);

        $datosCuentasBancarias = array_get($data, 'cuentas_bancarias');
        $this->setDatosCuentasBancarias($datosCuentasBancarias);

        $datosIngresosExtraordinarios = array_get($data, 'ingresos_extraordinarios');
        $this->setDatosIngresosExtraordinarios($datosIngresosExtraordinarios);

        $datosCuentasBancariasExterior = array_get($data, 'cuentas_bancarias_exterior');
        $this->setDatosCuentasBancariasExterior($datosCuentasBancariasExterior);

        $datosBonosTitulosAcciones = array_get($data, 'bonos_titulos_acciones');
        $this->setDatosBonosTitulosAcciones($datosBonosTitulosAcciones);

        $datosDerechosExpectativa = array_get($data, 'derecho_expectativa');
        $this->setDatosDerechoExpectativa($datosDerechosExpectativa);

        $datosDineroEfectivo = array_get($data, 'dinero_efectivo');
        $this->setDatosDineroEfectivo($datosDineroEfectivo);

        $datosAcreencias = array_get($data, 'acreencias');
        $this->setDatosAcreencias($datosAcreencias);

        $datosTarjetasCredito = array_get($data, 'tarjetas_credito');
        $this->setDatosTarjetasCredito($datosTarjetasCredito);

        $datosDeudas = array_get($data, 'deudas');
        $this->setDatosDeudas($datosDeudas);

        $datosIngresos = array_get($data, 'ingresos');
        $this->setDatosIngresos($datosIngresos);

        $datosIngresosExternos = array_get($data, 'ingresos_externos');
        $this->setDatosIngresosExternos($datosIngresosExternos);

        $datosInmuebles = array_get($data, 'inmuebles');
        $this->setDatosInmuebles($datosInmuebles);

        $datosVehiculos = array_get($data, 'vehiculos');
        $this->setDatosVehiculos($datosVehiculos);

        $datosBienes = array_get($data, 'bienes');
        $this->setDatosBienes($datosBienes);

        return $this;
    }

    public function create()
    {
        $datosDeclaracionJurada = $this->declaracionJuradaTransformer->prepareFormDataForCreate();

        /**
         * TODO
         * AGREGAR MODULO DE SEGURIDAD A ESTA PARTE
         * YA QUE NO VALIDA NINGUN TOKEN, HASH
         * Y SI VIENE POR 'GET' EL PARAMETRO, PASA DIRECTAMENTE AL FORMULARIO
         * */

        $accessFormData = Input::get('accessFormData',[]);

        if(empty($accessFormData)) return Redirect::to('accessForm');

        $datosCaraterDeclaracion = $this->caracterDeclaracionTransformer->prepareDataByAccessForm($accessFormData);

       // dd($datosCaraterDeclaracion );

        $datos = array_merge($datosDeclaracionJurada, $datosCaraterDeclaracion);

        return View::make('formularios.declaracion_jurada.create', $datos);
    }

    public function store()
    {
        $dataInput = Input::all();

        if(!Config::get('app.debug')){
            if(!$this->isValidReCaptcha($dataInput)) return $this->responseManager->responseWithErrors([],$this->getErrorMessages());
        }

        /*
         * Inicio Proceso Carga Primaria
        */

        $this->setDataInput($dataInput);

        if (!$this->createPersona()) return $this->responseManager->responseWithErrors([],$this->getErrorMessages());

        $this->createDeclaracionJurada();
        $this->setDataForStore();


        /**
         * TODO Si el caracter de la declaracion jurada es baja no se continua con el proceso.
         */

        $this->createFamiliares();

        /*
         * Inicio Proceso Carga Secundaria
        */

        $this->createInmueble();
        $this->createVehiculo();
        $this->createCuentaBancaria();
        $this->createCuentaBancariaExterior();
        $this->createBonosTitulosAcciones();
        $this->createDerechosExpectativa();
        $this->createBien();
        $this->createDineroEfectivo();
        $this->createAcreencias();

        if (!$this->createIngreso()) return $this->responseManager->responseWithErrors([],$this->getErrorMessages());

        $this->createIngresosExtraordinarios();
        $this->createIngresoExterno();
        $this->createTarjetaCredito();
        $this->createDeudas();

        /* Fin Proceso Carga */

        $this->sendMail();

        return $this->responseManager->responseOk([
            'url' => route('formulario.declaraciones_juradas.export_pdf', $this->declaracionJuradaTransformer->encrypt($this->getIdDeclaracionJurada())),
        ]);
    }

    public function edit($hash)
    {
        $id = $this->declaracionJuradaTransformer->descrypt($hash);

        if( !is_numeric($id) )
        {
            Log::info("ERROR is_numeric - ID: {$id} HASH: {$hash}");
            return 'Hubo un inconveniente con la declaración jurad a la que intenta acceder.';
        }

        $declaracionJurada = $this->declaracionJuradaRepo->getDeclaracionJuradaById($id);

        if(is_null($declaracionJurada))
        {
            Log::info("ERROR is_null_declaracion_jurada - ID: {$id} HASH: {$hash}");
            return 'Hubo un inconveniente con la declaración jurad a la que intenta acceder.';
        }

        try {
            $data = $declaracionJurada->toArray();
        } catch (FatalErrorException $e) {
            Log::info("HASH-SDJ: {$hash}");
            Log::info($e->getMessage());
            Log::info($e->getTrace());
        }

        $declaracionJuradaManager = new DeclaracionJuradaManager($declaracionJurada, $data);

        //TODO
        // Cuando redirecciona desde aca, luego lo reenvia al
        // accessForm al no enviarle los datos para el acces
        // lo que se debe hacer es enviar un flag o rearmar la logica para que esto no suceda
        if ($declaracionJuradaManager->canEditByTipoEstado()) {
            return Redirect::to('accessForm')->withErrors([
                'redirect' => $this->declaracionJuradaRepo->getMessageRedirect($hash, $declaracionJurada->tipo_estado)
            ]);
        }
        
        $dataForEdit = $this->declaracionJuradaTransformer->prepareFormDataForEdit($declaracionJurada);

        $view = $this->getTemplateForEdit($declaracionJurada['version_formulario']);

        return View::make($view, $dataForEdit);
    }


    public function update($hash)
    {
        $id = $this->declaracionJuradaTransformer->descrypt($hash);
        $dataInput = Input::all();

        if(!Config::get('app.debug')) {
            if (!$this->isValidReCaptcha($dataInput)) return $this->responseManager->responseWithErrors([], $this->getErrorMessages());
        }
        $this->setDataInput($dataInput);
        $this->setIdDeclaracionJurada($id);

        $datosDeclaracionJurada = [
            'id_persona' => $this->declaracionJuradaRepo->getIdPersonaByIdDeclaracionJurada($id),
            'id_declaracion_jurada' => $id
        ];
        
        $this->setDatosDeclaracionJurada($datosDeclaracionJurada);

        if ($this->updatePersona() == false) return $this->responseManager->responseWithErrors([],$this->getErrorMessages());

        $this->updateFamiliares();
        $this->updateInmueble();
        $this->updateVehiculo();
        $this->updateCuentaBancaria();
        $this->updateCuentaBancariaExterior();
        $this->updateBonosTitulosAcciones();
        $this->updateDerechosExpectativa();
        $this->updateBien();
        $this->updateDineroEfectivo();
        $this->updateAcreencias();

        if (!$this->updateIngreso()) return $this->responseManager->responseWithErrors([],$this->getErrorMessages());

        $this->updateIngresoExtraordinario();
        $this->updateIngresoExterno();
        $this->updateTarjetaCredito();
        $this->updateDeudas();

        $version = $this->declaracionJuradaRepo->getVersionByIdDeclaracionJurada($id);
        $incrementVersion = $version + 1;
        $this->updateDeclaracionJurada($id, ['tipo_estado' => 2,'version'=> $incrementVersion]);

        $this->sendMail();

        return $this->responseManager->responseOk([
            'url' => route('formulario.declaraciones_juradas.export_pdf', $this->declaracionJuradaTransformer->encrypt($this->getIdDeclaracionJurada())),
        ]);
    }

    private function createPersona()
    {
        $datosPersonales = array_get($this->getDataInput(), 'datos_personales');

        $personaRepo = new PersonaRepo();
        $personaManager = new PersonaManager($personaRepo->newPersona(), $datosPersonales);

        if ($personaManager->isValid()) {
            
            if($personaManager->isEqualToFakeField())
            {
                $idPersona = $personaManager->save();
                $this->setIdPersona($idPersona);
                return true;    
            }            
            
        }

        $this->addErrorMessages($personaManager->getErrors());

        return false;

    }

    private function createDeclaracionJurada()
    {
        $idPersona = $this->getIdPersona();

        $declaracionJurada = new DeclaracionJurada();

        $datosCaracterDeclaracion = array_get($this->dataInput,'caracter_declaracion');
        $idTipoCaracter = array_get($datosCaracterDeclaracion,'id_tipo_caracter');
        $actoAdministrativo = array_get($datosCaracterDeclaracion,'acto_administrativo');
        $numeroAnio = array_get($datosCaracterDeclaracion,'numero_anio');
        $fechaActoAdministrativo = array_get($datosCaracterDeclaracion,'fecha_acto_administrativo');
        $periodoFiscal = array_get($datosCaracterDeclaracion,'periodo_fiscal');


        $declaracionJurada->fill([
            'id_persona' => $idPersona,
            'id_tipo_caracter' => $idTipoCaracter,
            'id_tipo_acto_administrativo' => $actoAdministrativo,
            'numero_anio' => $numeroAnio,
            'fecha_acto_administrativo' => $fechaActoAdministrativo,
            'periodo_fiscal' => $periodoFiscal,
        ]);

        $declaracionJurada->save();

        $this->setIdDeclaracionJurada($declaracionJurada->id);

        return true;
    }

    private function createFamiliares()
    {
        $persona = new Persona();
        $familiar = new Familiar();
        $idPersona = array_get($this->getDatosDeclaracionJurada(), 'id_persona');

        $this->createConyuges($persona, $familiar, $idPersona);
        $this->createHijos($persona, $familiar, $idPersona);
        $this->createOtrasPersonas($persona, $familiar, $idPersona);
        $this->createConvivientesFuerza($persona, $familiar, $idPersona);
        $this->createPadreMadre($persona, $familiar, $idPersona);
    }

    private function createConyuges(&$persona, &$familiar, $idPersona)
    {
        $this->conyuges = $this->createEachFamiliar($persona, $familiar, $idPersona, $this->getDatosConyuge(), 'apellido', 1);
        return $this;
    }

    private function createPadreMadre(&$persona, &$familiar, $idPersona)
    {
        $this->conyuges = $this->createEachFamiliar($persona, $familiar, $idPersona, $this->getDatosPadreMadre(), 'apellido', 4);
        return $this;
    }

    private function createConvivientesFuerza(&$persona, &$familiar, $idPersona)
    {
        $this->convivientesFuerza = $this->createEachFamiliar($persona, $familiar, $idPersona, $this->getDatosConvivienteFuerza(), 'apellido', 5);
        return $this;
    }

    private function createEachFamiliar(&$persona, &$familiar, $idPersona, $rows, $noEmptyField, $tipoFamiliar)
    {
        $personas = [];
        if (is_array($rows)) {
            $esFamiliar = ['es_familiar' => 1];

            foreach ($rows as $row) {
                $fieldNotEmpty = !empty($row[$noEmptyField]);
                if ($fieldNotEmpty) {
                    $row = array_merge($row, $esFamiliar);
                    $newPersona = $persona->create($row);
                    $idFamiliar = $newPersona->id;
                    $familiar->create([
                        'id_persona' => $idPersona,
                        'id_familiar' => $idFamiliar,
                        'tipo' => $tipoFamiliar
                    ]);
                    array_push($personas, $idFamiliar);
                }
            }
        }

        return $personas;
    }

    private function createHijos(&$persona, &$familiar, $idPersona)
    {
        $this->hijos = $this->createEachFamiliar($persona, $familiar, $idPersona, $this->getDatosHijo(), 'apellido', 2);
        return $this;
    }

    private function createOtrasPersonas(&$persona, &$familiar, $idPersona)
    {
        $this->otrasPersonas = $this->createEachFamiliar($persona, $familiar, $idPersona, $this->getDatosOtraPersona(), 'apellido', 3);
        return $this;
    }

    private function createEachEntity($rows, &$model, $noEmptyField, $tipoPersona = null, $personas = null)
    {
        if (is_array($rows)) {

            $isFullTipoPersona = !empty($tipoPersona);

            if ($isFullTipoPersona) {
                $this->createEntityForPerson($rows, $model, $noEmptyField, $tipoPersona, $personas);
            } else {
                foreach ($rows as $row) {
                    //todo permitir que se pueda chequear varios campos
                    $isFullField = !empty($row[$noEmptyField]);

                    if ($isFullField) {
                        $row = array_merge($row, $this->getDatosDeclaracionJurada());
                        $model->create($row);
                    }
                    #return false;
                }
            }
        }
        #return true;
    }

    private function createCuentaBancaria()
    {
        $cuentaBancaria = new CuentaBancaria();
        $datosCuentasBancarias = $this->getDatosCuentasBancarias();
        $this->createEachEntity($datosCuentasBancarias, $cuentaBancaria, 'banco');

    }

    private function createIngresosExtraordinarios()
    {
        $ingresoExtraordinario = new IngresoExtraordinario();
        $datosIngresosExtraordinarios = $this->getDatosIngresosExtraordinarios();
        $this->createEachEntity($datosIngresosExtraordinarios, $ingresoExtraordinario, 'monto_anual_neto');

    }

    private function createCuentaBancariaExterior()
    {
        $cuentaBancariaExterior = new CuentaBancariaExterior();
        $datosCuentasBancariasExterior = $this->getDatosCuentasBancariasExterior();
        $this->createEachEntity($datosCuentasBancariasExterior, $cuentaBancariaExterior, 'banco');
    }

    private function createBonosTitulosAcciones()
    {
        $bonosTitulosAcciones = new BonosTitulosAcciones();
        $datosBonosTitulosAcciones = $this->getDatosBonosTitulosAcciones();
        $this->createEachEntity($datosBonosTitulosAcciones, $bonosTitulosAcciones, 'descripcion');
    }

    private function createDineroEfectivo()
    {
        $dineroEfectivo = new DineroEfectivo();
        $datosDineroEfectivo = $this->getDatosDineroEfectivo();
        $this->createEachEntity($datosDineroEfectivo, $dineroEfectivo, 'monto');
    }

    private function createAcreencias()
    {
        $acreencia = new Acreencia();
        $datosAcreencias = $this->getDatosAcreencias();
        $this->createEachEntity($datosAcreencias, $acreencia, 'monto');
    }

    private function createTarjetaCredito()
    {
        $tarjetaCredito = new TarjetaCredito();
        $datosTarjetasCredito = $this->getDatosTarjetasCredito();
        $this->createEachEntity($datosTarjetasCredito, $tarjetaCredito, 'banco');
    }

    private function createDeuda()
    {
        #$deuda = new Deuda();
        #$datosDeudas = $this->getDatosDeudas();
        #$this->createEachEntity($datosDeudas, $deuda, 'acreedor');
    }

    private function createIngresoExterno()
    {
        $ingreso = new IngresoExterno();
        $datosIngresosExternos =  $this->getDatosIngresosExternos();
        $this->createEachEntity($datosIngresosExternos, $ingreso, 'monto_neto_anual_total');
    }

    private function createIngreso()
    {
        $datosIngresos = $this->getDatosIngresos();
        $ingreso = new Ingreso();

        $ingresosFuerzas = [array_get($datosIngresos,'fuerza')];

        $this->createEachEntity($ingresosFuerzas, $ingreso, 'monto_anual_neto');

        return true;

    }

    private function createInmueble()
    {
        $inmueble = new Inmueble();
        $datosInmuebles = $this->getDatosInmuebles();

        $inmueblesPropios = array_get($datosInmuebles,'propios');
        $inmueblesConyuge = array_get($datosInmuebles,'conyuge');
        $inmueblesHijosMenores = array_get($datosInmuebles,'hijos_menores');

        $this->createEachEntity($inmueblesPropios, $inmueble, 'id_tipo_inmueble', 1);
        $this->createEachEntity($inmueblesConyuge, $inmueble, 'id_tipo_inmueble', 2, []);
        $this->createEachEntity($inmueblesHijosMenores, $inmueble, 'id_tipo_inmueble', 3, []);
    }

    private function createDeudas()
    {
        $deuda = new Deuda();
        $datosDeudas = $this->getDatosDeudas();
        $deudasPropios = array_get($datosDeudas,'propios');
        $deudasConyuge = array_get($datosDeudas,'conyuge');
        $deudasHijosMenores = array_get($datosDeudas,'hijos_menores');

        $this->createEachEntity($deudasPropios, $deuda, 'acreedor', 1);
        $this->createEachEntity($deudasConyuge, $deuda, 'acreedor', 2, []);
        $this->createEachEntity($deudasHijosMenores, $deuda, 'acreedor', 3, []);
    }

    private function createDerechosExpectativa()
    {
        $derechoExpectativa = new DerechoExpectativa();
        $datosDerechosExpectativa = $this->getDatosDerechoExpectativa();
        $derechosPropios = array_get($datosDerechosExpectativa,'propios');
        $derechosConyuge = array_get($datosDerechosExpectativa,'conyuge');
        $derechosHijosMenores = array_get($datosDerechosExpectativa,'hijos_menores');

        $this->createEachEntity($derechosPropios, $derechoExpectativa, 'id_tipo_derechos_expectativa', 1);
        $this->createEachEntity($derechosConyuge, $derechoExpectativa, 'id_tipo_derechos_expectativa', 2, []);
        $this->createEachEntity($derechosHijosMenores, $derechoExpectativa, 'id_tipo_derechos_expectativa', 3, []);
    }

    private function createVehiculo()
    {
        $vehiculo = new Vehiculo();
        $datosVehiculos = $this->getDatosVehiculos();
        $vehiculosPropios = array_get($datosVehiculos,'propios');
        $vehiculosConyuge = array_get($datosVehiculos,'conyuge');
        $vehiculosHijosMenores = array_get($datosVehiculos,'hijos_menores');
        $this->createEachEntity($vehiculosPropios, $vehiculo, 'marca', 1);
        $this->createEachEntity($vehiculosConyuge, $vehiculo, 'marca', 2, []);
        $this->createEachEntity($vehiculosHijosMenores, $vehiculo, 'marca', 3, []);
    }

    private function createBien()
    {
        $bien = new Bien();
        $datosBienes = $this->getDatosBienes();
        $bienesPropios = array_get($datosBienes,'propios');
        $bienesConyuge = array_get($datosBienes,'conyuge');
        $bienesHijosMenores = array_get($datosBienes,'hijos_menores');
        $this->createEachEntity($bienesPropios, $bien, 'descripcion', 1);
        $this->createEachEntity($bienesConyuge, $bien, 'descripcion', 2, []);
        $this->createEachEntity($bienesHijosMenores, $bien, 'descripcion', 3, []);
    }

    private function updatePersona()
    {
        $idPersona = array_get($this->getDatosDeclaracionJurada(),'id_persona');
        $data = $this->getDataInput();
        $datosPersonales = array_get($data, 'datos_personales');
        $personaRepo = new PersonaRepo();
        $persona = $personaRepo->getModel()->find($idPersona);
        $personaManager = new PersonaManager($persona, $datosPersonales);

        if ($personaManager->isValid()) {

            if($personaManager->isEqualToFakeField())
            {
                $idPersona = $personaManager->save();
                $this->setIdPersona($idPersona);
                return true;
            }

        }

        $this->addErrorMessages($personaManager->getErrors());

        return false;

    }

    private function updateFamiliares()
    {
        array_map([$this,'updateFamiliar'], [
            'datos_conyuge',
            'datos_hijo',
            'datos_otra_persona',
            'datos_conviviente_fuerza',
            'datos_padre_madre'
        ]);

        $this->createFamiliares();
    }

    private function updateFamiliar($type)
    {
        $rows = array_get($this->getDataInput(), $type);


        if (count($rows) > 0) {
            foreach ($rows as $row) {

                $id = $row['id'];
                $notEmpty = !empty($id);
                $isNumeric = is_numeric($id);
                $isForUpdate = $notEmpty && $isNumeric;

                if($isForUpdate)
                {
                    $persona = Persona::find($id);
                    $persona->fill($row);
                    $persona->save();
                } else {
                    switch($type)
                    {
                        case 'datos_conyuge':
                            $this->addDatosConyuge([$row]);
                            break;
                        case 'datos_conviviente_fuerza':
                            $this->addDatosConvivienteFuerza([$row]);
                            break;
                        case 'datos_padre_madre':
                            $this->addDatosPadreMadre([$row]);
                            break;
                        case 'datos_hijo':
                            $this->addDatosHijo([$row]);
                            break;
                        case 'datos_otra_persona':
                            $this->addDatosOtraPersona([$row]);
                            break;
                    }
                }
            }
        }
    }

    private function updateCuentaBancaria()
    {
        $datos = array_get($this->getDataInput(), 'cuentas_bancarias');
        $model = new CuentaBancaria();
        $this->updateEachEntity($datos, $model, 'addDatosCuentasBancarias');
        $this->createCuentaBancaria();
    }

    private function updateCuentaBancariaExterior()
    {
        $datos = array_get($this->getDataInput(), 'cuentas_bancarias_exterior');
        $model = new CuentaBancariaExterior();
        $this->updateEachEntity($datos, $model, 'addDatosCuentasBancariasExterior');
        $this->createCuentaBancariaExterior();
    }

    private function updateBonosTitulosAcciones()
    {
        $datos = array_get($this->getDataInput(), 'bonos_titulos_acciones');
        $model = new BonosTitulosAcciones();

        $this->updateEachEntity($datos, $model, 'addDatosBonosTitulosAcciones');

        $this->createBonosTitulosAcciones();
    }

    private function updateDineroEfectivo()
    {
        $datos = array_get($this->getDataInput(), 'dinero_efectivo');
        $model = new DineroEfectivo();
        $this->updateEachEntity($datos, $model, 'addDatosDineroEfectivo');
        $this->createDineroEfectivo();
    }

    private function updateAcreencias()
    {
        $datos = array_get($this->getDataInput(), 'acreencias');
        $model = new Acreencia();
        $this->updateEachEntity($datos, $model, 'addDatosAcreencias');
        $this->createAcreencias();
    }

    private function updateTarjetaCredito()
    {
        $datos = array_get($this->getDataInput(), 'tarjetas_credito');
        $model = new TarjetaCredito();
        $this->updateEachEntity($datos, $model, 'addDatosTarjetasCredito');
        $this->createTarjetaCredito();
    }

    private function updateIngresoExterno()
    {
        $datos = array_get($this->getDataInput(), 'ingresos_externos');
        $model = new IngresoExterno();
        $this->updateEachEntity($datos, $model, 'addDatosIngresoExterno');
        $this->createIngresoExterno();
    }

    private function updateIngresoExtraordinario()
    {
        $datos = array_get($this->getDataInput(), 'ingresos_extraordinarios');
        $model = new IngresoExtraordinario();
        $this->updateEachEntity($datos, $model, 'addDatosIngresoExtraordinario');
        $this->createIngresosExtraordinarios();
    }

    private function updateDeuda()
    {
        #$datos = array_get($this->getDataInput(), 'deudas');
        #$model = new Deuda();
        #$this->updateEachEntity($datos, $model, 'addDatosDeudas');
        #$this->createDeuda();
    }

    private function updateIngreso()
    {
        $datos = array_get($this->getDataInput(), 'ingresos');
        $model = new Ingreso();

        $ingresosFuerzas = [array_get($datos,'fuerza')];

        $this->updateEachEntity($ingresosFuerzas, $model, 'setDatosIngresosFuerza');

        $this->setDatosIngresos([
            'fuerza' => $this->getDatosIngresosFuerza(),
        ]);
        
        return $this->createIngreso();
    }

    private function updateInmueble()
    {
        $model = new Inmueble();
        $datos = array_get($this->getDataInput(), 'inmuebles');

        if (is_array($datos)) {
            $propios = array_get($datos,'propios');
            $conyuge = array_get($datos,'conyuge');
            $hijosMenores = array_get($datos,'hijos_menores');
            $this->updateEachEntity($propios, $model, 'addDatosInmueblesPropios');
            $this->updateEachEntity($conyuge, $model, 'addDatosInmueblesConyuge');
            $this->updateEachEntity($hijosMenores, $model, 'addDatosInmueblesHijosMenores');

            $this->setDatosInmuebles([
                'propios' => $this->getDatosInmueblesPropios(),
                'conyuge' => $this->getDatosInmueblesConyuge(),
                'hijos_menores' => $this->getDatosInmueblesHijosMenores(),
            ]);
            $this->createInmueble();
        }
    }

    private function updateDerechosExpectativa()
    {
        $model = new DerechoExpectativa();
        $datos = array_get($this->getDataInput(), 'derecho_expectativa');

        if (is_array($datos)) {
            $propios = array_get($datos,'propios');
            $conyuge = array_get($datos,'conyuge');
            $hijosMenores = array_get($datos,'hijos_menores');
            $this->updateEachEntity($propios, $model, 'addDatosDerechoExpectativaPropios');
            $this->updateEachEntity($conyuge, $model, 'addDatosDerechoExpectativaConyuge');
            $this->updateEachEntity($hijosMenores, $model, 'addDatosDerechoExpectativaHijosMenores');

            $this->setDatosDerechoExpectativa([
                'propios' => $this->getDatosDerechoExpectativaPropios(),
                'conyuge' => $this->getDatosDerechoExpectativaConyuge(),
                'hijos_menores' => $this->getDatosDerechoExpectativaHijosMenores(),
            ]);
            $this->createDerechosExpectativa();
        }
    }


    private function updateDeudas()
    {
        $model = new Deuda();
        $datos = array_get($this->getDataInput(), 'deudas');

        if (is_array($datos)) {
            $propios = array_get($datos,'propios');
            $conyuge = array_get($datos,'conyuge');
            $hijosMenores = array_get($datos,'hijos_menores');
            $this->updateEachEntity($propios, $model, 'addDatosDeudaPropios');
            $this->updateEachEntity($conyuge, $model, 'addDatosDeudaConyuge');
            $this->updateEachEntity($hijosMenores, $model, 'addDatosDeudaHijosMenores');

            $this->setDatosDeudas([
                'propios' => $this->getDatosDeudaPropios(),
                'conyuge' => $this->getDatosDeudaConyuge(),
                'hijos_menores' => $this->getDatosDeudaHijosMenores(),
            ]);
            
            $this->createDeudas();
        }
    }

    private function updateVehiculo()
    {
        $model = new Vehiculo();
        $datos = array_get($this->getDataInput(), 'vehiculos');

        if (is_array($datos)) {
            $propios = array_get($datos,'propios');
            $conyuge = array_get($datos,'conyuge');
            $hijosMenores = array_get($datos,'hijos_menores');
            $this->updateEachEntity($propios, $model, 'addDatosVehiculosPropios');
            $this->updateEachEntity($conyuge, $model, 'addDatosVehiculosConyuge');
            $this->updateEachEntity($hijosMenores, $model, 'addDatosVehiculosHijosMenores');

            $this->setDatosVehiculos([
                'propios' => $this->getDatosVehiculosPropios(),
                'conyuge' => $this->getDatosVehiculosConyuge(),
                'hijos_menores' => $this->getDatosVehiculosHijosMenores(),
            ]);
            $this->createVehiculo();
        }
    }

    private function updateBien()
    {
        $model = new Bien();
        $datos = array_get($this->getDataInput(), 'bienes');

        if (is_array($datos)) {
            $propios = array_get($datos,'propios');
            $conyuge = array_get($datos,'conyuge');
            $hijosMenores = array_get($datos,'hijos_menores');
            $this->updateEachEntity($propios, $model, 'addDatosBienesPropios');
            $this->updateEachEntity($conyuge, $model, 'addDatosBienesConyuge');
            $this->updateEachEntity($hijosMenores, $model, 'addDatosBienesHijosMenores');
            $this->setDatosBienes([
                'propios' => $this->getDatosBienesPropios(),
                'conyuge' => $this->getDatosBienesConyuge(),
                'hijos_menores' => $this->getDatosBienesHijosMenores(),
            ]);
            $this->createBien();
        }
    }

    private function updateEachEntity($rows, &$model, $methodSetNewEntity)
    {
        $not = ['setDatosIngresosFuerza','setDatosIngresosAdicional','setDatosIngresosOtros'];
        if (is_array($rows))
        {

            foreach ($rows as $row)
            {
                $id = $row['id'];
                $notEmpty = !empty($id);
                $isNumeric = is_numeric($id);
                $isForUpdate = $notEmpty && $isNumeric;

                if($isForUpdate)
                {
                    $updateModel = $model->find($id);
                    $updateModel->fill($row);
                    $updateModel->save();

                } else {
                    if(in_array($methodSetNewEntity,$not)){
                        $data  = $row;
                    }else{
                        $data = [$row];
                    }
                    call_user_func([$this,$methodSetNewEntity],$data);
                }

            }
        }
    }

    public function exportPdf($hash)
    {
        $id = $this->declaracionJuradaTransformer->descrypt($hash);
        $declaracionJurada = $this->declaracionJuradaRepo->getDeclaracionJuradaById($id);
        $declaracionesJuradas = $this->declaracionJuradaTransformer->prepareDataForShow($declaracionJurada);
        //dd($declaracionesJuradas);

        $result = json_decode(json_encode($declaracionesJuradas));

        $contenido = View::make('solo_lectura.index', array("data" => $result))->render();

        //dd($contenido);

        return PDF::load($contenido, 'A4', 'portrait')->show();
    }

    public function updateAceptada($hash)
    {
        $now = Carbon::now();
        $idDeclaracionJurada = $this->declaracionJuradaTransformer->descrypt($hash);
        $this->updateDeclaracionJurada($idDeclaracionJurada, [
            'tipo_estado' => 4,
            'accepted_at' => $now
        ]);
        return Redirect::back();
    }

    public function updateEntregadoARecursosHumanos($hash)
    {
        $now = Carbon::now();
        $idDeclaracionJurada = $this->declaracionJuradaTransformer->descrypt($hash);
        $this->updateDeclaracionJurada($idDeclaracionJurada, [
            'tipo_estado' => 6,
            'accepted_at' => $now
        ]);
        return Redirect::back();
    }

    public function updateRecibida($hash)
    {
        $now = Carbon::now();
        $idDeclaracionJurada = $this->declaracionJuradaTransformer->descrypt($hash);
        $this->updateDeclaracionJurada($idDeclaracionJurada, [
            'tipo_estado' => 5,
            'received_at' => $now
        ]);
        return Redirect::back();
    }

    public function updateEliminada($hash)
    {
        $now = Carbon::now();
        $idDeclaracionJurada = $this->declaracionJuradaTransformer->descrypt($hash);
        $this->updateDeclaracionJurada($idDeclaracionJurada, [
            'tipo_estado' => 3,
            'deleted_at' => $now
        ]);
        return Redirect::back();
    }

    private function updateDeclaracionJurada($id, $data)
    {
        $declaracionJurada = $this->declaracionJuradaRepo->getModel()->find($id);
        $declaracionJurada->fill($data);
        $declaracionJurada->save();
        return $this;
    }

    private function sendMail()
    {
        echo "mail";
        $datosPersonales = array_get($this->getDataInput(), 'datos_personales');
        $email = array_get($datosPersonales,'email');
        $hash = $this->declaracionJuradaTransformer->encrypt($this->idDeclaracionJurada);
        $linkToEdit = link_to_route('formulario.declaraciones_juradas.edit', 'editar aqui', [$hash]);
        $linkToView = link_to_route('formulario.declaraciones_juradas.export_pdf', 'ver aqui', [$hash]);
        $contenido = View::make('mails.contenido', compact('linkToEdit', 'linkToView'))->render();
        $destinatarios = [
            [
                'mail' => $email
            ]
        ];
        $this->notifactionManager->notificate([
            'content' => $contenido,
            'subject' => 'Declaración Jurada',
            'sender' => 'Ministerio de Seguridad',
            'mails' => $destinatarios,
        ]);
    }

    public function obtener($id){
        $hash = $this->declaracionJuradaTransformer->encrypt($id);
        echo $hash;
        die;
    }

    /**
     * @param $rows
     * @param $model
     * @param $noEmptyField
     * @param $tipoPersona
     * @param $personas
     */
    private function createEntityForPerson($rows, &$model, $noEmptyField, $tipoPersona, $personas)
    {
        $i = 0;
        $idPersonaAnterior = 0;
        foreach ($rows as $row) {
            if (!empty($row[$noEmptyField])) {
                if (is_array($personas)) {

                    if (array_key_exists($i, $personas)) {
                        $idPersona = $personas[$i];
                    } else {
                        $idPersona = $idPersonaAnterior;
                    }

                    $idDeclaracionJurada = array_get($this->getDatosDeclaracionJurada(), 'id_declaracion_jurada');

                    $data = [
                        'id_persona' => $idPersona,
                        'id_declaracion_jurada' => $idDeclaracionJurada,
                        'tipo_persona' => $tipoPersona
                    ];

                    $idPersonaAnterior = $idPersona;

                } else {
                    $data = array_merge($this->getDatosDeclaracionJurada(), ['tipo_persona' => $tipoPersona]);
                }

                $row = array_merge($row, $data);
                $model->create($row);
                $i++;
            }
            #return false;
        }
    }

    private function isValidReCaptcha($dataInput)
    {
        $rules = [
            #'recaptcha_response_field' => 'required|recaptcha',
            'g-recaptcha-response' => 'required|recaptcha',
        ];

        $messages = [
            #'recaptcha_response_field.required' => '<label class="error">' . Config::get('app.textos.validaciones.recaptcha_response_field_required') . '</label>',
            #'recaptcha_response_field.recaptcha' => '<label class="error">' . Config::get('app.textos.validaciones.recaptcha_response_field_recaptcha') . '</label>',
            'g-recaptcha-response.required' => '<label class="error">' . Config::get('app.textos.validaciones.recaptcha_response_field_required') . '</label>',
            'g-recaptcha-response.recaptcha' => '<label class="error">' . Config::get('app.textos.validaciones.recaptcha_response_field_recaptcha') . '</label>',
        ];

        $validator = Validator::make($dataInput, $rules, $messages);

        if($validator->fails())
        {
            $this->addErrorMessages($validator->messages()->toArray());
            return false;
        }

        return true;
    }

    public function eliminar()
    {
        $id = Input::get('id');
        $model = Input::get('modelo');

        $this->declaracionJuradaRepo->deleteRecord($id,$model);

//        dd(Input::all());
    }

    private function getTemplateForEdit($version)
    {
        $template = ($version == 0) ? "formularios.declaracion_jurada.create" : "formularios.old.{$version}.declaracion_jurada.create";
        return $template;
    }

}
