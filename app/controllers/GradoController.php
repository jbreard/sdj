<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 28/03/17
 * Time: 11:18
 */
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

class GradoController extends \BaseController {

    public function getByFuerzaAndTipoPersonal()
    {
        $remember = Config::get('app.declaracion_jurada.cache.combos');
        $data = Input::all();
        $empty = [0 => ''];

        $idFuerza =$data['id_fuerza'];

        $idTipoPersonal = $data['id_tipo_personal'];

        $grados = Grado::remember($remember)
            ->where('id_fuerza',$idFuerza)
            ->where('id_tipo_personal',$idTipoPersonal)
            ->get(['id','descripcion as text'])
            ->toArray();


        foreach($grados as $grado) array_push($empty, $grado);

        $options_personal = $empty;

        return array(
            'data' => $options_personal
        );
    }

}