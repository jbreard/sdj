<?php

use DeclaracionJurada\Repository\ValidacionUsersRepo;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AuthController extends BaseController {
	/*
	 |--------------------------------------------------------------------------
	 | Controlador de la autenticación de usuarios
	 |--------------------------------------------------------------------------
	 */
	public function showLogin() {
		// Verificamos que el usuario no esté autenticado
		if (Auth::check()) {
			// Si está autenticado lo mandamos a la raíz donde estara el mensaje de bienvenida.
			return Redirect::to('usuarios');
		}
		// Mostramos la vista login.blade.php (Recordemos que .blade.php se omite.)
		return View::make('usuarios/login');
	}

	/**
	 * Valida los datos del usuario.
	 */
	public function postLogin() {

		$userdata = array('username' => Input::get('username'), 'password' => Input::get('password'));

        $rules = ['g-recaptcha-response' => 'required|recaptcha'];

        $data = Input::all();

        $validator = Validator::make($data,$rules);

        if ($validator->fails()) return Redirect::to('login') -> with('mensaje_error', 'El captchat no es correcto') -> withInput();

        if (Auth::attempt($userdata, Input::get('remember-me', 0))) {

			$activo = Auth::user()->activo;

			if( $activo == 0 ) return $this->logOut();


			return View::make('welcome');
		}


		return Redirect::to('login') -> with('mensaje_error', 'Tus datos son incorrectos') -> withInput();
	}

	public function logOut() {
		Auth::logout();
		return Redirect::to('login') -> with('mensaje_error', 'Tu sesión ha sido cerrada.');
	}

    public function accessForm()
    {
        $remember = Config::get('app.declaracion_jurada.cache.combos');
        $combo_fuerzas = ['' => 'Seleccione'] + Fuerza::remember($remember)->whereIn('id',[1,2,3,4])->lists('descripcion','id');

        return View::make('formularios.login_carga.login',compact('combo_fuerzas'));
    }

    public function validateFormAccess()
    {
        $data = Input::all();

        $data['email'] = trim($data['email']);
        $data['confirm_email'] = trim($data['confirm_email']);
        
        $validator = Validator::make($data,$this->getRules(),$this->getMessages());

        if ($validator->fails()) {

            Log::info('ERROR: Validacion para ingresar al formulario');
            Log::info($validator->messages());
            Log::info($data);

            return Redirect::to('accessForm')->withInput()->withErrors($validator);
        }

        $repo_validacion = new ValidacionUsersRepo();

        $usuario = $repo_validacion->getIfExists(
            Input::get('dni'),
            Input::get('legajo'),
            Input::get('id_fuerza')
        );
        
        if($usuario == NULL) {

            Log::info('ERROR: no existe usuario');
            Log::info($data);

            return Redirect::to('accessForm')->withInput()->with(
                'user_doesnt_exist',
                'La persona ingresada no existe en nuestra base de datos'
            );
        }

        $accessFormData = [
            'user' => $usuario->id,
            'user_email' => Input::get('email'),
        ];

        return Redirect::route('formulario.declaraciones_juradas.create',compact('accessFormData'));

    }

    private function getRules()
    {
        return [
            'id_fuerza' => 'required',
            'dni' => 'required | numeric | digits_between:7,8',
            'legajo' => 'required | numeric',
            'email' => 'required | email',
            "confirm_email" => "required | email | same:email",
            #'g-recaptcha-response' => 'required|recaptcha',
        ];
    }

    private function getMessages()
    {
        return [
            'id_fuerza.required' => 'Debe seleccionar una fuerza',
            'dni.numeric' => 'El campo DNI debe ser numérico',
            'dni.digits_between' => 'El campo DNI debe tener entre 7 y 8 números',
            'legajo.numeric' => 'El campo legajo debe ser numérico',
            'email.email' => 'El formato del email es incorrecto',
            'confirm_email.email' => 'El formato del email es incorrecto',
            'confirm_email.required' => 'Debe reingresar el mail',
            'confirm_email.same' => 'Debe reingresar el mail',
            #'g-recaptcha-response.required' => Config::get('app.textos.validaciones.recaptcha_response_field_required'),
            #'g-recaptcha-response.recaptcha' => Config::get('app.textos.validaciones.recaptcha_response_field_recaptcha')

        ];
    }

}
