<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 27/03/17
 * Time: 14:18
 */
use DeclaracionJurada\Repository\DeclaracionJuradaRepo;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

class PersonalController extends \BaseController {

    public function getPersonalByFuerza()
    {
        $remember = Config::get('app.declaracion_jurada.cache.combos');
        $data = Input::all();
        $empty = [
            '0' => ['id' => 0,'text'=>'']
        ];

        $idFuerza = $data['id_fuerza'];

        $response = [];

        if(!empty($idFuerza))
        {
            $ids = [1, 2];

            if($idFuerza == 1){#PFA Policia Federal Argentina
                $ids = [1, 2, 3];
            }else if($idFuerza == 4){#PSA Policia Seguridad Aeroportuaria

                $declaracionJuradaRepo = new DeclaracionJuradaRepo();
                $idDecalracionJurada = array_get($data, 'id_declaracion_jurada',null);
                
                $versionFormulario = (is_null($idDecalracionJurada)) 
                    ? 0 
                    : $declaracionJuradaRepo->getVersionFormularioById($idDecalracionJurada);
                
                if($versionFormulario > 0) {
                    $ids = [1, 2, 3];    
                } else {
                    $ids = [4,5];    
                }
                
            }

            $response = TipoPersonal::remember($remember)
                ->whereIn('id', $ids)
                ->get(['id','descripcion as text'])
                ->toArray();
        }

        return array(
            'data' => array_merge($empty , $response)
        );
    }

}