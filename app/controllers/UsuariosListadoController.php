<?php

use DeclaracionJurada\Repository\UsuarioRepo;
use DeclaracionJurada\Transformers\UsuarioListadoTransformer;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;

class UsuariosListadoController extends \BaseController {

    private $usuarioRepo;
    private $usuarioListadoTransformer;

    public function __construct(UsuarioRepo $usuarioRepo,
                                UsuarioListadoTransformer $usuarioListadoTransformer)
    {
        $this->usuarioRepo = $usuarioRepo;
        $this->usuarioListadoTransformer = $usuarioListadoTransformer;
    }

    public function index(){
        $remember = Config::get('app.declaracion_jurada.cache.combos');
        $combo_fuerzas = ['' => 'Seleccione Fuerza'] + Fuerza::remember($remember)->whereIn('id',[1,2,3,4])->lists('descripcion','id');
        $estados = Config::get('app.usuarios.estados');
        $perfiles = ['' => 'Seleccione Perfil'] + Perfil::remember($remember)->lists('descripcion','id');

        return View::make('listados.usuarios_busqueda',compact('combo_fuerzas','estados','perfiles'));
    }

    public function buscar()
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', 600);
        ini_set('set_time_limit', 0);

        $data = $this->usuarioListadoTransformer->prepareData();        

        $responseType = array_get($data,'responseType',0);
        
        $response = [
            0 => 'getSearch',
            1 => 'getExcel',
        ];

        return call_user_func([$this,$response[$responseType]],$data);

    }

    public function getSearch($data)
    {
        #Para transformar la respuesta si es que se necesita:
        #$paginate = $this->usuarioRepo->getData($data);
        #$response = $paginate->toArray();
        #$response = $this->usuarioListadoTransformer->prepareResponse($response);
        #return $response;
        
        return $this->usuarioRepo->getData($data);
    }

    public function getExcel($data)
    {
        return [];
    }
    

} 