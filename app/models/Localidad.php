<?php
class Localidad extends \Eloquent {
    protected $table = 'localidades';
    protected $fillable = array('descripcion','id_provincia','id_partido');
} 