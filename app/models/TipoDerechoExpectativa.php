<?php

class TipoDerechoExpectativa extends \Eloquent {
    protected $table = 'tipos_derecho_expectativa';
    protected $fillable = array('id','descripcion');
}