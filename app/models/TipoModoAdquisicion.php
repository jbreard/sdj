<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 05/04/17
 * Time: 15:40
 */

class TipoModoAdquisicion extends \Eloquent {
    protected $table = 'tipos_modos_adquisicion';
    protected $fillable = array('id','descripcion');
}