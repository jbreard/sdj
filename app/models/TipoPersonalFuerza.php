<?php

class TipoPersonalFuerza extends \Eloquent {
    protected $table = 'tipos_personal_fuerza';
    protected $fillable = array('id','descripcion');
}