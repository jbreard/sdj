<?php
class Inmueble extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_provincia',
        'id_localidad',
        'id_tipo_inmueble',
        'id_unidad_superficie',
        'id_origen',
        'id_persona',
        'id_declaracion_jurada',
        'id_unidad_superficie',

        'id_caracter',
        'id_modo_adquisicion',
        'id_forma_pago',
        'id_pais',
        'id_tipo_moneda_valor_adquisicion',
        'id_tipo_moneda_valor_estimado',

        'superficie_cubierta',
        'superficie_descubierta',
        'fecha_escritura',
        'valor_adquisicion',
        'valor_estimado',
        'otra_forma_pago',
        'domicilio_piso',
        'domicilio_depto',
        'domicilio',

        'domicilio_calle',
        'domicilio_numero',
        'superficie',
        'porcentaje',
        'tipo_persona',
        'otro_origen',
        'otro_pais_localidad',
        'otro_pais_provincia',
        'otro_tipo_moneda_valor_adquisicion',
        'otro_tipo_moneda_valor_estimado',
    );
    protected $table = 'inmuebles';

    public function monedaValorAdquisicion()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_tipo_moneda_valor_adquisicion');
    }

    public function monedaValorEstimado()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_tipo_moneda_valor_estimado');
    }


    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }

    public function provincia()
    {
        return $this->hasOne('Provincia', 'id','id_provincia');
    }

    public function localidad()
    {
        return $this->hasOne('Localidad', 'id','id_localidad');
    }

    public function tipoInmueble()
    {
        return $this->hasOne('TipoInmueble', 'id','id_tipo_inmueble');
    }

    public function origen()
    {
        return $this->hasOne('OrigenInmueble', 'id','id_origen');
    }

    public function unidadSuperficie()
    {
        return $this->hasOne('UnidadSuperficie', 'id','id_unidad_superficie');
    }

    public function tipoCaracter()
    {
        return $this->hasOne('TipoCaracter', 'id','id_caracter');
    }

    public function tipoModoAdquisicion()
    {
        return $this->hasOne('TipoModoAdquisicionInmueble', 'id','id_modo_adquisicion');
    }

    public function tipoFormaPago()
    {
        return $this->hasOne('TipoFormaPago', 'id','id_forma_pago');
    }

    public function pais()
    {
        return $this->hasOne('Pais', 'id','id_pais');
    }

    public function setFechaEscrituraAttribute($value)
    {
        $this->attributes['fecha_escritura'] = \Time::FormatearToMysql($value);
    }

    public function getFechaEscrituraAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }



}