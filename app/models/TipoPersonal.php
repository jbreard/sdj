<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 27/03/17
 * Time: 10:41
 */

class TipoPersonal extends \Eloquent {
    protected $table = 'tipos_personal';
    protected $fillable = array('id','descripcion');
}