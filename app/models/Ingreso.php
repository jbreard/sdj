<?php
class Ingreso extends Eloquent
{

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',
        'id_tipo_ingreso',
        'tipo_ingreso',
        'id_moneda',
        'id_salario',
        'monto_anual_aproximado',
        'servicio_policia_adicional',
        'otros_ingresos',
        'otro_tipo_moneda',

        'monto_anual_neto',
        'monto_anual_bruto',
        'otro_tipo_ingreso',
    );
    protected $table = 'ingresos';


    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }

    public function moneda()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_moneda');
    }

    public function salario()
    {
        return $this->hasOne('Fuerza', 'id', 'id_salario');
    }

    public function tipoIngresoExtraordinario()
    {
        return $this->hasOne('TipoIngresoExtraordinario', 'id', 'id_tipo_ingreso');
    }
}