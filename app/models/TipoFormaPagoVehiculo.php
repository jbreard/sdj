<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 05/04/17
 * Time: 15:41
 */

class TipoFormaPagoVehiculo extends \Eloquent {
    protected $table = 'tipos_formas_pagos_vehiculo';
    protected $fillable = array('id','descripcion');
}