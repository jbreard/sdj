<?php
class DineroEfectivo extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',
        'id_tipo_moneda',
        'origen_fondo',
        'monto',
        'otro_tipo_moneda',        
    );
    protected $table = 'dinero_efectivo';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }   

    public function tipoMoneda()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_tipo_moneda');
    }

}