<?php

class TipoIngresoExterno extends \Eloquent {
    protected $table = 'tipos_ingresos_externos';
    protected $fillable = array('id','descripcion');
}