<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 03/04/17
 * Time: 11:26
 */
class TipoVinculo extends \Eloquent {
    protected $table = 'tipos_vinculos';
    protected $fillable = array('id','descripcion');
}