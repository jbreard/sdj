<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 28/03/17
 * Time: 11:21
 */

class Grado extends \Eloquent {
    protected $table = 'grados';
    protected $fillable = array('id','descripcion','fuerza_id','tipo_personal_id');

}