<?php

class TipoTrabaja extends \Eloquent {
    protected $table = 'tipos_trabaja';
    protected $fillable = array('id','descripcion');
}