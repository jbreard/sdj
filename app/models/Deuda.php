<?php
class Deuda extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',
        'id_tipo_deuda',
        'id_tipo_moneda',
        'acreedor',
        'monto',
        'otro_tipo_moneda',
        'otro_tipo_deuda',
        'tipo_persona',
        'cuotas_restantes',
        'otro_credito_personal',
    );
    protected $table = 'deudas';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }

    public function tipoDeuda()
    {
        return $this->hasOne('TipoDeuda', 'id', 'id_tipo_deuda');
    }

    public function tipoMoneda()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_tipo_moneda');
    }
}