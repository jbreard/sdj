<?php
class UnidadSuperficie extends \Eloquent {
    protected $table = 'unidades_superficie';
    protected $fillable = array('id','descripcion');
} 