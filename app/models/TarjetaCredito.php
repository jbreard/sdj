<?php
class TarjetaCredito extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',
        'id_entidad_emisora',
        'id_tipo_tarjeta_credito',
        'banco',
        'numero',
        'otra_entidad_emisora',
        'id_tipo_titular',
        'cantidad_extension',
    );
    protected $table = 'tarjetas_credito';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }

    public function entidadEmisora()
    {
        return $this->hasOne('EntidadEmisoraTC', 'id', 'id_entidad_emisora');
    }

    public function tipoTarjetaCredito()
    {
        return $this->hasOne('TipoTarjetaCredito', 'id', 'id_tipo_tarjeta_credito');
    }
    public function tipoTitular()
    {
        return $this->hasOne('TipoTitular', 'id', 'id_tipo_titular');
    }
}