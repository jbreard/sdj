<?php
class TipoMoneda extends \Eloquent {
    protected $table = 'tipos_moneda';
    protected $fillable = array('id','descripcion');
} 