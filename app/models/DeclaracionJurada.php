<?php
class DeclaracionJurada extends Eloquent
{

    protected $fillable = array(
        'id',
        'id_persona',
        'id_tipo_caracter',
        'id_tipo_acto_administrativo',
        'numero_anio',
        'fecha_acto_administrativo',
        'tipo_estado',
        'periodo_fiscal',
        'deleted_at',
        'accepted_at',
        'received_at',
        'version',
    );
    protected $table = 'declaraciones_juradas';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }

    public function caracter()
    {
        return $this->hasOne('TipoCaracterDeclaracion', 'id', 'id_tipo_caracter');
    }

    public function actoAdministrativo()
    {
        return $this->hasOne('TipoActoAdministrativo', 'id', 'id_tipo_acto_administrativo');
    }

    public function cuentaBancaria()
    {
        return $this->hasMany('CuentaBancaria', 'id_declaracion_jurada', 'id');
    }

    public function ingresoExtraordinario()
    {
        return $this->hasMany('IngresoExtraordinario', 'id_declaracion_jurada', 'id');
    }

    public function cuentaBancariaExterior()
    {
        return $this->hasMany('CuentaBancariaExterior', 'id_declaracion_jurada', 'id');
    }

    public function derechoExpectativa()
    {
        return $this->hasMany('DerechoExpectativa', 'id_declaracion_jurada', 'id');
    }

    public function dineroEfectivo()
    {
        return $this->hasMany('DineroEfectivo', 'id_declaracion_jurada', 'id');
    }

    public function bonosTitulosAcciones()
    {
        return $this->hasMany('BonosTitulosAcciones', 'id_declaracion_jurada', 'id');
    }

    public function acreencias()
    {
        return $this->hasMany('Acreencia', 'id_declaracion_jurada', 'id');
    }

    public function deuda()
    {
        return $this->hasMany('Deuda', 'id_declaracion_jurada', 'id');
    }

    public function ingreso()
    {
        return $this->hasMany('Ingreso', 'id_declaracion_jurada', 'id');
    }

    public function ingresoExterno()
    {
        return $this->hasMany('IngresoExterno', 'id_declaracion_jurada', 'id');
    }

    public function tarjetaCredito()
    {
        return $this->hasMany('TarjetaCredito', 'id_declaracion_jurada', 'id');
    }

    public function inmueble()
    {
        return $this->hasMany('Inmueble', 'id_declaracion_jurada', 'id');
    }

    public function vehiculo()
    {
        return $this->hasMany('Vehiculo', 'id_declaracion_jurada', 'id');
    }

    public function bien()
    {
        return $this->hasMany('Bien', 'id_declaracion_jurada', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

    public function getDeletedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

    public function getAcceptedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

    public function getReceivedAtAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

}