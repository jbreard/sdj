<?php

class TipoIngresoActividad extends \Eloquent {
    protected $table = 'tipos_ingresos_actividades';
    protected $fillable = array('id','descripcion');
}