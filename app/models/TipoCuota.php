<?php

class TipoCuota extends \Eloquent {
    protected $table = 'tipos_cuotas';
    protected $fillable = array('id','descripcion');
}