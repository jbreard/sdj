<?php

class TipoBien extends \Eloquent {
    protected $table = 'tipos_bien';
    protected $fillable = array('id','descripcion');
}