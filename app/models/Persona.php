<?php
class Persona extends Eloquent
{
    protected $softDelete = true;

    protected $table = 'personas';
    protected $fillable = array(
        'id_provincia',
        'id_localidad',
        'id_tipo_sexo',
        'id_estado_civil',
        'id_fuerza',

        "id_tipo_personal",
        "id_grado",
        "id_situacion_revista",
        "id_tipo_vinculo",
        "id_tipo_categoria_tributaria",
        'id_tipo_agrupamiento_modalidad',
        'id_tipo_actividad',
        'id_tipo_documento',
        'id_tipo_hijo_convive',

        "trabaja",
        "desde_cuando_trabaja",
        "ocupacion",
        "ingreso_neto_anual",
        "llamados_servicio",
        "cuerpo_agrupamiento",
        "cargo_funcion",
        "escalafon",

        'nombres',
        'apellido',
        'nacimiento',
        'nacimiento_lugar',
        'documento',
        'cuil_cuit',
        'domicilio_real',
        'codigo_postal',
        'telefono',
        'email',
        'grado',
        'legajo_personal',
        'destino_revista',
        'ocupacion',
        'domicilio_laboral',
        'vinculo_parentesco',
        'es_familiar',

        'personal_fuerza',
    );

    public function declaracion_jurada() {
        return $this->hasMany('DeclaracionJurada', 'id_persona', 'id');
    }

    public function familiar()
    {
        return $this->hasMany('Familiar', 'id_persona', 'id');
    }

    public function inmueble()
    {
        return $this->hasOne('Inmueble', 'id_persona','id');
    }

    public function vehiculo()
    {
        return $this->hasOne('Vehiculo', 'id_persona','id');
    }

    public function fuerza()
    {
        return $this->hasOne('Fuerza', 'id','id_fuerza');
    }

    public function provincia()
    {
        return $this->hasOne('Provincia', 'id','id_provincia');
    }

    public function localidad()
    {
        return $this->hasOne('Localidad', 'id','id_localidad');
    }

    public function estadoCivil()
    {
        return $this->hasOne('EstadoCivil', 'id','id_estado_civil');
    }
    public function tipoPersonal()
    {
        return $this->hasOne('TipoPersonal', 'id','id_tipo_personal');
    }
    public function tipoPersonalFuerza()
    {
        return $this->hasOne('TipoPersonalFuerza', 'id','personal_fuerza');
    }
    public function tipoAgrupamientoModalidad()
    {
        return $this->hasOne('TipoAgrupamientoModalidad', 'id','id_tipo_agrupamiento_modalidad');
    }
    public function grado()
    {
        return $this->hasOne('Grado', 'id','id_grado');
    }
    public function situacionRevista()
    {
        return $this->hasOne('TipoSituacionRevista', 'id','id_situacion_revista');
    }
    public function tipoVinculo()
    {
        return $this->hasOne('TipoVinculo', 'id','id_tipo_vinculo');
    }
    public function categoriaTributaria()
    {
        return $this->hasOne('TipoCategoriaTributaria', 'id','id_tipo_categoria_tributaria');
    }

    public function tipoActividadConvivienteFuerza()
    {
        return $this->hasOne('TipoConvivienteFuerzaActividad', 'id','id_tipo_actividad');
    }

    public function tipoDocumento()
    {
        return $this->hasOne('TipoDocumento', 'id','id_tipo_documento');
    }

    public function tipoHijoConvive()
    {
        return $this->hasOne('TipoHijoConvive', 'id','id_tipo_hijo_convive');
    }

    public function sexo()
    {
        return $this->hasOne('TipoSexo', 'id','id_tipo_sexo');
    }

    public function setNacimientoAttribute($value)
    {
        $this->attributes['nacimiento'] = \Time::FormatearToMysql($value);
    }

    public function getNacimientoAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

    public function getTrabajaAttribute($value)
    {
        $options = array(
            1=>'No',
            2=>'Si',
        );

        return (is_null($value) || empty($value)) ? '' : $options[$value];
    }

}