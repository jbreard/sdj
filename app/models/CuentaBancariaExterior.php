<?php
class CuentaBancariaExterior extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',
        'id_tipo_banco',
        'tipo_banco',
        'banco',
        'numero_cuenta',

        'id_tipo_moneda',
        'otro_tipo_moneda',
        'saldo',
    );
    protected $table = 'cuentas_bancarias_exterior';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }

    public function tipoCuentaBancariaExterior()
    {
        return $this->hasOne('TipoCuentaBancaria', 'id', 'id_tipo_banco');
    }

    public function tipoMoneda()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_tipo_moneda');
    }
}