<?php
class TipoCuentaBancaria extends \Eloquent {
    protected $table = 'tipos_cuentas_bancarias';
    protected $fillable = array('id','descripcion');
} 