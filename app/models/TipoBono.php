<?php
class TipoBono extends \Eloquent {
    protected $table = 'tipos_bonos';
    protected $fillable = array('id','descripcion');
}