<?php

class TipoActoAdministrativo extends \Eloquent {
    protected $table = 'tipos_actos_administrativos';
    protected $fillable = array('id','descripcion');
}