<?php
class TipoDeuda extends \Eloquent {
    protected $table = 'tipos_deuda';
    protected $fillable = array('id','descripcion');
} 