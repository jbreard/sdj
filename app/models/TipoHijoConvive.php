<?php

class TipoHijoConvive extends \Eloquent {
    protected $table = 'tipos_hijo_convive';
    protected $fillable = array('id','descripcion');
}