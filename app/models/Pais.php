<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 05/04/17
 * Time: 15:41
 */
class Pais extends \Eloquent {
    protected $table = 'paises';
    protected $fillable = array('id','descripcion');
}