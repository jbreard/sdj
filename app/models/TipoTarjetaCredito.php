<?php
class TipoTarjetaCredito extends \Eloquent {
    protected $table = 'tipos_tarjeta_credito';
    protected $fillable = array('id','descripcion');
} 