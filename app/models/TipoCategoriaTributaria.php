<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 03/04/17
 * Time: 11:27
 */

class TipoCategoriaTributaria extends \Eloquent {
    protected $table = 'tipos_categorias_tributarias';
    protected $fillable = array('id','descripcion');
}