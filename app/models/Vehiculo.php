<?php
class Vehiculo extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_tipo_vehiculo',
        'id_origen',
        'id_persona',
        'id_declaracion_jurada',
        'marca',
        'modelo',
        'dominio',
        'anio',
        'valor_aproximado',
        'porcentaje',
        'tipo_persona',
        'otro_origen',
        'otro_tipo_moneda_valor_estimado',
        'otro_tipo_moneda_valor_seguro',

        'id_tipo_vehiculo_nuevo',
        'id_caracter',
        'id_modo_adquisicion',
        'id_forma_pago',
        'id_pais',
        'id_provincia',
        'id_localidad',
        'id_tipo_moneda_valor_estimado',
        'id_tipo_moneda_valor_seguro',
        'tipo',
        'fecha_transferencia_dominio',
        'valor_adquisicion',
        'valor_seguro',
        'radicacion',
        'otro_pais_localidad',
        'otro_pais_provincia',


    );
    protected $table = 'vehiculos';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }

    public function origen()
    {
        return $this->hasOne('OrigenVehiculo', 'id', 'id_origen');
    }

    public function tipoVehiculo()
    {
        return $this->hasOne('TipoVehiculo', 'id', 'id_tipo_vehiculo');
    }

    public function tipoVehiculoNuevo()
    {
        return $this->hasOne('TipoVehiculo', 'id', 'id_tipo_vehiculo_nuevo');
    }

    public function tipoCaracter()
    {
        return $this->hasOne('TipoCaracter', 'id', 'id_caracter');
    }
    public function tipoModoAdquisicion()
    {
        return $this->hasOne('TipoModoAdquisicion', 'id', 'id_modo_adquisicion');
    }
    public function formaPago()
    {
        return $this->hasOne('TipoFormaPagoVehiculo', 'id', 'id_forma_pago');
    }
    public function pais()
    {
        return $this->hasOne('Pais', 'id', 'id_pais');
    }
    public function provincia()
    {
        return $this->hasOne('Provincia', 'id', 'id_provincia');
    }
    public function localidad()
    {
        return $this->hasOne('Localidad', 'id', 'id_localidad');
    }

    public function setFechaTransferenciaDominioAttribute($value)
    {
        $this->attributes['fecha_transferencia_dominio'] = \Time::FormatearToMysql($value);
    }

    public function getFechaTransferenciaDominioAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }

    public function monedaValorEstimado()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_tipo_moneda_valor_estimado');
    }

    public function monedaValorSeguro()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_tipo_moneda_valor_seguro');
    }
}