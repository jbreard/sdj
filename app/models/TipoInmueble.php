<?php

class TipoInmueble extends \Eloquent {
    protected $table = 'tipos_inmueble';
    protected $fillable = array('id','descripcion');
}