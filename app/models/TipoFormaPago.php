<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 05/04/17
 * Time: 15:41
 */

class TipoFormaPago extends \Eloquent {
    protected $table = 'tipos_formas_pagos';
    protected $fillable = array('id','descripcion');
}