<?php

class TipoCuotaRestante extends \Eloquent {
    protected $table = 'tipos_cuotas_restantes';
    protected $fillable = array('id','descripcion');
}