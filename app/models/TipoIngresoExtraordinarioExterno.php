<?php

class TipoIngresoExtraordinarioExterno extends \Eloquent {
    protected $table = 'tipos_ingresos_extraordinarios_externos';
    protected $fillable = array('id','descripcion');
}