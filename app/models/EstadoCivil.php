<?php

class EstadoCivil extends \Eloquent {
	protected $fillable = ['descripcion'];

	protected $table = 'estados_civiles';
}