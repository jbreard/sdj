<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class User extends Eloquent implements UserInterface, RemindableInterface {
	protected $fillable = array('nombre', 'apellido', 'email', 'id_fuerza', 'id_perfil', 'dni', 'password','testigo','username','area','telefono','contacto_laboral','remember_token');
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier() {
		return $this -> getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword() {
		return $this -> password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail() {
		return $this -> email;
	}

	public function getRememberToken() {
		return $this -> remember_token;
	}

	public function setRememberToken($value) {
		$this -> remember_token = $value;
	}

	public function getRememberTokenName() {
		return 'remember_token';
	}

	public $errors;
 
	public function isValid($data)
	{
		$messages = Config::get('app.textos.validaciones');

        $idUsuario = (isset($this->id)) ? $this->id : '';

		$rules = array( 'email'    => 'required|email', 
						'nombre'   => 'required|max:40', 
						'username'   => 'required|max:100|unique:usuarios,username,' . $idUsuario,
						'apellido' => 'required|max:40',
						'dni'      => 'required|numeric');

		$validator = Validator::make($data, $rules, $messages);

		if ($validator -> passes()) {
			return true;
		}

		$this -> errors = $validator -> errors();

		return false;
	}

	public function isValidClave($data){

		$rules = array(
			"clave_old"=>"required",
			"clave_nueva"=>"required|min:6",
			"clave_confirma"=>"required|same:clave_nueva"
		);
		$validator = Validator::make($data, $rules);

		if ($validator -> passes()) {
			return true;
		}

		$this -> errors = $validator -> errors();

		return false;
		
	}
	
	public function perfil()
	{
		return $this->hasOne('Perfil', 'id','id_perfil');
	}

	public function fuerza()
	{
		return $this->hasOne('Fuerza', 'id','id_fuerza');
	}

}
