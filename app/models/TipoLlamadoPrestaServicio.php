<?php

class TipoLlamadoPrestaServicio extends \Eloquent {
    protected $table = 'tipos_llamados_prestar_servicio';
    protected $fillable = array('id','descripcion');
}