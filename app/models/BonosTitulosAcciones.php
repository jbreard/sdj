<?php
class BonosTitulosAcciones extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',
        'id_tipo_bono',
        'otro_tipo_bono',
        'descripcion',
        'cantidad',
        'valor_nominal',
        'entidad_emisora_otorgante',
        'fecha_adquisicion',

    );
    protected $table = 'bonos_titulos_acciones';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }

    public function tipoBono()
    {
        return $this->hasOne('TipoBono', 'id', 'id_tipo_bono');
    }

    public function setFechaAdquisicionAttribute($value)
    {
        $this->attributes['fecha_adquisicion'] = \Time::FormatearToMysql($value);
    }

    public function getFechaAdquisicionAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }
}