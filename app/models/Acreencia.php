<?php
class Acreencia extends Eloquent
{
    protected $softDelete = true;

    protected $table = 'acreencias';

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',
        'id_tipo_moneda',
        'otro_tipo_moneda',
        'identidad_deudor',
        'monto',
        'fecha',
    );

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }    

    public function tipoMoneda()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_tipo_moneda');
    }

    public function setFechaAttribute($value)
    {
        $this->attributes['fecha'] = \Time::FormatearToMysql($value);
    }

    public function getFechaAttribute($value)
    {
        return \Time::FormatearToNormal($value);
    }
}