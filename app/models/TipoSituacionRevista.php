<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 29/03/17
 * Time: 14:45
 */


class TipoSituacionRevista extends \Eloquent {
    protected $table = 'tipos_situaciones_revista';
    protected $fillable = array('id','descripcion');
}