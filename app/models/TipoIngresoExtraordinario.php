<?php

class TipoIngresoExtraordinario extends \Eloquent {
    protected $table = 'tipos_ingresos_extraordinarios';
    protected $fillable = array('id','descripcion');
}