<?php
class OrigenInmueble extends \Eloquent {
    protected $table = 'origenes_inmueble';
    protected $fillable = array('id','descripcion');
} 