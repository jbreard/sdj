<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 05/04/17
 * Time: 15:31
 */

class TipoCaracter extends \Eloquent {
    protected $table = 'tipos_caracter';
    protected $fillable = array('id','descripcion');
}