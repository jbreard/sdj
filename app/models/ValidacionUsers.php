<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 07/06/17
 * Time: 16:25
 */
class ValidacionUsers extends \Eloquent {
    
    protected $table = 'validacion_users';

    protected $fillable = array('id','nro_ord','id_fuerza','grado','apellido','nombre','dni','legajo',
        'tipo','periodo_fiscal','id_usuario','backup','created_at','updated_at');

    public function fuerza()
    {
        return $this->hasOne('Fuerza', 'id','id_fuerza');
    }

}