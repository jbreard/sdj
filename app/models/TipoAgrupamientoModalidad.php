<?php
class TipoAgrupamientoModalidad extends \Eloquent {
    protected $table = 'tipos_agrupamiento_modalidad';
    protected $fillable = array('id','descripcion','id_tipo_personal');

    public function tipoPersonal()
    {
        return $this->hasOne('TipoPersonal', 'id','id_tipo_personal');
    }
} 