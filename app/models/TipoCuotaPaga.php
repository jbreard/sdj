<?php

class TipoCuotaPaga extends \Eloquent {
    protected $table = 'tipos_cuotas_pagas';
    protected $fillable = array('id','descripcion');
}