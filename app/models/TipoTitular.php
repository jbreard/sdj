<?php

class TipoTitular extends \Eloquent {
    protected $table = 'tipos_titular';
    protected $fillable = array('id','descripcion');
}