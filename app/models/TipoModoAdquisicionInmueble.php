<?php
/**
 * Created by PhpStorm.
 * User: damian
 * Date: 05/04/17
 * Time: 15:40
 */

class TipoModoAdquisicionInmueble extends \Eloquent {
    protected $table = 'tipos_modos_adquisicion_inmueble';
    protected $fillable = array('id','descripcion');
}