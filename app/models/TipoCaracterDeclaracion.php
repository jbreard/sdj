<?php
class TipoCaracterDeclaracion extends \Eloquent {
    protected $table = 'tipos_caracter_declaracion';
    protected $fillable = array('id','descripcion');
} 