<?php
class DerechoExpectativa extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',
        'id_tipo_derechos_expectativa',
        'id_tipo_fuente',
        'id_tipo_cuotas',
        'id_cuotas_pagas',
        'id_cuotas_restantes',
        'cantidad_cuotas_pagas',
        'cantidad_cuotas_restantes',
        'fuente_cantidad_cuotas',
        'tipo_persona',

    );

    protected $table = 'derechos_expectativa';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }
    public function tipoDerechoExpectativa()
    {
        return $this->hasOne('TipoDerechoExpectativa', 'id', 'id_tipo_derechos_expectativa');
    }
    public function tipoFuente()
    {
        return $this->hasOne('TipoFuente', 'id', 'id_tipo_fuente');
    }
    public function tipoCuotaFuente()
    {
        return $this->hasOne('TipoCuota', 'id', 'id_tipo_cuotas');
    }

    public function tipoCuotaPaga()
    {
        return $this->hasOne('TipoCuotaPaga', 'id', 'id_cuotas_pagas');
    }public function tipoCuotaRestante()
    {
        return $this->hasOne('TipoCuotaRestante', 'id', 'id_cuotas_restantes');
    }

}