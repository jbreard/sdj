<?php

class TipoFuente extends \Eloquent {
    protected $table = 'tipos_fuentes';
    protected $fillable = array('id','descripcion');
}