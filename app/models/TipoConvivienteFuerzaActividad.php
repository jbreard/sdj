<?php
class TipoConvivienteFuerzaActividad extends \Eloquent {
    protected $table = 'tipos_conviviente_fuerza_actividad';
    protected $fillable = array('id','descripcion');
} 