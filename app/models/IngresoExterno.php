<?php
class IngresoExterno extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',
        'id_tipo_ingreso',
        'id_tipo_moneda',
        'id_fuerza',
        'id_ingreso_otra_actividad',
        'id_ingreso_extraordinario',
        'id_categoria_tributaria',
        'monto_neto_anual_total',
        'otro_tipo_moneda',
        'otro_tipo_ingreso',
        'otro_tipo_ingreso_extraordinario',
    );
    protected $table = 'ingresos_externos';


    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }

    public function moneda()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_tipo_moneda');
    }

    public function fuerza()
    {
        return $this->hasOne('Fuerza', 'id', 'id_fuerza');
    }
    public function tipoIngreso()
    {
        return $this->hasOne('TipoIngresoExterno', 'id', 'id_tipo_ingreso');
    }
    public function ingresoExtraordinario()
    {
        return $this->hasOne('TipoIngresoExtraordinarioExterno', 'id', 'id_ingreso_extraordinario');
    }
    public function ingresoOtraActividad()
    {
        return $this->hasOne('TipoIngresoActividad', 'id', 'id_ingreso_otra_actividad');
    }
    public function categoriaTributaria()
    {
        return $this->hasOne('TipoCategoriaTributaria', 'id', 'id_categoria_tributaria');
    }
}