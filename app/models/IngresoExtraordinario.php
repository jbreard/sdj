<?php
class IngresoExtraordinario extends Eloquent
{
    protected $softDelete = true;

    protected $fillable = array(
        'id_persona',
        'id_declaracion_jurada',

        'id_tipo_moneda',
        'id_tipo_ingreso',
        'otro_tipo_ingreso',
        'monto_anual_neto',
        'otro_tipo_moneda',

    );
    protected $table = 'ingresos_extraordinarios';

    public function persona()
    {
        return $this->hasOne('Persona', 'id', 'id_persona');
    }

    public function moneda()
    {
        return $this->hasOne('TipoMoneda', 'id', 'id_tipo_moneda');
    }

    public function tipoIngreso()
    {
        return $this->hasOne('TipoIngresoExtraordinario', 'id', 'id_tipo_ingreso');
    }

}