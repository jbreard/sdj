<?php
class OrigenVehiculo extends \Eloquent {
    protected $table = 'origenes_vehiculo';
    protected $fillable = array('id','descripcion');
} 