<?php
class EntidadEmisoraTC extends \Eloquent {
    protected $table = 'entidades_emisoras_tc';
    protected $fillable = array('id','descripcion');
} 