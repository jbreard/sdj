<?php
class TipoDocumento extends \Eloquent {
    protected $table = 'tipos_documento';
    protected $fillable = array('id','descripcion');
} 