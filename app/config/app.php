<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/
	
	'nombre_sistema' => '',

	'debug' => true,

	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| This URL is used by the console to properly generate URLs when using
	| the Artisan command line tool. You should set this to the root of
	| your application so that it is used when running Artisan tasks.
	|
	*/

	'url' => 'http://127.0.0.1',
    //    'url' => 'http://sdj.minseg.gob.ar',


	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| Here you may specify the default timezone for your application, which
	| will be used by the PHP date and date-time functions. We have gone
	| ahead and set this to a sensible default for you out of the box.
	|
	*/

	'timezone' => 'America/Argentina/Buenos_Aires',

	/*
	|--------------------------------------------------------------------------
	| Application Locale Configuration
	|--------------------------------------------------------------------------
	|
	| The application locale determines the default locale that will be used
	| by the translation service provider. You are free to set this value
	| to any of the locales which will be supported by the application.
	|
	*/

	'locale' => 'es',

	/*
	|--------------------------------------------------------------------------
	| Encryption Key
	|--------------------------------------------------------------------------
	|
	| This key is used by the Illuminate encrypter service and should be set
	| to a random, 32 character string, otherwise these encrypted strings
	| will not be safe. Please do this before deploying an application!
	|
	*/

	'key' => 'XrCiubGEeAh3Tn32U8rYH97JzPiYRkqC',

	/*
	|--------------------------------------------------------------------------
	| Autoloaded Service Providers
	|--------------------------------------------------------------------------
	|
	| The service providers listed here will be automatically loaded on the
	| request to your application. Feel free to add your own services to
	| this array to grant expanded functionality to your applications.
	|
	*/

	'providers' => array(

		'Illuminate\Foundation\Providers\ArtisanServiceProvider',
		'Illuminate\Auth\AuthServiceProvider',
		'Illuminate\Cache\CacheServiceProvider',
		'Illuminate\Session\CommandsServiceProvider',
		'Illuminate\Foundation\Providers\ConsoleSupportServiceProvider',
		'Illuminate\Routing\ControllerServiceProvider',
		'Illuminate\Cookie\CookieServiceProvider',
		'Illuminate\Database\DatabaseServiceProvider',
		'Illuminate\Encryption\EncryptionServiceProvider',
		'Illuminate\Filesystem\FilesystemServiceProvider',
		'Illuminate\Hashing\HashServiceProvider',
		'Illuminate\Html\HtmlServiceProvider',
		'Illuminate\Log\LogServiceProvider',
		'Illuminate\Mail\MailServiceProvider',
		'Illuminate\Database\MigrationServiceProvider',
		'Illuminate\Pagination\PaginationServiceProvider',
		'Illuminate\Queue\QueueServiceProvider',
		'Illuminate\Redis\RedisServiceProvider',
		'Illuminate\Remote\RemoteServiceProvider',
		'Illuminate\Auth\Reminders\ReminderServiceProvider',
		'Illuminate\Database\SeedServiceProvider',
		'Illuminate\Session\SessionServiceProvider',
		'Illuminate\Translation\TranslationServiceProvider',
		'Illuminate\Validation\ValidationServiceProvider',
		'Illuminate\View\ViewServiceProvider',
		'Illuminate\Workbench\WorkbenchServiceProvider',
        'Barryvdh\Debugbar\ServiceProvider',
        'Thujohn\Pdf\PdfServiceProvider',
		'Way\Generators\GeneratorsServiceProvider',
		#'Maatwebsite\Excel\ExcelServiceProvider',
        'Greggilbert\Recaptcha\RecaptchaServiceProvider',


    ),

	/*
	|--------------------------------------------------------------------------
	| Service Provider Manifest
	|--------------------------------------------------------------------------
	|
	| The service provider manifest is used by Laravel to lazy load service
	| providers which are not needed for each request, as well to keep a
	| list of all of the services. Here, you may set its storage spot.
	|
	*/

	'manifest' => storage_path().'/meta',

	/*
	|--------------------------------------------------------------------------
	| Class Aliases
	|--------------------------------------------------------------------------
	|
	| This array of class aliases will be registered when this application
	| is started. However, feel free to register as many as you wish as
	| the aliases are "lazy" loaded so they don't hinder performance.
	|
	*/

	'aliases' => array(

		'App'             => 'Illuminate\Support\Facades\App',
		'Artisan'         => 'Illuminate\Support\Facades\Artisan',
		'Auth'            => 'Illuminate\Support\Facades\Auth',
		'Blade'           => 'Illuminate\Support\Facades\Blade',
		'Cache'           => 'Illuminate\Support\Facades\Cache',
		'ClassLoader'     => 'Illuminate\Support\ClassLoader',
		'Config'          => 'Illuminate\Support\Facades\Config',
		'Controller'      => 'Illuminate\Routing\Controller',
		'Cookie'          => 'Illuminate\Support\Facades\Cookie',
		'Crypt'           => 'Illuminate\Support\Facades\Crypt',
		'DB'              => 'Illuminate\Support\Facades\DB',
		'Eloquent'        => 'Illuminate\Database\Eloquent\Model',
		'Event'           => 'Illuminate\Support\Facades\Event',
		'File'            => 'Illuminate\Support\Facades\File',
		'Form'            => 'Illuminate\Support\Facades\Form',
		'Hash'            => 'Illuminate\Support\Facades\Hash',
		'HTML'            => 'Illuminate\Support\Facades\HTML',
		'Input'           => 'Illuminate\Support\Facades\Input',
		'Lang'            => 'Illuminate\Support\Facades\Lang',
		'Log'             => 'Illuminate\Support\Facades\Log',
		'Mail'            => 'Illuminate\Support\Facades\Mail',
		'Paginator'       => 'Illuminate\Support\Facades\Paginator',
		'Password'        => 'Illuminate\Support\Facades\Password',
		'Queue'           => 'Illuminate\Support\Facades\Queue',
		'Redirect'        => 'Illuminate\Support\Facades\Redirect',
		'Redis'           => 'Illuminate\Support\Facades\Redis',
		'Request'         => 'Illuminate\Support\Facades\Request',
		'Response'        => 'Illuminate\Support\Facades\Response',
		'Route'           => 'Illuminate\Support\Facades\Route',
		'Schema'          => 'Illuminate\Support\Facades\Schema',
		'Seeder'          => 'Illuminate\Database\Seeder',
		'Session'         => 'Illuminate\Support\Facades\Session',
		'SSH'             => 'Illuminate\Support\Facades\SSH',
		'Str'             => 'Illuminate\Support\Str',
		'URL'             => 'Illuminate\Support\Facades\URL',
		'Validator'       => 'Illuminate\Support\Facades\Validator',
		'View'            => 'Illuminate\Support\Facades\View',
		'Carbon'		  => 'Carbon\Carbon',
        'PDF'             => 'Thujohn\Pdf\PdfFacade',
		#'Excel' => 'Maatwebsite\Excel\Facades\Excel',
	),
	'modulos' => array(
		'mensajes' => true,
	),
	'textos' => array (
		'validaciones' => array(
			'redirect' => 'Debe completar todos los campos obligatorios para continuar, los mismos estan identificados con asteriscos rojo.',
			'no_edit_by_tipoEstado' => 'Atención: <br>La declaración jurada [%s] no se puede modificar ya que se encuentra en el estado: %s',
			'apellido'=> 'Completar apellido',
			'apellido_fake'=> 'El apellido del declarante no coincide con el validado en el formulario de ingreso',
			'nombres'=> 'Completar nombre',
			'nombres_fake'=> 'El nombre del declarante no coincide con el validado en el formulario de ingreso',
			'nacimiento'=> 'Completar fecha de nacimiento',
			'estado_civil'=> 'Completar estado civil',
			'select_default'=> 'Completar combo',
			'provincia'=> 'Completar provincia',
			'localidad'=> 'Completar localidad',
			'id_tipo_personal'=> 'Completar Personal',
			'id_grado'=> 'Completar Grado',
			'id_situacion_revista'=> 'Completar situación de revista',
			'id_tipo_sexo'=> 'Completar género',
			'destino_revista'=> 'Completar destino de revista',
			'cargo_funcion'=> 'Completar cargo o función',
			'cuerpo_agrupamiento'=> 'Completar cuerpo/agrupamiento',
			'escalafon'=> 'Completar escalafón',
			'llamados_servicio'=> 'Completar Llamados a prestar servicio',
			'domicilio_real'=> 'Completar domicilio real',
			'documento'=> 'Completar dni',
			'documento_fake'=> 'El documento del declarante no coincide con el validado en el formulario de ingreso',
			'telefono'=> 'Completar telefono de contacto',
			'legajo_personal'=> 'Completar legajo personal',
			'email'=> 'Completar correo electrónico',
			'email_no_valido'=> 'Correo electrónico no válido',
			'monto_anual_aproximado'=> 'Completar monto anual aproximado',
			'monto_anual_neto'=> 'Completar monto anual neto',
			'monto_anual_bruto'=> 'Completar monto anual bruto',
			'cuil_cuit'=> 'Debe ingresar un cuit/cuil obligatoriamente y válido',
			'fuerza_perteneciente'=> 'Completar fuerza a la que pertenece',
			'recaptcha_response_field_required' => 'El campo reCaptcha es requerido',
			'recaptcha_response_field_recaptcha' => 'reCaptcha Incorrecto',
			'unique' => 'El nombre de usuario ya esta en uso.',
			'campo_requerido' => 'Este campo es requerido.',
		),
		'notas_carga' => array(
			'caracter_declaracion' => [
				'a' => 'Las declaraciones juradas Anuales podrán <b>cargarse</b> en el SDJ durante 4 meses. Y deberán ser <b>presentadas</b> en alguna dependencia habilitada antes de finalizado el plazo de 4 meses. La carga de las declaraciones juradas de Alta y Baja estará habilitada durante todo el año. Para las DDJJ de Baja el declarante tendrá plazo para su carga en el SDJ y presentación en una dependencia habilitada en el plazo de 60 días corridos de resuelto o dispuesto el acto administrativo. Para completar la declaración jurada de Baja tendrá que tener número y fecha del acto administrativo.',
			],
			'derecho_expectativa' => array(
				'a' => 'Recuerde que si selecciona este ítem Derechos en Expectativa, la opción plan de vivienda o plan de ahorro es porque aún no posee la escritura del inmueble o  la transferencia del dominio.',
			),
			'datos_familiares' => array(
				'a' => 'Aquí deberá consignar los datos de la persona con la que convive, ésta puede ser su cónyuge o no; es decir, puede que esté separado/a de hecho y conviviendo con otra persona.'
			),
			'inmueble' => array(
				'a' => 'Declarar en este ítem el o los inmuebles que estén escriturados, es decir, si poseen título de propiedad. Si están escriturados y adeuda pagar dinero deberá completar el ítem “Deudas”. En el caso de que no estén escriturados y estén por ejemplo en un plan de pagos, deberá declararlo/s en el ítem “Derechos en Expectativa”.',
				'b' => 'El porcentaje (%) refiere exclusivamente a lo que figura en la escritura de compra. Si sólo figura una persona, aunque sea ganancial, debe consignarse el 100%. Si figuran los dos cónyuges como compradores debe consignarse el 50% o el porcentaje que figure en la escritura.  Si es ganancial y en la escritura hay un tercero, señalar el porcentaje que corresponda deducido la porción del tercero.' ,
				'c' => 'Si no se cargan datos de la radicación del inmueble (Calle Y Número), se procedará a imprimir la leyenda "No Declara".' ,
			),
			'vehiculos' => array(
				'a' => 'Declarar en este ítem el o los vehículos de los que posee el dominio. Si el dominio ha sido transferido y adeuda pagar dinero deberá completar el ítem “Deudas”. Si el dominio no está transferido y está en un plan de pagos deberá declararlo en el ítem “Derechos en expectativa”.',
				'b' => 'El porcentaje (%) refiere exclusivamente a lo que figura en la escritura de compra. Si sólo figura una persona, aunque sea ganancial, debe consignarse el 100%. Si figuran los dos cónyuges como compradores debe consignarse el 50% o el porcentaje que figure en la escritura. Si es ganancial y en la escritura hay un tercero, señalar el porcentaje que corresponda deducido la porción del tercero.' ,
			),
			'ingresos' => array(
				'a' => 'En el campo monto anual neto consignar el saldo final luego de restar cargas sociales, retenciones y deducciones impositivas (se refiere al dinero que finalmente Usted recibe en su cuenta sueldo).',
			),
			'acreencias' => array(
				'a' => 'Consignar montos prestados a otras personas.',
			),
			'tarjetas_credito' => array(
				'a' => 'Si tiene deuda de tarjetas de créditos debe consignar la deuda total incluyendo los adicionales/extensiones.',
			),
		),
        'declaracion_legal' => 'Afirmo y declaro que los datos consignados en este formulario son correctos y completos, sin omitir ni falsear dato alguno.  El presente documento tiene carácter de declaración jurada en los términos y alcances de la Resolución Ministerial 190/2016 y de la Disposición DI-2017-1-APN-DNCIFS#MSG del Ministerio de Seguridad de la Nación.',
        'guia_pdf' => 'PDF - Guía para elaborar y presentar la Declaración Jurada Patrimonial',
        'mapa_cantidad_ddjjff' => array(
            'fecha_dato' => '2017-11-10'
        ),
	),
	'usuarios' => array(
		'cache' => array(
			'listado' => 0,
		),
		'estados' => array(
			0=>'No Habilitado',
			1=>'Habilitado',
		),
		'perfiles' => array(
			0=>'No Habilitado',
			1=>'Habilitado',
		),
	),
	'declaracion_jurada' => array(
        'cache' => array(
			'combos' => 0,
			'listado' => 0,
            'graficos' => 10,
		),
		'estados' => array(
			'Cargado',
			'Modificado',
			'Eliminado',
			'Entregado en Dependencia',
			'Recibido',
			'Entregado a RRHH',			
		),
		'tipo_ingreso' => array(
			'Propio de la Fuerza',
			'Adicional',
			'Otros'
		),
		'order' => array(
			'reportes' => array(
				'excel' => array(
					0 => ['description'=>'Id','order'=>0],
					1 => ['description'=>'Version','order'=>11],
					2 => ['description'=>'Tipo Estado','order'=>5],
					3 => ['description'=>'Fecha','order'=>6],
					4 => ['description'=>'Fecha Aceptado','order'=>7],
					5 => ['description'=>'Fecha Recibido','order'=>8],
					6 => ['description'=>'Fecha Eliminado','order'=>9],
					7 => ['description'=>'Nombre','order'=>1],
					8 => ['description'=>'Apellido','order'=>2],
					9 => ['description'=>'DNI','order'=>3],
					10 => ['description'=>'Fuerza','order'=>4],
					11 => ['description'=>'Hash','order'=>10],
				)
			)
		)
	),

);
